using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class HelpChatLogRepository : RepositoryBase<tblHelpChatLog>, IHelpChatLogRepository
    {

        public HelpChatLogRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

    }
}