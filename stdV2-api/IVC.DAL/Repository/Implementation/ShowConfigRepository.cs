using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class ShowConfigRepository : RepositoryBase<tblShowConfig>, IShowConfigRepository
    {
        public ShowConfigRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}