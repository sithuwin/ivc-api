using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class AdminRepository : RepositoryBase<tbl_Administrator>, IAdminRepository
    {
        public AdminRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }


    }
}