using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation {
    public class RepositoryWrapper : IRepositoryWrapper {
        private AppDB _repoContext;
        private ISiteRunNumberRepository _siteRunNumber;
        private IAdminRepository _admin;

        private IAccountRepository _account;
        private IConfrenceRepository _confrence;
        private IConferenceUsersRepository _conferenceUser;
        private IZoomParticipantsRepository _zoomParticipant;
        private IZoomConfigurationRepository _zoomConfiguration;
        private IZoomPollResultRepository _zoomPollResult;

        private ISpeakerRepository _speaker;
        private ISpeakerCategoryRepository _speakerCategory;
        private IConferenceSpeakerRepository _conferenceSpeaker;
        private IConferenceCategoryRepository _conferenceCategory;
        private IDayRepository _day;
        private IMaterialRepository _material;
        private IUsersFavouriteRepository _usersFavourite;
        private IFriendShipRepository _friendShip;
        private IShowConfigRepository _showConfig;
        private IAccessRightRepository _accessRight;
        private INotificationTemplateRepository _notificationTemplate;
        private INotificationSendListRepository _notificationSendList;
        private IEPosterRepository _eposter;
        private ICommantRatingRepository _commantRating;
        private IAuditRepository _audit;
        private IHelpDeskRepository _helpDesk;
        private IHelpChatLogRepository _helpChatLog;
        private INetworkLoungeRepository _networkLounge;
        private IAccountLogicBlockerRepository _accountLogicBlocker;
        private INewsRepository _news;
        private IEmailConfigRepository _emailConfig;

        public RepositoryWrapper (AppDB repositoryContext) {
            _repoContext = repositoryContext;
        }

        public void Save () {
            _repoContext.SaveChanges ();
        }
        public ISiteRunNumberRepository SiteRunNumber {
            get {
                if (_siteRunNumber == null) {
                    _siteRunNumber = new SiteRunNumberRepository (_repoContext);
                }
                return _siteRunNumber;
            }
        }
        public IAdminRepository Admin {
            get {
                if (_admin == null) {
                    _admin = new AdminRepository (_repoContext);
                }
                return _admin;
            }
        }

        public IAccountRepository Account {
            get {
                if (_account == null) {
                    _account = new AccountRepository (_repoContext);
                }
                return _account;
            }
        }
        public IConfrenceRepository Confrence {
            get {
                if (_confrence == null) {
                    _confrence = new ConfrenceRepository (_repoContext);
                }
                return _confrence;
            }
        }
        public IConferenceUsersRepository ConferenceUser {
            get {
                if (_conferenceUser == null) {
                    _conferenceUser = new ConferenceUsersRepository (_repoContext);
                }
                return _conferenceUser;
            }
        }
        public IZoomParticipantsRepository ZoomParticipant {
            get {
                if (_zoomParticipant == null) {
                    _zoomParticipant = new ZoomParticipantsRepository (_repoContext);
                }
                return _zoomParticipant;
            }
        }
        public IZoomConfigurationRepository ZoomConfiguration {
            get {
                if (_zoomConfiguration == null) {
                    _zoomConfiguration = new ZoomConfigurationRepository (_repoContext);
                }
                return _zoomConfiguration;
            }
        }
        public IZoomPollResultRepository ZoomPollResult {
            get {
                if (_zoomPollResult == null) {
                    _zoomPollResult = new ZoomPollResultRepository (_repoContext);
                }
                return _zoomPollResult;
            }
        }
        public ISpeakerRepository Speaker {
            get {
                if (_speaker == null) {
                    _speaker = new SpeakerRepository (_repoContext);
                }
                return _speaker;
            }
        }
        public ISpeakerCategoryRepository SpeakerCategory {
            get {
                if (_speakerCategory == null) {
                    _speakerCategory = new SpeakerCategoryRepository (_repoContext);
                }
                return _speakerCategory;
            }
        }
        public IConferenceSpeakerRepository ConferenceSpeaker {
            get {
                if (_conferenceSpeaker == null) {
                    _conferenceSpeaker = new ConferenceSpeakerRepository (_repoContext);
                }
                return _conferenceSpeaker;
            }
        }
        public IConferenceCategoryRepository ConferenceCategory {
            get {
                if (_conferenceCategory == null) {
                    _conferenceCategory = new ConferenceCategoryRepository (_repoContext);
                }
                return _conferenceCategory;
            }
        }

        public IDayRepository Day {
            get {
                if (_day == null) {
                    _day = new DayRepository (_repoContext);
                }
                return _day;
            }
        }
        public IMaterialRepository Material {
            get {
                if (_material == null) {
                    _material = new MaterialRepository (_repoContext);
                }
                return _material;
            }
        }
        public IUsersFavouriteRepository UsersFavourite {
            get {
                if (_usersFavourite == null) {
                    _usersFavourite = new UsersFavouriteRepository (_repoContext);
                }
                return _usersFavourite;
            }
        }
        public IFriendShipRepository FriendShip {
            get {
                if (_friendShip == null) {
                    _friendShip = new FriendShipRepository (_repoContext);
                }
                return _friendShip;
            }
        }
        public IShowConfigRepository ShowConfig {
            get {
                if (_showConfig == null) {
                    _showConfig = new ShowConfigRepository (_repoContext);
                }
                return _showConfig;
            }
        }
        public IAccessRightRepository AccessRight {
            get {
                if (_accessRight == null) {
                    _accessRight = new AccessRightRepository (_repoContext);
                }
                return _accessRight;
            }
        }
        public INotificationTemplateRepository NotificationTemplate {
            get {
                if (_notificationTemplate == null) {
                    _notificationTemplate = new NotificationTemplateRepository (_repoContext);
                }
                return _notificationTemplate;
            }
        }
        public INotificationSendListRepository NotificationSendList {
            get {
                if (_notificationSendList == null) {
                    _notificationSendList = new NotificationSendListRepository (_repoContext);
                }
                return _notificationSendList;
            }
        }
        public IEPosterRepository EPoster {
            get {
                if (_eposter == null) {
                    _eposter = new EPosterRepository (_repoContext);
                }
                return _eposter;
            }
        }
        public ICommantRatingRepository CommantRating {
            get {
                if (_commantRating == null) {
                    _commantRating = new CommantRatingRepository (_repoContext);
                }
                return _commantRating;
            }
        }
        public IAuditRepository Audit {
            get {
                if (_audit == null) {
                    _audit = new AuditRepository (_repoContext);
                }
                return _audit;
            }
        }
        public IHelpDeskRepository HelpDesk {
            get {
                if (_helpDesk == null) {
                    _helpDesk = new HelpDeskRepository (_repoContext);
                }
                return _helpDesk;
            }
        }
        public IHelpChatLogRepository HelpChatLog {
            get {
                if (_helpChatLog == null) {
                    _helpChatLog = new HelpChatLogRepository (_repoContext);
                }
                return _helpChatLog;
            }
        }

        public INetworkLoungeRepository NetworkLounge {
            get {
                if (_networkLounge == null) {
                    _networkLounge = new NetworkLoungeRepository (_repoContext);
                }
                return _networkLounge;
            }
        }

        public IAccountLogicBlockerRepository AccountLogicBlocker {
            get {
                if (_accountLogicBlocker == null) {
                    _accountLogicBlocker = new AccountLoginBlockerRepository (_repoContext);
                }
                return _accountLogicBlocker;
            }
        }

        public INewsRepository News {
            get {
                if (_news == null) {
                    _news = new NewsRepository (_repoContext);
                }
                return _news;
            }
        }

        public IEmailConfigRepository EmailConfig {
            get {
                if (_emailConfig == null) {
                    _emailConfig = new EmailConfigRepository (_repoContext);
                }
                return _emailConfig;
            }
        }
    }
}