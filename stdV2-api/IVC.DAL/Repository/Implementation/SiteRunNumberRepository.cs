using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class SiteRunNumberRepository : RepositoryBase<SiteRunNumber>, ISiteRunNumberRepository
    {
        public SiteRunNumberRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public string GetNewId(string runKey)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();
            return siteRun.prefix + siteRun.runno_curr;
        }

        public void UpdateNumberByRunKey(string runKey)
        {
            SiteRunNumber siteRun = (from s in RepositoryContext.SiteRunNumbers where s.prefix == runKey select s).FirstOrDefault();

            siteRun.runno_curr += siteRun.runno_incr;
            Update(siteRun);
            RepositoryContext.SaveChanges();

        }
    }
}