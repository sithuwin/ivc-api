using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace  IVC.DAL.Repository.Implementation
{
    public class EmailConfigRepository:RepositoryBase<tblConfigEmail>, IEmailConfigRepository
    {
        public EmailConfigRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}