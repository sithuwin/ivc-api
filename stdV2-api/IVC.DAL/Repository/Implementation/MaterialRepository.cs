using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class MaterialRepository : RepositoryBase<tbl_Material>, IMaterialRepository
    {
        public MaterialRepository(AppDB repositoryContext) : base(repositoryContext) { }

        public dynamic GetAllMaterials(string userId, string confId)
        {
            var main = (from m in RepositoryContext.Materials
                        join mu in RepositoryContext.UsersFavourites.Where(x => x.UserId == userId)
                        on m.MatId equals mu.FavItemId into mtmp from mutmp in mtmp.DefaultIfEmpty()

                        join acc in (from a in RepositoryContext.Accounts
                                     where a.deleteFlag == false
                                     select new { a.a_ID,a.a_fullname }) on m.UploadUserId equals acc.a_ID into al
                        from am in al.DefaultIfEmpty()

                        join ad in (from s in RepositoryContext.Administrators
                                    where s.admin_isdeleted == false
                                    select new { adminId = s.admin_id.ToString(), s.admin_name }) on m.UploadUserId equals ad.adminId into adl
                        from adlist in adl.DefaultIfEmpty()

                        where m.Status == 1 && m.ConferenceId == confId
                        select new
                        {
                            m.MatId,
                            m.Title,
                            m.Description,
                            m.UploadUserId,
                            m.Url,
                            FavouriteStatus = (mutmp == null ? 0 : mutmp.Status),
                            UploadUser = am.a_ID != null ? am.a_fullname : adlist.adminId != null ? adlist.admin_name : ""
                        }).Distinct().ToList();
            return main;
        }

        public dynamic GetFavouriteMaterialListByUserId(string userId)
        {
            var main = (from m in RepositoryContext.Materials
                        join mu in RepositoryContext.UsersFavourites on m.MatId equals mu.FavItemId
                        join c in RepositoryContext.Conferences on m.ConferenceId equals c.c_ID
                        where m.Status == 1 && mu.Status == 1
                        && c.deleteFlag == false
                        && mu.UserId == userId
                        select new
                        {
                            m.MatId,
                            m.Title,
                            m.Description,
                            m.UploadUserId,
                            m.Url,
                            FavouriteStatus = (mu == null ? 0 : mu.Status),
                            UploadUser = (m.UploadUserType == "O" ? (from a in RepositoryContext.Administrators where a.admin_id.ToString() == m.UploadUserId && a.admin_isdeleted == false select a.admin_name).FirstOrDefault()
                            : (from a in RepositoryContext.Accounts where a.a_ID == m.UploadUserId && a.deleteFlag == false select a.a_fullname).FirstOrDefault())
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public dynamic GetFavouriteMaterialListByUserIdAndConderenceId(string userId, string confId)
        {
            var main = (from m in RepositoryContext.Materials
                        join mu in RepositoryContext.UsersFavourites on m.MatId equals mu.FavItemId
                        join a in RepositoryContext.Accounts on m.UploadUserId equals a.a_ID into atmp
                        from autmp in atmp.DefaultIfEmpty()
                        join ad in RepositoryContext.Administrators on m.UploadUserId equals ad.admin_id.ToString() into adtmp
                        from adutmp in adtmp.DefaultIfEmpty()
                        where m.Status == 1 && m.ConferenceId == confId && mu.Status == 1 && mu.UserId == userId
                        select new
                        {
                            m.MatId,
                            m.Title,
                            m.Description,
                            m.UploadUserId,
                            m.Url,
                            FavouriteStatus = (mu == null ? 0 : mu.Status),
                            UploadUser = (m.UploadUserType == "O" ? (adutmp == null ? "" : adutmp.admin_name) : (autmp == null ? "" : autmp.a_fullname))
                        }).ToList();
            return main;
        }

        public dynamic GetMaterialListBySpeakerId(string cId, string userId)
        {
            var main = (from m in RepositoryContext.Materials
                        join c in RepositoryContext.Conferences on m.ConferenceId equals c.c_ID
                        join acc in (from a in RepositoryContext.Accounts
                                     where a.deleteFlag == false
                                     select new { a.a_ID,a.a_fullname }) on m.UploadUserId equals acc.a_ID into al
                        from am in al.DefaultIfEmpty()
                        join ad in (from s in RepositoryContext.Administrators
                                    where s.admin_isdeleted == false
                                    select new { adminId = s.admin_id.ToString(), s.admin_name }) on m.UploadUserId equals ad.adminId into adl
                        from adlist in adl.DefaultIfEmpty()

                        where m.ConferenceId == cId && m.Status == 1
                        && (m.UploadUserId == userId || m.UploadUserId == adlist.adminId)
                        select new
                        {
                            m,
                            UploadedUser = am.a_ID != null ? am.a_fullname : adlist.adminId != null ? adlist.admin_name : "",
                            c.c_date,
                            c.c_title
                        }).ToList();
            return main;
        }
        public dynamic GetMaterialListForOrganization(string cId)
        {
            var main = (from m in RepositoryContext.Materials
                        join c in RepositoryContext.Conferences on m.ConferenceId equals c.c_ID
                        join acc in (from a in RepositoryContext.Accounts
                                     where a.deleteFlag == false
                                     select new { a.a_ID,a.a_fullname }) on m.UploadUserId equals acc.a_ID into al
                        from am in al.DefaultIfEmpty()
                        join ad in (from s in RepositoryContext.Administrators
                                    where s.admin_isdeleted == false
                                    select new { adminId = s.admin_id.ToString(), s.admin_name }) on m.UploadUserId equals ad.adminId into adl
                        from adlist in adl.DefaultIfEmpty()
                        
                        where m.ConferenceId == cId && m.Status == 1
                        select new
                        {
                            m,
                            UploadedUser = am.a_ID != null ? am.a_fullname : adlist.adminId != null ? adlist.admin_name : "",
                            c.c_date,
                            c.c_title
                        }).ToList();
            return main;
        }

        public dynamic MaterialViewReport(string mId, dynamic obj, string sortField, string sortBy, bool isExport)
        {
            dynamic result = null;
            var mainQuery = (from m in RepositoryContext.Materials
                             join au in RepositoryContext.Audits on m.MatId equals au.AlternativeId
                             join a in RepositoryContext.Accounts on au.UserId equals a.a_ID
                             join ct in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.CountryName, c.ISO3Digit }) on a.a_country equals ct.ISO3Digit into cl
                             from ctl in cl.DefaultIfEmpty()

                             where a.deleteFlag == false && au.LogType == AuditLogType.ViewMaterial
                             && m.Status == 1 && m.MatId == mId
                             select new
                             {
                                 UserId = a.a_ID,
                                 FirstName = a.a_fname,
                                 LastName = a.a_lname,
                                 Company = a.a_company,
                                 JobTitle = a.a_designation,
                                 Country = ctl.CountryName,
                                 Email = a.a_email,
                                 MobileNo = a.a_Mobile,
                                 RegistrationId = a.RegPortalID
                             });

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForMaterialViewReport(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }

            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForMaterialViewReport(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic MaterialViewReportByCounty(string mId)
        {
            var main = (from m in RepositoryContext.Materials
                        join au in RepositoryContext.Audits on m.MatId equals au.AlternativeId
                        join a in RepositoryContext.Accounts on au.UserId equals a.a_ID
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.CountryName, c.ISO3Digit }) on a.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where a.deleteFlag == false && m.Status == 1 && m.MatId == mId
                        && au.LogType == AuditLogType.ViewMaterial
                        select new
                        {
                            Country = ctl.CountryName,
                            ViewCount = (from aud in RepositoryContext.Audits
                                         join acc in RepositoryContext.Accounts on aud.UserId equals acc.a_ID
                                         where acc.a_country == a.a_country && aud.LogType == AuditLogType.ViewMaterial
                                         && aud.AlternativeId == mId
                                         select aud.UserId).Distinct().Count()
                        }).AsNoTracking().Distinct().ToList().OrderBy(x => x.Country);
            return main;
        }

        public dynamic GetMaterialListbyConferenceId(string cId)
        {
            var main = (from m in RepositoryContext.Materials
                        where m.ConferenceId == cId && m.Status == 1
                        select new
                        {
                            m.MatId,
                            m.Title
                        }).AsNoTracking().Distinct().ToList();
            return main;
        }
        private dynamic FilterSessionForMaterialViewReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                FirstName = default(string),
                LastName = default(string),
                Company = default(string),
                JobTitle = default(string),
                Country = default(string),
                Email = default(string),
                MobileNo = default(string),
                RegistrationId = default(string),

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "UserName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.FirstName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForMaterialViewReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                FirstName = default(string),
                LastName = default(string),
                Company = default(string),
                JobTitle = default(string),
                Country = default(string),
                Email = default(string),
                MobileNo = default(string),
                RegistrationId = default(string),
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

    }
}