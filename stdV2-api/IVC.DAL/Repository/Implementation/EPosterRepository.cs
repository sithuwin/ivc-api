using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
namespace IVC.DAL.Repository.Implementation
{
    public class EPosterRepository : RepositoryBase<tblEPoster>, IEPosterRepository
    {
        public EPosterRepository(AppDB repositoryContent) : base(repositoryContent)
        {

        }

        public dynamic GetEPosterList(string userId, string sortField, string sortBy, dynamic obj)
        {

            var main = (from e in RepositoryContext.EPosters
                        join a in RepositoryContext.Accounts on e.ep_author equals a.a_ID
                        where e.deleteFlag == false && a.deleteFlag == false && a.a_type == UserType.Speaker
                        select new
                        {
                            e.ep_ID,
                            topic = e.ep_title,
                            e.ep_category,
                            ep_author = a.a_fullname,
                            e.ep_coauthor,
                            e.ep_abstract,
                            e.ep_venue,
                            e.ep_duration,
                            e.video
                        });
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSession(main, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            main = PaginationSession(main, obj);
            var objResult = main.ToList(); //.OrderBy (x => x.a_fullname);
            var objTotal = main.ToList().Count();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetEPostById(string id)
        {
            dynamic main = (from e in RepositoryContext.EPosters
                            where e.ep_ID == id && e.deleteFlag == false
                            select new
                            {
                                e.ep_ID,
                                e.ep_title,
                                e.ep_category,
                                e.ep_author,
                                e.ep_coauthor,
                                e.ep_venue,
                                e.ep_duration,
                                e.ep_abstract,

                            }).FirstOrDefault();
            return main;
        }

        public dynamic GetCategoryList()
        {
            var main = (from ep in RepositoryContext.EPosters where ep.deleteFlag == false select ep.ep_category).Distinct().ToList();
            return main;
        }

        public string GetEPosterFile(string id, string returnUrl)
        {
            dynamic main = (from e in RepositoryContext.EPosters
                            where e.ep_ID == id && e.deleteFlag == false
                            select new
                            {
                                ep_file = ((e.ep_file == "" || e.ep_file == null) ? "" : returnUrl + e.ep_file),

                            }).FirstOrDefault();
            string file = main.ep_file;
            return file;
        }
        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ep_ID = default(string),
                topic = default(string),
                ep_category = default(string),
                ep_author = default(string),
                ep_coauthor = default(string),
                ep_abstract = default(string),
                ep_venue = default(string),
                ep_duration = default(string),
                video = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "author")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.ep_author.ToLower().Contains(Name) || x.ep_coauthor.ToLower().Contains(Name));

                        }
                    }
                    if (filterName == "category")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.ep_category.ToLower().Contains(Name));

                        }
                    }
                    if (filterName == "title")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.topic.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ep_ID = default(string),
                topic = default(string),
                ep_category = default(string),
                ep_author = default(string),
                ep_coauthor = default(string),
                ep_abstract = default(string),
                ep_venue = default(string),
                ep_duration = default(string),
                video = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ep_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ep_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

    }
}