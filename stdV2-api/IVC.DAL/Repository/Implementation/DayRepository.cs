using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class DayRepository : RepositoryBase<tbl_Day>, IDayRepository
    {
        public DayRepository(AppDB repositoryContext) : base(repositoryContext) { }

        public dynamic GetConferenceDatelist()
        {
            dynamic main = (from c in RepositoryContext.Days
                            where c.deleteFlag == false
                            select new
                            {
                                c.d_date
                            }).Distinct().ToList();
            return main;
        }

        public dynamic GetMinimunDate()
        {

            string minDateString = (from d in RepositoryContext.Days where d.deleteFlag == false select d.d_date).Min();
            DateTime minDate = Convert.ToDateTime(minDateString);
            return minDate.ToString("dd MMMM yyyy");
        }

        public dynamic GetMaximumDate()
        {

            dynamic maxDateString = (from d in RepositoryContext.Days where d.deleteFlag == false select d.d_date).Max();
            DateTime maxDate = Convert.ToDateTime(maxDateString);
            return maxDate.ToString("dd MMMM yyyy");
        }

        public dynamic GetCategories()
        {
            dynamic main = (from c in RepositoryContext.Days
                            where c.deleteFlag == false && c.d_desc != null
                            orderby c.d_desc
                            select new
                            {
                                ID = c.d_ID,
                                Category = c.d_desc
                            }).Distinct().ToList();
            return main;
        }
    }
}