using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class NetworkLoungeRepository : RepositoryBase<tbl_NetworkLounge>, INetworkLoungeRepository
    {
        public NetworkLoungeRepository(AppDB repositoryContext): base(repositoryContext){
            
        }

         public void CreateNetworkLounge(dynamic obj,string ID)
        {
            tbl_NetworkLounge  nlObj = new tbl_NetworkLounge();
            nlObj.nl_ID = ID;
            nlObj.nl_Name = obj.name;
            nlObj.nl_Desc = obj.desc;
            nlObj.nl_seq = obj.seqNo;
            nlObj.nl_Status = NetworkLoungeStatus.InitialState;

            nlObj.deleteFlag = false;
            nlObj.CreatedDate = DateTime.Now;
            RepositoryContext.NetworkLounges.Add(nlObj);
            RepositoryContext.SaveChanges();
        }

        public void UpdateNetworkLounge(dynamic obj, tbl_NetworkLounge  nlObj){
            nlObj.nl_Name = obj.name;
            nlObj.nl_Desc = obj.desc;
            nlObj.nl_seq = obj.seqNo;
            nlObj.deleteFlag = false;
            nlObj.UpdatedDate = DateTime.Now;
            RepositoryContext.NetworkLounges.Update(nlObj);
            RepositoryContext.SaveChanges();
        }

        public dynamic SelectAllNetworkLounge(dynamic obj)
        {
            var main = (from nl in RepositoryContext.NetworkLounges
            orderby nl.nl_Name
            where nl.deleteFlag == false
            select new 
            {
                ID = nl.nl_ID,
                Name = nl.nl_Name,
                Description = nl.nl_Desc,
                Status = nl.nl_Status,
                seqNo = nl.nl_seq,
                CreatedDate = nl.CreatedDate
            });
            var objTotal = main.ToList().Count();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForNetworkLounge(main, obj);
                }
            }
            string sortField = null;
            string sortBy = "";

            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "seqNo";
            if (sortBy == null || sortBy == "")
                sortBy = "asc";

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            main = PaginationSessionForNetworkLounge(main, obj);
            var objResult = main.AsNoTracking().ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

         public dynamic SelectAllPublishedNetworkLounge()
        {
            var main = (from nl in RepositoryContext.NetworkLounges
            orderby nl.nl_Name
            where nl.deleteFlag == false && nl.nl_Status == NetworkLoungeStatus.Published
            select new 
            {
                ID = nl.nl_ID,
                Name = nl.nl_Name,
                Description = nl.nl_Desc,
                Status = nl.nl_Status,
                seqNo = nl.nl_seq,
                CreatedDate = nl.CreatedDate.Date
            }).AsNoTracking().Distinct().ToList().OrderBy(x => x.seqNo);
            return main;
        }

        public dynamic SelectNetworkLoungeById(string Id)
        {
            var main = (from nl in RepositoryContext.NetworkLounges
            orderby nl.nl_Name
            where nl.deleteFlag == false && nl.nl_ID == Id 
            select new 
            {
                ID = nl.nl_ID,
                Name = nl.nl_Name,
                Description = nl.nl_Desc,
                seqNo = nl.nl_seq,
                Status = nl.nl_Status
            }).Distinct().ToList();
            return main;
        }

        public void UpdateNetworkLoungeStatuByID(string ID,string status,tbl_NetworkLounge networkLounge){

            networkLounge.nl_Status = status;
            networkLounge.UpdatedDate = DateTime.Now;
            RepositoryContext.NetworkLounges.Update(networkLounge);
            RepositoryContext.SaveChanges();
        }

        public void DeleteNetworkLoungeByID(string ID,tbl_NetworkLounge networkLounge)
        {
            networkLounge.deleteFlag = true;
            networkLounge.UpdatedDate = DateTime.Now;
            RepositoryContext.NetworkLounges.Update(networkLounge);
            RepositoryContext.SaveChanges();
        }

        #region  Filter Session
        private dynamic FilterSessionForNetworkLounge(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ID = default(string),
                Name = default(string),
                Description  = default(string),
                Status  = default(string),
                seqNo = default(int?),
                CreatedDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Name.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "Description")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Description.ToLower().Contains(Name));

                            }
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion

        #region  Pagination Session
        private dynamic PaginationSessionForNetworkLounge(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                ID = default(string),
                Name = default(string),
                Description  = default(string),
                Status  = default(string),
                seqNo = default(int?),
                CreatedDate = default(DateTime)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        #endregion

    }
}