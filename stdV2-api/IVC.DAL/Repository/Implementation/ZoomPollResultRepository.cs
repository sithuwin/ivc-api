using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class ZoomPollResultRepository : RepositoryBase<tbl_ZoomPollResult>, IZoomPollResultRepository
    {
        public ZoomPollResultRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetPollColumns(string webinarId)
        {
            var questionQuery = (from p in RepositoryContext.ZoomPollResults where p.WebinarId == webinarId && p.Status == 1 select p.Question).Distinct();

            var questoinList = questionQuery.ToList();

            //Columns List
            List<dynamic> columns = new List<dynamic>();
            Dictionary<string, string> columnMap = new Dictionary<string, string>();
            columnMap.Add("columnDef", "userName");
            columnMap.Add("header", "User Name");
            columns.Add(columnMap);

            foreach (var colItem in questoinList)
            {
                Dictionary<string, string> quesColumnMap = new Dictionary<string, string>();
                quesColumnMap.Add("columnDef", colItem);
                quesColumnMap.Add("header", colItem);
                columns.Add(quesColumnMap);
            }

            dynamic result = new { data = columns, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetPollListByWebinarId(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport)
        {
            dynamic result = null;
            var mainQuery = (from p in RepositoryContext.ZoomPollResults where p.WebinarId == webinarId && p.Status == 1 select new { p.UserName }).Distinct();

            var questionQuery = (from p in RepositoryContext.ZoomPollResults where p.WebinarId == webinarId && p.Status == 1 select p.Question).Distinct();

            var questoinList = questionQuery.ToList();
            int objTotal = 0;

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSession(mainQuery, obj);
                }
            }

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);

            if (!isExport)
            {
                objTotal = mainQuery.Count();
                mainQuery = PaginationSession(mainQuery, obj);
            }

            var mainResult = new List<dynamic>();
            foreach (var item in mainQuery)
            {
                Dictionary<string, string> QAMap = new Dictionary<string, string>();
                var answerQuery = (from p in RepositoryContext.ZoomPollResults where p.WebinarId == webinarId && p.Status == 1 && p.UserName == item.UserName select p);

                QAMap.Add("userName", item.UserName);
                foreach (var queItem in questoinList)
                {
                    var qusItemQuery = (from q in answerQuery where q.Question == queItem select q).FirstOrDefault();

                    if (qusItemQuery != null)
                    {
                        QAMap.Add(queItem, qusItemQuery.Answer);
                    }

                }
                mainResult.Add(QAMap);
            }
            if (isExport)
            {
                result = mainResult;
            }
            else
            {
                result = new { data = mainResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            }

            return result;
        }

        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserName = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "UserName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.UserName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {

            var tmpQuery = Enumerable.Repeat(new
            {
                UserName = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserName);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserName) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public void UpdatePollData(dynamic response, string webinarId,string userId)
        {
            for (int i = 0; i < response.Count; i++)
            {
                if (response[i].name != "Anonymous Attendee")
                {
                    for (int j = 0; j < response[i].question_details.Count; j++)
                    {
                        string userName = response[i].name;
                        string userEmail = response[i].email;
                        string ques = response[i].question_details[j].question;
                        string ans = response[i].question_details[j].answer;

                        tbl_ZoomPollResult  pollObj = RepositoryContext.ZoomPollResults.Where(x => x.Status == 1 && x.WebinarId == webinarId 
                        && x.Email == userEmail  && x.Question == ques).FirstOrDefault();
                        if (pollObj == null)
                        {
                            tbl_ZoomPollResult poll = new tbl_ZoomPollResult();
                            poll.UserName = userName;
                            poll.Email = userEmail;
                            poll.Question = ques;
                            poll.Answer = ans;
                            poll.WebinarId = webinarId;
                            poll.Status = 1;
                            poll.CreatedDate = DateTime.Now;
                            poll.ModifiedDate = DateTime.Now;
                            poll.CreatedBy = userId;
                            RepositoryContext.ZoomPollResults.Add(poll);
                            RepositoryContext.SaveChanges();
                        }else{
                            pollObj.Question = ques;
                            pollObj.Answer = ans;
                            pollObj.Status = 1;
                            pollObj.ModifiedDate = DateTime.Now;
                            RepositoryContext.ZoomPollResults.Update(pollObj);
                            RepositoryContext.SaveChanges();
                        }
                    }
                }
            }
        }
    }

}