using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class AccessRightRepository : RepositoryBase<tblAccessRight>, IAccessRightRepository
    {
        public AccessRightRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}