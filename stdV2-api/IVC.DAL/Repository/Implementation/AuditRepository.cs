using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.AccessControl;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
namespace IVC.DAL.Repository.Implementation {
    public class AuditRepository : RepositoryBase<tblAudit>, IAuditRepository {
        public AuditRepository (AppDB repositoryContext) : base (repositoryContext) {

        }
        public dynamic GetCountryBreakDown () {

            var registerUserArray = (from a in RepositoryContext.Accounts where a.deleteFlag == false select a.a_ID).Distinct ().ToArray ();

            var atandentUserArray = (from c in RepositoryContext.Audits where c.LogType == AuditLogType.login select c.UserId).Distinct ().ToArray ();

            // var countryList = (from a in RepositoryContext.Accounts where a.deleteFlag == false orderby a.a_country.Count() select a.a_country).Take (10);

            var countryArray = (from c in RepositoryContext.Countries join a in RepositoryContext.Accounts on c.ISO3Digit equals a.a_country where a.deleteFlag == false && c.deleteFlag == false orderby a.a_country select c.CountryName).Distinct ().ToList ();

            List<dynamic> data = new List<dynamic> ();

            for (int i = 0; i < countryArray.Count; i++) {

                dynamic obj = new ExpandoObject ();

                List<dynamic> seriesList = new List<dynamic> ();
                dynamic reginsterCount = (from acc in RepositoryContext.Accounts join coun in RepositoryContext.Countries on acc.a_country equals coun.ISO3Digit where coun.CountryName == countryArray[i] select acc).Count ();
                dynamic attendancedCount = (from acc in RepositoryContext.Accounts join coun in RepositoryContext.Countries on acc.a_country equals coun.ISO3Digit where atandentUserArray.Contains (acc.a_ID) && coun.CountryName == countryArray[i] select acc).Count ();
                dynamic registerObj = new ExpandoObject ();
                registerObj.name = "Registered";
                registerObj.value = reginsterCount;

                dynamic attendObj = new ExpandoObject ();
                attendObj.name = "Attended";
                attendObj.value = attendancedCount;

                seriesList.Add (registerObj);
                seriesList.Add (attendObj);

                obj.name = countryArray[i];
                obj.series = seriesList;
                obj.total = attendancedCount; //+ attendancedCount;

                data.Add (obj);
            }

            var main = data.OrderByDescending (x => x.total).Take (10);

            return main;
        }

        public dynamic GetActiveUserDetail (string sortField, string sortBy, dynamic obj, bool isExport) {

            var main = (from audit in RepositoryContext.Audits join u in RepositoryContext.Accounts on audit.UserId equals u.a_ID join country in RepositoryContext.Countries on u.a_country equals country.ISO3Digit where audit.LogType == AuditLogType.login select new {
                u.a_ID,
                    u.a_fullname,
                    u.a_type,
                    u.a_email,
                    country.CountryName
            }).Distinct ();

            if (obj.filter != null) {
                if (obj.filter.filters != null) {
                    main = FilterSession (main, obj);
                }
            }
            var objSort = new SortModel ();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel> ();
            sortList.Add (objSort);
            main = main.OrderBy (sortList);
            main = PaginationSession (main, obj);
            if (isExport) {
                return main.ToList ();
            }
            var objResult = main.ToList (); //.OrderBy (x => x.a_fullname);
            var objTotal = main.ToList ().Count ();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSession (dynamic mainQuery, dynamic obj) {
            var tmpQuery = Enumerable.Repeat (new {
                a_ID = default (string),
                    a_fullname = default (string),
                    a_type = default (string),
                    a_email = default (string),
                    CountryName = default (string),
            }, 0).AsQueryable ();

            try {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++) {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "email") {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty (Name)) {
                            Name = Name.ToLower ();
                            tmpQuery = tmpQuery.Where (x => x.a_email.ToLower ().Contains (Name) || x.a_email.ToLower ().Contains (Name));

                        }
                    }
                    if (filterName == "name") {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty (Name)) {
                            Name = Name.ToLower ();
                            tmpQuery = tmpQuery.Where (x => x.a_fullname.ToLower ().Contains (Name) || x.a_fullname.ToLower ().Contains (Name));

                        }
                    }
                    if (filterName == "country") {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty (Name)) {
                            Name = Name.ToLower ();
                            tmpQuery = tmpQuery.Where (x => x.CountryName.ToLower ().Contains (Name) || x.CountryName.ToLower ().Contains (Name));

                        }
                    }
                }
                return tmpQuery;

            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession (dynamic mainQuery, dynamic obj) {
            var tmpQuery = Enumerable.Repeat (new {
                a_ID = default (string),
                    a_fullname = default (string),
                    a_type = default (string),
                    a_email = default (string),
                    CountryName = default (string),
            }, 0).AsQueryable ();

            try {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string> ();
                foreach (var item in tmpQuery) {
                    pageDictionary.Add (pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0) {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0) {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++) {
                    rowList.Add (countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList) {
                    if (pageDictionary.ContainsKey (item))
                        PaginationFilterArray.Add (pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains (main.a_ID) select main);
                return tmpQuery;
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
                return tmpQuery;
            }

        }

    }
}