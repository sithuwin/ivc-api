using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class SpeakerRepository : RepositoryBase<tbl_Speaker>, ISpeakerRepository
    {
        public SpeakerRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}