using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using IVC.DAL;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class ConferenceUsersRepository : RepositoryBase<tbl_ConferenceUser>, IConferenceUsersRepository
    {
        public ConferenceUsersRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public dynamic HasAuthorizedUser(string webinarId)
        {
            var mainQuery = (from s in RepositoryContext.ConferenceUsers where s.Status == 1 && s.WebinarId == webinarId select s).Count();
            return mainQuery;
        }

        public void UpdateCurrentJoinStatus(string userId, string webinarId)
        {
            //All Change Status to False
            ((from t in RepositoryContext.ConferenceUsers where t.UserId == userId && t.WebinarId == webinarId && t.Status == 1 && t.CurrentJoinStatus == true select t).ToList()).ForEach(x => x.CurrentJoinStatus = false);
            RepositoryContext.SaveChanges();

            //Change Current Status to True
            tbl_ConferenceUser userWebinarQuery = (from s in RepositoryContext.ConferenceUsers where s.UserId == userId && s.WebinarId == webinarId && s.Status == 1 select s).FirstOrDefault();

            if (userWebinarQuery != null)
            {
                userWebinarQuery.CurrentJoinStatus = true;
                userWebinarQuery.JoinStatus = 1;
                Update(userWebinarQuery);
            }
        }

        public dynamic GetConferenceListbySpeakerId(string userId, string returnUrl)
        {
            var main = (from a in RepositoryContext.Accounts
                        join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                        join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                        where c.deleteFlag == false && cu.Status == 1 &&
                        a.deleteFlag == false && a.a_type == UserType.Speaker && a.a_ID == userId
                        && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || 
                        cu.AccountType == ConferenceSpeakerType.SessionModerator || cu.AccountType == ConferenceSpeakerType.GuestOfHonor)
                        select new
                        {
                            Id = c.c_ID,
                            Topic = c.c_title,
                            Description = c.c_brief,
                            ModifiedDate = c.UpdatedDate,
                            StartDate = c.c_date,
                            StartTime = c.c_stime,
                            EndTime = DateTime.ParseExact(c.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(c.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                            SpeakerDetails = (
                                        from account in RepositoryContext.Accounts
                                        join cs in (from c in RepositoryContext.Countries
                                                    where c.deleteFlag == false
                                                    select new { c.ISO3Digit, c.CountryName }) on account.a_country equals cs.ISO3Digit into cts
                                        from ctl in cts.DefaultIfEmpty()
                                        join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                        join con in RepositoryContext.Conferences on conuser.WebinarId equals con.c_ID
                                        orderby conuser.SpeakerSeqNo
                                        where account.deleteFlag == false && conuser.Status == 1 && account.a_type == UserType.Speaker
                                        && con.deleteFlag == false && conuser.WebinarId == c.c_ID
                                        && (conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                        || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                        || conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                        || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                        select new
                                        {
                                            speakerId = account.a_ID,
                                            Speaker = account.a_fullname,
                                            desigination = account.a_designation,
                                            Salutation = (account.a_sal == "" || account.a_sal == null || account.a_sal == "." || account.a_sal.ToString() == "0") ? "" : account.a_sal,
                                            company = account.a_company,
                                            country = ctl.CountryName,
                                            SpeakerType = conuser.AccountType,
                                            isMainSpeaker = con.MainSpeakerId == account.a_ID ? true : false,
                                            SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                        }).AsNoTracking().Select(x => x).ToList()
                        });
            main = main.AsNoTracking().OrderBy(x => x.StartDate).ThenBy(x => x.StartTime);
            return main;
        }

        public dynamic GetAttandanceConferencByUserId(string userId, string returnUrl, string confDate)
        {
            var confeObj = (from cu in RepositoryContext.ConferenceUsers
                            join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                            join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                            //join cat in RepositoryContext.ConferenceCategories on c.c_cc equals cat.cc_ID
                            where a.a_ID == userId && cu.JoinStatus == 1 && cu.Status == 1 && a.deleteFlag == false
                            && c.deleteFlag == false && c.c_Type != ConferenceStructureType.Room
                            && (confDate == null || confDate == "" || c.c_date == confDate)
                            select new { c.c_date, c.c_ID }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           join c in confeObj on d.d_date equals c.c_date
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select d.d_date).Distinct().ToArray();

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(confDate) && dayCount == 1)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy == confDate)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }
                if (doQuery)
                {
                    var mainQuery = from c in RepositoryContext.Conferences
                                    join cobj in confeObj on c.c_ID equals cobj.c_ID
                                    join cat in (from cc in RepositoryContext.ConferenceCategories
                                                 where cc.deleteFlag == false
                                                 select new { cc.cc_ID, cc.cc_categoryname })
                                    on c.c_cc equals cat.cc_ID into ccs
                                    from clst in ccs.DefaultIfEmpty()

                                    where c.c_Type != ConferenceStructureType.Room && c.deleteFlag == false
                                    select new
                                    {
                                        Id = c.c_ID,
                                        Topic = c.c_title,
                                        SessionTitle = c.c_sessionTitle,
                                        Description = c.c_brief,
                                        Category = clst.cc_categoryname,
                                        ModifiedDate = c.UpdatedDate,
                                        StartDate = c.c_date,
                                        StartTime = c.c_stime,
                                        EndTime = DateTime.ParseExact(c.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(c.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers
                                                      where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (from account in RepositoryContext.Accounts
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == c.c_ID && account.a_type == UserType.Speaker
                                                          && (conuser.AccountType == ConferenceSpeakerType.GuestOfHonor || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                          || conuser.AccountType == ConferenceSpeakerType.Speaker || conuser.AccountType == ConferenceSpeakerType.SessionModerator)
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              SpeakerPhoto = (account.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                          }).Select(x => x).ToList(),

                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = c.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers
                                                                 where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == ChatJoinStatus.chat
                                                                 select cu).Count() > 0 ? true : false,
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers
                                                                        where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat
                                                                        select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = c.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers
                                                                        where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator)
                                                                        select cu).Count() > 0 ? true : false,
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1
                                                          && (cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                          || cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.SessionModerator)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where c.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        c.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == c.c_ID && cu.JoinStatus == 1 && cu.Status == 1 && cu.UserId == userId select c.Video).FirstOrDefault(),
                                        Type = c.Type,
                                        favouriteStatus = (from f in RepositoryContext.UsersFavourites
                                                           where f.Status == 1 && f.ItemType == FavouriteItem.Conference && f.UserId == userId
                                                           && f.FavItemId == c.c_ID
                                                           select f.FavItemId).Count() > 0 ? true : false,
                                        SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                          join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                          where cs.deleteFlag == false && sp.deleteFlag == false && cs.csponsor_conferenceID == c.c_ID
                                                          select new
                                                          {
                                                              csponsor_ID = cs.csponsor_sponsorID,
                                                              cs.csponsor_title,
                                                              sp.s_logo,
                                                              sp.templateId,
                                                              sp.s_url
                                                          }).Select(x => x).ToList()
                                    };
                    objResponse.conferenceList = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle);
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }

        public List<string> GetUserListByType(string cId, string type)
        {

            List<string> main = (from cu in RepositoryContext.ConferenceUsers join acc in RepositoryContext.AccessRights on cu.AccessRightId equals acc.AccessId where cu.WebinarId == cId && cu.Status == 1 && (acc.AccessRightActions.Contains(type)) select cu.UserId).Distinct().ToList();
            return main;
        }

        public bool CheckUserInSoftOpening(string userId, tbl_Conference conference)
        {
            bool isOk = false;
            int main = (from cu in RepositoryContext.ConferenceUsers
                        join acc in RepositoryContext.AccessRights on cu.AccessRightId equals acc.AccessId
                        where cu.UserId == userId &&
cu.WebinarId == conference.c_ID && cu.Status == 1 &&
acc.AccessRightActions.Contains(CurrentConferenceStatus.SoftOpening)
                        select cu.UserId).Count();
            if (main > 0)
            {
                isOk = true;
            }
            return isOk;
        }

        public dynamic GetRegistrantList(string webinarId)
        {
            var main = (from cu in RepositoryContext.ConferenceUsers
                        join u in RepositoryContext.Accounts on cu.UserId equals u.a_ID
                        where cu.WebinarId == webinarId && cu.Status == 1
                        select new
                        {
                            registrantId = cu.RegistrantId,
                            email = u.a_email
                        }).ToList();
            return main;
        }

        public dynamic GetConferenceDelegateList(string cId, string sortField, string sortBy, dynamic obj)
        {
            var main = (from cu in RepositoryContext.ConferenceUsers
                        join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                        join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.CountryName, c.ISO3Digit }) on a.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where cu.WebinarId == cId && a.deleteFlag == false && cu.Status == 1 
                        && (cu.AccountType == UserType.Delegate || cu.AccountType == UserType.FreeDelegate) &&
                        c.deleteFlag == false
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            //a.a_email,
                            a.a_company,
                            a_country = ctl.CountryName,
                            c.c_date,
                            c.c_title
                        });
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForDelegate(main, obj);
                }
            }
            var objTotal = main.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);
            main = PaginationSessionForDelegate(main, obj);
            var objResult = main.AsNoTracking().ToList(); 

            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetGroupChatAnalysisReport(string cId, string sortField, string sortBy, dynamic obj, bool isExport)
        {
            dynamic result = null;
            var main = (from cu in RepositoryContext.ConferenceUsers
                        join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.CountryName, c.ISO3Digit }) on a.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where cu.Status == 1 && a.deleteFlag == false && cu.WebinarId == cId
                        && cu.ChatChannelJoinStatus == ChatJoinStatus.chat
                        select new
                        {
                            UserId = a.a_ID,
                            FirstName = a.a_fname,
                            LastName = a.a_lname,
                            Company = a.a_company,
                            JobTitle = a.a_designation,
                            Country = ctl.CountryName,
                            Email = a.a_email,
                            Mobile = a.a_Mobile,
                            RegistationId = a.RegPortalID
                        });

            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForGroupChatAnalysisReport(main, obj);
                }
            }
            var objTotal = main.ToList().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);
            if (isExport)
            {
                return main.AsNoTracking().ToList();
            }
            main = PaginationSessionForGroupChatAnalysisReport(main, obj);
            var objResult = main.AsNoTracking().ToList(); 

            result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                //a_email = default(string),
                a_company = default(string),
                a_country = default(string),
                c_date = default(string),
                c_title = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "Name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name));
                            }
                        }
                        if (filterName == "Company")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_company.ToLower().Contains(Name));
                            }
                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                //a_email = default(string),
                a_company = default(string),
                a_country = default(string),
                c_date = default(string),
                c_title = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.a_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        private dynamic FilterSessionForGroupChatAnalysisReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                FirstName = default(string),
                LastName = default(string),
                Company = default(string),
                JobTitle = default(string),
                Country = default(string),
                Email = default(string),
                Mobile = default(string),
                RegistationId = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "Name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.FirstName.ToLower().Contains(Name));
                            }
                        }
                        if (filterName == "Email")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Email.ToLower().Contains(Name));
                            }
                        }
                        if (filterName == "Company")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Company.ToLower().Contains(Name));
                            }
                        }
                    }

                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        private dynamic PaginationSessionForGroupChatAnalysisReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                FirstName = default(string),
                LastName = default(string),
                Company = default(string),
                JobTitle = default(string),
                Country = default(string),
                Email = default(string),
                Mobile = default(string),
                RegistationId = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public void UpdateSpeakerSequenceNo(dynamic obj)
        {
            dynamic objConSpeakers = obj.data;
            int count = 1;
            foreach (var val in objConSpeakers)
            {
                int conId = Convert.ToInt16(val);
                tbl_ConferenceUser objConUser = new tbl_ConferenceUser();
                objConUser = (from c in RepositoryContext.ConferenceUsers
                              where c.Id == conId
                              select c).FirstOrDefault();
                if (objConUser != null)
                {
                    objConUser.SpeakerSeqNo = count;
                    objConUser.ModifiedDate = DateTime.Now;
                    RepositoryContext.ConferenceUsers.Update(objConUser);
                    RepositoryContext.SaveChanges();
                    count++;
                }

            }
        }

        public dynamic GetAttandanceConferencGroupTemplate(string userId, string returnUrl, string confDate)
        {
            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).Distinct().AsNoTracking().ToList();

            if (string.IsNullOrEmpty(confDate))
            {
                confDate = dayList[0].day;
            }

            var mainQuery = from c in RepositoryContext.Conferences
                            join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId
                            join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                            where c.c_Type != ConferenceStructureType.Room && c.deleteFlag == false
                            && cu.Status == 1 && a.deleteFlag == false && a.a_ID == userId && cu.JoinStatus == 1
                            && c.c_date == confDate
                            select new
                            {
                                Id = c.c_ID,
                                Topic = c.c_title,
                                SessionTitle = c.c_sessionTitle,
                                Description = c.c_brief,
                                ModifiedDate = c.UpdatedDate,
                                StartDate = c.c_date,
                                StartTime = c.c_stime,
                                EndTime = DateTime.ParseExact(c.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(c.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                Type = c.Type,
                                c.Video,
                                SpeakerDetails = (from account in RepositoryContext.Accounts
                                                  join ct in (from c in RepositoryContext.Countries
                                                              where c.deleteFlag == false
                                                              select new { c.ISO3Digit, c.CountryName }) on account.a_country equals ct.ISO3Digit
                                                  into cl from clist in cl.DefaultIfEmpty()
                                                  join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                  orderby conuser.SpeakerSeqNo
                                                  where account.deleteFlag == false && conuser.Status == 1
                                                  && conuser.WebinarId == c.c_ID
                                                  && (conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                  || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                  || conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                  || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                                  select new
                                                  {
                                                      speakerId = account.a_ID,
                                                      Speaker = account.a_fullname,
                                                      desigination = account.a_designation,
                                                      Salutation = (account.a_sal == "" || account.a_sal == null || account.a_sal == "." || account.a_sal.ToString() == "0") ? "" : account.a_sal,
                                                      company = account.a_company,
                                                      country = clist.CountryName,
                                                      SpeakerType = conuser.AccountType,
                                                      SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                  }).AsNoTracking().Select(x => x).ToList(),
                                
                               
                            };
            var SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                  join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                  join c in RepositoryContext.Conferences on cs.csponsor_conferenceID equals c.c_ID
                                  join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId
                                  join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID

                                  where cs.deleteFlag == false && sp.deleteFlag == false
                                  && c.deleteFlag == false && a.deleteFlag == false
                                  && c.c_date == confDate && c.c_Type != ConferenceStructureType.Room
                                  && cu.Status == 1 && a.a_ID == userId && cu.JoinStatus == 1

                                  select new
                                  {
                                      c.c_ID,
                                      cs.csponsor_title,
                                      data = new
                                      {
                                          csponsor_ID = cs.csponsor_sponsorID,
                                          sp.s_logo,
                                          sp.templateId,
                                          sp.s_url
                                      }

                                  }).ToList()
                                                  .GroupBy(x => new { x.c_ID, x.csponsor_title }, (key, g) =>
                                                   new { csponsor_title = key.csponsor_title, cId = key.c_ID, sponsorlist = g.Select(x => x.data) })
                                                   .ToLookup(x => x.cId).ToDictionary(t => t.Key, t => t);
            
            var result = mainQuery.AsNoTracking().Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime)
            .GroupBy(x => new { x.StartTime, x.EndTime })
            .Select(b => new
            {
                value = b.ToList()
            });

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            objResponse.conferenceList = result;
            objResponse.sponsorList = SponsorDetails;
            response.Add(objResponse);
            return response;
        }

        public dynamic GetAttandanceConferencGroupByTime(string userId, string returnUrl, string confDate)
        {
            var confeObj = (from cu in RepositoryContext.ConferenceUsers
                            join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                            join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                            where a.a_ID == userId && cu.JoinStatus == 1 && cu.Status == 1 && a.deleteFlag == false
                            && c.deleteFlag == false
                            && (confDate == null || confDate == "" || c.c_date == confDate) 
                            && c.c_Type != ConferenceStructureType.Room
                            select new { c.c_date, c.c_ID }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).Distinct().ToArray();

            if (string.IsNullOrEmpty(confDate))
            {
                confDate = dayList[0].day;
            }

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(confDate) && dayCount == 1)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy.day == confDate)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }
                if (doQuery)
                {
                    var mainQuery = from c in RepositoryContext.Conferences
                                    join cobj in confeObj on c.c_ID equals cobj.c_ID
                                    join cat in (from cc in RepositoryContext.ConferenceCategories where cc.deleteFlag == false select new { cc.cc_ID, cc.cc_categoryname })
                                    on c.c_cc equals cat.cc_ID into ccs
                                    from clst in ccs.DefaultIfEmpty()

                                    where c.c_Type != ConferenceStructureType.Room && c.deleteFlag == false
                                    select new
                                    {
                                        Id = c.c_ID,
                                        Topic = c.c_title,
                                        SessionTitle = c.c_sessionTitle,
                                        Description = c.c_brief,
                                        Category = clst.cc_categoryname,
                                        ModifiedDate = c.UpdatedDate,
                                        StartDate = c.c_date,
                                        StartTime = c.c_stime,
                                        EndTime = DateTime.ParseExact(c.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(c.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers
                                                      where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (from account in RepositoryContext.Accounts
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          join cc in (from c in RepositoryContext.Countries where c.deleteFlag == false select new { c.ISO3Digit, c.CountryName })
                                                          on account.a_country equals cc.ISO3Digit into cl
                                                          from ctl in cl.DefaultIfEmpty()
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == c.c_ID && account.a_type == UserType.Speaker
                                                          && conuser.AccountType == ConferenceSpeakerType.Speaker
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = c.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                          }).Select(x => x).ToList(),

                                        GuestOfHonorDetails = (from account in RepositoryContext.Accounts
                                                               join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                               join cc in (from c in RepositoryContext.Countries where c.deleteFlag == false select new { c.ISO3Digit, c.CountryName })
                                                            on account.a_country equals cc.ISO3Digit into cl
                                                               from ctl in cl.DefaultIfEmpty()
                                                               orderby conuser.SpeakerSeqNo
                                                               where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == c.c_ID
                                                               && account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                               select new
                                                               {
                                                                   speakerId = account.a_ID,
                                                                   Speaker = account.a_fullname,
                                                                   desigination = account.a_designation,
                                                                   Salutation = account.a_sal,
                                                                   company = account.a_company,
                                                                   country = ctl.CountryName,
                                                                   isMainSpeaker = c.MainSpeakerId == account.a_ID ? true : false,
                                                                   SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                               }).Select(x => x).ToList(),
                                        KeyNoteSpeaker = (from account in RepositoryContext.Accounts
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          join cc in (from c in RepositoryContext.Countries where c.deleteFlag == false select new { c.ISO3Digit, c.CountryName })
                                                          on account.a_country equals cc.ISO3Digit into cl
                                                          from ctl in cl.DefaultIfEmpty()
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == c.c_ID
                                                          && account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = c.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                          }).Select(x => x).ToList(),

                                        sessionModerator = (from account in RepositoryContext.Accounts
                                                            join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                            join cc in (from c in RepositoryContext.Countries where c.deleteFlag == false select new { c.ISO3Digit,c.CountryName }) 
                                                            on account.a_country equals cc.ISO3Digit into cl from ctl in cl.DefaultIfEmpty()
                                                            orderby conuser.SpeakerSeqNo
                                                            where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == c.c_ID && account.a_type == UserType.Speaker
                                                            && conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                            select new
                                                            {
                                                                speakerId = account.a_ID,
                                                                Speaker = account.a_fullname,
                                                                desigination = account.a_designation,
                                                                Salutation = account.a_sal,
                                                                company = account.a_company,
                                                                country = ctl.CountryName,
                                                                isMainSpeaker = c.MainSpeakerId == account.a_ID ? true : false,
                                                                SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                            }).Select(x => x).ToList(),
                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = c.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers
                                                                 where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == "Chat"
                                                                 select cu).Count() > 0 ? true : false,
                                        
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = c.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator) select cu).Count() > 0 ? true : false,
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1
                                                          && (cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                          || cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.SessionModerator)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where c.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == c.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        c.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers 
                                        where cu.WebinarId == c.c_ID && cu.Status == 1 
                                        && cu.UserId == userId select c.Video).FirstOrDefault(),
                                        Type = c.Type,
                                        favouriteStatus = (from f in RepositoryContext.UsersFavourites
                                                           where f.Status == 1 && f.ItemType == FavouriteItem.Conference && f.UserId == userId
                                                           && f.FavItemId == c.c_ID
                                                           select f.FavItemId).Count() > 0 ? true : false,
                                        SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                          join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                          where cs.deleteFlag == false && sp.deleteFlag == false && cs.csponsor_conferenceID == c.c_ID
                                                          select new
                                                          {
                                                              csponsor_ID = cs.csponsor_sponsorID,
                                                              cs.csponsor_title,
                                                              sp.s_logo,
                                                              sp.templateId,
                                                              sp.s_url
                                                          }).Select(x => x).ToList()
                                    };
                    var result = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle)
                    .GroupBy(x => new { x.StartTime, x.EndTime })
                    .Select(b => new
                    {
                        value = b.ToList()
                    });
                    objResponse.conferenceList = result;
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }


    }
}