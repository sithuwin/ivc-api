using System;
using System.Globalization;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class ZoomConfigurationRepository : RepositoryBase<tbl_ZoomConfiguration>, IZoomConfigurationRepository
    {

        public ZoomConfigurationRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        // public dynamic GetConfiguration (string userId) {
        //     ConfigKeys main = (from c in RepositoryContext.Configurations join a in RepositoryContext.Admins on c.ConfigId equals a.ConfigId where a.AdminId == userId select new ConfigKeys {
        //         ScrectKey = c.ScrectKey,
        //             APIKey = c.APIKey
        //     }).FirstOrDefault ();
        //     return main;
        // }

        public dynamic GetConfigurationByConfigId(int ConfigId)
        {
            ConfigKeys main = (from c in RepositoryContext.ZoomConfigurations
                               where c.ConfigId == ConfigId
                               select new ConfigKeys
                               {
                                   ScrectKey = c.ScrectKey,
                                   APIKey = c.APIKey,
                                   AccountId = c.AccountId
                               }).FirstOrDefault();
            return main;
        }

        public dynamic GetConfigurationList()
        {
            dynamic main = (from c in RepositoryContext.ZoomConfigurations
                            select new
                            {
                                c.ConfigId,
                                c.ConfigName
                            }).ToList();
            return main;
        }

    }
}