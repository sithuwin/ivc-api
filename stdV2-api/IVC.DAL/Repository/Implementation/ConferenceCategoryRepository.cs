using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using System.Linq;
namespace IVC.DAL.Repository.Implementation
{
    public class ConferenceCategoryRepository : RepositoryBase<tbl_ConferenceCategory>, IConferenceCategoryRepository
    {
        public ConferenceCategoryRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetConferenceCategorylist()
        {
            dynamic main = (from cc in RepositoryContext.ConferenceCategories
                            where cc.deleteFlag == false
                            select new
                            {
                                ID=cc.cc_ID,
                                Name=cc.cc_categoryname,
                                Order=cc.cc_seq,
                                Cateogory=cc.cc_Type
                            }).Distinct().ToList();
            return main;
        }

         public string GetConferenceCateName(string Id){
            string result = (from c in RepositoryContext.ConferenceCategories
            where c.deleteFlag == false && c.cc_ID == Id
            select c.cc_categoryname).FirstOrDefault();
            return result;
        }
    }
}