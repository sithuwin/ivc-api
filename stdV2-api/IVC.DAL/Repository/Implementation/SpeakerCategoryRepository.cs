using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class SpeakerCategoryRepository : RepositoryBase<tbl_SpeakerCategory>, ISpeakerCategoryRepository
    {
        public SpeakerCategoryRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}