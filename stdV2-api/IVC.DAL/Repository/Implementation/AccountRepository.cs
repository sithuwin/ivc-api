using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class AccountRepository : RepositoryBase<tbl_Account>, IAccountRepository
    {
        string returnUrl = "";
        public AccountRepository(AppDB repositoryContext) : base(repositoryContext)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:SpearkerUrl").Value;
        }

        public dynamic GetDelegateList(string userId, string sortField, string sortBy, dynamic obj)
        {
            var friendQuery = (from f in RepositoryContext.FriendShips
                               where f.RequestUserId == userId || f.ResponseUserId == userId
                               select new
                               {
                                   Status = f.Status,
                                   FriendId = (f.RequestUserId == userId ? f.ResponseUserId : f.RequestUserId),
                                   Request = (f.RequestUserId == userId ? true : false)
                               });
            var hideUsers = (from h in RepositoryContext.AccountHideLists where h.deleteFlag == false select h.AccountId).Distinct().ToArray();

            var main = (from a in RepositoryContext.Accounts
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on a.a_country equals ct.ISO3Digit into ctl
                        from dl in ctl.DefaultIfEmpty()
                        join f in friendQuery on a.a_ID equals f.FriendId into ftmp
                        from frtmp in ftmp.DefaultIfEmpty()

                        where
                        a.deleteFlag == false && a.a_ID != userId
                        //&& a.a_type != UserType.VIP && a.a_type != UserType.Media
                        && (a.a_type == UserType.Delegate || a.a_type == UserType.Speaker 
                        || a.a_type == UserType.MainExhibitor || a.a_type == UserType.ContactPerson)
                        && !hideUsers.Contains(a.a_ID)

                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a_country = dl.CountryName,
                            a.a_company,
                            Status = frtmp != null ? frtmp.Status : "",
                            Request = frtmp.Request,
                            shareVCard = a.ShareVCard == 1 ? true : false,
                            nfcid = a.NFCID
                        });
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForDelegate(main, obj);
                }
            }
            var objTotal = main.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);

            main = PaginationSessionForDelegate(main, obj);
            var objResult = main.AsNoTracking().ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetDelegateById(string id)
        {
            var main = (from a in RepositoryContext.Accounts
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on a.a_country equals ct.ISO3Digit into ctl
                        from dl in ctl.DefaultIfEmpty()
                        where a.deleteFlag == false && a.a_ID == id
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a_country = dl.CountryName,
                            a.a_company,
                            //a.a_email,
                            //a.a_Mobile,
                            a.a_designation,
                            a.a_addressHome,
                            ProfilePic = (a.ProfilePic != null && a.ProfilePic != "" ? (returnUrl + a.ProfilePic) : (returnUrl + "defaultSpeaker.jpg")),
                            a.Biography,
                            a.a_fname,
                            a.a_lname,
                            shareVCard = a.ShareVCard == 1 ? true : false,
                            vCard = a.ShareVCard.ToString() != "1" ? "" : a.VCard,
                            leaderBoardPoints = (from lb in RepositoryContext.leaderBoards where lb.deleteFlag == false && lb.ParticipantID == id select lb.TotalPoints).FirstOrDefault()
                        }).FirstOrDefault();
            return main;
        }

        public dynamic GetSpeakerList(string returnUrl)
        {
            var main = (from a in RepositoryContext.Accounts
                        join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                        join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                        where a.deleteFlag == false && a.a_type == UserType.Speaker && cu.Status == 1
                        && c.deleteFlag == false
                        && (cu.AccountType == ConferenceSpeakerType.Speaker ||
                        cu.AccountType == ConferenceSpeakerType.GuestOfHonor ||
                        cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
                        cu.AccountType == ConferenceSpeakerType.SessionModerator)
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a.a_designation,
                            a.a_subTitle,
                            a.a_Time,
                            ProfilePic = (a.ProfilePic != null && a.ProfilePic != "" ? (returnUrl + a.ProfilePic) : (returnUrl + "defaultSpeaker.jpg"))
                        }).Distinct().ToList().OrderBy(x => x.a_fullname);
            return main;
        }

        public dynamic GetSpeakerDetailsById(string returnUrl, string sId)
        {
            var main = (from a in RepositoryContext.Accounts
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.ISO3Digit, c.CountryName }) on a.a_country equals ct.ISO3Digit into ctl
                        from sl in ctl.DefaultIfEmpty()

                        join lb in (from l in RepositoryContext.leaderBoards
                                    where l.deleteFlag == false
                                    select new { l.ParticipantID, l.TotalPoints }) on a.a_ID equals lb.ParticipantID into lb1
                        from lb2 in lb1.DefaultIfEmpty()
                        where a.a_ID == sId && a.deleteFlag == false && a.a_type == UserType.Speaker
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a.a_designation,
                            a.a_company,
                            a_country = sl.CountryName,
                            a.Biography,
                            ProfilePic = (a.ProfilePic != null && a.ProfilePic != "" ? (returnUrl + a.ProfilePic) : (returnUrl + "defaultSpeaker.jpg")),
                            shareVCard = a.ShareVCard == 1 ? true : false,
                            vCard = a.ShareVCard.ToString() != "1" ? "" : a.VCard,
                            a.a_subTitle,
                            a.a_Time,
                            leaderBoardPoints = lb2.TotalPoints
                            //leaderBoardPoints = (from lb in RepositoryContext.leaderBoards where lb.deleteFlag == false && lb.ParticipantID == sId select lb.TotalPoints).FirstOrDefault()
                        }).AsNoTracking().FirstOrDefault();
            return main;
        }

        private dynamic FilterSessionForDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                Status = default(string),
                Request = default(bool),
                shareVCard = default(bool),
                nfcid = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "company")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_company.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "country")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_country.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "mobile_search")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name)
                                || x.a_company.ToLower().Contains(Name) || x.a_country.ToLower().Contains(Name));

                            }
                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                Status = default(string),
                Request = default(bool),
                shareVCard = default(bool),
                nfcid = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.a_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public int CheckValidation(string aId, string email)
        {
            int result = 0;
            int emailCount = RepositoryContext.Accounts.Where(x => x.a_ID != aId && x.a_email == email && x.deleteFlag == false).Count();
            if (emailCount > 0)
            {
                result = 1;
            }
            return result;

        }
        public dynamic ChangePasword()
        {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string salt = Configuration.GetSection("SaltKey").Value;
            try
            {
                (from acc in RepositoryContext.Accounts select acc).ToList()
                    .ForEach(x => x.a_Password = DataAccess.SaltedHash.ComputeHash(salt, x.a_Password));
                RepositoryContext.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                result = false;
            }

            return result;

        }

        public dynamic GetDeviceUsageReport(dynamic obj, string sortField, string sortBy, bool isExport)
        {
            dynamic result = null;
            var CountryArray = (from u in RepositoryContext.Accounts join audit in RepositoryContext.Audits on u.a_ID equals audit.UserId where audit.LogType == AuditLogType.login select u.a_country).Distinct().ToArray();

            var mainQuery = (from account in RepositoryContext.Accounts
                             join ct in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.CountryName, c.ISO3Digit }) on account.a_country equals ct.ISO3Digit into cl
                             from ctl in cl.DefaultIfEmpty()
                             where account.deleteFlag == false && CountryArray.Contains(account.a_country)
                             select new
                             {
                                 Country = ctl.CountryName,
                                 Window = (from a in RepositoryContext.Accounts
                                           join au in RepositoryContext.Audits on a.a_ID equals au.UserId
                                           where a.a_country == account.a_country && a.deleteFlag == false
                                           && au.AlternativeId == DeviceType.Window
                                           select a.a_ID).Distinct().Count(),
                                 Android = (from a in RepositoryContext.Accounts join au in RepositoryContext.Audits on a.a_ID equals au.UserId where a.a_country == account.a_country && a.deleteFlag == false && au.AlternativeId == DeviceType.Android select a.a_ID).Distinct().Count(),
                                 IOS = (from a in RepositoryContext.Accounts join au in RepositoryContext.Audits on a.a_ID equals au.UserId where a.a_country == account.a_country && a.deleteFlag == false && au.AlternativeId == DeviceType.IOS select a.a_ID).Distinct().Count(),
                             }).Distinct();

            var objTotal = mainQuery.Count();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForDeviceUsuageReport(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.ToList();
            }

            mainQuery = PaginationSessionForDeviceUsuageReport(mainQuery, obj);

            var objResult = mainQuery.ToList();

            result = new { data = objResult, dataFoundRowsCount = objTotal };

            return result;

        }

        private dynamic FilterSessionForDeviceUsuageReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                Window = default(int),
                Android = default(int),
                IOS = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "Country")
                    {
                        string Country = filterValue;
                        if (!String.IsNullOrEmpty(Country))
                        {
                            Country = Country.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Country.ToLower().Contains(Country));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForDeviceUsuageReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                Window = default(int),
                Android = default(int),
                IOS = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.Country);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.Country) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public dynamic GetRegistrationListByDate(dynamic obj)
        {
            DateTime fromdate = Convert.ToDateTime(obj.fromDate);
            DateTime todate = Convert.ToDateTime(obj.toDate);

            var accountData = (from a in RepositoryContext.Accounts
                               where a.deleteFlag == false
                               && (a.a_type == UserType.Visitor || a.a_type == UserType.Delegate 
                               || a.a_type == UserType.FreeDelegate || a.a_type == UserType.Speaker
                               || a.a_type == UserType.MainExhibitor || a.a_type == UserType.ContactPerson)
                               select new
                               {
                                   name = a.a_type == UserType.Visitor ? "Visitor" :
                                   a.a_type == UserType.Delegate ? "Delegate" :
                                   a.a_type == UserType.Speaker ? "Speaker" :
                                   (a.a_type == UserType.MainExhibitor || a.a_type == UserType.ContactPerson) ? "Exhibitor" :
                                   a.a_type == UserType.FreeDelegate ? "FreeDelegate" : "",
                                   a.a_ID,
                                   createdDate = Convert.ToDateTime(a.UpdatedDate).Date
                               }).Distinct().ToList().OrderBy(x => x.createdDate);

            var main = (from c in accountData
                        where c.createdDate >= fromdate.Date && c.createdDate <= todate.Date
                        select new
                        {
                            c.name,
                            series = new
                            {
                                name = c.createdDate.ToString("yyyy/MM/dd"),
                                value = (from b in accountData
                                         where c.name == b.name && c.createdDate == b.createdDate
                                         select b.a_ID).Distinct().Count()
                            }
                        }).Distinct().ToList().GroupBy(x => x.name, (key, g) => new { name = key, series = g.Select(x => x.series) });
            //main = main.ToList().OrderBy(x => x.name);
            main = main.OrderBy(x => x.name);
            return main;
        }

        public dynamic GetRegistrationGroup()
        {
            var main = (from a in RepositoryContext.Accounts
                        where a.deleteFlag == false
                        && (a.a_type == UserType.Visitor || a.a_type == UserType.Delegate || a.a_type == UserType.Speaker
                        || a.a_type == UserType.MainExhibitor || a.a_type == UserType.ContactPerson || a.a_type == UserType.FreeDelegate)
                        select new
                        {
                            name = a.a_type == UserType.Visitor ? "Visitor" :
                            a.a_type == UserType.Delegate ? "Delegate" :
                            a.a_type == UserType.Speaker ? "Speaker" : 
                            a.a_type == UserType.FreeDelegate ? "FreeDelegate" :
                            (a.a_type == UserType.MainExhibitor || a.a_type == UserType.ContactPerson) ? "Exhibitor" : "",
                            a.a_ID
                        }).Distinct().ToList().GroupBy(x => x.name, (key, g) =>
                        new { name = key, value = g.Count() });

            return main;
        }

        public dynamic GetSpeakerAccessUserList(string userId, string confId, string sortField, string sortBy, dynamic obj, string[] accessEmailList,string[] panelitEmail,string conType)
        {
            var main = (from a in RepositoryContext.Accounts
                        join ca in RepositoryContext.ConferenceUsers on a.a_ID equals ca.UserId
                        join c in RepositoryContext.Conferences on ca.WebinarId equals c.c_ID
                        where (ca.AccountType == ConferenceSpeakerType.Speaker || ca.AccountType == ConferenceSpeakerType.SessionModerator || ca.AccountType == ConferenceSpeakerType.GuestOfHonor || ca.AccountType == ConferenceSpeakerType.KeynoteSpeaker)
                        && a.deleteFlag == false && ca.Status == 1 && c.deleteFlag == false 
                        && c.c_ID == confId && ca.AccessRightId != null && panelitEmail.Contains(a.a_email)
                        && (conType == ConferenceType.Embedded || (conType == ConferenceType.Default && accessEmailList.Contains(a.a_email)))
                        select new
                        {
                            id = a.a_ID,
                            fullName = a.a_fullname,
                            firstName = a.a_fname,
                            lastName = a.a_lname,
                            company = a.a_company,
                            isAccess = accessEmailList.Contains(a.a_email),
                        }).Distinct();
            var objTotal = main.ToList().Count();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForAccessUserList(main, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            main = PaginationSessionForAccessUserList(main, obj);
            var objResult = main.ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetDelegateAccessUserList(string userId, string confId, string sortField, string sortBy, dynamic obj, string[] accessEmailList, string type)
        {
            var main = (from a in RepositoryContext.Accounts
                        join ca in RepositoryContext.ConferenceUsers on a.a_ID equals ca.UserId
                        join c in RepositoryContext.Conferences on ca.WebinarId equals c.c_ID
                        where a.deleteFlag == false && ca.Status == 1 && c.deleteFlag == false 
                        && ca.AccountType == UserType.Delegate && c.c_ID == confId && ca.AccessRightId != null
                        &&(type == ConferenceType.Embedded || (type == ConferenceType.Default && accessEmailList.Contains(a.a_email)))
                        select new
                        {
                            id = a.a_ID,
                            fullName = a.a_fullname,
                            firstName = a.a_fname,
                            lastName = a.a_lname,
                            company = a.a_company,
                            isAccess = type == ConferenceType.Embedded ? true : (accessEmailList.Contains(a.a_email) ? !String.IsNullOrEmpty(ca.AccessRightId) : false)//,//&& !String.IsNullOrEmpty(ca.AccessRightId)
                        }).Distinct();
            var objTotal = main.ToList().Count();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForAccessUserList(main, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            main = PaginationSessionForAccessUserList(main, obj);
            var objResult = main.ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForAccessUserList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                fullName = default(string),
                firstName = default(string),
                lastName = default(string),
                company = default(string),
                isAccess = default(bool)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "name")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.firstName.ToLower().Contains(Name));

                        }
                    }
                    if (filterName == "company")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.company.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForAccessUserList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                fullName = default(string),
                firstName = default(string),
                lastName = default(string),
                company = default(string),
                isAccess = default(bool)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        public dynamic GetSpeakerListByCategory(dynamic obj, string returnUrl)
        {
            string category = obj.category;
            var main = (from a in RepositoryContext.Accounts
                        join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                        join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                        join d in RepositoryContext.Days on c.c_date equals d.d_date
                        where a.deleteFlag == false && a.a_type == UserType.Speaker && cu.Status == 1
                        && (cu.AccountType == ConferenceSpeakerType.Speaker ||
                        cu.AccountType == ConferenceSpeakerType.GuestOfHonor ||
                        cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
                        cu.AccountType == ConferenceSpeakerType.SessionModerator)
                        && c.deleteFlag == false 
                        && (category == "" || category == null || d.d_ID == category)
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a.a_designation,
                            a.a_subTitle,
                            a.a_Time,
                            ProfilePic = (a.ProfilePic != null && a.ProfilePic != "" ? (returnUrl + a.ProfilePic) : (returnUrl + "defaultSpeaker.jpg"))
                        }).AsNoTracking().Distinct().ToList();
            return main.OrderBy(x => x.a_fullname);
        }

        private dynamic FilterSessionForDelegate001(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                fullName = default(string),
                designation = default(string),
                subTitle = default(string),
                a_Time = default(string),
                ProfilePic = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "fullName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.fullName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForDelegate001(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                fullName = default(string),
                designation = default(string),
                subTitle = default(string),
                a_Time = default(string),
                ProfilePic = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.a_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
    }

}