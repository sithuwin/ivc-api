using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class UsersFavouriteRepository : RepositoryBase<tbl_UsersFavourite>, IUsersFavouriteRepository
    {
        public UsersFavouriteRepository(AppDB repositoryContext) : base(repositoryContext) { }
    }
}