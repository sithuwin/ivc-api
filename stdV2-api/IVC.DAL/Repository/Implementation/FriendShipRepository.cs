using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class FriendShipRepository : RepositoryBase<tbl_FriendShip>, IFriendShipRepository
    {
        public FriendShipRepository(AppDB repositoryContext) : base(repositoryContext) { }
        public dynamic GetRequestedDelegateList(string userId, dynamic obj, string sortField, string sortBy)
        {
            var main = (from f in RepositoryContext.FriendShips
                        join d in RepositoryContext.Accounts on f.RequestUserId equals d.a_ID
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.CountryName, c.ISO3Digit }) on d.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where d.deleteFlag == false && f.Status == Status.Pending && f.ResponseUserId == userId
                        //&& d.a_type != UserType.Media && d.a_type != UserType.VIP
                        select new
                        {
                            f.Id,
                            d.a_ID,
                            d.a_fullname,
                            a_country = ctl.CountryName,
                            d.a_company,
                            f.Status,
                            nfcid = d.NFCID
                        });
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForRequestDelegate(main, obj);
                }
            }
            var objTotal = main.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);

            main = PaginationSessionForRequestDelegate(main, obj);
            var objResult = main.AsNoTracking().ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetMyConnection(string userId, dynamic obj, string sortField, string sortBy)
        {
            var main = (from a in RepositoryContext.Accounts
                        join f1 in (from f in RepositoryContext.FriendShips
                                    where (f.RequestUserId == userId || f.ResponseUserId == userId) && f.Status == Status.Done
                                    select new
                                    {
                                        Status = f.Status,
                                        FriendId = (f.RequestUserId == userId ? f.ResponseUserId : f.RequestUserId),
                                        Request = (f.RequestUserId == userId ? true : false)
                                    }) on a.a_ID equals f1.FriendId
                        join ct in (from c in RepositoryContext.Countries
                                    where c.deleteFlag == false
                                    select new { c.CountryName, c.ISO3Digit }) on a.a_country equals ct.ISO3Digit into cl
                        from ctl in cl.DefaultIfEmpty()
                        where a.deleteFlag == false
                        select new
                        {
                            a.a_ID,
                            a.a_fullname,
                            a_country = ctl.CountryName,
                            a.a_company,
                            a.a_designation,
                            Status = f1.Status,
                            Request = f1.Request,
                            shareVCard = a.ShareVCard == 1 ? true : false,
                            VCard = a.ShareVCard == 1 ? a.VCard : "",
                            nfcid = a.NFCID
                        });
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForMyConnection(main, obj);
                }
            }
            var objTotal = main.AsNoTracking().Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.AsNoTracking().OrderBy(sortList);
            main = PaginationSessionForMyConnection(main, obj);
            var objResult = main.AsNoTracking().ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public dynamic GetNetworkingReport(dynamic obj, string sortField, string sortBy, bool isExport)
        {
            dynamic result = null;

            var mainQuery = (from account in RepositoryContext.Accounts
                             where account.deleteFlag == false
                             select new
                             {
                                 UserId = account.a_ID,
                                 UserName = account.a_fullname,
                                 TotalExchange = (from f in RepositoryContext.FriendShips where f.Status == Status.Done && (f.RequestUserId == account.a_ID || f.ResponseUserId == account.a_ID) select f.Id).Count(),
                                 TotalAccepted = (from f in RepositoryContext.FriendShips where f.RequestUserId == account.a_ID && f.Status == Status.Done select f.Id).Distinct().Count(),
                                 TotalRejectedRequest = (from f in RepositoryContext.FriendShips where f.ResponseUserId == account.a_ID && f.Status == Status.Reject select f.Id).Distinct().Count()
                             }).Distinct();

            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForNetwokingReport(mainQuery, obj);
                }
            }
            var objTotal = mainQuery.Count();
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }
            mainQuery = PaginationSessionForNetwokingReport(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal };
            return result;

        }

        private dynamic FilterSessionForRequestDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Id = default(int),
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                Status = default(string),
                nfcid = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "company")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_company.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "country")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_country.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "mobile_search")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name)
                                || x.a_company.ToLower().Contains(Name) || x.a_country.ToLower().Contains(Name));

                            }
                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForRequestDelegate(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Id = default(int),
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                Status = default(string),
                nfcid = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.a_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }
        private dynamic FilterSessionForMyConnection(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                a_designation = default(string),
                Status = default(string),
                Request = default(bool),
                shareVCard = default(bool),
                VCard = default(string),
                nfcid = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "name")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "company")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_company.ToLower().Contains(Name));

                            }
                        }
                        if (filterName == "country")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_country.ToLower().Contains(Name));

                            }
                        }

                        if (filterName == "mobile_search")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.a_fullname.ToLower().Contains(Name)
                                || x.a_company.ToLower().Contains(Name) || x.a_country.ToLower().Contains(Name));

                            }
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForMyConnection(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                a_ID = default(string),
                a_fullname = default(string),
                a_country = default(string),
                a_company = default(string),
                a_designation = default(string),
                Status = default(string),
                Request = default(bool),
                shareVCard = default(bool),
                VCard = default(string),
                nfcid = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.a_ID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.a_ID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        private dynamic FilterSessionForNetwokingReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                UserName = default(string),
                TotalExchange = default(int),
                TotalAccepted = default(int),
                TotalRejectedRequest = default(int),

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "UserName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.UserName.ToLower().Contains(Name));

                        }
                    }

                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForNetwokingReport(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                UserName = default(string),
                TotalExchange = default(int),
                TotalAccepted = default(int),
                TotalRejectedRequest = default(int),

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

    }
}