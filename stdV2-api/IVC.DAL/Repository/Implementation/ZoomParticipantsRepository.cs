using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class ZoomParticipantsRepository : RepositoryBase<tbl_ZoomParticipants>, IZoomParticipantsRepository
    {
        public ZoomParticipantsRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public void UpdateParticipants(dynamic response,string cid,string userId)
        {
            for (int i = 0; i < response.Count; i++)
            {
                string userEmail = response[i].user_email;
                string joinTime = response[i].join_time;
                

                tbl_ZoomParticipants oldParticipantsSummary = RepositoryContext.ZoomParticipants.Where(x => x.UserEmail == userEmail 
                && x.WebinarId == cid && x.JoinTime == joinTime).AsNoTracking().FirstOrDefault();
                if (oldParticipantsSummary == null)
                {
                    tbl_ZoomParticipants participantsSummary = new tbl_ZoomParticipants();
                    participantsSummary.Id = response[i].id;
                    participantsSummary.UserId = response[i].user_id;
                    participantsSummary.Name = response[i].name;
                    participantsSummary.UserEmail = response[i].user_email;
                    participantsSummary.JoinTime = response[i].join_time;
                    participantsSummary.LeaveTime = response[i].leave_time;
                    participantsSummary.Duration = response[i].duration;
                    participantsSummary.AttentivenessScore = response[i].attentiveness_score;
                    participantsSummary.WebinarId = cid;
                    participantsSummary.AdminId = userId;
                    participantsSummary.CreatedDate = DateTime.Now;
                    participantsSummary.ModifiedDate = DateTime.Now;
                    RepositoryContext.ZoomParticipants.Add(participantsSummary);
                    RepositoryContext.SaveChanges();
                }else{
                    oldParticipantsSummary.JoinTime = response[i].join_time;
                    oldParticipantsSummary.LeaveTime = response[i].leave_time;
                    oldParticipantsSummary.Duration = response[i].duration;
                    oldParticipantsSummary.ModifiedDate = DateTime.Now;
                    RepositoryContext.ZoomParticipants.Update(oldParticipantsSummary);
                    RepositoryContext.SaveChanges();

                }
            }
        }

        public dynamic GetParticipantsSummaryByWebinarId(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport)
        {
            dynamic result = null;
            var joinedUserArray = RepositoryContext.ZoomParticipants.Where(x => x.WebinarId == webinarId).Select(x => x.UserEmail).Distinct().ToArray();
            
            var mainQuery = from u in RepositoryContext.Accounts
                            join cu in RepositoryContext.ConferenceUsers on u.a_ID equals cu.UserId
                            join ct in (from c in RepositoryContext.Countries
                                        where c.deleteFlag == false
                                        select new { c.CountryName, c.ISO3Digit }) on u.a_country equals ct.ISO3Digit into cl
                            from ctl in cl.DefaultIfEmpty()

                            where u.deleteFlag == false && cu.Status == 1 && cu.WebinarId == webinarId
                            && joinedUserArray.Contains(u.a_email)
                            select new
                            {
                                UserId = u.a_ID,
                                UserName = u.a_fullname,
                                Email = u.a_email,
                                JobTitle = u.a_designation,
                                Country = ctl.CountryName,
                                JoinCount = RepositoryContext.ZoomParticipants.Where(x => x.UserEmail == u.a_email && x.WebinarId == webinarId).Select(x => x.Id).Count()
                            };

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSession(mainQuery, obj);
                }
            }

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }
            var objTotal = mainQuery.Count();
            mainQuery = PaginationSession(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                UserName = default(string),
                Email = default(string),
                JobTitle = default(string),
                Country = default(string),
                JoinCount = default(int)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "UserName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.UserName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                UserName = default(string),
                Email = default(string),
                JobTitle = default(string),
                Country = default(string),
                JoinCount = default(int)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public dynamic GetCountrySummary(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport)
        {
            dynamic result = null;
            var joinedUserArray = RepositoryContext.ZoomParticipants.Where(x => x.WebinarId == webinarId).Select(x => x.UserEmail).Distinct().ToArray();
            var mainQuery = (from u in RepositoryContext.Accounts
                             join cu in RepositoryContext.ConferenceUsers on u.a_ID equals cu.UserId
                             join ct in (from c in RepositoryContext.Countries
                                         where c.deleteFlag == false
                                         select new { c.CountryName, c.ISO3Digit }) on u.a_country equals ct.ISO3Digit into cl
                             from ctl in cl.DefaultIfEmpty()
                             where u.deleteFlag == false && cu.Status == 1 && cu.WebinarId == webinarId
                             select new
                             {
                                 Country = ctl.CountryName,
                                 RegisterCount = (from a in RepositoryContext.Accounts join wu in RepositoryContext.ConferenceUsers on a.a_ID equals wu.UserId where a.deleteFlag == false && wu.WebinarId == webinarId && wu.Status == 1 && a.a_country == u.a_country select a.a_ID).Distinct().Count(),
                                 AttendanceCount = (from a in RepositoryContext.Accounts join wu in RepositoryContext.ConferenceUsers on a.a_ID equals wu.UserId where a.deleteFlag == false && wu.WebinarId == webinarId && wu.Status == 1 && a.a_country == u.a_country && joinedUserArray.Contains(a.a_email) select a.a_ID).Distinct().Count(),
                             }).Distinct();

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = CountryReportFilterSession(mainQuery, obj);
                }
            }

            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }
            var objTotal = mainQuery.Count();
            mainQuery = CountryReportPaginationSession(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal };
            return result;
        }

        public dynamic GetAttandenceDetails(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport)
        {
            dynamic result = null;
            TimeSpan test = TimeSpan.FromMinutes(365);
            var joinedUserArray = RepositoryContext.ZoomParticipants.Where(x => x.WebinarId == webinarId).Select(x => x.UserEmail).Distinct().ToArray();

            var mainQuery = from u in RepositoryContext.Accounts
                            join wu in RepositoryContext.ConferenceUsers on u.a_ID equals wu.UserId
                            join ct in (from c in RepositoryContext.Countries
                                        where c.deleteFlag == false
                                        select new { c.CountryName, c.ISO3Digit }) on u.a_country equals ct.ISO3Digit into cl
                            from ctl in cl.DefaultIfEmpty()
                            where u.deleteFlag == false && wu.WebinarId == webinarId && wu.Status == 1 
                            && joinedUserArray.Contains(u.a_email)
                            select new
                            {
                                UserId = u.a_ID,
                                RegistrationId = u.RegPortalID,
                                UserName = u.a_fullname,
                                FirstName = u.a_fname,
                                LastName = u.a_lname,
                                Email = u.a_email,
                                JobTitle = u.a_designation,
                                Country = ctl.CountryName,
                                Duration = TimeSpan.FromSeconds(
                                Convert.ToDouble(
                                (from z in RepositoryContext.ZoomParticipants where z.UserEmail == u.a_email && z.WebinarId == webinarId select z.Duration).Sum()
                                )).ToString(@"hh\:mm\:ss"),
                                MobileNo = u.a_Mobile,
                                Address = u.a_addressHome,
                                Company = u.a_company
                            };

            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    mainQuery = FilterSessionForAttandenceDetails(mainQuery, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            mainQuery = mainQuery.AsNoTracking().OrderBy(sortList);
            if (isExport)
            {
                return mainQuery.AsNoTracking().ToList();
            }

            var objTotal = mainQuery.Count();
            mainQuery = PaginationSessionForAttandenceDetails(mainQuery, obj);

            var objResult = mainQuery.AsNoTracking().ToList();
            result = new { data = objResult, dataFoundRowsCount = objTotal, result = true, message = "Successfully retrieved" };

            return result;
        }

        private dynamic FilterSessionForAttandenceDetails(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                RegistrationId = default(string),
                UserName = default(string),
                FirstName = default(string),
                LastName = default(string),
                Email = default(string),
                JobTitle = default(string),
                Country = default(string),
                Duration = default(string),
                MobileNo = default(string),
                Address = default(string),
                Company = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "UserName")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.UserName.ToLower().Contains(Name));

                        }
                    }
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForAttandenceDetails(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                UserId = default(string),
                RegistrationId = default(string),
                UserName = default(string),
                FirstName = default(string),
                LastName = default(string),
                Email = default(string),
                JobTitle = default(string),
                Country = default(string),
                Duration = default(string),
                MobileNo = default(string),
                Address = default(string),
                Company = default(string)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.UserId);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.UserId) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        private dynamic CountryReportFilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                RegisterCount = default(int),
                AttendanceCount = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;
                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;

                    if (filterName == "Country")
                    {
                        string Country = filterValue;
                        if (!String.IsNullOrEmpty(Country))
                        {
                            Country = Country.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Country.ToLower().Contains(Country));
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic CountryReportPaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                Country = default(string),
                RegisterCount = default(int),
                AttendanceCount = default(int)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.Country);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.Country) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public class NameValuePair
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public dynamic GetEventAttendanceReport()
        {
            List<dynamic> response = new List<dynamic>();
            List<string> conferences = (from conf in RepositoryContext.Conferences where conf.deleteFlag == false orderby conf ascending select conf.c_date).Distinct().ToList();

            for (int i = 0; i < conferences.Count; i++)
            {
                dynamic objResponse = new System.Dynamic.ExpandoObject();
                objResponse.name = "Day " + (i + 1);
                var date = conferences[i];
                List<dynamic> dayDataList = new List<dynamic>();
                /*Coming*/
                NameValuePair newComing = new NameValuePair();
                newComing.value = GetParticipantCount(date, true);
                newComing.name = "Coming";
                dayDataList.Add(newComing);
                /*Not Coming*/
                NameValuePair newNotComing = new NameValuePair();
                newNotComing.value = GetParticipantCount(date, false);
                newNotComing.name = "Not Coming";
                dayDataList.Add(newNotComing);

                objResponse.series = dayDataList;
                response.Add(objResponse);
            }

            return response;
        }

        public int GetParticipantCount(string date, bool isComing)
        {
            int result = 0;
            try
            {
                dynamic main = null;
                if(isComing)
                {
                    main =
                        (from a in RepositoryContext.Accounts
                        where a.deleteFlag == false 
                        && 
                        (from zp in RepositoryContext.ZoomParticipants where zp.Name!="Event Admin"
                        select zp.UserEmail).Contains(a.a_email)
                        &&
                        (from cu in RepositoryContext.ConferenceUsers
                        where cu.Status==1 
                        &&
                        (from c in RepositoryContext.Conferences
                        where c.deleteFlag== false && c.c_date==date select c.c_ID).Contains(cu.WebinarId)
                        select cu.UserId)
                        .Contains(a.a_ID)
                         select a).ToList();                   
                }
                else
                {
                    main =
                        (from a in RepositoryContext.Accounts
                        where a.deleteFlag == false 
                        && 
                        !(from zp in RepositoryContext.ZoomParticipants where zp.Name!="Event Admin"
                        select zp.UserEmail).Contains(a.a_email)
                        &&
                        (from cu in RepositoryContext.ConferenceUsers
                        where cu.Status==1 
                        &&
                        (from c in RepositoryContext.Conferences
                        where c.deleteFlag== false && c.c_date==date select c.c_ID).Contains(cu.WebinarId)
                        select cu.UserId)
                        .Contains(a.a_ID)
                        select a).ToList();
                }
                result=main.Count;
            }
            catch(Exception ex)
            {}            
            return result;
        }

    }
}