using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class ConferenceSpeakerRepository : RepositoryBase<tbl_ConferenceSpeakers>, IConferenceSpeakerRepository
    {
        public ConferenceSpeakerRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}