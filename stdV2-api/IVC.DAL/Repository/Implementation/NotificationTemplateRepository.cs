using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class NotificationTemplateRepository : RepositoryBase<tblNotificationTemplate>, INotificationTemplateRepository
    {
        public NotificationTemplateRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}