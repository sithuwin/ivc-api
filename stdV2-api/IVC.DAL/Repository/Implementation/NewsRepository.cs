using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using System.Linq;
namespace IVC.DAL.Repository.Implementation
{
    public class NewsRepository : RepositoryBase<tblNews>, INewsRepository
    {
        public NewsRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetNewslist()
        {
            dynamic main = (from nn in RepositoryContext.News
                            where nn.deleteFlag == false
                            select new
                            {
                                ID=nn.n_ID,
                                Title=nn.n_title,
                                Content=nn.n_content
                            }).Distinct().ToList();
            return main;
        }

         public string GetNews(string Id){
            string result = (from nn in RepositoryContext.News
            where nn.deleteFlag == false && nn.n_ID == Id
            select nn.n_content).FirstOrDefault();
            return result;
        }
    }
}