using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace IVC.DAL.Repository.Implementation
{
    public class ConfrenceRepository : RepositoryBase<tbl_Conference>, IConfrenceRepository
    {
        public ConfrenceRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetConferenceById(string Id, string userId, tbl_Conference conObj)
        {
            string curCId = Id;
            if (!String.IsNullOrEmpty(conObj.RoomId))
            {
                curCId = conObj.RoomId;
            }
            var mainQuery = (from main in RepositoryContext.Conferences
                             where main.deleteFlag == false && main.c_ID == curCId
                             select new
                             {
                                 Id = main.c_ID,
                                 Topic = main.c_title,
                                 SessionTitle = main.c_sessionTitle,
                                 Description = main.c_brief,
                                 StartDate = main.c_date,
                                 StartTime = main.c_stime,
                                 InfoPic = main.InfoPic,
                                 EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                 SpeakerDetails = (from account in RepositoryContext.Accounts
                                                   join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                   join c in RepositoryContext.Countries on account.a_country equals c.ISO3Digit into ct
                                                   from ctl in ct.DefaultIfEmpty()
                                                   where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID && account.a_type == "S"
                                                   select new
                                                   {
                                                       speakerId = account.a_ID,
                                                       Speaker = account.a_fullname,
                                                       desigination = account.a_designation,
                                                       company = account.a_company,
                                                       country = ctl.CountryName,
                                                       isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                   }).Distinct().ToList(),
                                 main.SponsorLogo,
                                 confUserType = (from ss in RepositoryContext.ConferenceUsers where ss.UserId == userId && ss.Status == 1 && ss.WebinarId == conObj.c_ID select ss.AccountType).FirstOrDefault(),
                                 main.c_Type
                                 // cmainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                 //                      join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                 //                      where main.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                 //                      select new
                                 //                      {
                                 //                          mainSpeakerName = a.a_fullname

                                 //                      })
                             }).FirstOrDefault();
            return mainQuery;
        }

        public dynamic GetAllWebinarListForUser(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            string conferenceCate = obj.conferenceCategory;
            string conferenceTitle = obj.conferenceTitle;
            string speaker = obj.speaker;
            string conferenceType = obj.conferenceType;

            var confeObj = (from c in RepositoryContext.Conferences
                            join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId into t
                            from cuobj in t.DefaultIfEmpty()
                            join a in RepositoryContext.Accounts on cuobj.UserId equals a.a_ID into acc
                            from cspojb in acc.DefaultIfEmpty()
                            where c.deleteFlag == false &&
c.c_Type != ConferenceStructureType.Room &&
(cuobj.Status.ToString() == null || cuobj.Status == 1) &&
(conferenceTitle == null || conferenceTitle == "" || c.c_title.Contains(conferenceTitle)) &&
(conferenceCate == null || conferenceCate == "" || conferenceCate == c.c_cc) &&
(conferenceType == null || conferenceType == "" || conferenceType == c.c_Type) &&
(speaker == null || speaker == "" ||
(speaker == cspojb.a_ID && cspojb.a_type == UserType.Speaker && cuobj.Status == 1))
                            select new { c.c_date, c.c_ID }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days join c in confeObj on d.d_date equals c.c_date where d.deleteFlag == false orderby d.d_date ascending select d.d_date).Distinct().ToArray();

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(conferenceDate) && dayCount == 1)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy == conferenceDate)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }

                if (doQuery)
                {
                    var mainQuery = from main in RepositoryContext.Conferences
                                    join cobj in confeObj on main.c_ID equals cobj.c_ID
                                    join cat in (from cc in RepositoryContext.ConferenceCategories where cc.deleteFlag == false select new { cc.cc_ID, cc.cc_categoryname })
                                    on main.c_cc equals cat.cc_ID into ccs
                                    from clst in ccs.DefaultIfEmpty()
                                    where main.deleteFlag == false && main.c_date == filterDate && main.c_Type != ConferenceStructureType.Room
                                    select new
                                    {
                                        Id = main.c_ID,
                                        Topic = main.c_title,
                                        SessionTitle = main.c_sessionTitle,
                                        Description = main.c_brief,
                                        Category = clst.cc_categoryname,
                                        ModifiedDate = main.UpdatedDate,
                                        StartDate = main.c_date,
                                        StartTime = main.c_stime,
                                        EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers
                                                      where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (from account in RepositoryContext.Accounts
                                                          join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                          from ctl in cts.DefaultIfEmpty()
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID && account.a_type == UserType.Speaker &&
(conuser.AccountType == ConferenceSpeakerType.GuestOfHonor || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
conuser.AccountType == ConferenceSpeakerType.SessionModerator || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                              conuser.SpeakerSeqNo
                                                          }).Select(x => x).ToList(),

                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = main.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = main.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator) select cu).Count() > 0 ? true : false,
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 &&
(cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor ||
cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where main.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        main.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.JoinStatus == 1 && cu.Status == 1 && cu.UserId == userId select main.Video).FirstOrDefault(),
                                        Type = main.Type,
                                        favouriteStatus = (from f in RepositoryContext.UsersFavourites where f.Status == 1 && f.ItemType == FavouriteItem.Conference && f.UserId == userId && f.FavItemId == main.c_ID select f.FavItemId).Count() > 0 ? true : false,
                                        SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                          join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                          where cs.deleteFlag == false && sp.deleteFlag == false && cs.csponsor_conferenceID == main.c_ID
                                                          select new
                                                          {
                                                              csponsor_ID = cs.csponsor_sponsorID,
                                                              cs.csponsor_title,
                                                              sp.s_logo,
                                                              sp.templateId,
                                                              sp.s_url
                                                          }).Select(x => x).ToList()
                                    };
                    objResponse.conferenceList = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle);
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }

        #region PaginationSessionForConferencelist [23-7-2020]
        private dynamic PaginationSessionForConferencelist(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(string),
                topic = default(string),
                description = default(string),
                modifiedDate = default(string),
                startDate = default(string),
                startTime = default(string),
                endTime = default(string),
                joinStatus = default(bool),
                speakerDetails = default(List<dynamic>),
                defaultPhoto = default(string),
                chatChannelStatus = default(string),
                joinChatChannelStatus = default(bool?),
                joinSpeakerChatChannelStatus = default(bool),
                speakerChatChannelStatus = default(string),
                speakerjoinChatChannelStatus = default(bool),
                materialAccess = default(bool),
                mainSpeakerDetail = default(List<dynamic>),
                accessRightAction = default(string),
                currentConferenceStatus = default(string),
                video = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }
        #endregion

        public dynamic GetConferenceDatelist()
        {
            dynamic main = (from c in RepositoryContext.Conferences
                            where c.deleteFlag == false
                            select new
                            {
                                c.c_date
                            }).Distinct().ToList();
            return main;
        }

        public dynamic GetConferencesBySpeakerId(string aId, string returnUrl, string confDate)
        {
            var main = (from i in RepositoryContext.Accounts
                        join cu in RepositoryContext.ConferenceUsers on i.a_ID equals cu.UserId
                        join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                        where c.c_date == confDate && i.deleteFlag == false && cu.Status == 1 && i.a_ID == aId && c.deleteFlag == false //&& i.a_type == "S"
                        select new
                        {
                            Id = c.c_ID,
                            Topic = c.c_title,
                            SessionTitle = c.c_sessionTitle,
                            Description = c.c_brief,
                            ModifiedDate = c.UpdatedDate,
                            StartDate = c.c_date,
                            StartTime = c.c_stime,
                            EndTime = c.c_etime,
                            JoinStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == c.c_ID && cu.UserId == aId && cu.Status == 1 select cu).Count() > 0 ? true : false,
                            Speaker = i.a_fullname,
                            SpeakerPhoto = (i.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + i.ProfilePic)),
                            defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                            chatChannelStatus = c.chatChannelStatus,
                            joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers
                                                     where cu.WebinarId == c.c_ID && cu.UserId == aId && cu.Status == 1 && cu.ChatChannelJoinStatus == "Chat"
                                                     select cu).Count() > 0 ? true : false,
                            mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                 join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                 where c.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                 select new
                                                 {
                                                     mainSpeakerName = a.a_fullname,
                                                     mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),
                                                 }).FirstOrDefault()
                        }).ToList();
            main.OrderByDescending(x => x.StartDate).ThenByDescending(x => x.StartTime);
            return main;
        }

        public dynamic GetMyConferenceDateList(string userId)
        {
            dynamic main = (from a in RepositoryContext.Accounts
                            join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                            join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
                            where a.deleteFlag == false && cu.Status == 1 && c.deleteFlag == false && a.a_ID == userId && cu.JoinStatus == 1
                            select new
                            {
                                d_date = c.c_date
                            }).Distinct().ToList();
            return main;
        }

        public string GetMainSpeakerImage(string cId)
        {
            string image = (from c in RepositoryContext.Conferences join a in RepositoryContext.Accounts on c.MainSpeakerId equals a.a_ID where c.deleteFlag == false && a.deleteFlag == false && c.c_ID == cId select a.ProfilePic).FirstOrDefault();
            return image;
        }

        public dynamic GetConferencelistForOrganization(dynamic obj)
        {
            string conferenceDate = obj.conferenceDate;
            string conferenceCate = obj.conferenceCategory;
            string conferenceTitle = obj.conferenceTitle;
            string speaker = obj.speaker;
            //string conferenceType = obj.conferenceType;
            int configId = obj.configId;

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).AsNoTracking().Distinct().ToList();

            if (string.IsNullOrEmpty(conferenceDate))
            {
                conferenceDate = dayList[0].day;
            }

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            var mainQuery = from main in RepositoryContext.Conferences
                            join p in (from a in RepositoryContext.Accounts
                                       join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                                       where cu.Status == 1 && a.deleteFlag == false
                                       && a.a_type == UserType.Speaker && (cu.AccountType == ConferenceSpeakerType.SessionModerator
                                       || cu.AccountType == ConferenceSpeakerType.GuestOfHonor
                                       || cu.AccountType == ConferenceSpeakerType.Speaker
                                       || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker)
                                       select new { cu.WebinarId, a.a_ID }) on main.c_ID equals p.WebinarId into plist
                            from pp in plist.DefaultIfEmpty()

                            where main.deleteFlag == false
                            && (configId == 0 || main.ConfigId == configId)
                            && (conferenceTitle == null || conferenceTitle == "" || main.c_title.Contains(conferenceTitle))
                            && (conferenceCate == null || conferenceCate == "" || conferenceCate == main.c_cc)
                            && (speaker == null || speaker == "" || pp.a_ID == speaker)
                            where main.deleteFlag == false && main.c_date == conferenceDate
                            select new
                            {
                                main.c_ID,
                                main.c_cc,
                                main.c_date,
                                main.c_stime,
                                main.c_etime,
                                EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                main.c_title,
                                main.c_sessionTitle,
                                main.c_venue,
                                main.c_brief,
                                main.chatChannelStatus,
                                main.MainSpeakerId,
                                main.CurrentConferenceStatus,
                                main.Video,
                                main.AutoNotiSend,
                                main.SpeakerChatChannelStatus,
                                main.SponsorLogo,
                                main.InfoPic,
                                roomType = main.c_Type == ConferenceStructureType.Room ? ConferenceRoomType.MainRoom :
                                (main.c_Type != ConferenceStructureType.Room && main.c_Type != null &&
                                main.RoomId != null && main.RoomId != "") ? ConferenceRoomType.ShareRoom : ConferenceRoomType.SeparateRoom,
                                configName = (from zc in RepositoryContext.ZoomConfigurations where zc.ConfigId == main.ConfigId select zc.ConfigName).FirstOrDefault(),
                                mainRoomName = (from mr in RepositoryContext.Conferences where mr.deleteFlag == false && mr.c_ID == main.RoomId select mr.c_title).FirstOrDefault()
                            };

            objResponse.conferenceList = mainQuery.Distinct().ToList().OrderBy(x => x.c_stime).ThenBy(x => x.EndTime).ThenBy(x => x.c_sessionTitle);
            response.Add(objResponse);
            return response;
        }

        public dynamic GetconferenceListWithGroupForOrganization(dynamic currentLiveResult, bool IsLiveOnly)
        {
            List<dynamic> response = new List<dynamic>();
            List<LiveWebinar> liveWebinarList = null;
            var confDateList = (from c in RepositoryContext.Conferences where c.deleteFlag == false select c.c_date).Distinct().ToArray();
            if (IsLiveOnly && currentLiveResult != null)
            {
                var currentTempLiveResult = JsonConvert.DeserializeObject<LiveWebinarList>(currentLiveResult.ToString());
                liveWebinarList = currentTempLiveResult.webinars;
            }
            //isLive = liveWebinarList.Exists(x => x.id == ZoomId);
            foreach (var date in confDateList)
            {
                dynamic objResponse = new System.Dynamic.ExpandoObject();
                objResponse.date = date;
                List<tbl_Conference> conferences = (from conf in RepositoryContext.Conferences where conf.c_date == date && conf.deleteFlag == false orderby conf.c_stime, conf.c_sessionTitle select conf).ToList();
                List<dynamic> confList = new List<dynamic>();
                for (int i = 0; i < conferences.Count; i++)
                {
                    // List<string> confObj = new List<string> ();
                    //Dictionary<string, string> confObj = new Dictionary<string, string> ();
                    // confObj.Add ("value:" + "'" + conferences[i].c_ID + "'," + "viewValue:" + "'" + conferences[i].c_title + "'");
                    //confObj.Add ("value:" + "'" + conferences[i].c_ID + "'", "viewValue:" + "'" + conferences[i].c_title + "'");
                    //confList.Add (confObj);
                    if (IsLiveOnly && liveWebinarList.Exists(x => x.id == conferences[i].ZoomId))
                    {
                        GroupDropdown newGroupDropdown = new GroupDropdown();
                        newGroupDropdown.value = conferences[i].c_ID;
                        newGroupDropdown.viewValue = conferences[i].c_title;
                        confList.Add(newGroupDropdown);
                    }
                    else if (!IsLiveOnly)
                    {
                        GroupDropdown newGroupDropdown = new GroupDropdown();
                        newGroupDropdown.value = conferences[i].c_ID;
                        newGroupDropdown.viewValue = conferences[i].c_title;
                        confList.Add(newGroupDropdown);
                    }
                }
                objResponse.conferenceList = confList;
                response.Add(objResponse);
            }

            return response;
        }

        public string GetSponsorLogoByConferenceID(string confID)
        {
            string result = (from c in RepositoryContext.Conferences where c.deleteFlag == false && c.c_ID == confID select c.SponsorLogo).FirstOrDefault();
            return result;
        }

        public dynamic GetMyFavouriteConferences(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            var confeObj = (from c in RepositoryContext.Conferences
                            join f in RepositoryContext.UsersFavourites on c.c_ID equals f.FavItemId
                            where c.deleteFlag == false && f.Status == 1 && f.UserId == userId && f.ItemType == FavouriteItem.Conference && c.c_Type != ConferenceStructureType.Room

                            select new { c.c_date, c.c_ID, f.Status }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days join c in confeObj on d.d_date equals c.c_date where d.deleteFlag == false orderby d.d_date ascending select d.d_date).Distinct().ToArray();

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(conferenceDate) && dayCount == 1)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy == conferenceDate)
                {
                    filterDate = dy;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }

                if (doQuery)
                {
                    var mainQuery = from main in RepositoryContext.Conferences
                                    join cobj in confeObj on main.c_ID equals cobj.c_ID
                                    join cat in RepositoryContext.ConferenceCategories on main.c_cc equals cat.cc_ID
                                    where main.deleteFlag == false && main.c_date == filterDate && main.c_Type != ConferenceStructureType.Room
                                    select new
                                    {
                                        Id = main.c_ID,
                                        Topic = main.c_title,
                                        SessionTitle = main.c_sessionTitle,
                                        Description = main.c_brief,
                                        Category = cat.cc_categoryname,
                                        ModifiedDate = main.UpdatedDate,
                                        StartDate = main.c_date,
                                        StartTime = main.c_stime,
                                        EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers
                                                          //join acr in RepositoryContext.AccessRights on cu.AccessRightId equals acr.AccessId
                                                      where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (from account in RepositoryContext.Accounts
                                                          join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                          from ctl in cts.DefaultIfEmpty()
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID && account.a_type == UserType.Speaker &&
(conuser.AccountType == ConferenceSpeakerType.GuestOfHonor || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
conuser.AccountType == ConferenceSpeakerType.SessionModerator || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                          }).Select(x => x).ToList(),

                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = main.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = main.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator) select cu).Count() > 0 ? true : false,
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 &&
(cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
cu.AccountType == ConferenceSpeakerType.SessionModerator || cu.AccountType == ConferenceSpeakerType.Speaker)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where main.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        main.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.JoinStatus == 1 && cu.Status == 1 && cu.UserId == userId select main.Video).FirstOrDefault(),
                                        Type = main.Type,
                                        favouriteStatus = cobj.Status
                                    };
                    objResponse.conferenceList = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime);
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }

        public dynamic GetConferenceSpeakersByConferenceId(string Id, string speakerType)
        {
            var main = (from c in RepositoryContext.Accounts
                        join cu in RepositoryContext.ConferenceUsers on c.a_ID equals cu.UserId
                        where c.deleteFlag == false && c.a_type == UserType.Speaker
                        && cu.Status == 1 && cu.WebinarId == Id
                        && cu.AccountType == speakerType

                        select new
                        {
                            cu.Id,
                            c.a_fullname,
                            cu.SpeakerSeqNo
                        }).Distinct().ToList().OrderBy(x => x.SpeakerSeqNo);
            return main;
        }
        public dynamic GetLiveConferences(string userId)
        {
            bool isOrganizer = CheckIsOrganizer(userId);
            dynamic confList = null;
            if (isOrganizer)
            {
                confList = (from main in RepositoryContext.Conferences 
                where main.deleteFlag == false &&
                (main.CurrentConferenceStatus == CurrentConferenceStatus.SoftOpening 
                || main.CurrentConferenceStatus == CurrentConferenceStatus.GrandOpening) 
                select main.c_ID).AsNoTracking().Distinct().ToArray();
            }
            else
            {
                confList = (from main in RepositoryContext.Conferences
                            join user in RepositoryContext.ConferenceUsers on main.c_ID equals user.WebinarId
                            join acc in RepositoryContext.AccessRights on user.AccessRightId equals acc.AccessId

                            where main.deleteFlag == false &&
                            (main.CurrentConferenceStatus == CurrentConferenceStatus.GrandOpening ||
                            (acc.AccessRightActions.Contains(CurrentConferenceStatus.SoftOpening) 
                            && main.CurrentConferenceStatus == CurrentConferenceStatus.SoftOpening 
                            && user.UserId == userId && user.Status == 1))
                            select main.c_ID).AsNoTracking().Distinct().ToArray();

                // where main.CurrentConferenceStatus == CurrentConferenceStatus.GrandOpening
                // && user.Status == 1 && main.deleteFlag == false && user.UserId == userId
                // || (userId == null || userId == "" || (userId == user.UserId &&
                // acc.AccessRightActions.Contains(CurrentConferenceStatus.SoftOpening)))
            }
            return confList;
        }

        private bool CheckIsOrganizer(string userId)
        {
            bool result = false;
            dynamic isOrganizer = (from a in RepositoryContext.Administrators where (a.admin_id.ToString()) == userId select a).FirstOrDefault();
            if (isOrganizer != null)
            {
                result = true;
            }
            return result;
        }

        public dynamic GetAllWebinarListGroupByTime(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            string conferenceCate = obj.conferenceCategory;
            string conferenceTitle = obj.conferenceTitle;
            string speaker = obj.speaker;
            string conferenceType = obj.conferenceType;

            var confeObj = (from c in RepositoryContext.Conferences

                            join p in (from a in RepositoryContext.Accounts
                                       join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId
                                       where cu.Status == 1 && a.deleteFlag == false && a.a_type == UserType.Speaker
                                       && (cu.AccountType == ConferenceSpeakerType.GuestOfHonor
                                       || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                       || cu.AccountType == ConferenceSpeakerType.SessionModerator
                                       || cu.AccountType == ConferenceSpeakerType.Speaker)
                                       select new { cu.WebinarId, a.a_ID }) on c.c_ID equals p.WebinarId into plist
                            from pp in plist.DefaultIfEmpty()

                            where c.deleteFlag == false && c.c_Type != ConferenceStructureType.Room
                            && (conferenceTitle == null || conferenceTitle == "" || c.c_title.Contains(conferenceTitle))
                            && (conferenceCate == null || conferenceCate == "" || conferenceCate == c.c_cc)
                            && (conferenceType == null || conferenceType == "" || conferenceType == c.c_Type)
                            && (speaker == null || speaker == "" || pp.a_ID == speaker)

                            select new { c.c_date, c.c_ID }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           join c in confeObj on d.d_date
                           equals c.c_date
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).Distinct().ToArray();

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(conferenceDate) && dayCount == 1)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy.day == conferenceDate)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }

                if (doQuery)
                {
                    var mainQuery = from main in RepositoryContext.Conferences
                                    join cobj in confeObj on main.c_ID equals cobj.c_ID
                                    join cat in (from cc in RepositoryContext.ConferenceCategories where cc.deleteFlag == false select new { cc.cc_ID, cc.cc_categoryname })
                                    on main.c_cc equals cat.cc_ID into ccs
                                    from clst in ccs.DefaultIfEmpty()

                                    where main.deleteFlag == false && main.c_date == filterDate
                                    && main.c_Type != ConferenceStructureType.Room
                                    select new
                                    {
                                        Id = main.c_ID,
                                        Topic = main.c_title,
                                        SessionTitle = main.c_sessionTitle,
                                        Description = main.c_brief,
                                        Category = clst.cc_categoryname,
                                        ModifiedDate = main.UpdatedDate,
                                        StartDate = main.c_date,
                                        StartTime = main.c_stime,
                                        EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers

                                                      where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (
                                        from account in RepositoryContext.Accounts
                                        join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                        from ctl in cts.DefaultIfEmpty()
                                        join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                        orderby conuser.SpeakerSeqNo
                                        where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
                                        account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.Speaker
                                        select new
                                        {
                                            speakerId = account.a_ID,
                                            Speaker = account.a_fullname,
                                            desigination = account.a_designation,
                                            Salutation = account.a_sal,
                                            company = account.a_company,
                                            country = ctl.CountryName,
                                            isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                            SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                        }).Select(x => x).ToList(),
                                        GuestOfHonorDetails = (from account in RepositoryContext.Accounts
                                                               join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                               from ctl in cts.DefaultIfEmpty()
                                                               join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                               orderby conuser.SpeakerSeqNo
                                                               where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
                                                               account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                               select new
                                                               {
                                                                   speakerId = account.a_ID,
                                                                   Speaker = account.a_fullname,
                                                                   desigination = account.a_designation,
                                                                   Salutation = account.a_sal,
                                                                   company = account.a_company,
                                                                   country = ctl.CountryName,
                                                                   isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                                   SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                               }).Select(x => x).ToList(),
                                        KeyNoteSpeaker = (from account in RepositoryContext.Accounts
                                                          join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                          from ctl in cts.DefaultIfEmpty()
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
                                                          account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker

                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))
                                                          }).Select(x => x).ToList(),
                                        sessionModerator = (from account in RepositoryContext.Accounts
                                                            join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                            from ctl in cts.DefaultIfEmpty()
                                                            join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                            orderby conuser.SpeakerSeqNo
                                                            where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID && account.a_type == "S" && conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                            select new
                                                            {
                                                                speakerId = account.a_ID,
                                                                Speaker = account.a_fullname,
                                                                desigination = account.a_designation,
                                                                Salutation = account.a_sal,
                                                                company = account.a_company,
                                                                country = ctl.CountryName,
                                                                isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                                SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic))

                                                            }).Select(x => x).ToList(),
                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = main.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = main.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator) select cu).Count() > 0 ? true : false, //cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat &&
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where main.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        main.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers
                                                 where cu.WebinarId == main.c_ID && cu.Status == 1 && cu.UserId == userId
                                                 select main.Video).FirstOrDefault(),
                                        Type = main.Type,
                                        favouriteStatus = (from f in RepositoryContext.UsersFavourites where f.Status == 1 && f.ItemType == FavouriteItem.Conference && f.UserId == userId && f.FavItemId == main.c_ID select f.FavItemId).Count() > 0 ? true : false,
                                        SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                          join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                          where cs.deleteFlag == false && sp.deleteFlag == false && cs.csponsor_conferenceID == main.c_ID
                                                          select new
                                                          {
                                                              csponsor_ID = cs.csponsor_sponsorID,
                                                              cs.csponsor_title,
                                                              sp.s_logo,
                                                              sp.templateId,
                                                              sp.s_url
                                                          }).Select(x => x).ToList()
                                    };
                    var result = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle)
                        .GroupBy(x => new { x.StartTime, x.EndTime })
                        .Select(b => new
                        {
                            value = b.ToList()
                        });
                    objResponse.conferenceList = result;
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }

        public dynamic GetMyFavouriteConferencesGroupByTime(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            var confeObj = (from c in RepositoryContext.Conferences
                            join f in RepositoryContext.UsersFavourites on c.c_ID equals f.FavItemId
                            where c.deleteFlag == false && f.Status == 1 && f.UserId == userId && f.ItemType == FavouriteItem.Conference && c.c_Type != ConferenceStructureType.Room

                            select new { c.c_date, c.c_ID, f.Status }).Distinct();

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           join c in confeObj on d.d_date equals c.c_date
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).Distinct().ToArray();

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            int dayCount = 1;
            foreach (var dy in dayList)
            {
                string filterDate = "";
                bool isbreak = false;
                bool doQuery = false;
                if (string.IsNullOrEmpty(conferenceDate) && dayCount == 1)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else if (dy.day == conferenceDate)
                {
                    filterDate = dy.day;
                    isbreak = true;
                    doQuery = true;
                }
                else
                {
                    filterDate = "";
                }

                if (doQuery)
                {
                    var mainQuery = from main in RepositoryContext.Conferences
                                    join cobj in confeObj on main.c_ID equals cobj.c_ID
                                    join cat in (from cc in RepositoryContext.ConferenceCategories where cc.deleteFlag == false select new { cc.cc_ID, cc.cc_categoryname })
                                    on main.c_cc equals cat.cc_ID into ccs
                                    from clst in ccs.DefaultIfEmpty()

                                    where main.deleteFlag == false && main.c_date == filterDate && main.c_Type != ConferenceStructureType.Room
                                    select new
                                    {
                                        Id = main.c_ID,
                                        Topic = main.c_title,
                                        SessionTitle = main.c_sessionTitle,
                                        Description = main.c_brief,
                                        Category = clst.cc_categoryname,
                                        ModifiedDate = main.UpdatedDate,
                                        StartDate = main.c_date,
                                        StartTime = main.c_stime,
                                        EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                        JoinStatus = (from cu in RepositoryContext.ConferenceUsers
                                                      where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 //&& acr.AccessRightActions.Split(',').Contains(main.CurrentConferenceStatus) 
                                                      select cu).Count() > 0 ? true : false,
                                        SpeakerDetails = (from account in RepositoryContext.Accounts
                                                          join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                          from ctl in cts.DefaultIfEmpty()
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.Speaker
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                          }).Select(x => x).ToList(),
                                        GuestOfHonorDetails = (from account in RepositoryContext.Accounts
                                                               join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                               from ctl in cts.DefaultIfEmpty()
                                                               join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                               orderby conuser.SpeakerSeqNo
                                                               where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                               select new
                                                               {
                                                                   speakerId = account.a_ID,
                                                                   Speaker = account.a_fullname,
                                                                   desigination = account.a_designation,
                                                                   Salutation = account.a_sal,
                                                                   company = account.a_company,
                                                                   country = ctl.CountryName,
                                                                   isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                                   SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                               }).Select(x => x).ToList(),
                                        KeyNoteSpeaker = (from account in RepositoryContext.Accounts
                                                          join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                          from ctl in cts.DefaultIfEmpty()
                                                          join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                          orderby conuser.SpeakerSeqNo
                                                          where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID &&
account.a_type == UserType.Speaker && conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                          select new
                                                          {
                                                              speakerId = account.a_ID,
                                                              Speaker = account.a_fullname,
                                                              desigination = account.a_designation,
                                                              Salutation = account.a_sal,
                                                              company = account.a_company,
                                                              country = ctl.CountryName,
                                                              isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                              SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                          }).Select(x => x).ToList(),
                                        sessionModerator = (from account in RepositoryContext.Accounts
                                                            join ct in RepositoryContext.Countries on account.a_country equals ct.ISO3Digit into cts
                                                            from ctl in cts.DefaultIfEmpty()
                                                            join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                            orderby conuser.SpeakerSeqNo
                                                            where account.deleteFlag != true && conuser.Status == 1 && conuser.WebinarId == main.c_ID && account.a_type == UserType.Speaker &&
conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                            select new
                                                            {
                                                                speakerId = account.a_ID,
                                                                Speaker = account.a_fullname,
                                                                desigination = account.a_designation,
                                                                Salutation = account.a_sal,
                                                                company = account.a_company,
                                                                country = ctl.CountryName,
                                                                isMainSpeaker = main.MainSpeakerId == account.a_ID ? true : false,
                                                                SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                            }).Select(x => x).ToList(),

                                        defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                        chatChannelStatus = main.chatChannelStatus,
                                        joinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.ChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        joinSpeakerChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat select cu).Count() > 0 ? true : false,
                                        speakerChatChannelStatus = main.SpeakerChatChannelStatus,
                                        SpeakerjoinChatChannelStatus = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 && cu.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat && (cu.AccountType == ConferenceSpeakerType.Speaker || cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker || cu.AccountType == ConferenceSpeakerType.SessionModerator) select cu).Count() > 0 ? true : false,
                                        materialAccess = (from cu in RepositoryContext.ConferenceUsers
                                                          where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 &&
(cu.AccountType == ConferenceSpeakerType.GuestOfHonor || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker ||
cu.AccountType == ConferenceSpeakerType.SessionModerator || cu.AccountType == ConferenceSpeakerType.Speaker)
                                                          select cu).Count() > 0 ? true : false,
                                        mainSpeakerDetail = (from cu in RepositoryContext.ConferenceUsers
                                                             join a in RepositoryContext.Accounts on cu.UserId equals a.a_ID
                                                             where main.MainSpeakerId == a.a_ID && a.deleteFlag == false && cu.Status == 1
                                                             select new
                                                             {
                                                                 mainSpeakerName = a.a_fullname,
                                                                 mainSpeakerPhoto = (a.ProfilePic == null ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + a.ProfilePic)),

                                                             }).FirstOrDefault(),
                                        accessRightAction = (from cu in RepositoryContext.ConferenceUsers join ac in RepositoryContext.AccessRights on cu.AccessRightId equals ac.AccessId where cu.WebinarId == main.c_ID && cu.UserId == userId && cu.Status == 1 select ac.AccessRightActions).FirstOrDefault(),
                                        main.CurrentConferenceStatus,
                                        video = (from cu in RepositoryContext.ConferenceUsers where cu.WebinarId == main.c_ID && cu.Status == 1 && cu.UserId == userId select main.Video).FirstOrDefault(),
                                        Type = main.Type,
                                        favouriteStatus = cobj.Status,
                                        SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                          join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                          where cs.deleteFlag == false && sp.deleteFlag == false && cs.csponsor_conferenceID == main.c_ID
                                                          select new
                                                          {
                                                              csponsor_ID = cs.csponsor_sponsorID,
                                                              cs.csponsor_title,
                                                              sp.s_logo,
                                                              sp.templateId,
                                                              sp.s_url
                                                          }).Select(x => x).ToList()
                                    };
                    var result = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle)
                        .GroupBy(x => new { x.StartTime, x.EndTime })
                        .Select(b => new
                        {
                            value = b.ToList()
                        });
                    objResponse.conferenceList = result;
                    response.Add(objResponse);
                    if (isbreak) { break; }
                }
                dayCount++;
            }
            return response;
        }
        public dynamic GetExhibitorShowCase(string exhId)
        {
            var main = (from s in RepositoryContext.Sponsors
                        join cs in RepositoryContext.ConferenceSponsors on s.s_ID equals cs.csponsor_sponsorID
                        join c in RepositoryContext.Conferences on cs.csponsor_conferenceID equals c.c_ID
                        where s.deleteFlag == false && s.s_exhID == exhId && c.deleteFlag == false
                        && cs.deleteFlag == false
                        select new
                        {
                            s.s_exhID,
                            c.c_date,
                            startTime = c.c_stime,
                            endTime = DateTime.ParseExact(c.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(c.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                            track = c.c_sessionTitle,
                            topic = c.c_title
                        }).Distinct().ToList().OrderBy(x => x.c_date).ThenBy(x => x.startTime);
            return main;
        }

        public dynamic GetConferenceSpeakerType()
        {
            List<dynamic> result = new List<dynamic>();
            Type type = typeof(ConferenceSpeakerType);
            foreach (var val in type.GetFields())
            {
                Dictionary<string, string> lst = new Dictionary<string, string>();
                var s = val.GetValue(null);
                lst.Add("name", val.Name);
                lst.Add("value", s.ToString());
                result.Add(lst);
            }
            return result;
        }

        public int GetRegistrantCount(string webinerId)
        {
            int main = (from a in RepositoryContext.Accounts join cu in RepositoryContext.ConferenceUsers on a.a_ID equals cu.UserId where a.deleteFlag == false && cu.WebinarId == webinerId && cu.Status == 1 select cu.UserId).Distinct().Count();
            return main;
        }

        public void UpdateConferenceDatasynStatus(tbl_Conference conference)
        {
            conference.IsDataSyn = false;
            RepositoryContext.Conferences.Update(conference);
            RepositoryContext.SaveChanges();
        }
        public string GetQuestionnaireIDByConferenceId(string cid)
        {
            string qID = (from c in RepositoryContext.Conferences
                          where c.deleteFlag == false && c.c_ID == cid
                          select c.c_qnaireId).FirstOrDefault();
            return qID;
        }

    
        public dynamic GetAllWebinarListGroupTemplate(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            string conferenceCate = obj.conferenceCategory;
            string conferenceTitle = obj.conferenceTitle;
            string speaker = obj.speaker;
            string conferenceType = obj.conferenceType;

            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).AsNoTracking().Distinct().ToList();

            if (string.IsNullOrEmpty(conferenceDate))
            {
                conferenceDate = dayList[0].day;
            }
            
            var mainQuery = from main in RepositoryContext.Conferences
                            where main.deleteFlag == false && main.c_date == conferenceDate
                            && main.c_Type != ConferenceStructureType.Room
                            select new
                            {
                                Id = main.c_ID,
                                Topic = main.c_title,
                                SessionTitle = main.c_sessionTitle,
                                Description = main.c_brief,
                                ModifiedDate = main.UpdatedDate,
                                StartDate = main.c_date,
                                StartTime = main.c_stime,
                                EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                main.CurrentConferenceStatus, 
                                Type = main.Type,
                                main.Video,
                                //speaker details
                                 SpeakerDetails = (from account in RepositoryContext.Accounts
                                                  join ct in (from c in RepositoryContext.Countries
                                                              where c.deleteFlag == false
                                                              select new { c.ISO3Digit, c.CountryName }) on account.a_country equals ct.ISO3Digit
                                                  into cl from clist in cl.DefaultIfEmpty()
                                                  join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                  orderby conuser.SpeakerSeqNo
                                                  where account.deleteFlag == false && conuser.Status == 1
                                                  && conuser.WebinarId == main.c_ID
                                                  && (conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                  || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                  || conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                  || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                                  select new
                                                  {
                                                      speakerId = account.a_ID,
                                                      Speaker = account.a_fullname,
                                                      desigination = account.a_designation,
                                                      Salutation = (account.a_sal == "" || account.a_sal == null || account.a_sal == "." || account.a_sal.ToString() == "0") ? "" : account.a_sal,
                                                      company = account.a_company,
                                                      country = clist.CountryName,
                                                      SpeakerType = conuser.AccountType,
                                                      SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                  }).AsNoTracking().Select(x => x).ToList(),
                            };
            
            var SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                  join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                  join c in RepositoryContext.Conferences on cs.csponsor_conferenceID equals c.c_ID
                                                  where cs.deleteFlag == false && sp.deleteFlag == false 
                                                  && c.deleteFlag == false && c.c_date == conferenceDate
                                                  && c.c_Type != ConferenceStructureType.Room
                                                  
                                                  select new
                                                  {
                                                      c.c_ID,
                                                      cs.csponsor_title,
                                                      data = new
                                                      {
                                                          csponsor_ID = cs.csponsor_sponsorID,
                                                          sp.s_logo,
                                                          sp.templateId,
                                                          sp.s_url
                                                      }
                                                      
                                                  }).ToList()
                                                  .GroupBy(x => new { x.c_ID, x.csponsor_title }, (key, g) => 
                                                   new { csponsor_title = key.csponsor_title , cId = key.c_ID , sponsorlist = g.Select( x => x.data )} )
                                                   .ToLookup(x => x.cId).ToDictionary(t => t.Key, t => t);

            var result = mainQuery.Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime)
                .GroupBy(x => new { x.StartTime, x.EndTime })
                .Select(b => new
                {
                    value = b.ToList()
                });

            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            objResponse.conferenceList = result;
            objResponse.sponsorList = SponsorDetails;
            response.Add(objResponse);
            return response;
        }

        public dynamic GetActiveConferenceList(dynamic obj, string currentUserId)
        {

            string con_groupChat = "ConferenceGroupChat";
            string con_speakerChat = "ConferenceSpeakerGroupChat";
            string con_Favourite = "ConferenceFavourite";
            string con_Video = "ConferenceVideo";
            string con_speakerAccess = "ConferenceSpeakerAccess";

            string param = obj.type;
            if (string.IsNullOrEmpty(param))
            {
                param = null;
            }
            else { param = param.ToLower(); }
            dynamic groupChat = null;
            dynamic speakerChat = null;
            dynamic videoObj = null;
            dynamic favObj = null;
            dynamic speakerAccessObj = null;

            if (param == null || param == con_groupChat.ToLower())
            {
                groupChat = (from c in RepositoryContext.Conferences
                             join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId
                             where c.deleteFlag == false && cu.UserId == currentUserId
                             && c.c_Type != ConferenceStructureType.Room
                             && cu.Status == 1
                             && c.chatChannelStatus == ChatJoinStatus.open
                             select new { c.c_ID }).AsNoTracking().Distinct().ToList().Select(x => x.c_ID);
            }
            if (param == null || param == con_speakerChat.ToLower())
            {
                speakerChat = (from c in RepositoryContext.Conferences
                               join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId
                               where c.deleteFlag == false && c.c_Type != ConferenceStructureType.Room
                               && cu.Status == 1 && cu.UserId == currentUserId
                               && c.SpeakerChatChannelStatus == ChatJoinStatus.open
                               select new { c.c_ID }).AsNoTracking().Distinct().ToList().Select(x => x.c_ID);
            }
            if (param == null || param == con_Video.ToLower())
            {
                videoObj = (from c in RepositoryContext.Conferences
                            join cu in RepositoryContext.ConferenceUsers on c.c_ID equals cu.WebinarId
                            where c.deleteFlag == false && c.c_Type != ConferenceStructureType.Room
                            && c.Video != null && cu.Status == 1 && cu.UserId == currentUserId
                            select new { c.c_ID }).AsNoTracking().Distinct().ToList().Select(x => x.c_ID);
            }
            if (param == null || param == con_Favourite.ToLower())
            {
                favObj = (from c in RepositoryContext.Conferences
                          join f in RepositoryContext.UsersFavourites on c.c_ID equals f.FavItemId
                          where c.deleteFlag == false && c.c_Type != ConferenceStructureType.Room
                          && f.UserId == currentUserId && f.Status == 1
                          && f.ItemType == FavouriteItem.Conference
                          select new { c.c_ID }).AsNoTracking().Distinct().ToList().Select(x => x.c_ID);
            }
            if(param == null || param == con_speakerAccess.ToLower()){
                speakerAccessObj = (from cu in RepositoryContext.ConferenceUsers
                where cu.Status == 1 && cu.UserId == currentUserId && cu.AccessRightId != null
                && (cu.AccountType == ConferenceSpeakerType.GuestOfHonor 
                || cu.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                || cu.AccountType == ConferenceSpeakerType.SessionModerator
                || cu.AccountType == ConferenceSpeakerType.Speaker)
                select new { cu.WebinarId }).AsNoTracking().Distinct().ToList().Select(x => x.WebinarId);
            }
            var result = new { GroupChat = groupChat, SpeakerChat = speakerChat, VideoData = videoObj, FavData = favObj, conferenceSpeakerAccess = speakerAccessObj };
            return result;
        }

        public dynamic GetMyFavouriteConferencesGroupTemplate(string userId, dynamic obj, string returnUrl)
        {
            string conferenceDate = obj.conferenceDate;
            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).AsNoTracking().Distinct().ToList();
            if (string.IsNullOrEmpty(conferenceDate))
            {
                conferenceDate = dayList[0].day;
            }

            var mainQuery = from main in RepositoryContext.Conferences
                            join f in RepositoryContext.UsersFavourites on main.c_ID equals f.FavItemId
                            where main.deleteFlag == false
                            && main.c_date == conferenceDate
                            && main.c_Type != ConferenceStructureType.Room && f.Status == 1
                            && f.UserId == userId && f.ItemType == FavouriteItem.Conference
                            select new
                            {
                                Id = main.c_ID,
                                Topic = main.c_title,
                                SessionTitle = main.c_sessionTitle,
                                Description = main.c_brief,
                                ModifiedDate = main.UpdatedDate,
                                StartDate = main.c_date,
                                StartTime = main.c_stime,
                                EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                defaultPhoto = returnUrl + "defaultSpeaker.jpg",
                                main.CurrentConferenceStatus,
                                Type = main.Type,
                                favouriteStatus = f.Status,
                                main.Video,
                                SpeakerDetails = (from account in RepositoryContext.Accounts
                                                  join ct in (from c in RepositoryContext.Countries
                                                              where c.deleteFlag == false
                                                              select new { c.ISO3Digit, c.CountryName }) on account.a_country equals ct.ISO3Digit into cl
                                                  from clist in cl.DefaultIfEmpty()
                                                  join conuser in RepositoryContext.ConferenceUsers on account.a_ID equals conuser.UserId
                                                  orderby conuser.SpeakerSeqNo
                                                  where account.deleteFlag == false && conuser.Status == 1
                                                  && conuser.WebinarId == main.c_ID
                                                  && (conuser.AccountType == ConferenceSpeakerType.GuestOfHonor
                                                  || conuser.AccountType == ConferenceSpeakerType.KeynoteSpeaker
                                                  || conuser.AccountType == ConferenceSpeakerType.SessionModerator
                                                  || conuser.AccountType == ConferenceSpeakerType.Speaker)
                                                  select new
                                                  {
                                                      speakerId = account.a_ID,
                                                      Speaker = account.a_fullname,
                                                      desigination = account.a_designation,
                                                      Salutation = (account.a_sal == "" || account.a_sal == null || account.a_sal == "." || account.a_sal.ToString() == "0") ? "" : account.a_sal,
                                                      company = account.a_company,
                                                      country = clist.CountryName,
                                                      SpeakerType = conuser.AccountType,
                                                      SpeakerPhoto = (account.ProfilePic == null || account.ProfilePic == "" ? (returnUrl + "defaultSpeaker.jpg") : (returnUrl + account.ProfilePic)),
                                                  }).AsNoTracking().Select(x => x).ToList()
                               
                            };

            var SponsorDetails = (from cs in RepositoryContext.ConferenceSponsors
                                                  join sp in RepositoryContext.Sponsors on cs.csponsor_sponsorID equals sp.s_ID
                                                  join c in RepositoryContext.Conferences on cs.csponsor_conferenceID equals c.c_ID
                                                  join f in RepositoryContext.UsersFavourites on c.c_ID equals f.FavItemId
                                                  where cs.deleteFlag == false && sp.deleteFlag == false 
                                                  && c.deleteFlag == false && c.c_date == conferenceDate
                                                  && c.c_Type != ConferenceStructureType.Room && f.Status == 1
                                                  && f.UserId == userId && f.ItemType == FavouriteItem.Conference
                                                  
                                                  select new
                                                  {
                                                      c.c_ID,
                                                      cs.csponsor_title,
                                                      data = new
                                                      {
                                                          csponsor_ID = cs.csponsor_sponsorID,
                                                          sp.s_logo,
                                                          sp.templateId,
                                                          sp.s_url
                                                      }
                                                      
                                                  }).ToList()
                                                  .GroupBy(x => new { x.c_ID, x.csponsor_title }, (key, g) => 
                                                   new { csponsor_title = key.csponsor_title , cId = key.c_ID , sponsorlist = g.Select( x => x.data )} )
                                                   .ToLookup(x => x.cId).ToDictionary(t => t.Key, t => t);

            var result = mainQuery.AsNoTracking().Distinct().ToList().OrderBy(x => x.StartTime).ThenBy(x => x.EndTime)
                .GroupBy(x => new { x.StartTime, x.EndTime })
                .Select(b => new
                {
                    value = b.ToList()
                });
            dynamic objResponse = new System.Dynamic.ExpandoObject();
            objResponse.dayList = dayList;
            objResponse.conferenceList = result;
            objResponse.sponsorList = SponsorDetails;
            response.Add(objResponse);
            return response;
        }

        public dynamic GetConfernceVideoList()
        {
            var mainQuery = (from main in RepositoryContext.Conferences
                             where main.deleteFlag == false
                             && main.c_Type != ConferenceStructureType.Room
                             && main.deleteFlag == false && !String.IsNullOrEmpty(main.Video)
                             select new
                             {
                                 Id = main.c_ID,
                                 Topic = main.c_title,
                                 SessionTitle = main.c_sessionTitle,
                                 Description = main.c_brief,
                                 StartDate = main.c_date,
                                 StartTime = main.c_stime,
                                 EndTime = DateTime.ParseExact(main.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes(Int32.Parse(main.c_etime)).ToString("HH:mm", CultureInfo.CurrentCulture),
                                 video = main.Video
                             }).Distinct().ToList().OrderBy(x => x.StartDate).ThenBy(x => x.StartTime).ThenBy(x => x.EndTime).ThenBy(x => x.SessionTitle);
            var result = mainQuery;
            return result;
        }

        public dynamic GetConferenceAccessRight(string userId){
            Dictionary<string,string> result = new Dictionary<string, string>();

            var main = (from cu in RepositoryContext.ConferenceUsers
            join c in RepositoryContext.Conferences on cu.WebinarId equals c.c_ID
            join a in RepositoryContext.AccessRights on cu.AccessRightId equals a.AccessId
            where c.deleteFlag == false && cu.Status == 1 && cu.UserId == userId && a.Status == 1
            select new 
            {
                cu.WebinarId,
                a.AccessRightActions,
            }).AsNoTracking().Distinct().ToList();

            foreach(var value in main){
                if (value != null)
                {
                    result.Add(value.WebinarId, value.AccessRightActions);
                }
            }
            return result;
        }
        public string GetParaDate(dynamic obj){
            string conferenceDate = obj.conferenceDate;
            List<dynamic> response = new List<dynamic>();
            var dayList = (from d in RepositoryContext.Days
                           where d.deleteFlag == false
                           orderby d.d_date ascending
                           select new
                           {
                               day = d.d_date,
                               showName = d.d_desc
                           }).AsNoTracking().Distinct().ToList();

            if (string.IsNullOrEmpty(conferenceDate))
            {
                conferenceDate = dayList[0].day;
            }
            return conferenceDate;
        }

    }
}