using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class NotificationSendListRepository : RepositoryBase<tblNotificationSendList>, INotificationSendListRepository
    {
        public NotificationSendListRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
        public void UpdateNotification(string userId)
        {
            ((from ns in RepositoryContext.NotificationSendLists where ns.UserId == userId && ns.ViewFlag == false select ns).ToList()).ForEach(x => x.ViewFlag = true);
            RepositoryContext.SaveChanges();
        }

        public dynamic GetNotificationSendList(string userId, dynamic obj, string sortField, string sortBy)
        {
            var main = (from ns in RepositoryContext.NotificationSendLists
                        where ns.UserId == userId //&& ns.ViewFlag == false
                        select new
                        {
                            id = ns.Id,
                            text = ns.CustomBody,
                            name = ns.CustomTitle,
                            image = ns.Image,
                            datetime = ns.SendDate,
                            time = ns.SendDate.ToString("yyyy-MM-dd HH:mm"),
                            flag = ns.ViewFlag,
                            //unViewCount = (from notis in RepositoryContext.NotificationSendLists where notis.UserId == userId && notis.ViewFlag == false select notis.ViewFlag).Count(),
                        });
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForNotificationList(main, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            var objTotal = main.Count();
            main = PaginationSessionForNotification(main, obj);
            var objResult = main.ToList();
            //var objResult = main.ToList().OrderBy(x => x.time);


            //Updaing viewflag into true
            // (from ns in RepositoryContext.NotificationSendLists where ns.UserId == userId && ns.ViewFlag == false select ns).ToList()
            //     .ForEach(x => x.ViewFlag = true);
            // RepositoryContext.SaveChanges();

            //return main;
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        private dynamic FilterSessionForNotificationList(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                text = default(string),
                name = default(string),
                image = default(string),
                datetime = default(DateTime),
                time = default(string),
                flag = default(bool)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;
                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForNotification(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                id = default(int),
                text = default(string),
                name = default(string),
                image = default(string),
                datetime = default(DateTime),
                time = default(string),
                flag = default(bool)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, int> pageDictionary = new Dictionary<int, int>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.id);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<int> PaginationFilterArray = new List<int> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.id) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

    }
}