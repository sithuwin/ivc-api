using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
namespace IVC.DAL.Repository.Implementation
{
    public class CommantRatingRepository : RepositoryBase<tblCommantRating>, ICommantRatingRepository
    {
        public CommantRatingRepository(AppDB repositoryContent) : base(repositoryContent)
        {

        }
    }
}