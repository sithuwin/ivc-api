using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.EntityFrameworkCore;

namespace IVC.DAL.Repository.Implementation
{
    public class HelpDeskRepository : RepositoryBase<tblHelpDesk>, IHelpDeskRepository
    {
        public HelpDeskRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }

        public dynamic GetAllIssueByUserId(string userId){
            var main = (from i in RepositoryContext.HelpDesks
                            where i.SubmitterID == userId
                            select new
                            {
                                i.IssueID,
                                i.Title,
                                i.BugContent,
                                i.ReplyContent,
                                i.SubmittedDate,
                                i.SubmitterID,
                                i.IsRead,
                                i.Status,
                                chatStatus = (from c in RepositoryContext.HelpChatLogs 
                                where c.IssueID == i.IssueID select c.Status).SingleOrDefault()
                            }).AsNoTracking().Distinct().ToList();
            return main;
        }

        public dynamic GetAllIssueForUser(string userId,dynamic obj)
        {
            string sortField = null;string sortBy = "";
            var main = (from i in RepositoryContext.HelpDesks
                            where i.SubmitterID == userId
                            select new
                            {
                                i.IssueID,
                                i.Title,
                                i.BugContent,
                                i.ReplyContent,
                                i.SubmittedDate,
                                i.SubmitterID,
                                i.IsRead,
                                i.Status,
                                chatStatus = (from c in RepositoryContext.HelpChatLogs where c.IssueID == i.IssueID select c.Status).SingleOrDefault()
                            }).AsNoTracking().Distinct();
            
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSessionForAllIssuebyUserId(main, obj);
                }
            }
            if (obj.sort.Count > 0)
            {
                var sort = obj.sort[0];
                sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                sortField = sort.field.Value;
            }
            if (sortField == null || sortField == "")
                sortField = "SubmittedDate";
            if (sortBy == null || sortBy == "")
                sortBy = "desc";
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var objTotal = main.Count();
            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);

            main = PaginationSessionForAllIssuebyUserId(main, obj);
            var objResult = main.AsNoTracking().ToList();
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }
        public dynamic GetIssueById(string issueId, string userId)
        {
            dynamic main = (from i in RepositoryContext.HelpDesks
                            where i.IssueID == issueId && i.SubmitterID == userId
                            select new
                            {
                                i.IssueID,
                                i.Title,
                                i.BugContent,
                                i.ReplyContent,
                            }).FirstOrDefault();
            return main;
        }

        public dynamic GetAllIssueForOrg(string sortField, string sortBy, dynamic obj)
        {
            var main = (from i in RepositoryContext.HelpDesks
                        join a in RepositoryContext.Accounts on i.SubmitterID equals a.a_ID
                        join hcl in RepositoryContext.HelpChatLogs on i.IssueID equals hcl.IssueID into ihcl
                        from gihcl in ihcl.DefaultIfEmpty()
                        where (i.Status == Status.Queu) || (gihcl.Status != Status.Close && i.Status == Status.Complete && gihcl != null)
                        select new
                        {
                            i.IssueID,
                            i.SubmitterID,
                            i.Title,
                            i.BugContent,
                            i.SubmittedDate,
                            i.Status,
                            a.a_fullname,
                            a.a_type,
                            ChatLogId = gihcl.HelpChatLogID,
                            LiveChatStatus = gihcl.Status,
                            RequestFor = (from cl in RepositoryContext.HelpChatLogs where cl.IssueID == i.IssueID select cl.IssueID).Count() > 0 ? "LiveChat" : "Answer",
                            ModifiedDate = gihcl.OpenedDate == null ? i.SubmittedDate : gihcl.OpenedDate
                        });
            var objTotal = main.ToList().Count();
            if (obj.filter != null)
            {
                if (obj.filter.filters != null)
                {
                    main = FilterSession(main, obj);
                }
            }
            var objSort = new SortModel();
            objSort.ColId = sortField;
            objSort.Sort = sortBy;

            var sortList = new System.Collections.Generic.List<SortModel>();
            sortList.Add(objSort);
            main = main.OrderBy(sortList);
            main = PaginationSession(main, obj);
            var objResult = main.ToList(); //.OrderBy (x => x.a_fullname);
            dynamic result = new { data = objResult, dataFoundRowsCount = objTotal, success = true, message = "Successfully retrieved" };
            return result;
        }

        public tbl_Account GetAccountByIssuId(string issueId)
        {
            tbl_Account account = (from h in RepositoryContext.HelpDesks join a in RepositoryContext.Accounts on h.SubmitterID equals a.a_ID where h.IssueID == issueId && a.deleteFlag == false select a).FirstOrDefault();
            return account;
        }
        private dynamic FilterSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                IssueID = default(string),
                SubmitterID = default(string),
                Title = default(string),
                BugContent = default(string),
                SubmittedDate = default(DateTime),
                Status = default(string),
                a_fullname = default(string),
                a_type = default(string),
                ChatLogId = default(int),
                LiveChatStatus = default(string),
                RequestFor = default(string),
                ModifiedDate = default(DateTime)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    var filterValue = obj.filter.filters[i].value;

                    if (filterName == "Title")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Title.ToLower().Contains(Name));
                        }
                    }
                    if (filterName == "Status")
                    {
                        string Name = filterValue;
                        if (!String.IsNullOrEmpty(Name))
                        {
                            Name = Name.ToLower();
                            tmpQuery = tmpQuery.Where(x => x.Status.ToLower().Contains(Name));
                        }
                    }

                }
                return tmpQuery;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSession(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                IssueID = default(string),
                SubmitterID = default(string),
                Title = default(string),
                BugContent = default(string),
                SubmittedDate = default(DateTime),
                Status = default(string),
                a_fullname = default(string),
                a_type = default(string),
                ChatLogId = default(int),
                LiveChatStatus = default(string),
                RequestFor = default(string),
                ModifiedDate = default(DateTime)

            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.IssueID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.IssueID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

         private dynamic FilterSessionForAllIssuebyUserId(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                IssueID = default(string),
                Title = default(string),
                BugContent = default(string),
                ReplyContent = default(string),
                SubmittedDate = default(DateTime),
                SubmitterID = default(string),
                IsRead = default(bool),
                Status = default(string),
                chatStatus = default(string)
                
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                for (int i = 0; i < obj.filter.filters.Count; i++)
                {
                    string filterName = obj.filter.filters[i].field;
                    string filterValue = obj.filter.filters[i].value;
                    if (!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
                    {
                        if (filterName == "Title")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Title.ToLower().Contains(Name));
                            }
                        }
                        if (filterName == "Status")
                        {
                            string Name = filterValue;
                            if (!String.IsNullOrEmpty(Name))
                            {
                                Name = Name.ToLower();
                                tmpQuery = tmpQuery.Where(x => x.Status.ToLower().Contains(Name));
                            }
                        }
                    }
                }
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }
        }

        private dynamic PaginationSessionForAllIssuebyUserId(dynamic mainQuery, dynamic obj)
        {
            var tmpQuery = Enumerable.Repeat(new
            {
                IssueID = default(string),
                Title = default(string),
                BugContent = default(string),
                ReplyContent = default(string),
                SubmittedDate = default(DateTime),
                SubmitterID = default(string),
                IsRead = default(bool),
                Status = default(string),
                chatStatus = default(string)
            }, 0).AsQueryable();

            try
            {
                tmpQuery = mainQuery;

                int currentPage = obj.skip;
                int rowsPerPage = obj.take;

                int pageDefinationCount = 1;
                Dictionary<int, string> pageDictionary = new Dictionary<int, string>();
                foreach (var item in tmpQuery)
                {
                    pageDictionary.Add(pageDefinationCount, item.IssueID);
                    pageDefinationCount++;
                }

                List<int> rowList = new List<int> { };
                int totalRowCount = (currentPage + 1) * rowsPerPage;
                int startCount = 0;
                if (currentPage != 0)
                {
                    startCount = (currentPage * rowsPerPage) + 1;
                }
                int endCount = rowsPerPage;
                if (currentPage != 0)
                {
                    endCount = (currentPage + 1) * rowsPerPage;
                }
                for (int countID = startCount; countID <= endCount; countID++)
                {
                    rowList.Add(countID);
                }

                List<string> PaginationFilterArray = new List<string> { };
                foreach (var item in rowList)
                {
                    if (pageDictionary.ContainsKey(item))
                        PaginationFilterArray.Add(pageDictionary[item]);
                }

                tmpQuery = (from main in tmpQuery where PaginationFilterArray.Contains(main.IssueID) select main);
                return tmpQuery;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return tmpQuery;
            }

        }

        public bool CheckLiveChatStatus(string userId)
        {
            var issueIdArray = (from h in RepositoryContext.HelpDesks where h.SubmitterID == userId select h.IssueID).Distinct().ToArray();

            bool CheckStatus = (from c in RepositoryContext.HelpChatLogs where issueIdArray.Contains(c.IssueID) && c.Status == Status.Open select c.IssueID).Count() > 0 ? true : false;
            return CheckStatus;
        }
    }
}