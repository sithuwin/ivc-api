using System;
using System.Collections.Generic;
using System.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;

using Microsoft.Extensions.Configuration;
namespace IVC.DAL.Repository.Implementation
{
    public class AccountLoginBlockerRepository : RepositoryBase<tblAccountLoginBlocker>, IAccountLogicBlockerRepository
    {
        public AccountLoginBlockerRepository(AppDB repositoryContext) : base(repositoryContext)
        {

        }
    }
}