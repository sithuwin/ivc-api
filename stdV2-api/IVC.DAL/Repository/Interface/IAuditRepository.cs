using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface {
    public interface IAuditRepository : IRepositoryBase<tblAudit> {
        dynamic GetCountryBreakDown ();

        dynamic GetActiveUserDetail (string sortField, string sortBy, dynamic obj, bool isExport);
    }
}