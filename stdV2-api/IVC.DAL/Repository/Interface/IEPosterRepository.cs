using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IEPosterRepository : IRepositoryBase<tblEPoster>
    {
        dynamic GetEPosterList(string userId, string sortField, string sortBy, dynamic obj);
        dynamic GetCategoryList();
        dynamic GetEPostById(string id);
        string GetEPosterFile(string id, string returnUrl);

    }
}