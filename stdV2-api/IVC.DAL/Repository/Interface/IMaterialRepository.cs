using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IMaterialRepository : IRepositoryBase<tbl_Material>
    {
        dynamic GetAllMaterials(string userId, string confId);
        dynamic GetFavouriteMaterialListByUserId(string userId);
        dynamic GetFavouriteMaterialListByUserIdAndConderenceId(string userId, string confId);
        dynamic GetMaterialListBySpeakerId(string cId, string userId);
        dynamic GetMaterialListForOrganization(string cId);
        dynamic MaterialViewReport(string mId, dynamic obj, string sortField, string sortBy, bool isExport);
        dynamic MaterialViewReportByCounty(string mId);
        dynamic GetMaterialListbyConferenceId(string cId);
    }
}