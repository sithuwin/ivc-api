using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface INewsRepository : IRepositoryBase<tblNews>
    {
        dynamic GetNewslist();
        string GetNews(string Id);
    }

}