using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IAdminRepository : IRepositoryBase<tbl_Administrator>
    {
    }
}