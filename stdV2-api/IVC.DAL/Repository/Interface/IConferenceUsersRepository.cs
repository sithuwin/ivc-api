using System.Collections.Generic;
using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IConferenceUsersRepository : IRepositoryBase<tbl_ConferenceUser>
    {
        dynamic HasAuthorizedUser(string webinarId);
        void UpdateCurrentJoinStatus(string userId, string webinarId);

        dynamic GetConferenceListbySpeakerId(string userId, string returnUrl);
        dynamic GetAttandanceConferencByUserId(string userId, string returnUrl, string confDate);
        List<string> GetUserListByType(string cId, string type);
        bool CheckUserInSoftOpening(string userId, tbl_Conference conference);
        dynamic GetRegistrantList(string webinarId);
        dynamic GetConferenceDelegateList(string cId, string sortField, string sortBy, dynamic obj);
        dynamic GetGroupChatAnalysisReport(string cId, string sortField, string sortBy, dynamic obj, bool isExport);
        void UpdateSpeakerSequenceNo(dynamic obj);
        dynamic GetAttandanceConferencGroupByTime(string userId, string returnUrl, string confDate);
        dynamic GetAttandanceConferencGroupTemplate(string userId, string returnUrl, string confDate);
    }
}