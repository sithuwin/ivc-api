using System.Collections.Generic;
using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IConfrenceRepository : IRepositoryBase<tbl_Conference>
    {
        //dynamic GetAllWebinarListForAdmin(dynamic obj, string sortField, string sortBy, string adminId);
        // dynamic GetAllWebinarListForUser (dynamic obj, string sortField, string sortBy, string userId);

        //=========================
        // dynamic GetAllWebinarListForAdmin (string adminId);
        dynamic GetConferenceById(string Id, string userId, tbl_Conference conObj);
        dynamic GetAllWebinarListForUser(string userId, dynamic obj, string returnUrl);

        // dynamic GetConfiguration (string webinarId);

        dynamic GetConferenceDatelist();

        dynamic GetConferencesBySpeakerId(string aId, string returnUrl, string confDate);
        dynamic GetMyConferenceDateList(string userId);
        string GetMainSpeakerImage(string cId);
        //dynamic GetConferencelistForOrganization(string confDate, int configId);
        dynamic GetconferenceListWithGroupForOrganization(dynamic currentLiveResult, bool IsLiveOnly);

        dynamic GetConferencelistForOrganization(dynamic obj);
        string GetSponsorLogoByConferenceID(string confID);
        dynamic GetMyFavouriteConferences(string userId, dynamic obj, string returnUrl);
        dynamic GetConferenceSpeakersByConferenceId(string Id, string speakerType);
        dynamic GetLiveConferences(string userId);
        dynamic GetAllWebinarListGroupByTime(string userId, dynamic obj, string returnUrl);
        dynamic GetMyFavouriteConferencesGroupByTime(string userId, dynamic obj, string returnUrl);
        dynamic GetExhibitorShowCase(string exhId);
        dynamic GetConferenceSpeakerType();
        int GetRegistrantCount(string webinerId);
        void UpdateConferenceDatasynStatus(tbl_Conference conference);
        string GetQuestionnaireIDByConferenceId(string cid);
        dynamic GetActiveConferenceList(dynamic obj, string currentUserId);
        dynamic GetAllWebinarListGroupTemplate(string userId, dynamic obj, string returnUrl);
        dynamic GetMyFavouriteConferencesGroupTemplate(string userId, dynamic obj, string returnUrl);
        dynamic GetConfernceVideoList();
        dynamic GetConferenceAccessRight(string userId);
        string GetParaDate(dynamic obj);
    }
}