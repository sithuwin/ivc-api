using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IZoomPollResultRepository : IRepositoryBase<tbl_ZoomPollResult>
    {
        dynamic GetPollColumns(string webinarId);
        dynamic GetPollListByWebinarId(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport);
        void UpdatePollData(dynamic response, string webinarId,string userId);
    }
}