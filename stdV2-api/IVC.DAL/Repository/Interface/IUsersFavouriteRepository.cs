using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IUsersFavouriteRepository : IRepositoryBase<tbl_UsersFavourite>
    {

    }
}