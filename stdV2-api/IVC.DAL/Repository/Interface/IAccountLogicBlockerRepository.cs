using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IAccountLogicBlockerRepository : IRepositoryBase<tblAccountLoginBlocker>
    {
    }
}