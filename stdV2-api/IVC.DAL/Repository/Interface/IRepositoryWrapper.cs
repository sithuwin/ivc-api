using IVC.DAL.Models;
using IVC.DAL.Repository.Implementation;
namespace IVC.DAL.Repository.Interface {
    public interface IRepositoryWrapper {
        ISiteRunNumberRepository SiteRunNumber { get; }
        IAdminRepository Admin { get; }
        IAccountRepository Account { get; }
        IConfrenceRepository Confrence { get; }
        IConferenceUsersRepository ConferenceUser { get; }
        IZoomParticipantsRepository ZoomParticipant { get; }
        IZoomConfigurationRepository ZoomConfiguration { get; }
        IZoomPollResultRepository ZoomPollResult { get; }
        ISpeakerRepository Speaker { get; }
        ISpeakerCategoryRepository SpeakerCategory { get; }
        IConferenceSpeakerRepository ConferenceSpeaker { get; }
        IConferenceCategoryRepository ConferenceCategory { get; }
        IDayRepository Day { get; }
        IMaterialRepository Material { get; }
        IUsersFavouriteRepository UsersFavourite { get; }
        IFriendShipRepository FriendShip { get; }
        IShowConfigRepository ShowConfig { get; }
        IAccessRightRepository AccessRight { get; }
        INotificationTemplateRepository NotificationTemplate { get; }
        INotificationSendListRepository NotificationSendList { get; }
        IEPosterRepository EPoster { get; }
        ICommantRatingRepository CommantRating { get; }
        IAuditRepository Audit { get; }
        IHelpDeskRepository HelpDesk { get; }
        IHelpChatLogRepository HelpChatLog { get; }
        INetworkLoungeRepository NetworkLounge { get; }
        IAccountLogicBlockerRepository AccountLogicBlocker { get; }
        INewsRepository News { get; } 
        IEmailConfigRepository EmailConfig { get; }
        void Save ();
    }
}