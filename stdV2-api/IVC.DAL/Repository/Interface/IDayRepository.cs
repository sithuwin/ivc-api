using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IDayRepository : IRepositoryBase<tbl_Day>
    {
        dynamic GetConferenceDatelist();
        dynamic GetMinimunDate();
        dynamic GetMaximumDate();
        dynamic GetCategories();
    }
}