using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using IVC.DAL.Repository.Implementation;
namespace IVC.DAL.Repository.Interface
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}