using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IConferenceSpeakerRepository : IRepositoryBase<tbl_ConferenceSpeakers>
    {

    }
}