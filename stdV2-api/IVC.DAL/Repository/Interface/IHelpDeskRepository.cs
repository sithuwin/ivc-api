using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IHelpDeskRepository : IRepositoryBase<tblHelpDesk>
    {
        dynamic GetAllIssueForUser(string userId,dynamic obj);
        dynamic GetAllIssueByUserId(string userId);
        dynamic GetIssueById(string issueId, string userId);
        dynamic GetAllIssueForOrg(string sortField, string sortBy, dynamic obj);
        tbl_Account GetAccountByIssuId(string issueId);
        bool CheckLiveChatStatus(string userId);
    }
}