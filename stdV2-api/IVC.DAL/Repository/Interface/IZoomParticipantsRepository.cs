using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IZoomParticipantsRepository : IRepositoryBase<tbl_ZoomParticipants>
    {
        dynamic GetParticipantsSummaryByWebinarId(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport);
        dynamic GetCountrySummary(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport);
        dynamic GetAttandenceDetails(dynamic obj, string sortField, string sortBy, string webinarId, bool isExport);
        dynamic GetEventAttendanceReport();
        void UpdateParticipants(dynamic response,string cid,string userId);
    }
}