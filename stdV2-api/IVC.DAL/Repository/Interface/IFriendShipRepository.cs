using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IFriendShipRepository : IRepositoryBase<tbl_FriendShip>
    {
        dynamic GetRequestedDelegateList(string userId, dynamic obj, string sortField, string sortBy);
        dynamic GetMyConnection(string userId, dynamic obj, string sortField, string sortBy);
        dynamic GetNetworkingReport(dynamic obj, string sortField, string sortBy, bool isExport);
    }
}