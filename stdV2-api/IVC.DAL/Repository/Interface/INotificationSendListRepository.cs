using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface INotificationSendListRepository : IRepositoryBase<tblNotificationSendList>
    {
        void UpdateNotification(string userId);
        dynamic GetNotificationSendList(string userId, dynamic obj, string sortField, string sortBy);
    }
}