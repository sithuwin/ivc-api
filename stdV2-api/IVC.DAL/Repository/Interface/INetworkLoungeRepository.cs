using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface INetworkLoungeRepository  : IRepositoryBase<tbl_NetworkLounge>
    {
        void CreateNetworkLounge(dynamic obj,string ID);
        void UpdateNetworkLounge(dynamic obj, tbl_NetworkLounge  nlObj);
        dynamic SelectAllNetworkLounge(dynamic obj);
        dynamic SelectNetworkLoungeById(string Id);
        void UpdateNetworkLoungeStatuByID(string ID,string status,tbl_NetworkLounge networkLounge);
        dynamic SelectAllPublishedNetworkLounge();
        void DeleteNetworkLoungeByID(string ID,tbl_NetworkLounge networkLounge);
        
    }
}