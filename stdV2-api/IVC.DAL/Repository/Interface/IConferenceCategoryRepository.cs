using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IConferenceCategoryRepository : IRepositoryBase<tbl_ConferenceCategory>
    {
        dynamic GetConferenceCategorylist();
        string GetConferenceCateName(string Id);
    }

}