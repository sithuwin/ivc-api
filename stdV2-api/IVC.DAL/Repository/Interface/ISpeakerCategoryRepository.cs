using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface ISpeakerCategoryRepository : IRepositoryBase<tbl_SpeakerCategory>
    {

    }
}