using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IZoomConfigurationRepository : IRepositoryBase<tbl_ZoomConfiguration>
    {
        // dynamic GetConfiguration (string userId);
        dynamic GetConfigurationByConfigId(int ConfigId);

        dynamic GetConfigurationList();
    }
}