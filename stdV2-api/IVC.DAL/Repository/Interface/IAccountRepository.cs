using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IAccountRepository : IRepositoryBase<tbl_Account>
    {
        dynamic GetDelegateList(string userId, string sortField, string sortBy, dynamic obj);
        dynamic GetDelegateById(string id);
        dynamic GetSpeakerList(string returnUrl);
        dynamic GetSpeakerDetailsById(string returnUrl, string sId);
        int CheckValidation(string aId, string email);
        dynamic ChangePasword();
        dynamic GetDeviceUsageReport(dynamic obj, string sortField, string sortBy, bool isExport);
        dynamic GetRegistrationGroup();
        dynamic GetRegistrationListByDate(dynamic obj);
        dynamic GetSpeakerAccessUserList(string userId, string confId, string sortField, string sortBy, dynamic obj, string[] accessEmailList,string[] panelitEmail,string conType);
        dynamic GetDelegateAccessUserList(string userId, string confId, string sortField, string sortBy, dynamic obj, string[] accessEmailList, string type);
        dynamic GetSpeakerListByCategory(dynamic obj,string returnUrl);
    }

}