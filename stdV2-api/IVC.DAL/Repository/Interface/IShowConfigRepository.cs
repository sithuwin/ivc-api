using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface IShowConfigRepository : IRepositoryBase<tblShowConfig>
    {

    }
}