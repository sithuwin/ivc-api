using IVC.DAL.Models;
namespace IVC.DAL.Repository.Interface
{
    public interface ISiteRunNumberRepository : IRepositoryBase<SiteRunNumber>
    {
        string GetNewId(string runKey);
        void UpdateNumberByRunKey(string runKey);
    }
}