using System.Security.AccessControl;
using IVC.DAL.Models;
using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore;
using System;
namespace IVC.DAL {
    public class AppDB : DbContext {
        public AppDB (DbContextOptions<AppDB> options) : base (options) { }
        public DbSet<SiteRunNumber> SiteRunNumbers { get; set; }
        public DbSet<tbl_Account> Accounts { get; set; }
        public DbSet<tbl_Administrator> Administrators { get; set; }
        public DbSet<tbl_Conference> Conferences { get; set; }
        public DbSet<tbl_ConferenceUser> ConferenceUsers { get; set; }
        public DbSet<tbl_ZoomParticipants> ZoomParticipants { get; set; }
        public DbSet<tbl_ZoomConfiguration> ZoomConfigurations { get; set; }
        public DbSet<tbl_ZoomPollResult> ZoomPollResults { get; set; }
        public DbSet<tbl_Speaker> Speakers { get; set; }
        public DbSet<tbl_SpeakerCategory> SpeakerCategories { get; set; }
        public DbSet<tbl_ConferenceCategory> ConferenceCategories { get; set; }
        public DbSet<tbl_ConferenceSpeakers> ConferenceSpeakers { get; set; }
        public DbSet<tbl_Day> Days { get; set; }
        public DbSet<tbl_Material> Materials { get; set; }
        public DbSet<tbl_UsersFavourite> UsersFavourites { get; set; }
        public DbSet<tbl_FriendShip> FriendShips { get; set; }
        public DbSet<tblShowConfig> ShowConfigs { get; set; }
        public DbSet<tblAccessRight> AccessRights { get; set; }
        public DbSet<tblNotificationTemplate> NotificationTemplates { get; set; }
        public DbSet<tblNotificationSendList> NotificationSendLists { get; set; }
        public DbSet<tblEPoster> EPosters { get; set; }
        public DbSet<tblCommantRating> CommantRatings { get; set; }
        public DbSet<tblAudit> Audits { get; set; }
        public DbSet<tblHelpDesk> HelpDesks { get; set; }
        public DbSet<tblHelpChatLog> HelpChatLogs { get; set; }
        public DbSet<tblSponsor> Sponsors { get; set; }
        public DbSet<tblSponsorCategory> SponsorCategories { get; set; }
        public DbSet<tblConferenceSponsor> ConferenceSponsors { get; set; }
        public DbSet<tbl_Country> Countries { get; set; }
        public DbSet<tbl_NetworkLounge> NetworkLounges { get; set; }
        public DbSet<tblLeaderBoard> leaderBoards { get; set; }
        public DbSet<tblAccountLoginBlocker> AccountLoginBlockers { get; set; }
        public DbSet<tblNews> News { get; set; }
        public DbSet<tblAccountHideList> AccountHideLists { get; set; }

        public DbSet<tblConfigEmail> ConfigEmails { get; set; }
        protected override void OnModelCreating (ModelBuilder modelBuilder) {

            modelBuilder.Entity<SiteRunNumber> ()
                .HasKey (c => new { c.prefix });
            modelBuilder.Entity<tbl_Account> ()
                .HasKey (c => new { c.a_ID });
            modelBuilder.Entity<tbl_Administrator> ()
                .HasKey (c => new { c.admin_id });
            modelBuilder.Entity<tbl_ConferenceUser> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tbl_Conference> ()
                .HasKey (c => new { c.c_ID });
            modelBuilder.Entity<tbl_ZoomParticipants> ()
                .HasKey (c => new { c.SummaryId });
            modelBuilder.Entity<tbl_ZoomConfiguration> ()
                .HasKey (c => new { c.ConfigId });
            modelBuilder.Entity<tbl_ZoomPollResult> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tbl_Speaker> ()
                .HasKey (c => new { c.s_ID });
            modelBuilder.Entity<tbl_SpeakerCategory> ()
                .HasKey (c => new { c.speakerCat_ID });
            modelBuilder.Entity<tbl_ConferenceCategory> ()
                .HasKey (c => new { c.cc_ID });
            modelBuilder.Entity<tbl_ConferenceSpeakers> ()
                .HasKey (c => new { c.cspeaker_ID });
            modelBuilder.Entity<tbl_Day> ()
                .HasKey (c => new { c.d_ID });
            modelBuilder.Entity<tbl_Material> ()
                .HasKey (c => new { c.MatId });
            modelBuilder.Entity<tbl_UsersFavourite> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tbl_FriendShip> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tblShowConfig> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tblAccessRight> ()
                .HasKey (c => new { c.AccessId });
            modelBuilder.Entity<tblNotificationTemplate> ()
                .HasKey (c => new { c.NotificationId });
            modelBuilder.Entity<tblNotificationSendList> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tblEPoster> ()
                .HasKey (c => new { c.ep_ID });
            modelBuilder.Entity<tblCommantRating> ()
                .HasKey (c => new { c.cr_ID });
            modelBuilder.Entity<tblAudit> ()
                .HasKey (c => new { c.Id });
            modelBuilder.Entity<tblHelpDesk> ()
                .HasKey (c => new { c.IssueID });
            modelBuilder.Entity<tblHelpChatLog> ()
                .HasKey (c => new { c.HelpChatLogID });
            modelBuilder.Entity<tblSponsorCategory> ()
                .HasKey (c => new { c.sponsorCat_ID });
            modelBuilder.Entity<tblSponsor> ()
                .HasKey (c => new { c.s_ID });
            modelBuilder.Entity<tblConferenceSponsor> ()
                .HasKey (c => new { c.csponsor_ID });
            modelBuilder.Entity<tbl_Country> ()
                .HasKey (c => new { c.ISO3Digit });
            modelBuilder.Entity<tbl_NetworkLounge> ()
                .HasKey (c => new { c.nl_ID });
            modelBuilder.Entity<tblLeaderBoard> ()
                .HasKey (c => new { c.ParticipantID });
            modelBuilder.Entity<tblAccountLoginBlocker> ()
                .HasKey (c => new { c.ID });
            modelBuilder.Entity<tblNews> ()
                .HasKey (c => new { c.n_ID });
            modelBuilder.Entity<tblAccountHideList> ()
                .HasKey (c => new { c.AccountId });

            modelBuilder.Entity<tblConfigEmail> ()
                .HasKey (c => new { c.em_id });
        }
    }
}