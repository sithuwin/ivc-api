namespace IVC.DAL.Util
{
    public static class Constants
    {

    }

    public static class SiteRunNumbers
    {
        public const string AdminRunKey = "ADM_";
        public const string UserRunKey = "U_";
        public const string MaterialRunKey = "Mat";
        public const string AccountRunKey = "A";
        public const string HelpDeskRunKey = "ISS";
        public const string NetworkLoungeRunKey = "NLG";
    }
    public static class UserType
    {
        public const string Delegate = "D";
        public const string Speaker = "S";
        public const string Visitor = "V";
        public const string Organizer = "O";
        public const string VIP = "VIP";
        public const string Media = "M";
        public const string FreeDelegate = "FD";
        public const string MainExhibitor = "ME";
        public const string ContactPerson = "E";
    }
    public static class ShowConfigNames
    {
        public const string ShowName = "ShowName";
        public const string ShowPrefix = "ShowPrefix";
        public const string ConferenceName = "ShowName";
        public const string Category = "Category";
        public const string Country = "Country";
        public const string Link = "Link";
        public const string SMTPUserName = "SMTPUserName";
        public const string SMTPPassword = "SMTPPassword";
        public const string SMTPPort = "SMTPPort";
        public const string SMTPHost = "SMTPHost";
        public const string EnableSSL = "EnableSSL";
        public const string FriendRequestStatus = "ConnectionRequestStatus";
        public const string FriendAcceptStatus = "ConnectionAcceptStatus";
        public const string ShowAdminName = "ShowAdminName";
        public const string WebSiteLink = "WebSiteLink";
        public const string Lobby = "Lobby";
        public const string FloorPlan = "Floor Plan";
        public const string ShowValue = "ShowValue";
    }

    public static class GroupChatType
    {
        public static string SpeakerGroupChat = "SpeakerGroupChat";
        public static string ConferenceGroupChat = "ConferenceGroupChat";
        public static string LiveGroupChat = "LiveGroupChat";
        public static string NetworkGroupChat = "NetworkGroupChat";
    }
    public static class CurrentConferenceStatus
    {
        public const string GrandOpening = "Grand Opening";
        public const string SoftOpening = "Soft Opening";
        public const string CloseConference = "Close Conference";
        public const string InitialState = "Initial State";
        public const string End = "End";
    }
    public static class CurrentGroupChatStatus
    {
        public const string open = "Open";
        public const string join = "Join";
    }
    public static class ChatJoinStatus
    {
        public const string chat = "Chat";
        public const string open = "Open";
    }

    public static class NotificationTemplate
    {
        public const string firendRequestNotification = "N_001";
        public const string firendResponseNotification = "N_002";
        public const string GroupChatOpenNotification = "N_003";
        public const string GroupChatCloseNotification = "N_004";
        public const string ConferenceStartNotification = "N_005";
        public const string ConferenceCloseNotification = "N_006";

    }
    public static class AuditLogType
    {
        public const string login = "LIN";
        public const string logout = "LOUT";
        public const string ViewMaterialDescription = "V_MATRLDesc";
        public const string ViewMaterial = "V_MATR";
        public const string JoinConference = "J_CONF";
        public const string JoinGroupChat = "J_GCHAT";
        public const string DownloadVCard = "D_VCARD";
        public const string ViewPoster = "V_POSTR";
        public const string VisitExhibitor = "V_EXH";
        public const string ViewPosterDescription = "V_PDESC";
        public const string VisitNetworkLounge = "V_NetLounge";
        public const string Started_JoinNetworkLogGroupChat = "CHAT_NetLogGROUP_STARTED";
        public const string Ended_JoinNetworkLogGroupChat = "CHAT_NetLogGROUP_ENDEDED";
        public const string Started_ChatWithExhibitor = "CHAT_EXH_STARTED";
        public const string Ended_ChatWithExhibitor = "CHAT_EXH_ENDED";
        public const string Started_JoinExhibitorGroupChat = "CHAT_EXHGROUP_STARTED";
        public const string Ended_JoinExhibitorGroupChat = "CHAT_EXHGROUP_ENDED";

    }
    public static class DeviceType
    {
        public const string Mobile = "Mobile";
        public const string Web = "Web";
        public const string Window = "Window";
        public const string Android = "Android";
        public const string IOS = "IOS";
    }
    public static class Status
    {
        public static string Pending = "Pending";
        public static string Cancel = "Cancel";
        public static string Block = "Block";
        public static string Reject = "Reject";
        public static string Done = "Done";
        public const string Send = "Send";
        public const string ERROR = "ERROR";
        // public const string Request = "Request";
        public const string Queu = "Q";
        public const string Complete = "Com";
        public const string Approve = "Approve";
        public const string Close = "Close";
        public const string Open = "Open";
    }
    public static class ConferenceType
    {
        public static string Default = "Default";
        public static string Embedded = "Embedded";
    }

    public static class FavouriteItem
    {
        public static string Product = "P";
        public static string Exhibitor = "E";
        public static string Visitor = "V";
        public static string Conference = "C";
    }
    public static class ConferenceSpeakerType
    {
        public const string GuestOfHonor = "GH";
        public const string KeynoteSpeaker = "KN";
        public const string SessionModerator = "SM";
        public const string Speaker = "S";
    }
    public static class ConferenceStructureType
    {
        public const string MainConference = "M";
        public const string Workshop = "W";
        public const string Room = "ROOM";
    }

    public static class NetworkLoungeStatus
    {
        public static string Published = "Published";
        public static string InitialState = "Initial State";
    }
    public static class Role
    {
        public const string Create = "1";
        public const string Join = "0";
    }

    public static class ConferenceRoomType
    {
        public const string MainRoom = "Main Room";
        public const string ShareRoom = "Share Room";
        public const string SeparateRoom = "Separate Room";
    }

    public static class AccessRight
    {
        public const string AccessRight_Delegate = "DelegateAccessRight";
    }

    public static class EmailTitle
    {
        public static string App_Confirmed = "Appointment Confirmed";
        public static string Fri_Request = "ConnectionRequest";
        public static string Fri_Accept = "ConnectionAccept";
    }

}