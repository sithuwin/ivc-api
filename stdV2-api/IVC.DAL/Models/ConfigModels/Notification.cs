namespace IVC.DAL.Models
{
    public class Notification
    {
        public string form_name { get; set; }
        public string request_id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string sound { get; set; }
        public string icon_url { get; set; }
    }
}