namespace IVC.DAL.Models
{
    public class ENUM_Cache
    {
        public static dynamic ConferenceList = new { key = "ConferenceList", resource = "ivc" };
        public static dynamic SponsorList =  new { key = "SponsorList", resource = "ive" };
        public static dynamic ExhibitionList = new  { key = "ExhibitionList", resource = "ive" };
        public static dynamic ExhibitingCompany = new { key = "ExhibitingCompany", resource = "ive" };
        public static dynamic ExhibitionBooth = new { key = "ExhibitionBooth", resource = "ive" };
        public static dynamic SpeakerList = new { key = "SpeakerList", resource = "ivc" };
    }
}