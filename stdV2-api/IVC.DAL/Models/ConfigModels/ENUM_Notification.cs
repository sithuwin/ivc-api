namespace IVC.DAL.Models
{
    public class ENUM_Notification
    {
        public static string AddFriend = "Add Friend";
        public static string AcceptFriend = "Accept Friend";
        public static string SoftOpening = "Soft Opening";
        public static string GrandOpening = "Grand Opening";
        public static string CloseOpening = "Close Conference";
        public static string OpenChatChannel = "Open Chat Channel";
        public static string CloseChatChannel = "Close Chat Channel";

    }

}