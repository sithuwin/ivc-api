using System;
namespace IVC.DAL.Models
{
    public class TokenData
    {
        public string Sub = "";  //Required Field, Used for core JWT
        public string Jti = ""; //Required Field, Used for core JWT
        public string Iat = ""; //Required Field, Used for core JWT
        public string UserID = "";
        public string Role = "";
        public string LoginType = "";
        public string Userlevelid = "";
        public DateTime TicketExpireDate = DateTime.UtcNow;
    }
}