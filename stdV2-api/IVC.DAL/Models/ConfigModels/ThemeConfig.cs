namespace IVC.DAL.Models
{
    public class ThemeConfig
    {
        public string maincolor { get; set; }
        public string supportcolor { get; set; }
        public string fontfamily { get; set; }
        public string fontsize { get; set; }
        public string labelcolor { get; set; }

    }
}