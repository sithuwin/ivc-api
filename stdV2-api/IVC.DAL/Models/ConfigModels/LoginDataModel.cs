namespace IVC.DAL.Models
{
    public class LoginDataModel
    {
        public string grant_type { get; set; }
        public string LoginType { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string deviceToken { get; set; }
        public string mobileDeviceId { get; set; }
        public string deviceType { get; set; }
    }
}