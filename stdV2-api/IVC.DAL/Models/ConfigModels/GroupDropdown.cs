namespace IVC.DAL.Models
{
    public class GroupDropdown
    {
        public string value { get; set; }
        public string viewValue { get; set; }
    }
}