namespace IVC.DAL.Models
{
    public class LogType
    {
        public static string Info = "Info";
        public static string Warn = "Warn";
        public static string Error = "Error";
        public static string Debug = "Debug";
        public static string Fatal = "Fatal";
    }
}