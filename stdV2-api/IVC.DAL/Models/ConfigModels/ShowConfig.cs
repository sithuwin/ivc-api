namespace IVC.DAL.Models
{
    public class ShowConfig
    {
        public string Image { get; set; }

        public string Location { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PassCode { get; set; }
        public string Country { get; set; }

        public string State { get; set; }

    }
}