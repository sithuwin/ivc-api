using System.Collections.Generic;
using System;
namespace IVC.DAL.Models
{
    public class LiveWebinar
    {
        public string uuid { get; set; }
        public string id { get; set; }
        public string topic { get; set; }
        public string host { get; set; }
        public string email { get; set; }
        public string user_type { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int participants { get; set; }
        public bool has_pstn { get; set; }
        public bool has_voip { get; set; }
        public bool has_3rd_party_audio { get; set; }
        public bool has_video { get; set; }
        public bool has_screen_share { get; set; }
        public bool has_recording { get; set; }
        public bool has_sip { get; set; }

    }

    public class LiveWebinarList
    {
        public string from { get; set; }
        public string to { get; set; }
        public int page_count { get; set; }
        public int total_records { get; set; }
        public string next_page_token { get; set; }
        public List<LiveWebinar> webinars { get; set; }
    }
}