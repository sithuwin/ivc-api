namespace IVC.DAL.Models
{
    public class Webinar
    {
        public string topic { get; set; }
        public int type { get; set; }
        public string start_time { get; set; }
        public int duration { get; set; }
        public string timezone { get; set; }
        public string password { get; set; }
        public string agenda { get; set; }
        public Recurrence recurrence { get; set; }
        public Settings settings { get; set; }
    }

    public class Recurrence
    {
        public int type { get; set; }
        public int repeat_interval { get; set; }
        public string end_date_time { get; set; }

    }

    public class Settings
    {
        public bool host_video { get; set; }
        public bool panelists_video { get; set; }
        public bool practice_session { get; set; }
        public bool hd_video { get; set; }
        public int approval_type { get; set; }
        public int registration_type { get; set; }
        public string audio { get; set; }
        public string auto_recording { get; set; }
        public bool enforce_login { get; set; }
        public string enforce_login_domains { get; set; }
        public string alternative_hosts { get; set; }
        public bool close_registration { get; set; }
        public bool show_share_button { get; set; }
        public bool allow_multiple_devices { get; set; }
        public bool registrants_email_notification { get; set; }

    }
}