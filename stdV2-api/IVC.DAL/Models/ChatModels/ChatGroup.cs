namespace IVC.DAL.Models
{
    public class ChatGroup
    {
        public string guid { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        //public string icon { get; set; }
        public string description { get; set; }
    }
}