namespace IVC.DAL.Models
{
    public class GroupParticipants
    {
        public string[] participants { get; set; }
    }

    public class GroupAdmins
    {
        public string[] admins { get; set; }
    }

    public class GroupModerators
    {
        public string[] moderators { get; set; }
    }
}