using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("CommentRating")]
    public class tblCommantRating
    {
        public int cr_ID { get; set; }
        public string cr_pID { get; set; }
        public string cr_comment { get; set; }
        public float? cr_rating { get; set; }
        public string cr_Uid { get; set; }
        public DateTime? cr_datetime { get; set; }

    }
}