using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblUsersFavourite")]
    public class tbl_UsersFavourite
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string FavItemId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ItemType { get; set; }

    }
}