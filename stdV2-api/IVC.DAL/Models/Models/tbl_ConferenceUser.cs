using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblConferenceUser")]
    public class tbl_ConferenceUser
    {

        public int Id { get; set; }
        public string AdminId { get; set; }
        public string UserId { get; set; }
        public string WebinarId { get; set; }
        public string RegistrantId { get; set; }
        public bool CurrentJoinStatus { get; set; }
        public int? JoinStatus { get; set; }
        public string DeviceId { get; set; }
        public int Status { get; set; }
        public string AccountType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ChatChannelJoinStatus { get; set; }
        public string SpeakerChatChannelJoinStatus { get; set; }
        public string AccessRightId { get; set; }
        public int? SpeakerSeqNo { get; set; }
    }
}