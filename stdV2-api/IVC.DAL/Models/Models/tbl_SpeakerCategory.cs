using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblSpeakerCategory")]
    public class tbl_SpeakerCategory
    {
        public string speakerCat_ID { get; set; }
        public string speakerCat_name { get; set; }
        public int speakerCat_seq { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }

    }
}