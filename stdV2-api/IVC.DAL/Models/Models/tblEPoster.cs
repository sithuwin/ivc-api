using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblE-Poster")]
    public class tblEPoster
    {
        public string ep_ID { get; set; }
        public string ep_title { get; set; }
        public string ep_category { get; set; }
        public string ep_author { get; set; }
        public string ep_coauthor { get; set; }
        public string ep_venue { get; set; }
        public string ep_duration { get; set; }
        public string ep_file { get; set; }
        public string ep_abstract { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
        public string video { get; set; }

    }
}