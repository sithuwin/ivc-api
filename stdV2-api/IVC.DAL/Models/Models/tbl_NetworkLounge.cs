using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tbl_NetworkLounge")]
    public class tbl_NetworkLounge
    {
        public string nl_ID { get; set; }
        public string nl_Name { get; set; }
        public string nl_Desc { get; set; }
        public string nl_Status { get; set; }
        public int? nl_seq { get; set;}
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}