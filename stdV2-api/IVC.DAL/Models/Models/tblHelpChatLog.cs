using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblHelpChatLog")]
    public class tblHelpChatLog
    {
        public int HelpChatLogID { get; set; }
        public string IssueID { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public DateTime OpenedDate { get; set; }
        public DateTime? ClosedDate { get; set; }

    }
}