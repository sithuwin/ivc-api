using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblAccessRight")]
    public class tblAccessRight
    {
        public string AccessId { get; set; }
        public string Name { get; set; }
        public string AccessRightActions { get; set; }
        public int Status { get; set; }
    }
}