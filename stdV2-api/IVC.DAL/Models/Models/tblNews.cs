using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblNews")]
    public class tblNews
    {
        public string n_ID { get; set; }
        public string n_title { get; set; }
        public string n_content { get; set; }
        public string n_picture { get; set; }
        public string menuID { get; set; }
        public int menlanguID { get; set; }
        public bool? deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
    }
}