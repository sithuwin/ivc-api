using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
namespace IVC.DAL.Models
{
    [Table ("tblSponsor")]
    public class tblSponsor {
        public string s_ID { get; set; }
        public string menuID { get; set; }
        public string s_name { get; set; }
        public string s_content { get; set; }
        public string s_logo { get; set; }
        public string s_category { get; set; }
        public int? s_seq { get; set; }
        public int lang { get; set; }
        public string s_url { get; set; }
        public string templateId { get; set; }
        public string s_exhID { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime UpdatedDate { get; set; }
    
    }
}