using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblAccountLoginBlocker")]
    public class tblAccountLoginBlocker
    {
        public int ID { get; set; }
        public string LoginBlocker { get; set; }
        public string Message { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime? createdDate { get; set; }
    }
}