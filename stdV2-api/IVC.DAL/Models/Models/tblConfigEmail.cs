using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models {
    [Table ("tb_ConfigEmail")]
    public class tblConfigEmail {
        public int em_id { get; set; }
        public string em_Title { get; set; }
        public string em_FromName { get; set; }
        public string em_FromEmail { get; set; }
        public string em_Subject { get; set; }
        public string em_Content { get; set; }
        public string em_cc { get; set; }
        public string em_bcc { get; set; }
        public string em_parameters { get; set; }
    }
}