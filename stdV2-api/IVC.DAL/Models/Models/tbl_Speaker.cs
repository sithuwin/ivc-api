using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblSpeaker")]
    public class tbl_Speaker
    {
        public string s_ID { get; set; }
        public string s_fullname { get; set; }
        public string s_job { get; set; }
        public string s_company { get; set; }
        public string s_email { get; set; }
        public string s_mobile { get; set; }
        public string s_address { get; set; }
        public string s_country { get; set; }
        public string s_bio { get; set; }
        public string s_profilepic { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }

    }
}