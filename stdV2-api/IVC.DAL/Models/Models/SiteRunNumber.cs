using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("gen_Key")]
    public class SiteRunNumber
    {
        public string prefix { get; set; }
        public string prefix_desc { get; set; }
        public string prefix_opt { get; set; }
        public Int16 prefix_seq { get; set; }
        public string runno_opt { get; set; }
        public Int16? runno_seq { get; set; }
        public int runno_init { get; set; }
        public int runno_curr { get; set; }
        public Int16 runno_incr { get; set; }
        public string status { get; set; }
        public string remark { get; set; }
        public DateTime create_date { get; set; }
        public string create_by { get; set; }
        public DateTime lupdate_date { get; set; }
        public string lupdate_by { get; set; }

    }
}