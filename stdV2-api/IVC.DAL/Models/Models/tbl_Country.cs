using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblCountry")]
    public class tbl_Country
    {
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public  string ISO3Digit { get; set; }
        public string Region { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}