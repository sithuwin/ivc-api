using System.ComponentModel.DataAnnotations.Schema;

namespace IVC.DAL.Models
{
    [Table("tblZoomConfiguration")]
    public class tbl_ZoomConfiguration
    {
        public int ConfigId { get; set; }
        public string ConfigName { get; set; }
        public string APIKey { get; set; }
        public string ScrectKey { get; set; }
        public string AccountId { get; set; }
        public string SignatureEndPoint { get; set; }

    }
}