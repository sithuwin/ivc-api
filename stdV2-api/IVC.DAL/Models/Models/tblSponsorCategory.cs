using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
namespace IVC.DAL.Models
{
    [Table ("tblSponsorCategory")]
    public class tblSponsorCategory {
        public string sponsorCat_ID { get; set; }
        public string sponsorCat_name { get; set; }
        public int? sponsorCat_seq { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
    }
}