using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblAccountHideList")]
    public class tblAccountHideList
    {
        public string AccountId { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? updatedDate { get; set; }
    }
}