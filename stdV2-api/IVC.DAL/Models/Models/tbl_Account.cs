using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblAccount")]
    public class tbl_Account
    {
        public string a_ID { get; set; }
        public string ExhId { get; set; }
        public string a_fullname { get; set; }
        public string a_type { get; set; }
        public string a_sal { get; set; }
        public string a_fname { get; set; }
        public string a_lname { get; set; }
        public string a_designation { get; set; }
        public string a_email { get; set; }
        public string a_company { get; set; }
        public string a_addressOfc { get; set; }
        public string a_addressHome { get; set; }
        public string a_country { get; set; }
        public string a_Tel { get; set; }
        public string a_Mobile { get; set; }
        public string a_LoginName { get; set; }
        public string a_Password { get; set; }
        public string a_profilepic { get; set; }
        public bool? a_isActivated { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string a_AttendDay { get; set; }
        public string NFCID { get; set; }
        public string RegPortalID { get; set; }
        public string ExhID_iScan { get; set; }
        public int? UserType { get; set; }
        // public string PanelistId { get; set; }
        // public string RegistrationId { get; set; }
        public string AdminId { get; set; }
        public string Biography { get; set; }
        public string ProfilePic { get; set; }
        public string VCard { get; set; }
        public int? ShareVCard { get; set; }
        public string DeviceId { get; set; }
        public string MobileDeviceId { get; set; }
        public string Fax { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string a_subTitle { get; set; }
        public string a_Time { get; set; }
        public string LoginBlocker { get; set; }
        public string ChatID { get; set; }

    }
}