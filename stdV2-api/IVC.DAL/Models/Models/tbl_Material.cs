using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblMaterial")]
    public class tbl_Material
    {
        public string MatId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UploadUserType { get; set; }
        public string UploadUserId { get; set; }
        public string MaterialType { get; set; }
        public string Url { get; set; }
        public string ConferenceId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CreatedBy { get; set; }

    }
}