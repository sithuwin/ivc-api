using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
namespace IVC.DAL.Models
{
    [Table ("tblConferenceSponsors")]
    public class tblConferenceSponsor
    {
        public string csponsor_ID { get; set; }
        public string csponsor_sponsorID { get; set; }
        public string csponsor_scategory { get; set; }
        public string csponsor_conferenceID { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string csponsor_title { get; set; }
    }
}