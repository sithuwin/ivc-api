using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblConference")]
    public class tbl_Conference
    {
        public string c_ID { get; set; }
        public string c_cc { get; set; }
        public string c_date { get; set; }
        public string c_stime { get; set; }
        public string c_etime { get; set; }
        public string c_title { get; set; }
        public string c_venue { get; set; }
        public string c_brief { get; set; }
        public bool? c_liveQA { get; set; }
        public string c_parentConfID { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool? c_Polling { get; set; }
        public bool? c_Survey { get; set; }
        public string ZoomId { get; set; }
        public int ConfigId { get; set; }
        public string c_TimeZone { get; set; }
        public string c_Password { get; set; }
        public string chatChannelStatus { get; set; }
        public string MainSpeakerId { get; set; }
        public string CurrentConferenceStatus { get; set; }
        public string Video { get; set; }
        public bool? AutoNotiSend { get; set; }
        public string SpeakerChatChannelStatus { get; set; }
        public string Type { get; set; }
        public string c_Type { get; set; }
        public string SponsorLogo { get; set; }
        public string RoomId { get; set; }
        public string c_sessionTitle { get; set; }
        public string InfoPic { get; set; }
        public bool? IsDataSyn { get; set; }
        public string c_qnaireId { get; set; }
    }
}