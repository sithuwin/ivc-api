using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblNotificationSendList")]
    public class tblNotificationSendList
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string NotificationId { get; set; }
        public string Status { get; set; }
        public bool ViewFlag { get; set; }
        public string Image { get; set; }
        public DateTime SendDate { get; set; }
        public string CustomTitle { get; set; }
        public string CustomBody { get; set; }
    }
}