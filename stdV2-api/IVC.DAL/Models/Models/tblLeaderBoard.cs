using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblLeaderBoard")]
    public class tblLeaderBoard
    {
        public string ParticipantID { get; set; }
        public int TotalPoints { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool deleteFlag { get; set; }
    }
}