using System.ComponentModel.DataAnnotations.Schema;
using System;
namespace IVC.DAL.Models
{
    [Table("tblFriendShip")]
    public class tbl_FriendShip
    {
        public int Id { get; set; }
        public string RequestUserId { get; set; }
        public string ResponseUserId { get; set; }
        public string Status { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? ResponseDate { get; set; }

    }
}