using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblConferenceCategory")]
    public class tbl_ConferenceCategory
    {
        public string cc_ID { get; set; }
        public string cc_categoryname { get; set; }
        public int cc_seq { get; set; }
        public string cc_Type { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }

    }
}