using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblZoomParticipants")]
    public class tbl_ZoomParticipants
    {

        public int SummaryId { get; set; }
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string UserEmail { get; set; }
        public string JoinTime { get; set; }
        public string LeaveTime { get; set; }
        public int? Duration { get; set; }
        public string AttentivenessScore { get; set; }
        public string WebinarId { get; set; }
        public string AdminId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}