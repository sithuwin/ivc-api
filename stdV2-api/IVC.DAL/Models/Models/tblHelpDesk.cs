using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblHelpDesk")]
    public class tblHelpDesk
    {
        public string IssueID { get; set; }
        public string SubmitterID { get; set; }
        public string AssignedID { get; set; }
        public string Title { get; set; }
        public string BugContent { get; set; }
        public string ReplyContent { get; set; }
        public string Status { get; set; }
        public DateTime SubmittedDate { get; set; }
        public DateTime? ReplyDate { get; set; }
        public bool IsRead { get; set; }
    }
}