using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblNotificationTemplate")]
    public class tblNotificationTemplate
    {
        public string NotificationId { get; set; }
        public string TitleMessage { get; set; }
        public string BodyMessage { get; set; }
        public string Image { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Form { get; set; }
        public string CreatedBy { get; set; }

    }
}