using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblDay")]
    public class tbl_Day
    {
        public string d_ID { get; set; }
        public string d_key { get; set; }
        public string d_date { get; set; }
        public string d_year { get; set; }
        public string d_desc { get; set;}
        public bool? deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
    }
}