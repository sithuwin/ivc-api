using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("Administrator")]
    public class tbl_Administrator
    {
        public int admin_id { get; set; }
        public string admin_username { get; set; }
        public string admin_password { get; set; }
        public string admin_name { get; set; }
        public int? admin_role { get; set; }
        public string admin_remark { get; set; }
        public bool? admin_isdeleted { get; set; }
        public string admin_profilepic { get; set; }
        public DateTime? admin_createddate { get; set; }
        public string ChatID { get; set; }

    }
}