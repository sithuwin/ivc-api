using System.ComponentModel.DataAnnotations.Schema;
namespace IVC.DAL.Models
{
    [Table("tblConferenceSpeakers")]
    public class tbl_ConferenceSpeakers
    {
        public string cspeaker_ID { get; set; }
        public string cspeaker_category { get; set; }
        public string cspeaker_conferenceID { get; set; }
        public string cspeaker_speakerID { get; set; }
        public string cspeaker_confcategory { get; set; }
        public int sorting { get; set; }
        public int lang { get; set; }
        public bool deleteFlag { get; set; }
        public string UpdatedDate { get; set; }
    }
}