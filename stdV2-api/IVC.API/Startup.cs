﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomTokenAuthProvider;
using IVC.DAL;
using IVC.DAL.Models;
using IVC.DAL.Repository.Implementation;
using IVC.DAL.Repository.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace IVC.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDB>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("defaultDatabase"))
            );
            services.ConfigureCors();
            //         services.AddCors(options =>
            // {
            //     options.AddPolicy("CorsPolicy",
            //         builder => builder//.WithOrigins("https://localhost:7001")
            //         .AllowAnyOrigin()
            //         //.WithOrigins(new[] { "https://localhost:7001" })
            //         .AllowAnyMethod()
            //         .AllowAnyHeader()
            //         .AllowCredentials());
            // });
            string signalSession = Configuration.GetSection("signalSession").Value;
            services.AddSignalR(options =>
            { // Faster pings for testing
                options.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(signalSession));
                options.ClientTimeoutInterval = TimeSpan.FromSeconds((Convert.ToInt32(signalSession)) * 2);
                options.HandshakeTimeout = TimeSpan.FromSeconds((Convert.ToInt32(signalSession)) * 2);
            });
            services.AddSignalR();
            services.AddControllers().AddNewtonsoftJson();
            //services.ConfigureCors();
            services.ConfigureIISIntegration();
            //For business logic repository
            services.ConfigureBLLRepositoryWrapper();
            // For data access layar repository
            services.ConfigureDALRepositoryWrapper();
            // For repository wrapper reposiotry
            services.ConfigureRepositoryWrapper();
            services.AddTransient<TokenProviderMiddleware>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IMemoryCache, MemoryCache>();
            services.AddDistributedMemoryCache();
            services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;

            });
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(3000);
            });
            string secretKey = Configuration.GetSection("TokenAuthentication:SecretKey").Value;
            var key = Encoding.ASCII.GetBytes(secretKey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            //If using Kestrel:
            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));
            // services.Configure<KestrelServerOptions>(options =>
            // {
            //     options.AllowSynchronousIO = true;
            // });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSession();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Files")),
                RequestPath = "/File"
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Files")),
                RequestPath = "/File"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images")),
                RequestPath = "/Image"
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images")),
                RequestPath = "/Image"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "VCards")),
                RequestPath = "/VCard"
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                        Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "VCards")),
                RequestPath = "/VCard"
            });

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            //app.UseCors("CorsPolicy");
            app.UseTokenProviderMiddleware();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseFileServer(enableDirectoryBrowsing: false);
            // app.UseSignalR(route=>{
            //      route.MapHub<SignalHub>("signalHub"); 
            // });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<SignalHub>("/api/signalHub").RequireCors("CorsPolicy");
            });
        }
    }
}