using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IVC.DAL.Repository.Implementation;
using IVC.DAL.Repository.Interface;
namespace IVC.API
{
    public static class ServiceExtensions
    {
        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
        //For All business logic repository
        public static void ConfigureBLLRepositoryWrapper(this IServiceCollection services) { }
        public static void ConfigureDALRepositoryWrapper(this IServiceCollection services) { }

        public static void ConfigureCors(this IServiceCollection services)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string url = Configuration.GetSection("appSettings:SingnalUrl").Value;

            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin(); // For anyone access.
            //corsBuilder.AllowCredentials();

            var corsBuilder2 = new CorsPolicyBuilder();
            corsBuilder2.WithOrigins(url);
            corsBuilder2.AllowAnyHeader();
            corsBuilder2.AllowAnyMethod();
            corsBuilder2.AllowCredentials();
            corsBuilder2.SetIsOriginAllowed((host) => true);
            services.AddCors(options =>
            {
                options.AddPolicy("CorsAllowAllPolicy", corsBuilder.Build());
                options.AddPolicy("CorsPolicy", corsBuilder2.Build());
            });
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
        }

    }

}