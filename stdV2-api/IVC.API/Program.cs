﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Formatting.Compact;
using Microsoft.Extensions.Configuration;

namespace IVC.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var Configuration = new ConfigurationBuilder()
            .AddJsonFile("appSettings.json")
            .Build();
            string url = Configuration.GetSection("appSettings:LogFilePath").Value;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
                .MinimumLevel.Override("System", Serilog.Events.LogEventLevel.Warning)
                //.ReadFrom.Configuration(configuration)
                .WriteTo.File(url + DateTime.Now.ToString("yyyy-MM-dd") + "_IVClog.txt")
                .WriteTo.File(new RenderedCompactJsonFormatter(), url + DateTime.Now.ToString("yyyy-MM-dd") +"_log.json")
                .CreateLogger();
            try
            {
                Log.Information("Application Starting Up");
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The application faied to start correctly");
            }

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseSerilog()
            .UseStartup<Startup>();
    }
}