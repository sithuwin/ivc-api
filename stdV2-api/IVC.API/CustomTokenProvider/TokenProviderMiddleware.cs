using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using IVC.API;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using IVC.API.ChatAPI;
namespace CustomTokenAuthProvider
{
    public class TokenProviderMiddleware : IMiddleware
    {
        private IRepositoryWrapper _repository;
        private readonly TokenProviderOptions _options;
        private readonly JsonSerializerSettings _serializerSettings;
        public static IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            return Task.FromResult(new ClaimsIdentity(new GenericIdentity(username, "Token"), new Claim[] { }));
        }
        public TokenProviderMiddleware(IHttpContextAccessor httpContextAccessor, IRepositoryWrapper repository)
        {
            _httpContextAccessor = httpContextAccessor;
            _repository = repository;
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            double expiretimespan = Convert.ToDouble(Configuration.GetSection("TokenAuthentication:TokenExpire").Value);
            TimeSpan expiration = TimeSpan.FromMinutes(expiretimespan);
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));

            _options = new TokenProviderOptions
            {
                Path = Configuration.GetSection("TokenAuthentication:TokenPath").Value,
                Audience = Configuration.GetSection("TokenAuthentication:Audience").Value,
                Issuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity,
                Expiration = expiration
            };
        }
        public async Task ResponseMessage(dynamic data, HttpContext context, int code = 400)
        {
            var response = new
            {
                success = data.success,
                message = data.message,
                data = data.data
            };
            context.Response.StatusCode = code;
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(JsonConvert.SerializeObject(response, _serializerSettings));
        }
        private async Task GenerateToken(HttpContext context)
        {
            LoginDataModel loginData = new LoginDataModel();
            string username = "";
            string password = "";
            string _loginType = "";
            string deviceToken = "";
            string showPrefix = "";
            string mobileDeviceToken = "";
            string deviceType = "";
            try
            {
                showPrefix = _repository.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                using (var reader = new System.IO.StreamReader(context.Request.Body))
                {
                    var request_body = reader.ReadToEnd();
                    loginData = JsonConvert.DeserializeObject<LoginDataModel>(request_body, _serializerSettings);
                    if (loginData.username == null) loginData.username = "";
                    if (loginData.password == null) loginData.password = "";
                    if (loginData.LoginType == null) loginData.LoginType = "";
                    username = loginData.username;
                    password = loginData.password;
                    _loginType = loginData.LoginType;
                    deviceToken = loginData.deviceToken;
                    mobileDeviceToken = loginData.mobileDeviceId;
                    deviceType = loginData.deviceType;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now.ToString() + " - " + ex.ToString());
            }

            string ipaddress = "127.0.0.2";
            // set ipaddress
            if (context.Connection.RemoteIpAddress != null) ipaddress = context.Connection.RemoteIpAddress.ToString();

            string clienturl = context.Request.Headers["Referer"];

            dynamic result = null;
            string LoginID = "";
            string LoginName = "";
            string Email = "";
            string UserType = "";
            string FullName = "";
            string firstName = "";
            string lastName = "";
            string Role = "";
            string ChatId = "";
            string chatAuthToken = "";
            if (_loginType == "1")
            {
                result = doAdminTypeloginValidation(username, password, clienturl, ipaddress);
                if (result != null)
                {
                    LoginID = result.admin_id.ToString();
                    LoginName = result.admin_username;
                    Email = "";
                    FullName = result.admin_name;
                    Role = "Organizer";
                    ChatId = result.ChatID;
                    //if (!string.IsNullOrEmpty(ChatId)) chatAuthToken = UserAPI.CreateUserAuthToken(ChatId);
                   
                }
            }

            if (_loginType == "0")
            {
                result = doUserTypeloginValidation(username, password, clienturl, ipaddress, deviceToken, mobileDeviceToken, deviceType);
                if (result != null)
                {
                    string lblocker = result.LoginBlocker;
                    string val = _repository.AccountLogicBlocker.FindByCondition(x => (x.ID.ToString() == lblocker 
                    || x.LoginBlocker == lblocker) && x.deleteFlag == false).Select(x => x.LoginBlocker).FirstOrDefault();
                    if (String.IsNullOrEmpty(val))
                    {
                        LoginID = result.a_ID;
                        LoginName = result.a_LoginName;
                        Email = result.a_email;
                        FullName = result.a_fullname;
                        firstName = result.a_fname;
                        lastName = result.a_lname;
                        string accounttype = result.a_type;
                        UserType = (accounttype == null ? "V" : accounttype);
                        ChatId = result.ChatID;
                        //if (!string.IsNullOrEmpty(ChatId)) chatAuthToken = UserAPI.CreateUserAuthToken(ChatId);
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                        string msg = result.Message;
                        await context.Response.WriteAsync(msg);
                        return;
                    }
                    if (UserType == IVC.DAL.Util.UserType.ContactPerson || UserType == IVC.DAL.Util.UserType.MainExhibitor)
                    {
                        Role = "Exhibitor";
                    }
                    else
                    {
                        Role = "Visitor";
                    }
                }
            }
            // if (_loginType == "2") {
            //     result = doSpeakerTypeloginValidation (username, password, clienturl, ipaddress);
            //     if (result != null) {
            //         LoginID = result.s_ID;
            //         LoginName = result.LoginName;
            //         Email = result.s_email;
            //         UserType = 1;
            //     }
            // }
            if (result == null)
            {
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync("Invalid username or password.");
                return;
            }


            string userID = LoginID;
            var now = DateTime.UtcNow;
            var _tokenData = new TokenData();
            _tokenData.Sub = LoginName;
            _tokenData.Jti = await _options.NonceGenerator();
            _tokenData.Iat = new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString();
            _tokenData.UserID = userID;
            _tokenData.Userlevelid = ""; //lt[0].AdminLevelID.ToString();
            _tokenData.LoginType = _loginType.ToString();
            _tokenData.TicketExpireDate = now.Add(_options.Expiration);

            var encodedJwt = GlobalFunction.GetToken(_tokenData, Role);
            SignalHub loginSignal = new SignalHub();
            await loginSignal.AddUserIntoSignalConnection(userID);

            var response = new
            {
                success = true,
                message = "successfully",
                data =
                new
                {
                    access_token = encodedJwt,
                    expires_in = (int)_options.Expiration.TotalSeconds,
                    UserID = userID,
                    //LoginType = _loginType.ToString(),
                    displayName = LoginName,
                    //Email = Email,
                    UserType = UserType,
                    showPrefix = showPrefix,
                    fullName = FullName,
                    firstName = firstName,
                    lastName = lastName,
                    chatId = ChatId,
                    chatUserAuthToken = chatAuthToken
                }
            };

            // Serialize and return the response
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(JsonConvert.SerializeObject(response, _serializerSettings));
        }
        dynamic doAdminTypeloginValidation(string username, string password, string clienturl, string ipaddress)
        {
            tbl_Administrator admin = _repository.Admin.FindByCondition(x => x.admin_username == username && x.admin_password == password && x.admin_isdeleted != true).FirstOrDefault();
            //tbl_Administrator admin = _repository.Admin.FindByCondition(x => x.admin_username == username && x.admin_isdeleted != true).FirstOrDefault();
            //tbl_Administrator admin = new tbl_Administrator ();
            if (admin == null)
            {
                return null;
            }
            //C@rpitADMIN123

            string LoginUserID = admin.admin_id.ToString();
            _session.SetString("LoginUserID", LoginUserID);
            _session.SetString("LoginRemoteIpAddress", ipaddress);
            _session.SetString("LoginTypeParam", "1");

            // var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            // var Configuration = appsettingbuilder.Build();
            // string salt = Configuration.GetSection("SaltKey").Value;

            // string oldhash = admin.admin_password;
            // string oldsalt = salt;
            // bool flag = Operational.Encrypt.SaltedHash.Verify(oldsalt, oldhash, password);
            // if (flag == false)
            // {
            //     return null;
            // }
            return admin;
        }
        dynamic doUserTypeloginValidation(string username, string password, string clienturl, string ipaddress, string deviceToken, string mobileDeviceToken, string deviceType)
        {

            try
            {
                tbl_Account user = _repository.Account.FindByCondition(x => x.a_LoginName == username && x.deleteFlag == false).FirstOrDefault();
                //   tbl_Account user = _repository.Account.FindByCondition (x => x.a_LoginName.Contains (username) && x.a_Password.Contains (password)).FirstOrDefault ();
                if (user == null)
                {
                    return null;
                }

                var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var Configuration = appsettingbuilder.Build();
                string salt = Configuration.GetSection("SaltKey").Value;

                string oldhash = user.a_Password;
                string oldsalt = salt;
                bool flag = Operational.Encrypt.SaltedHash.Verify(oldsalt, oldhash, password);
                if (flag == false)
                {
                    return null;
                }

                if (!String.IsNullOrEmpty(user.LoginBlocker))
                {
                    string blockRuleName = user.LoginBlocker;
                    tblAccountLoginBlocker blockRule = _repository.AccountLogicBlocker.FindByCondition(x => x.ID.ToString() == blockRuleName && x.deleteFlag == false 
                    && x.Message != null && x.Message != "").FirstOrDefault();
                    if (blockRule != null)
                    {
                        return blockRule;
                    }
                }

                if (deviceToken != "" && deviceToken != null)
                {

                    user.DeviceId = deviceToken;
                    _repository.Account.Update(user);
                    _repository.Save();
                }
                if (mobileDeviceToken != "" && mobileDeviceToken != null)
                {

                    user.MobileDeviceId = mobileDeviceToken;
                    _repository.Account.Update(user);
                    _repository.Save();
                }

                string LoginUserID = user.a_ID.ToString();
                _session.SetString("LoginUserID", LoginUserID);
                _session.SetString("LoginRemoteIpAddress", ipaddress);
                _session.SetString("LoginTypeParam", "0");


                string logmessage = " User is login ";
                GlobalFunction.SaveAuditLog(_repository, AuditLogType.login, logmessage, LoginUserID, deviceType, "GenerateToken", ipaddress);
                return user;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());

                return null;
            }
        }

        dynamic doSpeakerTypeloginValidation(string username, string password, string clienturl, string ipaddress)
        {

            try
            {
                tbl_Speaker user = _repository.Speaker.FindByCondition(x => x.LoginName == username && x.Password == password).FirstOrDefault();
                //tbl_Account user = new tbl_Account ();
                if (user == null)
                {
                    return null;
                }

                string LoginUserID = user.s_ID.ToString();
                _session.SetString("LoginUserID", LoginUserID);
                _session.SetString("LoginRemoteIpAddress", ipaddress);
                _session.SetString("LoginTypeParam", "0");

                // var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
                // var Configuration = appsettingbuilder.Build ();
                // string salt = Configuration.GetSection ("SaltKey").Value;

                // string oldhash = user.a_Password;
                // string oldsalt = salt;
                // bool flag = Operational.Encrypt.SaltedHash.Verify (oldsalt, oldhash, password);
                // if (flag == false) {
                //     return null;
                // }

                return user;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());

                return null;
            }
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            string ipaddress = "127.0.0.2";
            if (context.Connection.RemoteIpAddress != null) ipaddress = context.Connection.RemoteIpAddress.ToString();
            _session.SetString("LoginUserID", "0");
            _session.SetString("LoginRemoteIpAddress", ipaddress);
            _session.SetString("LoginTypeParam", "1");

            TokenData _tokenData = null;
            var access_token = "";
            var hdtoken = context.Request.Headers["Authorization"];
            if (hdtoken.Count > 0)
            {
                access_token = hdtoken[0];
                access_token = access_token.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var tokenS = handler.ReadToken(access_token) as JwtSecurityToken;
                _tokenData = GlobalFunction.GetTokenData(tokenS);
            }
            else
            {
                //TODO for some
            }
            //  _objdb = DB;
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                //Regenerate newtoken for not timeout at running
                string newToken = "";
                try
                {
                    var pathstr = context.Request.Path.ToString();
                    string[] patharr = pathstr.Split('/');
                    int prequest = Array.IndexOf(patharr, "PublicRequest");
                    int wrequest = Array.IndexOf(patharr, "WindowServiceRequest");
                    if (prequest < 1 && wrequest < 1)
                    {
                        if (patharr[2] != null)
                        {
                            if (patharr[2].ToString().ToLower() == "public" ||
                                patharr[2].ToString().ToLower() == "resetpassword" || patharr[2].ToString().ToLower() == "signalhub")
                            {
                                await next(context);
                            }
                            else
                            {
                                var handler = new JwtSecurityTokenHandler();
                                var tokenS = handler.ReadToken(access_token) as JwtSecurityToken;
                                var allow = false;

                                allow = true;
                                if (allow)
                                {
                                    // check token expired   
                                    double expireTime = Convert.ToDouble(_options.Expiration.TotalMinutes);
                                    DateTime issueDate = _tokenData.TicketExpireDate.AddMinutes(-expireTime);
                                    DateTime NowDate = DateTime.UtcNow;
                                    if (issueDate > NowDate || _tokenData.TicketExpireDate < NowDate)
                                    {
                                        // return "-2";
                                        newToken = "-2";
                                    }
                                    else
                                    {
                                        var now = DateTime.UtcNow;
                                        _tokenData.Jti = new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString();
                                        _tokenData.Jti = await _options.NonceGenerator();
                                        newToken = GlobalFunction.GetToken(_tokenData, "");
                                    }
                                    // end of token expired check

                                }
                                else
                                    newToken = "-1";
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "TokenProviderMiddleware", ex.Message.ToString(), "");
                }

                if (newToken == "-1")
                {
                    // _repository.EventLog.Info("TokenProviderMiddleWare", "Not include Authorization Header, Access Denied");
                    context.Response.StatusCode = 400;
                    await ResponseMessage(new { success = false, message = "Access Denied", data = "" }, context, 400);
                }
                else if (newToken == "-2")
                {
                    context.Response.StatusCode = 401;
                    //new { success = false, message = "The Token has expired", data = "" }
                    await ResponseMessage(new { success = false, message = "The Token has expired", data = "" }, context, 401);
                    //await ResponseMessage(new { status = 0, messages = "The Token has expired" }, context, 400);
                }
                else if (newToken != "")
                {
                    context.Response.Headers.Add("Access-Control-Expose-Headers", "newToken");
                    context.Response.Headers.Add("newToken", newToken);
                    //context.Succeed(requirement);
                    await next(context);
                }

            }
            else
            {
                await GenerateToken(context);
            }

        }
    }
}