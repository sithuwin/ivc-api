using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ParticipantSummaryReportController : BaseController
    {

        private IRepositoryWrapper _repositoryWrapper;
        public ParticipantSummaryReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}")]
        public dynamic GetparticipantsSummaryReport([FromBody] JObject param, String id)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "UserName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == id && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    GlobalFunction.DataSyn(_repositoryWrapper, conference, _tokenData.UserID);
                    var objZoomP = _repositoryWrapper.ZoomParticipant.FindByCondition(x => x.WebinarId == id).Select(x => x.WebinarId).FirstOrDefault();
                    if (objZoomP != null)
                    {
                        var objResult = _repositoryWrapper.ZoomParticipant.GetParticipantsSummaryByWebinarId(obj, sortField, sortBy, id, false);
                        objResponse = objResult;
                        result = true;
                    }
                }
                else
                {
                    message = "Conference does not exist."; result = false;
                    objResponse = new { data = "", dataFoundRowsCount = "", result = result, message = message };
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ParticipantSummaryReportController - GetparticipantsSummaryReport", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{id}")]
        public dynamic DataSynchronization(String id)
        {
            dynamic objResponse = null;
            string msg = "Failed to update.";
            bool result = false;
            try
            {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == id && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    GlobalFunction.DataSyn(_repositoryWrapper,conference,_tokenData.UserID);
                    msg = "Successfully updated.";
                    result = true;
                }else{
                    msg = "Conference does not exist.";
                    result = false;
                }
                objResponse = new { data = "", success = result, message = msg }; 
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ParticipantSummaryReportController - DataSynchronization", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to update." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}/Report")]
        public dynamic ExportParticipantSummaryReport([FromBody] JObject param, String id)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "UserName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.ZoomParticipant.GetParticipantsSummaryByWebinarId(obj, sortField, sortBy, id, true);
                List<string> Header = new List<string> { "UserId", "UserName", "Email", "JobTitle", "Country", "JoinCount" };
                List<string> fieldName = new List<string> { "UserId", "UserName", "Email", "JobTitle", "Country", "JoinCount" };
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "ParticipantSummaryReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ParticipantSummaryReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ParticipantSummaryReportController - ExportParticipantSummaryReport", ex.Message.ToString(), _tokenData.UserID);
                // objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

       
       
    }
}