using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NetworkingReport : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public NetworkingReport(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }
        [Authorize(Roles = "Organizer")]
        [HttpPost]
        public dynamic GetNewtworkingReport([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic obj = param;

            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "UserName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.FriendShip.GetNetworkingReport(obj, sortField, sortBy, false);
                objResponse = objResult;
                return StatusCode(StatusCodes.Status200OK, objResponse);

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "GetNewtworkingReport - GetNewtworkingReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        [Authorize(Roles = "Organizer")]
        [HttpPost("Report")]
        public dynamic ExportNetworkingReport([FromBody] JObject param)
        {
            dynamic obj = param;
            byte[] fileContents;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "UserName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.FriendShip.GetNetworkingReport(obj, sortField, sortBy, true);
                List<string> Header = new List<string> { "UserName", "TotalExchange", "TotalAccepted", "TotalRejectedRequest" };
                List<string> fieldName = new List<string> { "UserName", "TotalExchange", "TotalAccepted", "TotalRejectedRequest" };
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "NetworkingReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    // objResponse = new { data = "", success = false, message = "data does not have" };
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"NetworkingReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "GetNewtworkingReport - ExportNetworkingReport", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

    }
}