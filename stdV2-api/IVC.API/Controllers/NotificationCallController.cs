using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationCallController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public NotificationCallController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GetNotificationlist([FromBody] JObject param)
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "datetime";
                if (sortBy == null || sortBy == "")
                    sortBy = "desc";
                dynamic data = _repositoryWrapper.NotificationSendList.GetNotificationSendList(_tokenData.UserID, obj, sortField, sortBy);

                objResponse = data;
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NotificationCallController - GetNotificationlist", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet]
        public dynamic NotificationCount()
        {
            int count = 0;
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                count = _repositoryWrapper.NotificationSendList.FindByCondition(x => x.UserId == _tokenData.UserID && x.ViewFlag == false).Count();
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = count, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NotificationCallController - NotificationCount", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ReadNotification")]
        public dynamic ReadNotification()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                _repositoryWrapper.NotificationSendList.UpdateNotification(_tokenData.UserID);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = result, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NotificationCallController - ReadNotification", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}