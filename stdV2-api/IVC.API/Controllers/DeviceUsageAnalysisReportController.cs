using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using EASendMail;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DeviceUsageAnalysisReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public DeviceUsageAnalysisReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GetDeviceUsageAnalysisReport([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "Country";
                if (sortBy == null || sortBy == "")
                    sortBy = "desc";
                dynamic data = _repositoryWrapper.Account.GetDeviceUsageReport(obj, sortField, sortBy, false);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, result = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "DeviceUsageAnalysisReportController - GetDeviceUsageAnalysisReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("Report")]
        public dynamic ExportDeviceUsageAnalysisReport([FromBody] JObject param)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "Country";
                if (sortBy == null || sortBy == "")
                    sortBy = "desc";
                dynamic data = _repositoryWrapper.Account.GetDeviceUsageReport(obj, sortField, sortBy, true);
                List<string> Header = new List<string> { "Country", "Window", "Android", "IOS" };
                List<string> fieldName = new List<string> { "Country", "Window", "Android", "IOS" };
                fileContents = GlobalFunction.ExportExcel(data, Header, fieldName, "DeviceUsageAnalysisReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    // objResponse = new { data = "", success = false, message = "data does not have" };
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"DeviceUsageAnalysisReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "DeviceUsageAnalysisReportController - ExportDeviceUsageAnalysisReport", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}