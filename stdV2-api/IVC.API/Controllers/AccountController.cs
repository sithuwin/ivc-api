using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using EASendMail;
using IVC.API.ChatAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public AccountController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GenerateUser([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                string accId = obj.accId;
                string email = obj.email;
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == accId && x.deleteFlag == false).FirstOrDefault();
                int checkValidate = _repositoryWrapper.Account.CheckValidation(accId, email);
                if (checkValidate == 0)
                {
                    if (account != null)
                    {

                        message = "Failed to update";
                        UpdateUser(account, obj);
                        message = "Successfully updated.";
                        result = true;
                        GlobalFunction.WriteSystemLog(LogType.Info, "AccountController - GenerateUser", "(UpdateUser)AccountId: " + accId, _tokenData.UserID);
                    }
                    else
                    {
                        message = "Failed to insert";
                        SaveNewUser(obj);
                        message = "Successfully inserted.";
                        result = true;
                        GlobalFunction.WriteSystemLog(LogType.Info, "AccountController - GenerateUser", "(SaveNewUser)AccountId: " + accId, _tokenData.UserID);
                    }
                }
                else if (checkValidate == 1)
                {
                    message = "Email already exists.";
                    GlobalFunction.WriteSystemLog(LogType.Info, "AccountController - GenerateUser", "Email already exists: " + email, _tokenData.UserID);
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - GenerateUser", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("PasswordGenerate")]
        public dynamic UpdatePassword()
        {
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                bool isOk = _repositoryWrapper.Account.ChangePasword();
                if (isOk)
                {
                    message = "Successfully Updated DB";
                }
                else
                {
                    message = "Can't change update";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - UpdatePassword", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private void SaveNewUser(dynamic obj)
        {
            tbl_Account account = new tbl_Account();
            account.a_ID = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.AccountRunKey);
            account.a_fullname = obj.firstName + obj.lastName;
            account.a_type = obj.accountType;
            //account.a_sal = obj.
            account.a_fname = obj.firstName;
            account.a_lname = obj.lastName;
            account.a_designation = obj.designation;
            account.a_email = obj.email;
            account.a_company = obj.company;
            account.a_addressOfc = obj.address1;
            account.a_addressHome = obj.address2;
            account.a_country = obj.country;
            account.a_Tel = obj.telephoneNo;
            account.a_Mobile = obj.mobileNo;
            account.a_LoginName = obj.loginName;
            string password = obj.password;
            account.a_Password = GlobalFunction.GenerateSalthKey(password);
            account.Biography = obj.biography;
            account.ProfilePic = obj.profilePic;
            account.lang = 1;
            account.deleteFlag = false;
            account.UpdatedDate = DateTime.Now;
            _repositoryWrapper.Account.Create(account);
            _repositoryWrapper.Save();

            _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.AccountRunKey);
        }
        private void UpdateUser(tbl_Account account, dynamic obj)
        {
            account.a_fullname = obj.firstName + obj.lastName;
            account.a_type = obj.accountType;
            account.a_fname = obj.firstName;
            account.a_lname = obj.lastName;
            account.a_designation = obj.designation;
            account.a_email = obj.email;
            account.a_company = obj.company;
            account.a_addressOfc = obj.address1;
            account.a_addressHome = obj.address2;
            account.a_country = obj.country;
            account.a_Tel = obj.telephoneNo;
            account.a_Mobile = obj.mobileNo;
            account.a_LoginName = obj.loginName;
            account.Biography = obj.biography;
            account.ProfilePic = obj.profilePic;
            account.lang = 1;
            account.UpdatedDate = DateTime.Now;
            account.deleteFlag = false;
            _repositoryWrapper.Account.Update(account);
            _repositoryWrapper.Save();
        }

        [HttpGet("{aId}/{password}")]
        public dynamic GetHashKey(string aId, string password)
        {
            dynamic objResponse = null;
            string message = "Failed to change";
            bool result = false;
            try
            {
                string data = GlobalFunction.GenerateSalthKey(password);
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == aId && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    account.a_Password = data;
                    _repositoryWrapper.Account.Update(account);
                    _repositoryWrapper.Save();
                    result = true;
                    message = "Successfully change salth key";
                }
                else
                {
                    message = "User does not exist";
                    objResponse = new { data = "", success = result, message = message };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }

                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);

            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - GetHashKey", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{type}")]
        public dynamic LogOut(string type)
        {
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            // IsValidEmail (type);
            try
            {
                string aId = _tokenData.UserID;
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == aId && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    string lMessage = "User is logout";
                    string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                    if (type == DeviceType.Web)
                    {
                        account.DeviceId = null;
                        GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.logout, lMessage, aId, DeviceType.Web, "LogOut", ip);
                    }
                    else if (type == DeviceType.Mobile)
                    {
                        account.MobileDeviceId = null;
                        GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.logout, lMessage, aId, DeviceType.Mobile, "LogOut", ip);
                    }
                    _repositoryWrapper.Account.Update(account);
                    //_repositoryWrapper.Save ();
                    result = true;
                    message = "Successfully reterieve";

                }
                else
                {
                    message = "User does not exist";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - LogOut", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("GenereateCometUserToken/{aId}")]
        public dynamic GenerateCometChatUserToken(string aId)
        {
            dynamic objResponse = null;
            try
            {
                string aa = UserAPI.CreateUserAuthToken(aId);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = false, message = "" };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - UpdateCometChatUser", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
            return objResponse;
        }

        [HttpPost("CometChatUser/{aId}")]
        public dynamic UpdateCometChatUser(string aId, [FromBody] JObject obj)
        {
            dynamic objResponse = null;
            string message = "Successfully updated.";
            bool result = true;
            string chatId = "";
            string name = "";
            dynamic param = obj;
            try
            {
                //check user in acc, if success from cometchat, update in tblAccount/Administrator
                tbl_Account user = _repositoryWrapper.Account.FindByCondition(x => x.deleteFlag == false && x.a_ID == aId).FirstOrDefault();
                tbl_Administrator adminObj = _repositoryWrapper.Admin.FindByCondition(x => x.admin_isdeleted == false && x.admin_id.ToString() == aId).FirstOrDefault();
                if (user == null && adminObj == null)
                {
                    message = "There is no active account for this id: " + aId;
                    result = false;
                }
                else
                {
                    dynamic metadata = new System.Dynamic.ExpandoObject();
                    if (user != null)
                    {
                        chatId = user.ChatID;
                        name = user.a_fullname;
                        //Additional information about the user
                        metadata.email = user.a_email;
                        metadata.firstName = user.a_fname;
                        metadata.lastName = user.a_lname;
                    }
                    if (adminObj != null)
                    {
                        chatId = adminObj.ChatID;
                        name = adminObj.admin_name;
                        // metadata.email = adminObj.admin_name;
                        metadata.firstName = adminObj.admin_name;
                    }
                    if (param != null && param.name != null)
                    {
                        string paraName = param.name;
                        if (!string.IsNullOrEmpty(paraName))
                        {
                            name = paraName;
                        }
                    }
                    if (string.IsNullOrEmpty(chatId))
                    {
                        string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                        chatId = showPrefix + "_" + aId;
                        chatId = chatId.ToLower();

                        if (UserAPI.CreateUser(name, chatId, metadata))
                        {
                            if (user != null)
                            {
                                //update chatId to tblAccount\Administrator
                                user.ChatID = chatId;
                                user.ModifiedDate = DateTime.Now;
                                _repositoryWrapper.Account.Update(user);
                                _repositoryWrapper.Save();
                            }
                            if (adminObj != null)
                            {
                                adminObj.ChatID = chatId;
                                _repositoryWrapper.Admin.Update(adminObj);
                                _repositoryWrapper.Save();
                            }
                        }
                        else
                        {
                            result = false;
                            message = "Failed to create a new user on Cometchat.";
                        }
                    }
                    else
                    {
                        if (!UserAPI.UpdateUser(name, chatId))
                        {
                            result = false;
                            message = "Failed to update a user on Cometchat.";
                        }
                    }
                }

                string authToken = "";
                if (param.isLogin)
                {
                    authToken = UserAPI.CreateUserAuthToken(chatId);
                }

                objResponse = new { data = chatId, success = result, message = message, cometToken = authToken };
                GlobalFunction.WriteSystemLog(LogType.Info, "AccountController - UpdateCometChatUser", message + ", ChatID: " + chatId + ", UserId: " + aId + ", Name: " + name, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - UpdateCometChatUser", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("CountryBreakDown")]
        public dynamic GetCountryBreakDown()
        {
            dynamic objResponse = null;
            try
            {
                dynamic data = _repositoryWrapper.Audit.GetCountryBreakDown();
                objResponse = new { data = data, success = true, message = "Successfully reterieve." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AccountController - GetCountryBreakDown", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        // private bool IsValidEmail (string email) {
        //     try {
        //         SmtpMail oMail = new SmtpMail ("TryIt");

        //         // Set sender email address, please change it to yours
        //         oMail.From = "winsithu.corpit@gmail.com";

        //         // Set recipient email address, please change it to yours
        //         oMail.To = email;

        //         // Do not set SMTP server address
        //         SmtpServer oServer = new SmtpServer ("");

        //         Console.WriteLine ("start to test email address ...");

        //         EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient ();
        //         oSmtp.TestRecipients (oServer, oMail);

        //         Console.WriteLine ("email address was verified!");
        //     } catch (Exception ep) {
        //         Console.WriteLine ("failed to test email with the following error:");
        //         Console.WriteLine (ep.Message);
        //     }
        //     return false;
        // }

    }
}