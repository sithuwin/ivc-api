using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AttandanceReportController : BaseController
    {

        private IRepositoryWrapper _repositoryWrapper;
        public AttandanceReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{cId}")]
        public dynamic GetAttandanceReport([FromBody] JObject param, string cId)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            string message = "";
            try
            {
                
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    GlobalFunction.DataSyn(_repositoryWrapper,conference,_tokenData.UserID);
                    var objZoomPar = _repositoryWrapper.ZoomParticipant.FindByCondition(x => x.WebinarId == cId).Select(x => x.WebinarId).FirstOrDefault();
                    if (objZoomPar != null)
                    {
                        var objResult = _repositoryWrapper.ZoomParticipant.GetAttandenceDetails(obj, sortField, sortBy, cId, false);
                        objResponse = objResult;
                    }
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
                else
                {
                    message = "There is no active conference";
                    objResponse = new { data = "", message = message };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AttandanceReportController - GetAttandanceReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{cId}/Report")]
        public dynamic ExportAttandanceReport([FromBody] JObject param, string cId)
        {
            dynamic obj = param;
            byte[] fileContents;
            dynamic objResponse = null;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                var objResult = _repositoryWrapper.ZoomParticipant.GetAttandenceDetails(obj, sortField, sortBy, cId, true);
                List<string> Header = new List<string> { "RegistrationId", "FirstName", "LastName", "Email", "JobTitle", "Country", "Company", "MobileNo", "Duration" };
                List<string> fieldName = new List<string> { "RegistrationId", "FirstName", "LastName", "Email", "JobTitle", "Country", "Company", "MobileNo", "Duration" };
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "AttandanceReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    // objResponse = new { data = "", success = false, message = "data does not have" };
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"AttandanceReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AttandanceReportController - ExportAttandanceReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    
        [Authorize(Roles = "Organizer")]
        [HttpGet("EventAttendance")]
        public dynamic GetEventAttendanceReport()
        {
            dynamic objResponse = null;
            try
            {
                var objResult = _repositoryWrapper.ZoomParticipant.GetEventAttendanceReport();
                objResponse = objResult;
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AttandanceReportController - GetEventAttendanceReport", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Registration")]
        public dynamic GetRegistrationListByDate([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic objResponse = null;
            dynamic objparam = param;
            try
            {
                objResponse = _repositoryWrapper.Account.GetRegistrationListByDate(objparam);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AttandanceReportController - GetRegistrationListByDate", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("RegistrationGroup")]
        public dynamic GetRegistrationGroupList()
        {
            dynamic objResponse = null;
            try
            {
                objResponse = _repositoryWrapper.Account.GetRegistrationGroup();
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "AttandanceReportController - GetRegistrationGroupList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
            return objResponse;
        }
    }
}