using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MaterialViewReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public MaterialViewReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{mId}")]
        public dynamic GetMaterialViewReport([FromBody] JObject param, string mId)
        {
            dynamic objResponse = null;
            dynamic obj = param;

            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.Material.MaterialViewReport(mId, obj, sortField, sortBy, false);
                objResponse = objResult;
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "MaterialViewReportController - GetMaterialViewReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve." };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }
        [Authorize(Roles = "Organizer")]
        [HttpGet("{mId}")]
        public dynamic GetMaterialViewReportByCounty(string mId)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.Material.MaterialViewReportByCounty(mId);
                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "MaterialViewReportController - GetMaterialViewReportByCounty", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        [Authorize(Roles = "Organizer")]
        [HttpPost("{mId}/Report")]
        public dynamic ExportMaterialViewReport([FromBody] JObject param, string mId)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.Material.MaterialViewReport(mId, obj, sortField, sortBy, true);
                List<string> Header = new List<string> { "FirstName", "LastName", "Company", "JobTitle", "Country", "Email", "Mobile", "RegistrationId", };
                List<string> fieldName = new List<string> { "FirstName", "LastName", "Company", "JobTitle", "Country", "Email", "MobileNo", "RegistrationId" };
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "MaterialViewReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"MaterialView_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "MaterialViewReportController - ExportMaterialViewReport", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [Authorize(Roles = "Organizer")]
        [HttpGet("{mId}/Report")]
        public dynamic ExportMaterialViewReportByCountry(string mId)
        {
            byte[] fileContents;
            try
            {
                dynamic data = _repositoryWrapper.Material.MaterialViewReportByCounty(mId);
                List<string> Header = new List<string> { "Country", "ViewCount" };
                List<string> fieldName = new List<string> { "Country", "ViewCount" };
                fileContents = GlobalFunction.ExportExcel(data, Header, fieldName, "MaterialViewByCountryReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    // objResponse = new { data = "", success = false, message = "data does not have" };
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"MaterialViewByCountryReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "MaterialViewReportController - ExportMaterialViewReportByCountry", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}