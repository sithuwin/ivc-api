using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json.Linq;
using IVC.API.ChatAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class HelpDeskController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IHubContext<SignalHub> _hubContext;
        public HelpDeskController(IRepositoryWrapper RW, IHubContext<SignalHub> hubContext)
        {
            _repositoryWrapper = RW;
            _hubContext = hubContext;
        }

        [HttpPost("Issues")]
        public dynamic CreateIsssue([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "Failed to post.";
            bool result = false;
            try
            {
                SaveNewIssue(obj);
                message = "Successfully posted.";
                result = true;
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Info, "HelpDeskController - CreateIsssue", message, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - CreateIsssue", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [Authorize(Roles = "Exhibitor")]
        [HttpPost("IssueList")]
        public dynamic GetAllIssues([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            dynamic obj = param;
            try
            {
                dynamic data = _repositoryWrapper.HelpDesk.GetAllIssueForUser(_tokenData.UserID, obj);
                message = "Successfully reterieved.";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - GetAllIssues", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("IssueList")]
        public dynamic GetAllIssues()
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.HelpDesk.GetAllIssueByUserId(_tokenData.UserID);
                message = "Successfully reterieved.";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - GetAllIssues", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Issues/{issueId}")]
        public async Task<dynamic> GetIssueById(string issueId)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.HelpDesk.GetIssueById(issueId, _tokenData.UserID);
                UpdateReadFalg(issueId);
                GlobalFunction.WriteSystemLog(LogType.Info, "HelpDeskController - GetIssueById", "UpdateReadFalg, IssueID:" + issueId, _tokenData.UserID);
                await _hubContext.Clients.All.SendAsync("HelpDeskAnsCount", "Check Help Desk");
                message = "Successfully reterieved.";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - GetIssueById", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Issues/{issueId}/Reply")]
        public async Task<dynamic> ReplyIssueByAdmin([FromBody] JObject param, string issueId)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "Failed to reply issue.";
            bool result = false;
            try
            {
                tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issueId && x.Status == Status.Queu).FirstOrDefault();
                if (helpDesk != null)
                {
                    ReplyForIssue(obj, helpDesk);
                    await _hubContext.Clients.All.SendAsync("HelpDeskAnsCount", "Check Help Desk");
                    message = "Successfully reply for this issue.";
                    result = true;
                }
                
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - ReplyIssueByAdmin", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("Issues/{issueid}/Chats")]
        public dynamic ChatAction(string issueid)
        {
            dynamic objResponse = null;
            string message = "Failed to request.";
            bool result = false;
            try
            {
                SaveNewHelpChatLog(issueid);
                message = "Successfully request.";
                result = true;
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - ChatAction", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Issues/{issueId}/Chats/{chatId}/{type}")]
        public async Task<dynamic> UpdateChatAction(string issueId, int chatId, string type)
        {
            dynamic objResponse = null;
            string message = "Failed to update.";
            bool result = false;
            try
            {
                tblHelpChatLog helpChatLog = _repositoryWrapper.HelpChatLog.FindByCondition(x => x.IssueID == issueId && x.HelpChatLogID == chatId).FirstOrDefault();
                if (helpChatLog != null)
                {
                    if (type == Status.Open)
                    {
                        bool isOk = CreateGroupForHelpDesk(issueId);
                        if (isOk)
                        {
                            UpdateChatLogToOpen(helpChatLog);
                            message = "Chat opened successfully.";
                            result = true;
                            await _hubContext.Clients.All.SendAsync("HelpDeskLiveChatStatus", "Check Your Live Status");
                        }
                        else
                        {
                            message = "Failed to open chat.";
                        }
                    }
                    else if (type == Status.Close)
                    {
                        UpdateChatLogToClose(helpChatLog);
                        await _hubContext.Clients.All.SendAsync("HelpDeskLiveChatStatus", "Check Your Live Status");
                        message = "Chat successfully closed.";
                        result = true;
                    }
                    else
                    {
                        message = "Type does not exist.";
                        objResponse = new { data = "", success = result, message = message };
                        return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                    }

                    objResponse = new { data = "", success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
                else
                {
                    message = "Chatlog does not exist.";
                    objResponse = new { data = "", success = result, message = message };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - UpdateChatAction", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("Issues/Organization")]
        public dynamic GetAllIssuesForOrganization([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            bool result = false;
            string message = "";
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "ModifiedDate";
                if (sortBy == null || sortBy == "")
                    sortBy = "desc";
                dynamic data = _repositoryWrapper.HelpDesk.GetAllIssueForOrg(sortField, sortBy, obj);
                objResponse = data;
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - GetAllIssuesForOrganization", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Issues/JoinChat")]
        public dynamic JoinLiveChatChannel()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to Join.";
            bool isOK = false;
            try
            {
                string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();

                //tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issueId && x.SubmitterID == _tokenData.UserID).FirstOrDefault();
                //if (helpDesk != null)
                //{
                string gId = _tokenData.UserID.Replace("A", "I");
                isOK = GroupAPI.JoinMember(_tokenData.UserID, gId, UserType.Delegate, showPrefix);
                if (isOK)
                {
                    result = true;
                    message = "Successfully joined.";
                }
                else
                {
                    message = "You don't have permission to join";
                }
                // }
                // else
                // {
                //     message = "u dont have permission to join";
                // }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - JoinLiveChatChannel", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpGet("Issues/{issueId}/JoinAdminChat")]
        public dynamic JoinLiveChatChannelByAdmin(string issueId)
        {

            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to Join.";
            bool isOK = false;
            try
            {
                string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();

                tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issueId).FirstOrDefault();
                if (helpDesk != null)
                {
                    string gId = helpDesk.SubmitterID.Replace("A", "I");
                    isOK = GroupAPI.JoinMember(_tokenData.UserID, gId, UserType.Organizer, showPrefix);
                    if (isOK)
                    {
                        result = true;
                        message = "Successfully joined.";
                    }
                    else
                    {
                        message = "You don't have permission to join.";
                    }
                }
                else
                {
                    message = "You don't have permission to join.";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - JoinLiveChatChannelByAdmin", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpGet]
        public dynamic GetUnReadIssueCount()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve.";
            try
            {
                int Count = _repositoryWrapper.HelpDesk.FindByCondition(x => x.SubmitterID == _tokenData.UserID && x.IsRead == false).Count();
                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = Count, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);

            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - GetUnReadIssueCount", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ChatStatus")]
        public dynamic CheckLiveChatStatus()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve.";
            try
            {
                bool chatStaus = _repositoryWrapper.HelpDesk.CheckLiveChatStatus(_tokenData.UserID);
                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = chatStaus, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "HelpDeskController - CheckLiveChatStatus", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private void UpdateChatLogToOpen(tblHelpChatLog helpChatLog)
        {
            helpChatLog.Status = Status.Open;
            _repositoryWrapper.HelpChatLog.Update(helpChatLog);
            _repositoryWrapper.Save();
        }
        private void UpdateChatLogToClose(tblHelpChatLog helpChatLog)
        {
            helpChatLog.Status = Status.Close;
            helpChatLog.ClosedDate = DateTime.Now;
            _repositoryWrapper.HelpChatLog.Update(helpChatLog);
            _repositoryWrapper.Save();
        }
        private void SaveNewHelpChatLog(string issueId)
        {
            tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issueId).FirstOrDefault();
            if (helpDesk != null)
            {
                tblHelpChatLog helpChat = _repositoryWrapper.HelpChatLog.FindByCondition(x => x.IssueID == helpDesk.IssueID).FirstOrDefault();
                if (helpChat == null)
                {
                    tblHelpChatLog helpChatLog = new tblHelpChatLog();
                    helpChatLog.IssueID = issueId;
                    helpChatLog.Title = helpDesk.Title;
                    helpChatLog.Status = Status.Pending;
                    helpChatLog.OpenedDate = DateTime.Now;
                    _repositoryWrapper.HelpChatLog.Create(helpChatLog);
                    _repositoryWrapper.Save();
                }
                else
                {
                    if (helpChat.Status == Status.Pending || helpChat.Status == Status.Close)
                    {
                        helpChat.Status = Status.Pending;
                        helpChat.OpenedDate = DateTime.Now;
                        _repositoryWrapper.HelpChatLog.Update(helpChat);
                        _repositoryWrapper.Save();
                    }
                }
            }

        }
        private void SaveNewIssue(dynamic obj)
        {
            string issuId = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.HelpDeskRunKey);
            tblHelpDesk helpDesk = new tblHelpDesk();
            helpDesk.IssueID = issuId;
            helpDesk.SubmitterID = _tokenData.UserID;
            //helpDesk.AssignedID = "";
            helpDesk.Title = obj.title;
            helpDesk.BugContent = obj.bugContent;
            helpDesk.Status = Status.Queu;
            helpDesk.SubmittedDate = DateTime.Now;
            helpDesk.IsRead = true;
            _repositoryWrapper.HelpDesk.Create(helpDesk);
            _repositoryWrapper.Save();
            _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.HelpDeskRunKey);

        }
        private void ReplyForIssue(dynamic obj, tblHelpDesk helpDesk)
        {
            helpDesk.ReplyContent = obj.replyContent;
            helpDesk.AssignedID = _tokenData.UserID;
            helpDesk.ReplyDate = DateTime.Now;
            helpDesk.Status = Status.Complete;
            helpDesk.IsRead = false;
            _repositoryWrapper.HelpDesk.Update(helpDesk);
            _repositoryWrapper.Save();
        }
        private bool CreateGroupForHelpDesk(string issueId)
        {
            bool isOk = false;

            tbl_Account account = _repositoryWrapper.HelpDesk.GetAccountByIssuId(issueId);
            if (account != null)
            {
                string gId = account.a_ID.Replace("A", "I");
                string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                isOk = GroupAPI.CreateGroup(gId, account.a_fullname + " - HelpDesk", showPrefix);
                if (isOk)
                {
                    isOk = JoinLiveChat(issueId);
                }
            }
            return isOk;
        }
        private void UpdateReadFalg(string issuId)
        {
            tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issuId && x.IsRead == false).FirstOrDefault();
            if (helpDesk != null)
            {
                helpDesk.IsRead = true;
                _repositoryWrapper.HelpDesk.Update(helpDesk);
                _repositoryWrapper.Save();
            }
        }

        private bool JoinLiveChat(string issueId)
        {
            bool isOK = false;
            bool adminJoin = false;
            bool userJoin = false;
            string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();

            tblHelpDesk helpDesk = _repositoryWrapper.HelpDesk.FindByCondition(x => x.IssueID == issueId).FirstOrDefault();
            if (helpDesk != null)
            {
                string gId = helpDesk.SubmitterID.Replace("A", "I");
                adminJoin = GroupAPI.JoinMember(helpDesk.SubmitterID, gId, UserType.Delegate, showPrefix);
                userJoin = GroupAPI.JoinMember(_tokenData.UserID, gId, UserType.Organizer, showPrefix);
                if (adminJoin == true && userJoin == true)
                {
                    isOK = true;
                }
            }
            return isOK;

        }

    }
}