using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EASendMail;
using IVC.API.ChatAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class NetworkLoungeController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IHubContext<SignalHub> _hubContext;
        public NetworkLoungeController(IRepositoryWrapper RW, IHubContext<SignalHub> hubContext)
        {
            _repositoryWrapper = RW;
            _hubContext = hubContext;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost]
        public dynamic GetAllNetworkLounge([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            dynamic obj = param;
            try
            {
                dynamic data = _repositoryWrapper.NetworkLounge.SelectAllNetworkLounge(obj);
                if(data == null){
                    objResponse = new { data = "", success = result, message = message };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }
                return StatusCode(StatusCodes.Status200OK, data);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - GetAllNetworkLounge", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("PublishedNetworkLounge")]
        public dynamic GetAllPublishedNetworkLounge()
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                string userid = _tokenData.UserID;
                dynamic data = _repositoryWrapper.NetworkLounge.SelectAllPublishedNetworkLounge();

                //Updated AuditLog for visit to the network lounge
                string logMessage = "Visitor Visited Network Lounge.";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.VisitNetworkLounge, logMessage, userid, null, "GetAllPublishedNetworkLounge", ip);

                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - GetAllPublishedNetworkLounge", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{ID}")]
        public dynamic CreateorUpdateNetworkLounge([FromBody] JObject param, string ID)
        {
            dynamic objResponse = null;
            string message = "Failed to update.";
            bool result = false;
            dynamic obj = param;
            string seqNo = null;
            try
            {
                seqNo = obj.seqNo;
                var objSeq = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID != ID && x.deleteFlag == false && x.nl_seq.ToString() == seqNo && seqNo != null).Select(x => x.nl_ID).FirstOrDefault();
                if(objSeq != null){
                   message = "Duplicate sequence No.";
                }else{
                    tbl_NetworkLounge networkLounge = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID == ID && x.deleteFlag == false).FirstOrDefault();
                    if (networkLounge != null)
                    {
                        //Update
                        _repositoryWrapper.NetworkLounge.UpdateNetworkLounge(obj, networkLounge);
                        result = true;
                        message = "Successfully updated.";
                    }
                    else
                    {
                        //Save
                        ID = _repositoryWrapper.SiteRunNumber.GetNewId(SiteRunNumbers.NetworkLoungeRunKey);
                        _repositoryWrapper.NetworkLounge.CreateNetworkLounge(obj, ID);
                        _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey(SiteRunNumbers.NetworkLoungeRunKey);
                        result = true;
                        message = "Successfully updated.";
                    }    
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Info, "NetworkLoungeController - CreateorUpdateNetworkLounge", message + ", Id:"+ ID, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - CreateorUpdateNetworkLounge", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{ID}")]
        public dynamic GetNetworkLoungeById(string ID)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.NetworkLounge.SelectNetworkLoungeById(ID);
                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - GetNetworkLoungeById", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{ID}/ChangeStatus/{Status}")]
        public async Task<dynamic> UpdateNewtorkLoungeStatusById(string ID, string Status)
        {
            dynamic objResponse = null;
            string msg = "Successfully updated.";
            try
            {
                tbl_NetworkLounge networkLounge = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID == ID && x.deleteFlag == false).FirstOrDefault();
                if (networkLounge != null)
                {
                    _repositoryWrapper.NetworkLounge.UpdateNetworkLoungeStatuByID(ID, Status, networkLounge);
                    await _hubContext.Clients.All.SendAsync("SponserLoungeStatus", "Check SponserLounge Conference");

                }
                else
                {
                    msg = "No data to update.";
                }
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "NetworkLoungeController - UpdateNewtorkLoungeStatusById", msg +", Id:"+ ID + ", Status:"+ Status, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - UpdateNewtorkLoungeStatusById", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to Updated" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpDelete("{ID}")]
        public dynamic DeleteNetworkLoungeByID(string ID)
        {
            dynamic objResponse = null;
            string msg = "Successfully Deleted.";
            try
            {
                tbl_NetworkLounge networkLounge = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID == ID && x.deleteFlag == false).FirstOrDefault();
                if (networkLounge != null)
                {
                    _repositoryWrapper.NetworkLounge.DeleteNetworkLoungeByID(ID, networkLounge);
                }
                else
                {
                    msg = "No data to delete.";
                }
                objResponse = new { data = "", success = true, message = msg };
                GlobalFunction.WriteSystemLog(LogType.Info, "NetworkLoungeController - DeleteNetworkLoungeByID", msg, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "NetworkLoungeController - DeleteNetworkLoungeByID", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }
}