using System;
using System.Linq;
using System.Text;
using System.Threading;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResetPasswordController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IDataProtector _protector;
        public ResetPasswordController(IRepositoryWrapper RW, IDataProtectionProvider provider)
        {
            _repositoryWrapper = RW;
            _protector = provider.CreateProtector("test");
        }

        [HttpPost]
        public dynamic SendResetPasswordlink([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to send.";
            bool result = false;
            dynamic obj = param;
            try
            {
                string currentDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");

                string email = obj.email;
                string link = obj.link;
                var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var Configuration = appsettingbuilder.Build();

                //string link = Configuration.GetSection("ResetPassswordUrl:ResetLink").Value;
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_email == email && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    string accId = GlobalFunction.Base64Encode(account.a_LoginName);
                    string cDate = GlobalFunction.Base64Encode(currentDateTime);
                    string linkParam = accId + "$@$" + cDate;
                    //string elink = link + HttpUtility.UrlEncode(accId);
                    string elink = link + System.Net.WebUtility.UrlEncode(linkParam);
                    dynamic emailSetting = new System.Dynamic.ExpandoObject();
                    emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    emailSetting.toEmail = account.a_email;
                    emailSetting.receiverUserName = account.a_fullname;
                    emailSetting.senderUserName = Configuration.GetSection("ResetPassword:SenderUserName").Value;
                    emailSetting.subject = Configuration.GetSection("ResetPassword:Subject").Value;
                    string emessage = Configuration.GetSection("ResetPassword:Message").Value;
                    emailSetting.emessage = GetEmailBody(emessage, account, elink);
                    GlobalFunction.SendEmail(emailSetting);
                    result = true;
                    message = "Successfully sent.";
                }
                else
                {
                    message = "User does not exist.";
                    objResponse = new { data = "", success = result, message = message };
                    GlobalFunction.WriteSystemLog(LogType.Info, "ResetPasswordController - SendResetPasswordlink", message , _tokenData.UserID);
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }

                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Info, "ResetPasswordController - SendResetPasswordlink", message , _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "ResetPasswordController - SendResetPasswordlink", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private string GetEmailBody(string message, tbl_Account account, string link)
        {
            StringBuilder emessage = new StringBuilder(message);
            emessage.Replace("<UserName>", account.a_fullname);
            emessage.Replace("<link>", link);
            return emessage.ToString();
        }

        [HttpPost("Reset")]
        public dynamic ChangePassword([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to change.";
            bool result = false;
            dynamic obj = param;
            try
            {
                string loginName = obj.loginName;
                string paramData = System.Net.WebUtility.UrlDecode(loginName);
                string newLoginName = GlobalFunction.Base64Decode(paramData.Split("$@$")[0]);
                string password = obj.password;
                // string newLoginName = GlobalFunction.Base64Decode (loginName);
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_LoginName == newLoginName && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    string resetDate = GlobalFunction.Base64Decode(paramData.Split("$@$")[1]);
                    bool checkResetTime = CheckSessionParam(resetDate);
                    if (checkResetTime)
                    {
                        string newPassword = GlobalFunction.GenerateSalthKey(password);
                        account.a_Password = newPassword;
                        _repositoryWrapper.Account.Update(account);
                        _repositoryWrapper.Save();
                        result = true;
                        message = "Successfully change password";
                    }
                    else
                    {
                        message = "SessionEnd";
                    }
                }
                else
                {
                    message = "User does not exist";
                    objResponse = new { data = "", success = result, message = message };
                    GlobalFunction.WriteSystemLog(LogType.Info, "ResetPasswordController - ChangePassword", message + ", LoginName:" + newLoginName, _tokenData.UserID);
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ResetPasswordController - ChangePassword", message + ", LoginName:" + newLoginName, _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "ResetPasswordController - ChangePassword", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("Session/{data}")]
        public dynamic CheckResetSessionTime(string data)
        {
            dynamic objResponse = null;
            string message = "Url Session End.";
            bool result = false;
            string newLoginName = "";
            try
            {
                try
                {
                    string paramData = System.Net.WebUtility.UrlDecode(data);
                    newLoginName = GlobalFunction.Base64Decode(paramData.Split("$@$")[0]);
                    tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_LoginName == newLoginName && x.deleteFlag == false).FirstOrDefault();
                    if (account != null)
                    {
                        string resetDate = GlobalFunction.Base64Decode(paramData.Split("$@$")[1]);
                        // checking reset session still has or not 
                        bool checkResetTime = CheckSessionParam(resetDate);
                        if (checkResetTime)
                        {
                            message = "Session not end yet.";
                        }
                        result = checkResetTime;
                    }
                    else
                    {
                        message = "User doesn't exists.";
                    }
                }
                catch (Exception ex)
                {
                    message = "Url parameter is wrong.";
                    GlobalFunction.WriteSystemLog(LogType.Error, "ResetPasswordController - CheckResetSessionTime", ex.Message.ToString(), _tokenData.UserID);
                }
                GlobalFunction.WriteSystemLog(LogType.Info, "ResetPasswordController - CheckResetSessionTime", message + " User:" + newLoginName, _tokenData.UserID);
                objResponse = new { data = result, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "ResetPasswordController - GenerateZoomToken", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private bool CheckSessionParam(string resetDate)
        {
            bool result = false;

            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            // Session expire minute 
            string sessionMinute = Configuration.GetSection("appSettings:ResetPasswordSession").Value;

            DateTime rDate = Convert.ToDateTime(resetDate).AddMinutes(Convert.ToInt32(sessionMinute));
            DateTime currentDate = DateTime.Now;
            if (currentDate <= rDate)
            {
                result = true;
            }
            return result;
        }
    }
}