using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using EASendMail;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
using IVC.API.ChatAPI;
using IVC.API.ZoomAPI;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public TestController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic NewEmailGrantAccess([FromBody] JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                string cId = obj.cId;
                string type = obj.type;
                dynamic user = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.WebinarId == cId && x.Status == 1 && x.AccountType == type).Select(x => x.UserId).ToArray();
                objResponse = new { data = user, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "TestController - NewEmailGrantAccess", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{cid}/ZoomToken")]
        public dynamic GenerateZoomToken(string cid)
        {
            dynamic objResponse = null;
            try
            {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
                if (confrence != null)
                {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;

                    if (!String.IsNullOrEmpty(confrence.RoomId))
                    {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                    ZoomToken.ConfigKeys = configKeys;
                }
                ZoomToken.GenerateZoomToken();
                string tokenString = ZoomToken.ZoomTokenString;
                dynamic data = tokenString;
                objResponse = new { data = data, success = true, message = "" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = false, message = "" };
                GlobalFunction.WriteSystemLog(LogType.Error, "TestController - GenerateZoomToken", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}