using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using EASendMail;
using IVC.API.ChatAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers {
    [Authorize]
    [ApiController]
    [Route ("api/[controller]")]
    public class ActiveUserDetailReportController : BaseController {

        private IRepositoryWrapper _repositoryWrapper;
        public ActiveUserDetailReportController (IRepositoryWrapper RW) {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GetActiveUserDetail ([FromBody] JObject param) {
            dynamic objResponse = null;

            dynamic obj = param;
            try {

                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0) {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                dynamic data = _repositoryWrapper.Audit.GetActiveUserDetail (sortField, sortBy, obj,false);
                objResponse = data;
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ActiveUserDetailReportController - ExportCountrySummaryReport", ex.Message.ToString (), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to reterieve" };
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("Report")]
        public dynamic ExportCountrySummaryReport ([FromBody] JObject param) {
            byte[] fileContents;
            dynamic obj = param;
            try {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0) {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                var objResult =  _repositoryWrapper.Audit.GetActiveUserDetail (sortField, sortBy, obj,true);
                List<string> Header = new List<string> { "Name", "Email", "UserType" ,"CountryName"};
                List<string> fieldName = new List<string> { "a_fullname", "a_email", "a_type","CountryName" };

                fileContents = GlobalFunction.ExportExcel (objResult, Header, fieldName, "ActiveUserDetailReport");
                if (fileContents == null || fileContents.Length == 0) {
                    return NotFound ();
                }

                return File (
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"ActiveUserDetailReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ActiveUserDetailReportController - ExportCountrySummaryReport", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError);
            }
        }
    }
}