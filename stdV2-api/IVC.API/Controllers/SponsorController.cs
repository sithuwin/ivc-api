using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using EASendMail;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
using System.Threading.Tasks;
using IVC.API.ChatAPI;

namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SponsorController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public SponsorController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpGet("{ID}/ChatStart")]
        public dynamic SponsorChatGroup(string ID)
        {
            bool result = false;
            string message = "";
            dynamic response = null;
            bool isOk = false;

            try
            {
                tbl_NetworkLounge networkLounge = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID == ID && x.deleteFlag == false).FirstOrDefault();
                if (networkLounge != null)
                {

                    string groupID = ID;
                    string groupName = networkLounge.nl_Name;
                    string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    isOk = GroupAPI.CreateGroup(groupID, groupName, showPrefix);
                    message = "Chat channel started!";
                    GlobalFunction.WriteSystemLog(LogType.Info, "SponsorController - SponsorChatGroup", "Chat Group is created: GroupID:" +ID + "GroupName:"+groupName, _tokenData.UserID);
                }
                else
                {
                    message = "There is no data to create chat group.";
                    GlobalFunction.WriteSystemLog(LogType.Info, "SponsorController - SponsorChatGroup", "No data to create chat group:" +ID, _tokenData.UserID);
                }
                response = new { message = message, success = isOk, data = "" };
                return StatusCode(StatusCodes.Status200OK, response);

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorController - SponsorChatGroup", ex.Message.ToString(), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet("{ID}/JoinChat")]
        public dynamic JoinChatChannel(string ID)
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to Join.";
            bool isOK = false;
            try
            {
                tbl_NetworkLounge networkLounge = _repositoryWrapper.NetworkLounge.FindByCondition(x => x.nl_ID == ID && x.deleteFlag == false).FirstOrDefault();
                if (networkLounge != null)
                {
                    string groupID = ID;
                    string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    isOK = GroupAPI.JoinMember(_tokenData.UserID, groupID, UserType.Delegate, showPrefix);
                }
                if (isOK)
                {
                    result = true;
                    message = "Successfully join.";
                    GlobalFunction.WriteSystemLog(LogType.Info, "SponsorController - JoinChatChannel", "Join successfully. UserID:"+_tokenData.UserID +", GroupID:"+ID, _tokenData.UserID);
                }
                else
                {
                    message = "You don't have permission to join.";
                    GlobalFunction.WriteSystemLog(LogType.Info, "SponsorController - JoinChatChannel", "Don't have permission to join. UserID:"+_tokenData.UserID +", GroupID:"+ID, _tokenData.UserID);
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "SponsorController - JoinChatChannel", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }
    }
}