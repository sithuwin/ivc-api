using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class GroupChatAnalysisReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public GroupChatAnalysisReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{cId}")]
        public dynamic GetGroupChatAnalysisReport([FromBody] JObject param, string cId)
        {
            dynamic objResponse = null;
            dynamic obj = param;
            string message = "";
            bool result = false;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    var objResult = _repositoryWrapper.ConferenceUser.GetGroupChatAnalysisReport(cId, sortField, sortBy, obj, false);
                    objResponse = objResult;
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
                else
                {
                    message = "There is no active conference";
                    objResponse = new { data = "", message = message, result };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "GroupChatAnalysisReportController - GetGroupChatAnalysisReport", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{cId}/Report")]
        public dynamic ExportGroupChatAnalysisReport([FromBody] JObject param, string cId)
        {

            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "FirstName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.ConferenceUser.GetGroupChatAnalysisReport(cId, sortField, sortBy, obj, true);
                List<string> Header = new List<string> { "FirstName", "LastName", "Company", "JobTitle", "Country", "Email", "Mobile", "RegistrationId" };
                List<string> fieldName = new List<string> { "FirstName", "LastName", "Company", "JobTitle", "Country", "Email", "Mobile", "RegistrationId" };
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "AttandanceReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    // objResponse = new { data = "", success = false, message = "data does not have" };
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"AttandanceReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "GroupChatAnalysisReportController - ExportGroupChatAnalysisReport", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}