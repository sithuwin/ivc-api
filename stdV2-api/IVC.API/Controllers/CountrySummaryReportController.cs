using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CountrySummaryReportController : BaseController
    {

        private IRepositoryWrapper _repositoryWrapper;
        public CountrySummaryReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}")]
        public dynamic GetCountrySummaryReport([FromBody] JObject param, String id)
        {

            dynamic obj = param;
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "Country";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == id && x.deleteFlag == false).FirstOrDefault();
                if (conference == null)
                {
                    message = "Conference does not exist";
                    objResponse = new { data = "", dataFoundRowsCount = "", result = result, message = message };
                }
                else
                {
                    GlobalFunction.DataSyn(_repositoryWrapper,conference,_tokenData.UserID);
                    var objResult = _repositoryWrapper.ZoomParticipant.GetCountrySummary(obj, sortField, sortBy, id, false);
                    objResponse = objResult;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "CountrySummaryReportController - GetCountrySummaryReport", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;

        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}/Report")]
        public dynamic ExportCountrySummaryReport([FromBody] JObject param, String id)
        {
            byte[] fileContents;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "Country";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                var objResult = _repositoryWrapper.ZoomParticipant.GetCountrySummary(obj, sortField, sortBy, id, true);
                List<string> Header = new List<string> { "Country", "RegisterCount", "AttendanceCount" };
                List<string> fieldName = new List<string> { "Country", "RegisterCount", "AttendanceCount" };

                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "CountrySummaryReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"CountrySummaryReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "CountrySummaryReportController - ExportCountrySummaryReport", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
    }
}