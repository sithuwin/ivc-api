using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.BLL.Util;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ConfigurationController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public ConfigurationController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpGet]
        public dynamic GetConfigurationList()
        {
            return _repositoryWrapper.ZoomConfiguration.GetConfigurationList();
        }
    }
}