using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using IVC.BLL.Util;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class VCardController : BaseController
    {
        string returnUrl = "";
        private IRepositoryWrapper _repositoryWrapper;

        public VCardController(IRepositoryWrapper RW)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:SpearkerUrl").Value;
            _repositoryWrapper = RW;
        }

        [HttpPost("{id}")]
        public dynamic CreateNameCardForSpeaker(string id)
        {
            string url = "";
            dynamic objResponse = null;
            string message = "Failed to create vcard";
            bool result = false;
            try
            {
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == id && x.deleteFlag == false && x.a_type == UserType.Speaker).FirstOrDefault();
                if (account != null)
                {
                    var vcard = AddDataToVCard(account);
                    url = GenerateVCardFile(vcard, UserType.Speaker);
                    SaveVCard(account, url);
                    message = "Successfully created vcard";
                    result = true;
                    objResponse = new { data = url, success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);

                }
                else
                {
                    message = "User doesn't exist";
                    objResponse = new { data = url, success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "VCardController - CreateNameCardForSpeaker", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpPost("Delegate/{id}")]
        public dynamic CreateNameCardForDelegate(string id)
        {
            string url = "";
            dynamic objResponse = null;
            string message = "Failed to create vcard";
            bool result = false;
            try
            {
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == id && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    // Adding userdata to VCard
                    var vcard = AddDataToVCard(account);
                    // Creating VCard
                    url = GenerateVCardFile(vcard, UserType.Delegate);
                    // Saving VCard to Database
                    SaveVCard(account, url);
                    message = "Successfully created vcard";
                    result = true;
                    objResponse = new { data = url, success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);

                }
                else
                {
                    message = "User doesn't exist";
                    objResponse = new { data = url, success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "VCardController - LogOut", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpPost("Visitor/{id}")]
        public dynamic CreateNameCardForVisitor(string id)
        {
            string url = "";
            dynamic objResponse = null;
            string message = "Failed to create vcard";
            bool result = false;
            try
            {
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == id && x.deleteFlag == false && x.a_type == UserType.Visitor).FirstOrDefault();
                if (account != null)
                {
                    // Adding userdata to VCard
                    var vcard = AddDataToVCard(account);
                    // Creating VCard
                    url = GenerateVCardFile(vcard, UserType.Delegate);
                    // Saving VCard to Database
                    SaveVCard(account, url);
                    message = "Successfully created vcard";
                    result = true;
                    objResponse = new { data = "", success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);

                }
                else
                {
                    message = "User doesn't exist";
                    objResponse = new { data = url, success = result, message = message };
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "VCardController - CreateNameCardForDelegate", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpGet("{id}")]
        public dynamic GetVCardById(string id)
        {

            dynamic objResponse = null;

            string message = "Failed to get vcard";
            bool result = false;
            try
            {
                string url = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == id && x.deleteFlag == false).Select(x => x.VCard).FirstOrDefault();
                if (url == null)
                {
                    tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == id && x.deleteFlag == false).FirstOrDefault();
                    if (account != null)
                    {
                        var vcard = AddDataToVCard(account);
                        url = GenerateVCardFile(vcard, UserType.Delegate);
                        SaveVCard(account, url);
                    }
                }
                message = "Successfully reterieve vcard";
                result = true;
                string logMessage = "User download VCard";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.DownloadVCard, logMessage, _tokenData.UserID, id, "GetVCardById", ip);
                objResponse = new { data = url, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "VCardController - GetVCardById", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ShareVCard")]
        public dynamic SharingVCard()
        {
            dynamic objResponse = null;
            string message = "";
            bool result = false;
            try
            {
                string userId = _tokenData.UserID;
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();//&& (x.a_type == UserType.Delegate || x.a_type == UserType.Speaker)
                if (account != null)
                {
                    if (account.ShareVCard == 1)
                    {
                        message = "Failed to close sharing VCard";
                        account.ShareVCard = 0;
                        _repositoryWrapper.Account.Update(account);
                        _repositoryWrapper.Save();
                        result = true;
                        message = "Successfully close sharing VCard";
                    }
                    else
                    {
                        message = "Failed to share VCard";
                        account.ShareVCard = 1;
                        _repositoryWrapper.Account.Update(account);
                        _repositoryWrapper.Save();
                        result = true;
                        message = "Successfully share VCard";
                    }
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "VCardController - SharingVCard", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        private VCard AddDataToVCard(tbl_Account account)
        {
            var vcard = new VCard();
            vcard.FirstName = account.a_fname;
            vcard.LastName = account.a_lname;
            vcard.Email = account.a_email;
            vcard.City = "yangon";
            vcard.CountryName = account.a_country;
            vcard.Phone = account.a_Tel;
            vcard.Mobile = account.a_Mobile;
            //vcard.Organization = account.;
            vcard.JobTitle = account.a_designation;
            if (account.ProfilePic != null && account.ProfilePic != "")
            {
                string image = returnUrl + account.ProfilePic;
                using (var webClient = new WebClient())
                {
                    //byte[] imageBytes = webClient.DownloadData (image);
                    vcard.Image = webClient.DownloadData(image);
                }
            }
            return vcard;
        }
        private string GenerateVCardFile(VCard vCard, string userType)
        {
            string url;
            var fileName = vCard.GetFullName() + ".vcf";
            var disposition = "attachment; filename=" + fileName;
            string path = System.IO.Directory.GetCurrentDirectory() + "\\wwwroot\\VCards\\" + userType + "\\";
            string returnUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                using (var vCardFile = System.IO.File.OpenWrite(path + fileName))
                using (var swWriter = new System.IO.StreamWriter(vCardFile))
                {
                    swWriter.Write(vCard.ToString());
                }
            }
            else
            {
                using (var vCardFile = System.IO.File.OpenWrite(path + fileName))
                using (var swWriter = new System.IO.StreamWriter(vCardFile))
                {
                    swWriter.Write(vCard.ToString());
                }
            }
            url = returnUrl + "/VCard/" + userType + "/" + fileName;
            return url;
        }
        private void SaveVCard(tbl_Account account, string url)
        {
            account.VCard = url;
            _repositoryWrapper.Account.Update(account);
            _repositoryWrapper.Save();
        }
    }
}