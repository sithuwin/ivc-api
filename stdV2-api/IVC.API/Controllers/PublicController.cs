using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
using IVC.DAL.Models;
using IVC.API.ZoomAPI;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;

namespace IVC.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PublicController : BaseController
    {
        string returnUrl;
        private IRepositoryWrapper _repositoryWrapper;
        private IMemoryCache _memoryCache;
        public PublicController(IRepositoryWrapper RW,IMemoryCache memoryCache)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:SpearkerUrl").Value;
            _repositoryWrapper = RW;
            _memoryCache = memoryCache;
        }

        [HttpPost("Conferences")]
        public dynamic GetAllConferenceList([FromBody] Newtonsoft.Json.Linq.JObject param) //[FromBody] Newtonsoft.Json.Linq.JObject param 
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                var objResult = _repositoryWrapper.Confrence.GetAllWebinarListForUser("publish", obj, returnUrl);
                objResponse = new { data = objResult };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetAllConferenceList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ConferenceCategory")]
        public dynamic GetConferenceCateList()
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.ConferenceCategory.GetConferenceCategorylist();
                result = true;
                message = "Successfully reterieved";
                objResponse = new { data = data, success = result, message = message };

                Log.Information("Getting all conference category by userid={ID}", _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetConferenceCateList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Speakers")]
        public dynamic GetSpeakerList()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                dynamic data = _repositoryWrapper.Account.GetSpeakerList(returnUrl);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetSpeakerList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Speaker/{id}")]
        public dynamic GetSpeakerById(string id)
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                dynamic data = _repositoryWrapper.Account.GetSpeakerDetailsById(returnUrl, id);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetSpeakerById", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Date")]
        public dynamic GetConferenceDateList()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                dynamic data = _repositoryWrapper.Day.GetConferenceDatelist();
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetConferenceDatelist", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpGet("LiveConference")]
        public dynamic GetCurrentConferceDetails()
        {
            try
            {
                var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var Configuration = appsettingbuilder.Build();
                dynamic objResponse = new System.Dynamic.ExpandoObject();
                // objResponse.meetingName = Configuration.GetSection ("currentConference:meetingName").Value;
                objResponse.meetingName = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.category = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.Category && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.fromDate = _repositoryWrapper.Day.GetMinimunDate();
                objResponse.toDate = _repositoryWrapper.Day.GetMaximumDate();
                objResponse.country = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.Country && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.link = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.Link && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.lobby = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.Lobby && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.floorPlan = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.FloorPlan && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse.showValue = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowValue && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetCurrentConferceDetails", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, "");
            }
        }

        [HttpGet("{sId}")]
        public dynamic GetConferencelistBySpeakerId(string sId)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.ConferenceUser.GetConferenceListbySpeakerId(sId, returnUrl);
                result = true;
                message = "Successfully reterieved";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetConferencelistBySpeakerId", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        // [HttpGet("{userId}/EmbeddedJoin/{cId}")]
        // public dynamic GetComferenceForEmbeddedJoin(string userId, string cId)
        // {
        //     dynamic objResponse = null;
        //     string message = "Failed to reterieve";
        //     string returnUrl = "";
        //     string apiKey = "";
        //     string signatureEndpoint = "";
        //     bool result = false;
        //     try
        //     {
        //         dynamic conferenceData = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cId).FirstOrDefault();
        //         dynamic expendConfObj = new ExpandedObjectFromApi(conferenceData);
        //         dynamic userData = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId).FirstOrDefault();
        //         dynamic expendUserObj = new ExpandedObjectFromApi(userData);
        //         string ZoomId = "";
        //         if (conferenceData != null)
        //         {
        //             int ConfigId = conferenceData.ConfigId;
        //             ZoomId = conferenceData.ZoomId;

        //             if (!String.IsNullOrEmpty(conferenceData.RoomId))
        //             {
        //                 string RoomId = conferenceData.RoomId;
        //                 tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
        //                 ConfigId = shareRoomConf.ConfigId;
        //                 ZoomId = shareRoomConf.ZoomId;
        //             }
        //             ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
        //             if (configKeys != null)
        //             {
        //                 apiKey = configKeys.APIKey;
        //             }
        //         }
        //         tblShowConfig returnUrlData = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == "EmbeddedReturnUrl" && x.Status == 1).FirstOrDefault();
        //         if (returnUrlData != null)
        //         {
        //             returnUrl = returnUrlData.Value;
        //         }

        //         tblShowConfig signatureEndPointData = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == "SignatureEndPoint" && x.Status == 1).FirstOrDefault();
        //         if (returnUrlData != null)
        //         {
        //             signatureEndpoint = signatureEndPointData.Value;
        //         }

        //         dynamic resultData = new { meetingNumber = ZoomId, password = expendConfObj.c_Password, userName = expendUserObj.a_fullname, userEmail = expendUserObj.a_email, returnUrl = returnUrl, apiKey = apiKey, signatureEndpoint = signatureEndpoint };
        //         result = true;
        //         message = "Successfully reterieve";
        //         objResponse = new { data = resultData, success = result, message = message };
        //         return StatusCode(StatusCodes.Status200OK, objResponse);
        //     }
        //     catch (Exception ex)
        //     {
        //         objResponse = new { data = "", success = result, message = message };
        //         GlobalFunction.WriteSystemLog("ConferenceController  -  GetMaterialListByConferenceId - " + ex.Message.ToString());
        //         Log.Error("ConferenceController  -  GetMaterialListByConferenceId  - ErrorMessage = {message}  and userid={ID}", ex.Message.ToString(), _tokenData.UserID);
        //         return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
        //     }
        // }

        [HttpPost("Conferences/GroupTemplate")]
        public dynamic GetAllConferenceListGroupByTime([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic paraObj = param;
            dynamic objResponse = null;
            dynamic objResult = null;
            try
            {
                string paraDate = _repositoryWrapper.Confrence.GetParaDate(paraObj);
                if (GlobalFunction.isRequireCreateCache())
                {
                    dynamic confe = new ExpandedObjectFromApi (ENUM_Cache.ConferenceList);
                    string key = confe.key + paraDate;
                    if (!_memoryCache.TryGetValue(key, out objResult))
                    {
                        objResult = _repositoryWrapper.Confrence.GetAllWebinarListGroupTemplate("publish", paraObj, returnUrl);
                        GlobalFunction.CacheTryGetValueSet(key, objResult, _memoryCache);
                    }
                }
                else
                {
                    objResult = _repositoryWrapper.Confrence.GetAllWebinarListGroupTemplate("publish", paraObj, returnUrl);
                }
                //var objResult = _repositoryWrapper.Confrence.GetAllWebinarListGroupByTime("publish", paraObj, returnUrl);
                objResponse = new { data = objResult };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetAllConferenceListGroupByTime", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("ZoomAuth")]
        public dynamic GetZomAuthToken([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            ConfigKeys configKeys = new ConfigKeys();

            try
            {
                configKeys.APIKey = obj.APIKey;
                configKeys.ScrectKey = obj.SecretKey;
                ZoomToken.ConfigKeys = configKeys;
                ZoomToken.GenerateZoomToken();
                string tokenString = ZoomToken.ZoomTokenString;
                objResponse = new { data = tokenString };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetZomAuthToken", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("CurrentZoomUser")]
        public dynamic GetCurrentZoomUser([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            ConfigKeys configKeys = new ConfigKeys();

            try
            {
                configKeys.APIKey = obj.APIKey;
                configKeys.ScrectKey = obj.SecretKey;
                ZoomToken.ConfigKeys = configKeys;

                string userId = UserAPI.GetUserId();
                objResponse = new { data = userId };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetCurrentZoomUser", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("News")]
        public dynamic GetNewsList()
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.News.GetNewslist();
                result = true;
                message = "Successfully reterieved";
                objResponse = new { data = data, success = result, message = message };

                Log.Information("Getting all News by userid={ID}", _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetNewsList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        [HttpGet("News/{ID}")]
        public dynamic GetNews(string ID)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try
            {
                dynamic data = _repositoryWrapper.News.GetNews(ID);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetNews", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ShowCategory")]
        public dynamic GetShowCategory()
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            try
            {
                dynamic data = _repositoryWrapper.Day.GetCategories();
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetShowCategory", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("Speakers")]
        public dynamic GetSpeakerListByCategory([FromBody] Newtonsoft.Json.Linq.JObject param)
        {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            dynamic obj = param;
            dynamic data = null;
            try
            {
                if (GlobalFunction.isRequireCreateCache ()){
                    dynamic speakerlist = new ExpandedObjectFromApi (ENUM_Cache.SpeakerList);
                    string key = speakerlist.key;
                    if(!_memoryCache.TryGetValue(key,out data)){
                        data = _repositoryWrapper.Account.GetSpeakerListByCategory(obj, returnUrl);
                        GlobalFunction.CacheTryGetValueSet (key, data, _memoryCache);
                    }
                }else{
                    data = _repositoryWrapper.Account.GetSpeakerListByCategory(obj, returnUrl);
                }
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetSpeakerListByCategory", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("ShowConfig/{Name}")]
        public dynamic GetShowconfigValueByName(string Name)
        {
            dynamic objResponse = null;
            string value = "";
            try
            {
                value = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name.ToLower() == Name.ToLower() && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                objResponse = new { data = value, success = true, message = "Successfully retrieve." };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetShowconfigValueByName", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("DeleteCache")]
        public dynamic ClearCache([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic paraObj = param;
            string key = paraObj.key;
            string msg = "Successfully removed.";
            dynamic data = null;
            try
            {
                dynamic confkey = new ExpandedObjectFromApi(ENUM_Cache.ConferenceList);
                if (key == confkey.key)
                {
                    dynamic days = _repositoryWrapper.Day.FindByCondition(x => x.deleteFlag == false).
                    Select(x => x.d_date).AsNoTracking().ToList();
                    foreach (var day in days)
                    {
                        if (_memoryCache.TryGetValue(key + day, out data))
                        {
                            GlobalFunction.CacheRemove(key + day, _memoryCache);
                        }
                    }
                }
                else
                {
                    if (_memoryCache.TryGetValue(key, out data))
                    {
                        GlobalFunction.CacheRemove(key, _memoryCache);
                    }
                }
                objResponse = new { data = key, success = true, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - ClearCache", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("RetrieveCache")]
        public dynamic RetrieveFromCache([FromBody] JObject param)
        {
            dynamic objResponse = null;
            dynamic paraObj = param;
            string key = paraObj.key;
            string msg = "Successfully retrieved.";
            dynamic data = null;
            bool result = true;
            try
            {
                if (!_memoryCache.TryGetValue(key, out data))
                {
                    data = null;
                }
                if(data == null){
                    msg = "There is no data to retrieve.";
                    result = false;
                }
                objResponse = new { data = data, success = result, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - RetrieveFromCache", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("MemoryCache/Keys")]
        public dynamic GetMemoryCacheKeys()
        {
            dynamic objResponse = null;
            string msg = "Successfully retrieved.";
            try
            {
                List<dynamic> result = new List<dynamic>();
                Type type = typeof(ENUM_Cache);
                foreach (var val in type.GetFields())
                {
                    Dictionary<string, dynamic> lst = new Dictionary<string, dynamic>();
                    dynamic data = val.GetValue(null);
                    lst.Add("name", val.Name);
                    lst.Add("value", data);
                    result.Add(lst);
                }
                objResponse = new { data = result, success = true, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - GetMemoryCacheKeys", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to retrieve" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete("DeleteSpeakerCache")]
        public dynamic ClearCacheforSpeakerData()
        {
            dynamic objResponse = null;
            string msg = "Successfully removed.";
            dynamic data = null;
            string key = "";
            try
            {
                //ConferenceData
                dynamic confkey = new ExpandedObjectFromApi(ENUM_Cache.ConferenceList);
                key = confkey.key;
                dynamic days = _repositoryWrapper.Day.FindByCondition(x => x.deleteFlag == false).
                   Select(x => x.d_date).AsNoTracking().ToList();
                foreach (var day in days)
                {
                    if (_memoryCache.TryGetValue(key + day, out data))
                    {
                        GlobalFunction.CacheRemove(key + day, _memoryCache);
                    }
                }
                dynamic speakerkey = new ExpandedObjectFromApi(ENUM_Cache.SpeakerList);
                key = speakerkey.key;
                data = null;
                if (_memoryCache.TryGetValue(key, out data))
                {
                    GlobalFunction.CacheRemove(key, _memoryCache);
                }
                objResponse = new { data = "", success = true, message = msg };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PublicController - ClearCacheforSpeakerData", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "failed to delete" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

    }

}