using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using IVC.API;
using IVC.API.ChatAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers {
    [Authorize]
    [ApiController]
    [Route ("api/[controller]")]
    public class FriendController : BaseController {
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IHubContext<SignalHub> _hubContext;
        public FriendController (IRepositoryWrapper RW, IHubContext<SignalHub> hubContext) {
            _repositoryWrapper = RW;
            _hubContext = hubContext;
        }

        [HttpPost]
        public dynamic GetRequestedList ([FromBody] JObject param) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            dynamic obj = param;
            try {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0) {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                dynamic data = _repositoryWrapper.FriendShip.GetRequestedDelegateList (_tokenData.UserID, obj, sortField, sortBy);
                // message = "Successfully reterieve";
                // result = true;
                objResponse = data;
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "FriendController - GetRequestedList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("Request")]
        public async Task<dynamic> AddFriendRequest ([FromBody] JObject param) {
            dynamic objResponse = null;
            string message = "Failed to Request Friend";
            dynamic obj = param;
            bool result = true;
            try {
                string id = obj.a_ID;
                bool data = false;
                string friendId = "";
                tbl_FriendShip friendShip = _repositoryWrapper.FriendShip.FindByCondition (x => (x.RequestUserId == id && x.ResponseUserId == _tokenData.UserID) || (x.RequestUserId == _tokenData.UserID && x.ResponseUserId == id)).FirstOrDefault ();
                if (friendShip != null) {
                    if (friendShip.RequestUserId == _tokenData.UserID) {
                        friendId = friendShip.ResponseUserId;
                    } else {
                        friendId = friendShip.RequestUserId;
                    }
                    friendShip.RequestUserId = _tokenData.UserID;
                    friendShip.ResponseUserId = friendId;
                    friendShip.Status = Status.Pending;
                    friendShip.RequestDate = System.DateTime.Now;
                    _repositoryWrapper.FriendShip.Update (friendShip);
                    _repositoryWrapper.Save ();
                    string emailStatus = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.FriendRequestStatus && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    if (emailStatus == "1") {
                        SendMailToUser (friendShip.RequestUserId, friendShip.ResponseUserId, EmailTitle.Fri_Request);
                    }
                    data = true;
                } else {
                    friendId = SaveFriendship (obj);
                }

                await SendNotification (friendId, NotificationTemplate.firendRequestNotification);
                //bool isSendOkay = await SendNotification (friendId, NotificationTemplate.firendRequestNotification);
                message = "Successfully Request Friend";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "FriendController - AddFriendRequest", "RequestID:" + _tokenData.UserID + ", ResponseID:" + friendId + ", Status:" + Status.Pending, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "FriendController - AddFriendRequest", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("Response")]
        public async Task<dynamic> ResponseFriendRequest ([FromBody] JObject param) {
            dynamic objResponse = null;
            string message = "Failed to Response Friend";
            dynamic obj = param;
            bool result = true;
            try {
                bool data = await UpdateFriendship (obj);
                if (data == true) {
                    message = "Successfully Response Friend";
                    result = true;
                } else {
                    message = "Failed to accept friend";
                }

                objResponse = new { data = data, success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "FriendController - ResponseFriendRequest", message + obj.a_ID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "FriendController - ResponseFriendRequest", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        // private async Task<dynamic> SendNotification (string friendId, string notificationId) {
        //     string image = "";
        //     List<string> users = new List<string> ();
        //     users.Add (friendId);
        //     tbl_Account account = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault ();
        //     if (account != null) {
        //         image = account.ProfilePic;
        //     }
        //     return await GlobalFunction.SendNotification (_repositoryWrapper, users, notificationId, _tokenData.UserID, image);
        // }

        private async Task<bool> SendNotification (string friendId, string notificationId) {
            string image = "";
            List<string> users = new List<string> ();
            users.Add (friendId);
            tbl_Account account = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault ();
            tblNotificationTemplate notificationTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == notificationId).FirstOrDefault ();
            if (account != null && notificationTemplate != null) {
                image = account.ProfilePic;
                string bodyMessage = notificationTemplate.BodyMessage;
                notificationTemplate.BodyMessage = GetNotificationBodyMessage (bodyMessage, account.a_fullname);
            }
            return await GlobalFunction.SendNotification (_repositoryWrapper, users, notificationTemplate, _tokenData.UserID, image, _hubContext);
        }
        private string SaveFriendship (dynamic obj) {
            string friendId = "";
            try {
                tbl_FriendShip friendShip = new tbl_FriendShip ();
                friendShip.RequestUserId = _tokenData.UserID;
                friendShip.ResponseUserId = obj.a_ID;
                friendShip.RequestDate = System.DateTime.Now;
                friendShip.Status = Status.Pending;

                _repositoryWrapper.FriendShip.Create (friendShip);
                _repositoryWrapper.Save ();
                friendId = obj.a_ID;
                string emailStatus = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.FriendRequestStatus && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                if (emailStatus == "1") {
                    SendMailToUser (friendShip.RequestUserId, friendShip.ResponseUserId, EmailTitle.Fri_Request);
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "FriendController - SaveFriendship", ex.Message.ToString (), _tokenData.UserID);
            }
            return friendId;
        }

        private async Task<bool> UpdateFriendship (dynamic obj) {
            bool result = false;
            try {
                string id = obj.a_ID;
                string status = obj.status.Value;
                bool isUpdate = true;
                tbl_FriendShip friendShip = _repositoryWrapper.FriendShip.FindByCondition (x => (x.RequestUserId == id && x.ResponseUserId == _tokenData.UserID) || (x.RequestUserId == _tokenData.UserID && x.ResponseUserId == id)).FirstOrDefault ();
                if (friendShip != null) {
                    if (status == Status.Reject) {
                        friendShip.Status = Status.Reject;
                    }

                    if (status == Status.Done) {
                        friendShip.Status = Status.Done;
                        string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                        isUpdate = FriendAPI.AddFriend (friendShip.RequestUserId, friendShip.ResponseUserId, showPrefix);
                        if (isUpdate == true) {
                            bool sendNotiResult = await SendNotification (id, NotificationTemplate.firendResponseNotification);
                        }
                    }

                    if (status == Status.Cancel) {
                        friendShip.Status = Status.Cancel;
                    }

                    if (status == Status.Pending) {
                        friendShip.Status = Status.Pending;
                    }

                    friendShip.ResponseDate = System.DateTime.Now;
                    _repositoryWrapper.FriendShip.Update (friendShip);
                    if (isUpdate) {
                        _repositoryWrapper.Save ();
                        result = true;
                        if (status == Status.Done) {
                            string emailStatus = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.FriendAcceptStatus && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                            if (emailStatus == "1") {
                                SendMailToUser (friendShip.RequestUserId, friendShip.ResponseUserId, EmailTitle.Fri_Accept);

                            }
                        }

                    }
                }

                // if (status == Status.Done)
                // {
                //     string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                //     dynamic addFriendResult = FriendAPI.AddFriend(friendShip.RequestUserId, friendShip.ResponseUserId, showPrefix);
                // }

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "FriendController - UpdateFriendship", ex.Message.ToString (), _tokenData.UserID);
            }
            return result;
        }

        [HttpPost ("MyConnection")]
        public dynamic GetMyConnectionList([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                dynamic data = _repositoryWrapper.FriendShip.GetMyConnection(_tokenData.UserID, obj, sortField, sortBy);
                objResponse = data;
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "FriendController - GetMyConnectionList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private string GetNotificationBodyMessage (string message, string userName) {
            StringBuilder emessage = new StringBuilder (message);
            emessage.Replace ("<UserName>", userName);
            return emessage.ToString ();
        }

        private void SendMailToUser (string requestUserId, string responseUserId, string emailType) {
            dynamic emailConfig = null;
            tbl_Account requestUserDetail = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == requestUserId && x.deleteFlag == false).FirstOrDefault ();
            tbl_Account responseUserDetail = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == responseUserId && x.deleteFlag == false).FirstOrDefault ();

            if (requestUserDetail != null && responseUserDetail != null) {

                if (emailType == EmailTitle.Fri_Request) {
                    emailConfig = _repositoryWrapper.EmailConfig.FindByCondition (x => x.em_Title == EmailTitle.Fri_Request).FirstOrDefault ();

                } else if (emailType == EmailTitle.Fri_Accept) {
                    emailConfig = _repositoryWrapper.EmailConfig.FindByCondition (x => x.em_Title == EmailTitle.Fri_Accept).FirstOrDefault ();
                }

                if (emailConfig != null) {
                    dynamic emailSetting = new System.Dynamic.ExpandoObject ();
                    string websiteLink = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.WebSiteLink && x.Status == 1).Select (x => x.Value).FirstOrDefault ();

                    string showName = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.senderUserName = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowAdminName && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select (x => x.Value).FirstOrDefault ();

                    string bodyMsg = replaceBodyMessage (emailConfig.em_Content);
                    emailSetting.subject = emailConfig.em_Subject;
                    if (emailType == EmailTitle.Fri_Request) {
                        emailSetting.receiverUserName = responseUserDetail.a_fullname;
                        emailSetting.emessage = GetEmailBody (bodyMsg, responseUserDetail.a_fullname, websiteLink);
                        emailSetting.toEmail = responseUserDetail.a_email;
                    } else if (emailType == EmailTitle.Fri_Accept) {
                        emailSetting.receiverUserName = requestUserDetail.a_fullname;
                        emailSetting.emessage = GetEmailBody (bodyMsg, requestUserDetail.a_fullname, websiteLink);
                        emailSetting.toEmail = requestUserDetail.a_email;
                    }

                    GlobalFunction.SendEmail (emailSetting, true);

                }

            }

        }

        private string replaceBodyMessage (string message) {
            message = message.Replace ("\\n", "\n");
            return message;
        }

        private string GetEmailBody (string message, string userName, string websiteLink) {
            StringBuilder emessage = new StringBuilder (message);
            emessage.Replace ("<UserName>", userName);
            emessage.Replace ("<websitelink>", websiteLink);
            return emessage.ToString ();
        }

    }
}