using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.BLL.Util;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using Serilog;
namespace IVC.API.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class RealTimeAttendanceReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public RealTimeAttendanceReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{cid}")]
        public dynamic GetRealTimeAttendanceList(string cid)
        {

            int registerCount = 0;
            dynamic objResponse = null;
            tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
            if (confrence != null)
            {

                registerCount = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.WebinarId == cid && x.Status == 1).Count();
                int ConfigId = confrence.ConfigId;
                string ZoomId = confrence.ZoomId;

                if (!String.IsNullOrEmpty(confrence.RoomId))
                {
                    string RoomId = confrence.RoomId;
                    tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                    ConfigId = shareRoomConf.ConfigId;
                    ZoomId = shareRoomConf.ZoomId;
                }
                ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                ZoomToken.ConfigKeys = configKeys;
                dynamic response = RealtimeAttandanceAPI.GetRealtimeAttandance(ZoomId, _tokenData.UserID);
                if (response != null)
                {
                    objResponse = new { data = response, registerCount = registerCount };
                }
                else
                {
                    objResponse = new { data = response, registerCount = registerCount };
                }
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{cid}/RealTimeGraph")]
        public dynamic RealTimeGraph(string cid)
        {
            dynamic objResponse = null; bool isLive = false;
            int regCount = 0; int participantCount = 0; int absentCount = 0;
            List<dynamic> result = new List<dynamic>();
            try
            {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
                if (confrence != null)
                {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;

                    if (!String.IsNullOrEmpty(confrence.RoomId))
                    {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                    ZoomToken.ConfigKeys = configKeys;

                    dynamic currentLiveResult = WebinarAPI.GetCurrentLiveWebinar();
                    var currentTempLiveResult = JsonConvert.DeserializeObject<LiveWebinarList>(currentLiveResult.ToString());
                    List<LiveWebinar> liveWebinarList = currentTempLiveResult.webinars;
                    isLive = liveWebinarList.Exists(x => x.id == ZoomId);
                    if (isLive)
                    {
                        regCount = _repositoryWrapper.Confrence.GetRegistrantCount(cid);
                        dynamic response = RealtimeAttandanceAPI.GetRealtimeAttandance(ZoomId, _tokenData.UserID);
                        participantCount = response.Count - 1;// doesn't count for event admin
                        if (regCount > participantCount)
                        {
                            absentCount = regCount - participantCount;
                        }
                        var objReg = new Dictionary<string, string>() { { "name", "Registrants" }, { "value", regCount.ToString() } };
                        result.Add(objReg);
                        var objPart = new Dictionary<string, string>() { { "name", "Participants" }, { "value", participantCount.ToString() } };
                        result.Add(objPart);
                        var objAbsent = new Dictionary<string, string>() { { "name", "Absent" }, { "value", absentCount.ToString() } };
                        result.Add(objAbsent);
                    }
                }
                objResponse = new { data = result };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RealTimeAttendanceReportController - RealTimeGraph", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{cid}/Report")]
        public IActionResult ExportRealTimeAttendanceList(string cid)
        {

            int registerCount = 0;
            dynamic objResponse = null;
            byte[] fileContents;
            try
            {

                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
                if (confrence != null)
                {

                    registerCount = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.WebinarId == cid && x.Status == 1).Count();
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;

                    if (!String.IsNullOrEmpty(confrence.RoomId))
                    {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                    ZoomToken.ConfigKeys = configKeys;
                    dynamic response = RealtimeAttandanceAPI.GetRealtimeAttandance(ZoomId, _tokenData.UserID);
                    int count = response.Count;
                    if (response.Count > 0)
                    {
                        objResponse = new { data = response, registerCount = registerCount };
                        List<string> Header = new List<string> { "Participant Name", "Device", "IP Address", "Location", "NetWork Type", "Join Time" };
                        List<string> fieldName = new List<string> { "user_name", "device", "ip_address", "location", "network_type", "join_time" };
                        fileContents = GlobalFunction.ExportExcel(response, Header, fieldName, "RealTimeReport");
                        if (fileContents == null || fileContents.Length == 0)
                        {
                            return NotFound();
                        }

                        return File(
                            fileContents: fileContents,
                            contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            fileDownloadName: $"RealTimeReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                        );
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status400BadRequest);
                    }
                }
                else
                {
                    objResponse = new { data = "", success = false, message = "conference does not exist" };
                    return StatusCode(StatusCodes.Status400BadRequest, objResponse);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RealTimeAttendanceReportController - ExportRealTimeAttendanceList", ex.Message.ToString(), _tokenData.UserID);
                objResponse = new { data = "", success = false, message = "Failed to export" };
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }

        }

    }
}