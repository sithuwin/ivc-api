using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Serilog;
namespace IVC.API.Controllers {
    [Authorize]
    [ApiController]
    [Route ("api/[controller]")]
    public class MaterialController : BaseController {
        private IRepositoryWrapper _repositoryWrapper;
        public MaterialController (IRepositoryWrapper RW) {
            _repositoryWrapper = RW;
        }

        [HttpGet ("{cId}")]
        public dynamic GetAuthrozieMaterialsList (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = true;
            try {
                dynamic data = _repositoryWrapper.Material.GetAllMaterials (_tokenData.UserID, cId);
                dynamic conferenceData = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).SingleOrDefault ();
                //return _repositoryWrapper.Material.FindByCondition(x => x.ConferenceId == cId && x.Status == 1).ToList();
                message = "Successfully reterieve.";
                result = true;
                objResponse = new { data = data, success = result, message = message, confData = conferenceData };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetAuthrozieMaterialsList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Materials/{cId}/Speaker")]
        public dynamic GetMaterialListBySpeaker (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = true;
            try {
                dynamic data = _repositoryWrapper.Material.GetMaterialListBySpeakerId (cId, _tokenData.UserID);
                string lMessage = "User view Materials of the conference";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                GlobalFunction.SaveAuditLog (_repositoryWrapper, AuditLogType.ViewMaterial, lMessage, _tokenData.UserID, cId, "GetMaterialListBySpeaker", ip);

                message = "Successfully reterieve.";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetMaterialListBySpeaker", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpGet ("{mId}/File")]
        public dynamic GetMaterialFileById (string mId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try {
                string data = _repositoryWrapper.Material.FindByCondition (x => x.MatId == mId && x.Status == 1).Select (x => x.Url).FirstOrDefault ();
                message = "Successfully reterieve";
                string lMessage = "User view Material";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                GlobalFunction.SaveAuditLog (_repositoryWrapper, AuditLogType.ViewMaterial, lMessage, _tokenData.UserID, mId, "GetMaterialFileById", ip);
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetMaterialFileById", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("{cId}/Organization")]
        public dynamic GetMaterialListForOrganization (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try {
                dynamic data = _repositoryWrapper.Material.GetMaterialListForOrganization (cId);
                message = "Successfully reterieve";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetMaterialListForOrganization", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        // [HttpGet("{cId}/Favourite")]
        // public dynamic GetFavouriteMaterial(string cId)
        // {
        //     return _repositoryWrapper.Material.FindByCondition(x => x.Status == 1 && x.ConferenceId == cId).ToList();//&& x.MaterialType == "P"
        // }

        [HttpGet ("{cId}/Favourite")]
        public dynamic GetMaterialListByConfIdAndUserId (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try {
                dynamic data = _repositoryWrapper.Material.GetFavouriteMaterialListByUserIdAndConderenceId (_tokenData.UserID, cId);
                message = "Successfully reterieve";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetMaterialListByConfIdAndUserId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Favourite")]
        public dynamic GetFavouriteMaterialListByUserId () {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try {
                dynamic data = _repositoryWrapper.Material.GetFavouriteMaterialListByUserId (_tokenData.UserID);
                message = "Successfully reterieve";
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - GetFavouriteMaterialListByUserId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost]
        public dynamic UploadMaterial ([FromBody] JObject param) {
            dynamic obj = param;
            dynamic objResponse = null;
            string message = "Failed to add.";
            bool result = false;
            try {
                string matId = obj.matId;
                tbl_Material material = _repositoryWrapper.Material.FindByCondition (x => x.MatId == matId && x.Status == 1).FirstOrDefault ();
                if (material != null) {
                    message = "Failed to  update.";
                    UpdateMaterial (obj, material);
                    result = true;
                    message = "Successfully updated.";
                } else {
                    message = "Failed to insert.";
                    matId = SaveNewMaterial (obj);
                    result = true;
                    message = "Successfully inserted.";
                }
                objResponse = new { data = matId, success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "MaterialController - UploadMaterial", message + ", MatId:" + matId, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - UploadMaterial", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("Upload/{id}")]
        public async Task<dynamic> UploadFile (IFormFile file, string id) {
            dynamic objResponse = null;
            string message = "Failed to upload";
            bool result = false;
            try {
                tbl_Material material = _repositoryWrapper.Material.FindByCondition (x => x.MatId == id && x.Status == 1).FirstOrDefault ();
                if (material != null) {
                    // if (material.Url != null) {
                    //     Deletefile (material.Url, material.ConferenceId);
                    // }
                    var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
                    var Configuration = appsettingbuilder.Build ();

                    // string materialPath = SaveFile (file, material.ConferenceId);
                    string materialPath = await SaveFileToS3 (file, material.ConferenceId, Configuration);
                    material.Url = materialPath;
                    _repositoryWrapper.Material.Update (material);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Successfully uploaded";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "MaterialController - UploadFile", message + ", MatId:" + id, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - UploadFile", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("Favourite")]
        public dynamic AddToMyFavourite ([FromBody] JObject param) {
            dynamic obj = param;
            string matId = obj.matId;
            dynamic objResponse = null;
            string message = "Failed to add.";
            bool result = false;
            try {
                tbl_UsersFavourite materialUser = _repositoryWrapper.UsersFavourite.FindByCondition (x => x.FavItemId == matId && x.UserId == _tokenData.UserID).FirstOrDefault ();
                if (materialUser != null) {
                    UpdateToFavourite (obj, materialUser);
                    message = "Successfully add to favourite.";
                    result = true;
                } else {
                    SaveToFavourite (obj);
                    message = "Successfully add to favourite.";
                    result = true;
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "MaterialController - AddToMyFavourite", message + ", FavMaterialId:" + matId, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - AddToMyFavourite", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpDelete ("{id}")]
        public dynamic DeleteMaterial (string id) {
            dynamic objResponse = null;
            string message = "Failed to delete.";
            bool result = false;
            try {
                tbl_Material material = _repositoryWrapper.Material.FindByCondition (x => x.MatId == id && x.Status == 1).FirstOrDefault ();
                if (material != null) {
                    material.Status = 0;
                    if (material.Url != null) {
                        Deletefile (material.Url, material.ConferenceId);
                    }
                    _repositoryWrapper.Material.Update (material);
                    _repositoryWrapper.Save ();
                    message = "Successfully deleted";
                    result = true;
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "MaterialController - DeleteMaterial", message + ", MatId:" + id, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - DeleteMaterial", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }

        }

        [HttpDelete ("Favourite/{id}")]
        public dynamic RemoveMaterialFromFavouriteByUserId (string id) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to remove.";
            try {
                tbl_UsersFavourite materialUser = _repositoryWrapper.UsersFavourite.FindByCondition (x => x.UserId == _tokenData.UserID && x.Status == 1 && x.FavItemId == id).FirstOrDefault ();
                if (materialUser != null) {
                    materialUser.Status = 0;
                    materialUser.ModifiedDate = DateTime.Now;
                    _repositoryWrapper.UsersFavourite.Update (materialUser);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Successfully removed from favourite.";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "MaterialController - RemoveMaterialFromFavouriteByUserId", message + ", FavMaterialId:" + id, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "MaterialController - RemoveMaterialFromFavouriteByUserId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }

        }
        private string SaveNewMaterial (dynamic obj) {
            string matId = _repositoryWrapper.SiteRunNumber.GetNewId (SiteRunNumbers.MaterialRunKey);
            tbl_Material material = new tbl_Material ();
            material.MatId = matId;
            material.Title = obj.title;
            material.Description = obj.description;
            material.UploadUserId = _tokenData.UserID;
            material.MaterialType = obj.materialType;
            material.ConferenceId = obj.conferenceId;
            material.Status = 1;
            material.CreatedDate = DateTime.Now;
            material.ModifiedDate = DateTime.Now;
            material.CreatedBy = _tokenData.UserID;
            _repositoryWrapper.Material.Create (material);
            _repositoryWrapper.Save ();
            _repositoryWrapper.SiteRunNumber.UpdateNumberByRunKey (SiteRunNumbers.MaterialRunKey);
            _repositoryWrapper.Save ();
            return matId;
        }
        private void UpdateMaterial (dynamic obj, tbl_Material material) {
            material.Title = obj.title;
            material.Description = obj.description;
            //material.UploadUserId = _tokenData.UserID;
            material.MaterialType = obj.materialType;
            material.ConferenceId = obj.conferenceId;
            material.ModifiedDate = DateTime.Now;
            _repositoryWrapper.Material.Update (material);
            _repositoryWrapper.Save ();
        }
        private string SaveFile (IFormFile file, string confid) {
            string url;
            string filename = Path.GetFileNameWithoutExtension (file.FileName);
            string fileextension = Path.GetExtension (file.FileName);
            filename = filename + DateTime.Now.ToString ("yyyyMMdd_hhmmss") + fileextension;

            string materialPath = System.IO.Directory.GetCurrentDirectory () + "\\wwwroot\\Files\\";
            materialPath += confid + "\\";
            string returnUrl = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            if (!Directory.Exists (materialPath)) {
                Directory.CreateDirectory (materialPath);
                using (FileStream fileStream = System.IO.File.Create (materialPath + filename)) {
                    file.CopyTo (fileStream);
                    fileStream.Flush ();
                }
            } else {
                using (FileStream fileStream = System.IO.File.Create (materialPath + filename)) {
                    System.GC.Collect ();
                    System.GC.WaitForPendingFinalizers ();
                    file.CopyTo (fileStream);
                    fileStream.Flush ();
                }
            }
            url = returnUrl + "/File/" + confid + "/" + filename;
            return url;
        }

        private async Task<string> SaveFileToS3 (IFormFile file, string confid, dynamic configuration) {

            string url;
            string accessKey = configuration.GetSection ("AWSConfigs:accessKey").Value;
            string secretKey = configuration.GetSection ("AWSConfigs:secretKey").Value;
            string bucketPath = configuration.GetSection ("AWSConfigs:bucketPath").Value;
            bucketPath += confid;
            string mainUrl = configuration.GetSection ("AWSConfigs:mainUrl").Value;
            string folderPath = configuration.GetSection ("AWSConfigs:folderPath").Value;

            string filename = Path.GetFileNameWithoutExtension (file.FileName);
            string fileextension = Path.GetExtension (file.FileName);
            filename = filename + DateTime.Now.ToString ("yyyyMMdd_hhmmss") + fileextension;

            var client = new AmazonS3Client (accessKey, secretKey, Amazon.RegionEndpoint.APSoutheast1);

            using (var newMemoryStream = new MemoryStream ()) {
                file.CopyTo (newMemoryStream);
                var uploadRequest = new TransferUtilityUploadRequest {
                    InputStream = newMemoryStream,
                    Key = filename,
                    BucketName = bucketPath,
                    CannedACL = S3CannedACL.PublicRead
                };

                var fileTransferUtility = new TransferUtility (client);
                await fileTransferUtility.UploadAsync (uploadRequest);

                url = mainUrl + folderPath + confid + "/" + filename;

                return url;
            }
        }

        private void Deletefile (string url, string confId) {
            string materialPath = System.IO.Directory.GetCurrentDirectory () + "\\wwwroot\\Files\\";
            materialPath += confId + "\\";
            string fileName = url.Split ('/').Last ();
            materialPath += fileName;
            if (System.IO.File.Exists (materialPath)) {
                System.IO.File.Delete (materialPath);
            }
        }
        private void SaveToFavourite (dynamic obj) {
            tbl_UsersFavourite materialUser = new tbl_UsersFavourite ();
            materialUser.UserId = _tokenData.UserID;
            materialUser.FavItemId = obj.matId;
            materialUser.Status = 1;
            materialUser.CreatedDate = DateTime.Now;
            materialUser.ModifiedDate = DateTime.Now;
            materialUser.CreatedBy = _tokenData.UserID;
            _repositoryWrapper.UsersFavourite.Create (materialUser);
            _repositoryWrapper.Save ();
        }
        private void UpdateToFavourite (dynamic obj, tbl_UsersFavourite materialUser) {
            materialUser.Status = 1;
            materialUser.ModifiedDate = DateTime.Now;
            _repositoryWrapper.UsersFavourite.Update (materialUser);
            _repositoryWrapper.Save ();
        }
    }
}