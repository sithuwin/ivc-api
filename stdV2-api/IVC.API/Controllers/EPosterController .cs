using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EPosterController : BaseController
    {
        string returnUrl = "";
        private IRepositoryWrapper _repositoryWrapper;
        public EPosterController(IRepositoryWrapper RW)
        {
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            returnUrl = Configuration.GetSection("appSettings:SpearkerUrl").Value;
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GetEPosterList([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "topic";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";

                dynamic data = _repositoryWrapper.EPoster.GetEPosterList(_tokenData.UserID, sortField, sortBy, obj);
                objResponse = data;
                return StatusCode(StatusCodes.Status200OK, objResponse);

            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - GetEPosterList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{id}")]
        public dynamic GetEPosterById(string id)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try
            {
                dynamic data = _repositoryWrapper.EPoster.GetEPostById(id);
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                string lMessage = "User view poster description";
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.ViewPosterDescription, lMessage, _tokenData.UserID, id, "", ip);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - GetEPosterById", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("Post")]
        public dynamic UpdateCommandAndRating([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to post";
            bool result = true;
            dynamic obj = param;
            string eposterId = "";
            try
            {
                eposterId = obj.eId;
                tblEPoster ePoster = _repositoryWrapper.EPoster.FindByCondition(x => x.ep_ID == eposterId && x.deleteFlag == false).FirstOrDefault();
                if (ePoster != null)
                {
                    SaveEPostCommantAndRate(obj, eposterId);
                    result = true;
                    message = "Successfully posted";
                }
                else
                {
                    message = "EPoster does not exist anymore";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Info, "EPosterController - UpdateCommandAndRating", message + ", EPosterId:"+ eposterId, _tokenData.UserID);
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - UpdateCommandAndRating", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("Email/{id}")]
        public dynamic SendPosterToUser(string id)
        {
            dynamic objResponse = null;
            string message = "Failed to send";
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();

            try
            {
                dynamic emailSetting = new System.Dynamic.ExpandoObject();
                emailSetting.fromEmail = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPUserName && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.password = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPassword && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.port = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPPort && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.host = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.SMTPHost && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                emailSetting.enableSSL = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name == ShowConfigNames.EnableSSL && x.Status == 1).Select(x => x.Value).FirstOrDefault();

                tblEPoster ePoster = _repositoryWrapper.EPoster.FindByCondition(x => x.ep_ID == id && x.deleteFlag == false).FirstOrDefault();
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault();
                if (ePoster != null && account != null)
                {
                    string fileUrl = returnUrl + ePoster.ep_file;

                    emailSetting.toEmail = account.a_email;
                    emailSetting.receiverUserName = account.a_fullname;
                    emailSetting.senderUserName = Configuration.GetSection("Email:SenderUserName").Value;
                    emailSetting.subject = Configuration.GetSection("Email:Subject").Value;
                    string emessage = Configuration.GetSection("Email:Message").Value;
                    emailSetting.emessage = GetEmailBody(emessage, account, ePoster, fileUrl);
                    GlobalFunction.SendEmail(emailSetting);
                    result = true;
                    message = "Successfully sent ";
                }
                else
                {
                    message = "EPoster or user is not active anymore";
                }

                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - SendPosterToUser", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet]
        public dynamic GetCategoryList()
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            try
            {
                dynamic data = _repositoryWrapper.EPoster.GetCategoryList();
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - GetCategoryList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{id}/File")]
        public dynamic GetEPosterFile(string id)
        {
            dynamic objResponse = null;
            string message = "Failed to send";
            bool result = false;
            try
            {
                string data = _repositoryWrapper.EPoster.GetEPosterFile(id, returnUrl);

                result = true;
                message = "Successfully reterieve";
                string lMessage = "User view poster";
                string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.ViewPoster, lMessage, _tokenData.UserID, id, "GetEPosterFile", ip);
                objResponse = new { data = data, success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);

            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "EPosterController - GetEPosterFile", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        private void SaveEPostCommantAndRate(dynamic obj, string eId)
        {
            float rating = obj.rating;
            string comment = obj.comment;
            string eposterId = eId;
            tblCommantRating commantRating = new tblCommantRating();
            commantRating.cr_pID = eposterId;
            commantRating.cr_comment = comment;
            commantRating.cr_rating = rating;
            commantRating.cr_Uid = _tokenData.UserID;
            commantRating.cr_datetime = DateTime.Now;
            _repositoryWrapper.CommantRating.Create(commantRating);
            _repositoryWrapper.Save();
        }
        private string GetEmailBody(string message, tbl_Account account, tblEPoster ePoster, string fileUrl)
        {
            StringBuilder emessage = new StringBuilder(message);
            emessage.Replace("<UserName>", account.a_fullname);
            emessage.Replace("<epId>", ePoster.ep_ID);
            emessage.Replace("<titile>", ePoster.ep_title);
            emessage.Replace("<author>", ePoster.ep_author);
            emessage.Replace("<fileUrl>", fileUrl);
            return emessage.ToString();
        }
    }
}