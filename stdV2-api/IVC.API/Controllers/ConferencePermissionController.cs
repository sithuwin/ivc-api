using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore;

namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ConferencePermissionController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        private IMemoryCache _cache;
        public ConferencePermissionController(IRepositoryWrapper RW,IMemoryCache memoryCache)
        {
            _repositoryWrapper = RW;
            _cache = memoryCache;
        }

        //[Authorize(Roles = "Organizer")]
        [HttpPost]
        public dynamic AddUsersPermissionByWebinarId([FromBody] JObject param)
        {
            dynamic response = null;
            bool result = false;
            string message = "Failed to give permission";

            dynamic obj = param;
            try
            {
                string type = obj.userType;
                if (type == ConferenceSpeakerType.Speaker || type == UserType.Delegate || type == ConferenceSpeakerType.GuestOfHonor || type == ConferenceSpeakerType.KeynoteSpeaker || type == ConferenceSpeakerType.SessionModerator)
                {
                    for (int i = 0; i < obj.users.Count; i++)
                    {
                        string userId = obj.users[i];
                        tbl_Account user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();
                        if (user != null)
                        {
                            string tmpEmail = user.a_email;
                            bool isValidEmail = IsValidEmail(tmpEmail);
                            if (!isValidEmail)
                            {
                                message = "Input contains Invalid Email Format!";
                                result = false;
                                response = new { message = message, success = result, data = "" };
                                GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - AddUsersPermissionByWebinarId", message + ", UserId:" + userId + ", Email:" + tmpEmail, _tokenData.UserID);
                                return StatusCode(StatusCodes.Status400BadRequest, response);
                            }
                        }
                    }
                    dynamic grantResult = SaveUserWebinars(obj, type);
                    if (grantResult.success)
                    {
                        ClearConferenceCache();
                        message = "Successfully inserted";
                        result = true;
                        response = new { message = message, success = result, data = "" };
                        return StatusCode(StatusCodes.Status200OK, response);
                    }
                    else
                    {
                        message = grantResult.message;
                        result = false;
                        response = new { message = message, success = result, data = "" };
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - AddUsersPermissionByWebinarId", message, _tokenData.UserID);
                        return StatusCode(StatusCodes.Status400BadRequest, response);
                    }
                }
                else
                {
                    message = "UserType is not correct";
                    response = new { message = message, success = result, data = "" };
                    GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - AddUsersPermissionByWebinarId", message + ", AccountType:" + type, _tokenData.UserID);
                    return StatusCode(StatusCodes.Status200OK, response);
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ConferencePermissionController - AddUsersPermissionByWebinarId", ex.Message.ToString(), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }

        //[Authorize(Roles = "Organizer")]
        [HttpPost("Permission")]
        public dynamic RemoveUserPermissionByWebinarId([FromBody] JObject param)
        {
            dynamic response = null;
            bool result = false;
            string message = "Failed to remove.";

            dynamic obj = param;

            string webinarId = obj.cid;
            string userId = obj.user;
            string registratnId = "";
            Dictionary<string, string> registrants = new Dictionary<string, string>() { };

            try
            {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == webinarId && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    registratnId = DeleteUserWebinars(webinarId, userId);
                    tbl_Account user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();
                    // if (user != null) {
                    //     registrants.Add (registratnId, user.a_email);
                    //     ZoomToken.ConfigKeys = configKeys;
                    //     RegistrantAPI.UpdateregistrantStatus (conference.ZoomId, registrants, _tokenData.UserID, "cancel");
                    // }
                    message = "Successfully removed.";
                    result = true;
                    GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - RemoveUserPermissionByWebinarId", "WebinerId:" + webinarId + ", UserId: " + userId + ", RegistrantId:" + registratnId, _tokenData.UserID);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ConferencePermissionController - RemoveUserPermissionByWebinarId", ex.Message.ToString(), _tokenData.UserID);
            }
            response = new { message = message, success = result, data = "" };
            return response;
        }

        [HttpPost("Permission/ContactPerson")]
        public dynamic UpdateContactPersonAccessRight([FromBody] JObject param)
        {
            dynamic response = null;
            bool result = true;
            string message = "Successfully updated.";
            string err = "";
            dynamic obj = param;
            string userId = obj.UserId;
            string accessRightId = "";
            string userType = obj.userType;
            int successCount = 0;
            int failCount = 0;
            try
            {
                tbl_Account user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();
                if (user != null)
                {
                    string tmpEmail = user.a_email;
                    bool isValidEmail = IsValidEmail(tmpEmail);
                    accessRightId = _repositoryWrapper.ShowConfig.FindByCondition(x => x.Name.ToLower() == AccessRight.AccessRight_Delegate && x.Status == 1).Select(x => x.Value).FirstOrDefault();
                    if (string.IsNullOrEmpty(accessRightId))
                    {
                        message = "Failed to give conference access right permission. No AccessRightID for this account.";
                        response = new { message = message, success = false, data = "" };
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - UpdateContactPersonAccessRight", message + ", UserId:" + userId, _tokenData.UserID);
                        return StatusCode(StatusCodes.Status400BadRequest, response);
                    }
                    else if (!isValidEmail)
                    {
                        message = "Failed to give conference access right permission. Invalid email format!";
                        response = new { message = message, success = false, data = "" };
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - UpdateContactPersonAccessRight", message + ", UserId:" + userId + ", Email:" + tmpEmail, _tokenData.UserID);
                        return StatusCode(StatusCodes.Status400BadRequest, response);
                    }
                    else
                    {
                        List<string> userlist = new List<string>();
                        userlist.Add(userId);
                        if (string.IsNullOrEmpty(userType))
                        {
                            userType = "D";
                        }
                        var confObj = _repositoryWrapper.Confrence.FindByCondition(x => x.deleteFlag == false
                            && x.c_Type != null
                            && x.c_Type != ConferenceStructureType.Room
                            && x.c_Type != ConferenceStructureType.Workshop).Select(x => new { x.c_ID, x.c_title }).ToList();
                        foreach (var val in confObj)
                        {
                            string confId = val.c_ID;
                            string conName = val.c_title;
                            var objUser = new { users = userlist, cid = confId, accessId = accessRightId };
                            dynamic grantResult = SaveUserWebinars(objUser, userType);
                            if (!grantResult.success)
                            {
                                if (string.IsNullOrEmpty(err))
                                {
                                    err = conName;
                                }
                                else { err += ", " + conName; }
                                message = "Failed for giving conference permission: " + err;
                                result = false;
                                failCount += 1;
                            }
                            else { successCount += 1; }
                        }
                        message = "\nSuccess Count (" + successCount + ")" + "\nFailed Count (" + failCount + ")";
                    }
                }
                else
                {
                    result = false;
                    message = "Failed to give conference access right permission. This account doesn't exist.";
                }
                response = new { message = message, success = result, data = "" };
                return StatusCode(StatusCodes.Status200OK, response);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ConferencePermissionController - UpdateContactPersonAccessRight", ex.Message.ToString(), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode(StatusCodes.Status500InternalServerError, response);
            }
        }
        private dynamic SaveUserWebinars(dynamic obj, string type)
        {

            string message = "";
            bool result = true;
            string webinarId = obj.cid;
            string conferenceType = "";//obj.confType;
            tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == webinarId && x.deleteFlag == false).FirstOrDefault();
            if (conference == null)
            {
                result = false;
                message = "There is no acitve conference with " + obj.cid;
                GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveUserWebinars", message, _tokenData.UserID);
                return new { message = message, success = result, data = "" };

            }
            int ConfigId = conference.ConfigId;
            string ZoomId = conference.ZoomId;

            if (!String.IsNullOrEmpty(conference.RoomId))
            {
                string RoomId = conference.RoomId;
                tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                ConfigId = shareRoomConf.ConfigId;
                ZoomId = shareRoomConf.ZoomId;
            }

            ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
            if (configKeys == null)
            {
                result = false;
                message = "Zoom Configuration Id is incorrect!";
                GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveUserWebinars", message + ", WebinerId:" + webinarId + ", ConfigId:" + ConfigId + ", ZoomId:" + ZoomId, _tokenData.UserID);
                return new { message = message, success = result, data = "" };
            }
            conferenceType = conference.Type;

            Dictionary<string, string> registrants = new Dictionary<string, string>() { };
            Dictionary<string, string> panelists = new Dictionary<string, string>() { };
            bool hasStatusChanges = false;

            for (int i = 0; i < obj.users.Count; i++)
            {
                string userId = obj.users[i];
                string accessId = obj.accessId;
                tbl_ConferenceUser conferenceUser = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.UserId == userId && x.WebinarId == webinarId).FirstOrDefault();
                tbl_Account user = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == userId && x.deleteFlag == false).FirstOrDefault();

                if (conferenceType == ConferenceType.Embedded)
                {
                    if (conferenceUser != null)
                    {
                        result = UpdateUserWebinars(conferenceUser, accessId, type);
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveUserWebinars", "(UpdateUserWebinars), WebinarId:" + webinarId + ", UserID:" + userId + ", CofType:" + ConferenceType.Embedded + ", AccessRightId:" + accessId, _tokenData.UserID);
                    }
                    else
                    {
                        result = SaveNewUserEmbeddWebinars(conference, user, type, accessId);
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveUserWebinars", "(SaveNewUserEmbeddWebinars), WebinarId:" + webinarId + ", UserID:" + userId + ", CofType:" + ConferenceType.Embedded + ", AccessRightId:" + accessId, _tokenData.UserID);
                    }
                }
                else // Default Conference Type
                {
                    if (conferenceUser != null)
                    {
                        // if user alreaddy exists in ConfrenceUser table
                        result = UpdateUserWebinars(conferenceUser, accessId, type);
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveUserWebinars", "(UpdateUserWebinars), WebinarId:" + webinarId + ", UserID:" + userId + ", CofType:" + ConferenceType.Default + ", AccessRightId:" + accessId, _tokenData.UserID);

                        //Only Work on Default
                        try
                        {
                            ZoomToken.ConfigKeys = configKeys;
                            dynamic response = RegistrantAPI.AddNewUserRegistrant(ZoomId, user, _tokenData.UserID);
                            if (response != null)
                            {
                                conferenceUser.RegistrantId = response.registrant_id;
                                _repositoryWrapper.ConferenceUser.Update(conferenceUser);
                                _repositoryWrapper.Save();

                                hasStatusChanges = true;
                                if (!registrants.ContainsKey(user.a_email))
                                {
                                    registrants.Add(user.a_email, conferenceUser.RegistrantId);
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            GlobalFunction.WriteSystemLog(LogType.Error, "ConferencePermissionController - SaveUserWebinars", e.Message.ToString(), _tokenData.UserID);
                        }
                    }
                    else
                    {
                        // if user is new 
                        string registrantId = SaveNewUserWebinars(conference, configKeys, user, type, accessId, ZoomId);
                        GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveNewUserWebinars", "(SaveNewUserWebinars), WebinarId:" + webinarId + ", UserID:" + userId + ", CofType:" + ConferenceType.Default + ", AccessRightId:" + accessId, _tokenData.UserID);

                        //Only Work on Default
                        if (registrantId != "" && registrantId != null)
                        {
                            hasStatusChanges = true;
                            if (!registrants.ContainsKey(user.a_email))
                            {
                                registrants.Add(user.a_email, registrantId);
                            }
                        }
                        else
                        {
                            result = false;
                            message = "Check your conference is expired or not and are you sure the accounts(users) use the unique email address?";
                            GlobalFunction.WriteSystemLog(LogType.Info, "ConferencePermissionController - SaveNewUserWebinars", message + "WebinarId:" + webinarId + ", UserID:" + userId + ", CofType:" + ConferenceType.Default, _tokenData.UserID);
                            return new { message = message, success = result, data = "" };
                        }
                    }
                    // Add Panellist User
                    //Only Work on Default
                    // if (type == UserType.Speaker)
                    // {
                    //     if (user != null)
                    //     {
                    //         if (!panelists.ContainsKey(user.a_email))
                    //         {
                    //             panelists.Add(user.a_email, user.a_fullname);
                    //         }
                    //     }

                    // }

                }


                if (type == ConferenceSpeakerType.Speaker || type == ConferenceSpeakerType.GuestOfHonor || type == ConferenceSpeakerType.KeynoteSpeaker || type == ConferenceSpeakerType.SessionModerator)
                {
                    if (user != null)
                    {
                        if (!panelists.ContainsKey(user.a_email))
                        {
                            panelists.Add(user.a_email, user.a_fullname);
                        }
                    }

                }
            }

            if (conferenceType != ConferenceType.Embedded)
            {
                //Only Work on Default
                if (hasStatusChanges)
                {
                    ZoomToken.ConfigKeys = configKeys;
                    RegistrantAPI.UpdateregistrantStatus(ZoomId, registrants, _tokenData.UserID, "approve");
                }
                // if (type == UserType.Speaker)
                // {
                //     ZoomToken.ConfigKeys = configKeys;
                //     if (panelists.Count > 0)
                //     {
                //         PanelistAPI.AddPanelist(panelists, conference.ZoomId, _tokenData.UserID);
                //     }
                // }
            }

            if (type == ConferenceSpeakerType.Speaker || type == ConferenceSpeakerType.GuestOfHonor || type == ConferenceSpeakerType.KeynoteSpeaker || type == ConferenceSpeakerType.SessionModerator)
            {
                ZoomToken.ConfigKeys = configKeys;
                if (panelists.Count > 0)
                {
                    PanelistAPI.AddPanelist(panelists, ZoomId, _tokenData.UserID);
                }
            }
            return new { message = message, success = result, data = "" };
        }
        private string SaveNewUserWebinars(tbl_Conference conference, ConfigKeys configKeys, tbl_Account user, string type, string accessId, string ZoomId)
        {
            string registrantId = "";
            ZoomToken.ConfigKeys = configKeys;

            dynamic response = RegistrantAPI.AddNewUserRegistrant(ZoomId, user, _tokenData.UserID);
            if (response != null)
            {
                tbl_ConferenceUser conferenceUser = new tbl_ConferenceUser();
                conferenceUser.WebinarId = conference.c_ID;
                conferenceUser.AdminId = _tokenData.UserID;
                conferenceUser.UserId = user.a_ID;
                conferenceUser.Status = 1;
                conferenceUser.CreatedDate = DateTime.Now;
                conferenceUser.ModifiedDate = DateTime.Now;
                conferenceUser.RegistrantId = response.registrant_id;
                conferenceUser.CurrentJoinStatus = false;
                conferenceUser.AccountType = type;
                conferenceUser.AccessRightId = accessId;

                _repositoryWrapper.ConferenceUser.Create(conferenceUser);
                _repositoryWrapper.Save();

                registrantId = conferenceUser.RegistrantId;
            }
            return registrantId;

        }

        private bool SaveNewUserEmbeddWebinars(tbl_Conference conference, tbl_Account user, string type, string accessId)
        {
            bool isOK = false;
            try
            {
                tbl_ConferenceUser conferenceUser = new tbl_ConferenceUser();
                conferenceUser.WebinarId = conference.c_ID;
                conferenceUser.AdminId = _tokenData.UserID;
                conferenceUser.UserId = user.a_ID;
                conferenceUser.Status = 1;
                conferenceUser.CreatedDate = DateTime.Now;
                conferenceUser.ModifiedDate = DateTime.Now;
                conferenceUser.RegistrantId = "";
                conferenceUser.CurrentJoinStatus = false;
                conferenceUser.AccountType = type;
                conferenceUser.AccessRightId = accessId;

                _repositoryWrapper.ConferenceUser.Create(conferenceUser);
                _repositoryWrapper.Save();
                isOK = true;
            }
            catch
            {
                isOK = false;
            }
            return isOK;
        }
        private bool UpdateUserWebinars(tbl_ConferenceUser conferenceUser, string accessId, string type)
        {
            bool isOK = false;
            try
            {
                conferenceUser.Status = 1;
                conferenceUser.AccountType = type;
                conferenceUser.AccessRightId = accessId;
                conferenceUser.ModifiedDate = DateTime.Now;
                _repositoryWrapper.ConferenceUser.Update(conferenceUser);
                _repositoryWrapper.Save();
                isOK = true;
            }
            catch
            {
                isOK = false;
            }
            return isOK;
        }
        private string DeleteUserWebinars(string webinarId, string userId)
        {
            string registrantId = "";
            tbl_ConferenceUser userWebinars = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.UserId == userId && x.WebinarId == webinarId && x.Status == 1).FirstOrDefault();
            if (userWebinars != null)
            {
                userWebinars.Status = 0;
                _repositoryWrapper.ConferenceUser.Update(userWebinars);
                _repositoryWrapper.Save();
                registrantId = userWebinars.RegistrantId;
            }
            return registrantId;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                bool regTest = Regex.IsMatch(email, @"^[a-zA-Z0-9._%+-]+[a-zA-Z0-9+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");
                return (addr.Address == email) && regTest;
            }
            catch
            {
                return false;
            }
        }
        
        private void ClearConferenceCache(){
            dynamic data = null;
            dynamic keyObj = new ExpandedObjectFromApi(ENUM_Cache.ConferenceList);
            string key = keyObj.key;
            dynamic days = _repositoryWrapper.Day.FindByCondition(x => x.deleteFlag == false).
            Select(x => x.d_date).AsNoTracking().ToList();
            foreach (var day in days)
            {
                if (_cache.TryGetValue(key + day, out data))
                {
                    GlobalFunction.CacheRemove(key + day, _cache);
                }
            }
        }


    }
}