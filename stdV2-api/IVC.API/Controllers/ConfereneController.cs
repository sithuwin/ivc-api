using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using IVC.API;
using IVC.API.ChatAPI;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace IVC.API.Controllers {
    [Authorize]
    [ApiController]
    [Route ("api/[controller]")]
    public class ConferenceController : BaseController {
        string returnUrl = "";
        private IRepositoryWrapper _repositoryWrapper;
        private readonly IHubContext<SignalHub> _hubContext;
        private IMemoryCache _cache;
        static readonly char[] padding = { '=' };
        public ConferenceController (IRepositoryWrapper RW, IHubContext<SignalHub> hubContext, IMemoryCache memoryCache) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            returnUrl = Configuration.GetSection ("appSettings:SpearkerUrl").Value;
            _repositoryWrapper = RW;
            _hubContext = hubContext;
            _cache = memoryCache;
        }

        [HttpGet ("Date")]
        public dynamic GetDateListByUserId () {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.Confrence.GetMyConferenceDateList (_tokenData.UserID);
                result = true;
                message = "Successfully reterieved";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetDateListByUserId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cid}")]
        public dynamic GetConferenceByConferenceId (string cid) {
            dynamic responseObj = null;
            dynamic response = null;
            bool result = false;
            string message = "Can't retrieve Conference Data";
            string conferenceStatus = "";
            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;

                    if (!String.IsNullOrEmpty (confrence.RoomId)) {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    ZoomToken.ConfigKeys = configKeys;
                    response = WebinarAPI.GetWebinarDetails (ZoomId, _tokenData.UserID);
                    result = true;
                    //Log.Information("conference's data reterieve by userid={ID}", _tokenData.UserID);
                    message = "Conference Data is successfully retrieved";
                    result = true;
                    conferenceStatus = confrence.CurrentConferenceStatus;
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceByConferenceId", ex.Message.ToString (), _tokenData.UserID);
            }
            GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetConferenceByConferenceId", message + "ConferenceId:" + cid, _tokenData.UserID);
            responseObj = new { data = response, success = result, message = message, status = conferenceStatus };
            return responseObj;
        }

        [HttpGet ("{cid}/JoinStatus")]
        public dynamic CheckJoinStatus (string cid) {
            bool result = false;
            string msg = "Can't Join the Conference.";
            try {
                string userId = _tokenData.UserID;
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    bool isOk = CheckCurrentJoinStatus (userId, confrence);
                    if (isOk == true) {
                        tbl_Account user = (_repositoryWrapper.Account.FindByCondition (x => x.a_ID == userId && x.deleteFlag == false)).FirstOrDefault ();
                        if (user == null) {
                            result = false;
                            msg = "No active user.";
                            return new { result = result, message = msg };
                        }

                        result = true;
                        msg = "Conference is succcessfully joined";
                        _repositoryWrapper.ConferenceUser.UpdateCurrentJoinStatus (userId, cid);
                        _repositoryWrapper.Save ();
                        string logMessage = "User join the  conference";
                        string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                        GlobalFunction.SaveAuditLog (_repositoryWrapper, AuditLogType.JoinConference, logMessage, userId, cid, "CheckJoinStatus", ip);

                        // Close Embedded
                        // int ConfigId = confrence.ConfigId;
                        // string ZoomId = confrence.ZoomId;
                        // if (!String.IsNullOrEmpty(confrence.RoomId))
                        // {
                        //     string RoomId = confrence.RoomId;
                        //     tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                        //     ConfigId = shareRoomConf.ConfigId;
                        //     ZoomId = shareRoomConf.ZoomId;
                        // }
                        // ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                        // if (configKeys == null)
                        // {
                        //     result = false;
                        //     msg = "Zoom configuration Id is incorrect!";
                        //     GlobalFunction.WriteSystemLog(LogType.Info, "ConferenceController - CheckJoinStatus",msg + ", ConfigId:" +ConfigId + ", ZoomId:" + ZoomId, _tokenData.UserID);
                        //     return new { result = result, message = msg };
                        // }
                        // ZoomToken.ConfigKeys = configKeys;
                        // int isCurrentJoin = 0;
                        // bool isLive = false;
                        // dynamic currentLiveResult = WebinarAPI.GetCurrentLiveWebinar();
                        // var currentTempLiveResult = JsonConvert.DeserializeObject<LiveWebinarList>(currentLiveResult.ToString());
                        // List<LiveWebinar> liveWebinarList = currentTempLiveResult.webinars;
                        // isLive = liveWebinarList.Exists(x => x.id == ZoomId);

                        // dynamic currentJoinQuery = _repositoryWrapper.ConferenceUser.FindByCondition(x => x.UserId == userId && x.Status == 1 && x.CurrentJoinStatus == true);
                        // if (currentJoinQuery != null)
                        // {
                        //     foreach (var currentItem in currentJoinQuery)
                        //     {
                        //         if (isLive)
                        //         {
                        //             string nextPageToken = "";
                        //             LiveWebinar webinarList = liveWebinarList.Find(x => x.id == ZoomId);
                        //             dynamic participantQuery = WebinarAPI.GetWebinarLiveParticipants(ZoomId, "300", "");

                        //             if (participantQuery != null)
                        //             {
                        //                 ParticipantList currentTempParticipantResult = JsonConvert.DeserializeObject<ParticipantList>(participantQuery.ToString());
                        //                 List<Participant> liveParticipants = currentTempParticipantResult.participants;
                        //                 if (liveParticipants != null)
                        //                 {
                        //                     liveParticipants = liveParticipants.FindAll(x => x.user_name == user.a_fullname && x.email == user.a_email && (x.leave_time == "" || x.leave_time == null));
                        //                     if (liveParticipants.Count() > 0)
                        //                     {
                        //                         isCurrentJoin++;
                        //                         break;
                        //                     }
                        //                     else
                        //                     {
                        //                         nextPageToken = currentTempParticipantResult.next_page_token;
                        //                         while (!string.IsNullOrEmpty(nextPageToken))
                        //                         {
                        //                             participantQuery = WebinarAPI.GetWebinarLiveParticipants(ZoomId, "300", nextPageToken);
                        //                             if (participantQuery != null)
                        //                             {
                        //                                 currentTempParticipantResult = JsonConvert.DeserializeObject<ParticipantList>(participantQuery.ToString());
                        //                                 liveParticipants = currentTempParticipantResult.participants;
                        //                                 if (liveParticipants != null)
                        //                                 {
                        //                                     liveParticipants = liveParticipants.FindAll(x => x.user_name == user.a_fullname && x.email == user.a_email && (x.leave_time == "" || x.leave_time == null));
                        //                                     if (liveParticipants.Count() > 0)
                        //                                     {
                        //                                         isCurrentJoin++;
                        //                                         break;
                        //                                     }
                        //                                 }
                        //                             }
                        //                             else
                        //                             {
                        //                                 nextPageToken = "";
                        //                             }
                        //                         }
                        //                     }
                        //                 }
                        //             }

                        //         }
                        //     }
                        // }

                        // if (isCurrentJoin == 0)
                        // {
                        //     if (isLive)
                        //     {
                        //         result = true;
                        //         msg = "Conference is succcessfully joined";
                        //         _repositoryWrapper.ConferenceUser.UpdateCurrentJoinStatus(userId, cid);
                        //         _repositoryWrapper.Save();
                        //         string logMessage = "User join the  conference";
                        //         string ip = HttpContext.Connection.RemoteIpAddress.ToString();
                        //         GlobalFunction.SaveAuditLog(_repositoryWrapper, AuditLogType.JoinConference, logMessage, userId, cid, "CheckJoinStatus", ip);
                        //     }
                        //     else
                        //     {
                        //         result = false;
                        //         msg = "Conference is not started yet!";
                        //     }
                        // }
                        // else
                        // {
                        //     result = false;
                        //     msg = "You are already joined a meeting!";
                        // }
                    } else {
                        msg = "Conference has not started.";
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - CheckJoinStatus", ex.Message.ToString (), _tokenData.UserID);
            }
            GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - CheckJoinStatus", msg + cid, _tokenData.UserID);
            return new { result = result, message = msg };
        }

        [HttpGet ("{cid}/Join")]
        public dynamic GetJoinUrl (string cid) {
            dynamic response = null;
            try {
                string userId = _tokenData.UserID;
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                tbl_ConferenceUser userWebinars = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.WebinarId == cid && x.UserId == userId && x.Status == 1).FirstOrDefault ();

                if (confrence != null && userWebinars != null) {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;
                    if (!String.IsNullOrEmpty (confrence.RoomId)) {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    ZoomToken.ConfigKeys = configKeys;
                    if (confrence.Type == ConferenceType.Embedded) {
                        dynamic resultData = PanelistAPI.GetPanelists (ZoomId, _tokenData.UserID);
                        var panelists = resultData.panelists;
                        tbl_Account user = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == _tokenData.UserID).FirstOrDefault ();

                        foreach (var panlist in panelists) {
                            string Email = panlist.email;
                            if (Email == user.a_email) {
                                response = panlist;
                            }
                        }
                    } else {
                        response = RegistrantAPI.GetRegistrant (ZoomId, userWebinars.RegistrantId, _tokenData.UserID);
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetJoinUrl", ex.Message.ToString (), _tokenData.UserID);
            }
            return response;
        }

        [HttpGet ("CreatedConferences")]
        public dynamic GetAllConference () {
            dynamic objResponse = null;
            string userid = _tokenData.UserID;

            try {
                var objResult = _repositoryWrapper.Confrence.FindByCondition (x => x.deleteFlag != true);
                objResponse = new { data = objResult };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAllConference", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("ConferenceCategory")]
        public dynamic GetAllConferenceCate () {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.ConferenceCategory.GetConferenceCategorylist ();
                result = true;
                message = "Successfully reterieved";
                objResponse = new { data = data, success = result, message = message };

                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetAllConferenceCate", "Get all conference category.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAllConferenceCate", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{cid}")]
        public dynamic CreateConferenceInZoom ([FromBody] Newtonsoft.Json.Linq.JObject param, string cid) {
            dynamic obj = param;
            dynamic responseObj = null;
            string message = "";
            dynamic response = null;
            bool result = false;

            try {
                //int configId = obj.configuration;
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (confrence.ConfigId); //configId
                    if (configKeys == null) {
                        message = "Zoom Configuration Id is not correct!";
                        response = new { success = result, data = "", message = message };
                        return StatusCode (StatusCodes.Status400BadRequest, response);
                    }
                    ZoomToken.ConfigKeys = configKeys;
                    responseObj = WebinarAPI.CreateWebinar (obj, _tokenData.UserID, configKeys.AccountId);

                    if (responseObj.success) {
                        message = "Save Conference Successfully!";
                        Log.Information ("Created new conference by userid={ID}", _tokenData.UserID);
                        result = true;
                        response = new { success = result, data = responseObj.data, message = message };
                        GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - CreateConferenceInZoom", message + ", ConferenceId:" + cid, _tokenData.UserID);
                        return StatusCode (StatusCodes.Status200OK, response);
                    } else {
                        if (responseObj.message == "")
                            message = "Save Conference UnSuccessfully!";
                        else
                            message = responseObj.message;
                        result = false;
                        GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - CreateConferenceInZoom", message + ", ConferenceId:" + cid, _tokenData.UserID);
                        response = new { success = result, data = responseObj.data, message = message };
                        return StatusCode (StatusCodes.Status400BadRequest, response);
                    }
                } else {
                    message = "There is no active Conference with " + cid;
                    result = false;
                    response = new { success = result, data = responseObj, message = message };
                    GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - CreateConferenceInZoom", message, _tokenData.UserID);
                    return StatusCode (StatusCodes.Status400BadRequest, response);
                }
            } catch (Exception ex) {
                message = "Can't Save Conference!";
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - CreateConferenceInZoom", ex.Message.ToString (), _tokenData.UserID);
                response = new { success = result, data = responseObj, message = message, error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpPost]
        public dynamic GetAllConferenceList ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic paraObj = param;
            dynamic objResponse = null;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;
                if (userType == "0") {
                    var objResult = _repositoryWrapper.Confrence.GetAllWebinarListForUser (userId, paraObj, returnUrl);
                    objResponse = new { data = objResult };
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAllConferenceList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("OrganizationConferences")]
        public dynamic GetAllConferenceForOrg ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic objResponse = null;
            string userid = _tokenData.UserID;
            dynamic obj = param;
            string message = "Failed to reterieve";
            bool result = false;

            try {
                dynamic objResult = null;
                objResult = _repositoryWrapper.Confrence.GetConferencelistForOrganization (obj);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = objResult, success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetAllConferenceForOrg", "Retrieve conference list for organizer site.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAllConferenceForOrg", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("MyConferences")]
        public dynamic GetConferenceBySpeakerId ([FromBody] JObject param) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            dynamic obj = param;
            try {
                string confDate = obj.conferenceDate;
                string aId = _tokenData.UserID;
                dynamic conferenceList = _repositoryWrapper.ConferenceUser.GetAttandanceConferencByUserId (aId, returnUrl, confDate);
                message = "Successfully reterieved";
                result = true;
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetConferenceBySpeakerId", message, _tokenData.UserID);
                objResponse = new { data = conferenceList, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceBySpeakerId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPatch ("{cid}")]
        public dynamic UpdateConferenceInZoom ([FromBody] Newtonsoft.Json.Linq.JObject param, string cid) {
            dynamic obj = param;
            bool result = false;
            string message = "Failed to update";
            dynamic response = null;

            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;
                    string ConType = confrence.Type;
                    if (!String.IsNullOrEmpty (confrence.RoomId)) {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                        ConType = shareRoomConf.Type;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    ZoomToken.ConfigKeys = configKeys;
                    if (configKeys == null) {
                        message = "Zoom Configuration Id is not correct!";
                    } else {
                        dynamic responseObj = WebinarAPI.UpdateWebinar (obj, ZoomId, _tokenData.UserID, ConType);
                        if (responseObj.data) {
                            message = "Successfully updated";
                            result = true;
                        } else {
                            if (responseObj.message == "")
                                message = "Update Conference UnSuccessfully!";
                            else
                                message = responseObj.message;
                            result = false;
                        }
                    }
                } else {
                    message = "There is no active Conference with " + cid;
                    result = false;
                }
                response = new { message = message, success = result, data = "" };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - UpdateConferenceInZoom", message + ", ConferenceID:" + cid, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, response);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - UpdateConferenceInZoom", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/ChatStart/{type}")]
        public async Task<dynamic> ConferenceChatStart (string cid, string type) {
            bool result = false;
            string message = "";
            dynamic response = null;

            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    bool isOk = false;
                    string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();

                    if (type == GroupChatType.SpeakerGroupChat) {
                        //speaker group chat API create
                        string gId = confrence.c_ID.Replace ("C", "S");
                        isOk = GroupAPI.CreateGroup (gId, confrence.c_title + "( Speaker Group )", showPrefix);
                        if (isOk) {
                            confrence.SpeakerChatChannelStatus = CurrentGroupChatStatus.open;
                            confrence.UpdatedDate = DateTime.Now;
                            _repositoryWrapper.Confrence.Update (confrence);
                            _repositoryWrapper.Save ();
                            await SendNotificationForChat (confrence, NotificationTemplate.GroupChatOpenNotification, type);
                            message = "Speaker Chat Channel Started!";
                            result = true;
                            await _hubContext.Clients.All.SendAsync ("ConferenceSpeakerGroupChat", confrence);
                        }
                    } else if (type == GroupChatType.ConferenceGroupChat) {
                        //Group Chat API Create
                        isOk = GroupAPI.CreateGroup (confrence.c_ID, confrence.c_title, showPrefix);
                        if (isOk) {
                            confrence.chatChannelStatus = CurrentGroupChatStatus.open;
                            confrence.UpdatedDate = DateTime.Now;
                            _repositoryWrapper.Confrence.Update (confrence);
                            _repositoryWrapper.Save ();
                            await SendNotificationForChat (confrence, NotificationTemplate.GroupChatOpenNotification, type);
                            message = "Conference Chat Channel Started!";
                            result = true;
                            await _hubContext.Clients.All.SendAsync ("ConferenceGroupChat", confrence);
                        }
                    } else {
                        message = type + " is not correct type";
                    }

                } else {
                    message = "There is no active Conference with " + cid;
                    result = false;
                    response = new { message = message, success = result, data = "" };
                    return StatusCode (StatusCodes.Status400BadRequest, response);
                }

                response = new { message = message, success = result, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - ConferenceChatStart", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/ChatStop/{type}")]
        public async Task<dynamic> ConferenceChatStop (string cid, string type) {
            bool result = false;
            string message = "";
            dynamic response = null;

            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    if (type == GroupChatType.SpeakerGroupChat) {
                        confrence.SpeakerChatChannelStatus = CurrentGroupChatStatus.join;
                        confrence.UpdatedDate = DateTime.Now;
                        _repositoryWrapper.Confrence.Update (confrence);
                        _repositoryWrapper.Save ();
                        await SendNotificationForChat (confrence, NotificationTemplate.GroupChatCloseNotification, type);
                        message = "Chat Channel Closed!";
                        result = true;
                        await _hubContext.Clients.All.SendAsync ("ConferenceSpeakerGroupChat", confrence);
                    } else if (type == GroupChatType.ConferenceGroupChat) {
                        confrence.chatChannelStatus = CurrentGroupChatStatus.join;
                        confrence.UpdatedDate = DateTime.Now;
                        _repositoryWrapper.Confrence.Update (confrence);
                        _repositoryWrapper.Save ();
                        await SendNotificationForChat (confrence, NotificationTemplate.GroupChatCloseNotification, type);
                        message = "Chat Channel Closed!";
                        result = true;
                        await _hubContext.Clients.All.SendAsync ("ConferenceGroupChat", confrence);
                    } else {
                        message = type + " is not correct type";
                    }
                } else {
                    message = "There is no active Conference with " + cid;
                    result = false;
                    response = new { message = message, success = result, data = "" };
                    return StatusCode (StatusCodes.Status400BadRequest, response);
                }

                response = new { message = message, success = result, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - ConferenceChatStop", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/JoinChatChannel/{type}")]
        public dynamic JoinChatChannel (string cid, string type) {
            bool result = false;
            string message = "";
            dynamic response = null;
            string userId = _tokenData.UserID;

            try {
                tbl_ConferenceUser userWebinars = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.UserId == userId && x.WebinarId == cid && x.Status == 1).FirstOrDefault ();
                //tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
                if (userWebinars != null) {
                    bool isOK = false;
                    string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                    // Checking type (SpeakerGroupChat OR ConferenceGroupChat)
                    // if ConferenceGroupChat Type
                    if (type == GroupChatType.SpeakerGroupChat) {
                        if (userWebinars.AccountType == ConferenceSpeakerType.Speaker || userWebinars.AccountType == ConferenceSpeakerType.GuestOfHonor || userWebinars.AccountType == ConferenceSpeakerType.KeynoteSpeaker || userWebinars.AccountType == ConferenceSpeakerType.SessionModerator) {
                            string gId = cid.Replace ("C", "S");
                            isOK = GroupAPI.JoinMember (userId, gId, userWebinars.AccountType, showPrefix);
                            if (isOK) {
                                userWebinars.SpeakerChatChannelJoinStatus = ChatJoinStatus.chat;
                                userWebinars.ModifiedDate = System.DateTime.Now;
                                _repositoryWrapper.ConferenceUser.Update (userWebinars);
                                _repositoryWrapper.Save ();
                                result = true;
                                message = "Successfully join";

                            } else {
                                message = "Failed to join";
                            }
                        } else {
                            message = "This user is not assigned to this conference!";
                        }

                    } else if (type == GroupChatType.ConferenceGroupChat) {
                        isOK = GroupAPI.JoinMember (userId, cid, userWebinars.AccountType, showPrefix);
                        if (isOK) {
                            userWebinars.ChatChannelJoinStatus = ChatJoinStatus.chat;
                            userWebinars.ModifiedDate = System.DateTime.Now;
                            _repositoryWrapper.ConferenceUser.Update (userWebinars);
                            _repositoryWrapper.Save ();
                            string logMessage = "User  joined GroupChat ";
                            string ip = HttpContext.Connection.RemoteIpAddress.ToString ();
                            GlobalFunction.SaveAuditLog (_repositoryWrapper, AuditLogType.JoinGroupChat, logMessage, userId, cid, "JoinChatChannel", ip);
                            result = true;
                            message = "Successfully join";

                        } else {
                            message = "Failed to join";
                        }
                    } else {
                        message = type + " is not correct type";
                    }

                } else {
                    message = "This user is not assigned to this conference!";
                    result = false;
                    response = new { message = message, success = result, data = "" };
                    return StatusCode (StatusCodes.Status400BadRequest, response);
                }

                response = new { message = message, success = result, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - JoinChatChannel", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/JoinAdminChat/{type}")]
        public dynamic JoinAdminChat (string cid, string type) {
            bool result = false;
            string message = "";
            dynamic response = null;
            string userId = _tokenData.UserID;
            string gId = "";

            try {
                bool isOK = false;
                string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                // Checking type (SpeakerGroupChat OR ConferenceGroupChat)
                // if ConferenceGroupChat Type
                if (type == GroupChatType.SpeakerGroupChat) {
                    gId = cid.Replace ("C", "S");
                    isOK = GroupAPI.JoinMember (userId, gId, UserType.Organizer, showPrefix);
                } else if (type == GroupChatType.ConferenceGroupChat) {
                    isOK = GroupAPI.JoinMember (userId, cid, UserType.Organizer, showPrefix);
                } else if (type == GroupChatType.LiveGroupChat) {
                    gId = cid.Replace ("C", "l");
                    isOK = GroupAPI.JoinMember (userId, cid, UserType.Organizer, showPrefix);
                } else if (type == GroupChatType.NetworkGroupChat) {
                    isOK = GroupAPI.JoinMember (userId, cid, UserType.Organizer, showPrefix);
                }

                response = new { message = message, success = result, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - JoinAdminChat", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/Channel")]
        public dynamic GetGroupData (string cid) {
            bool result = false;
            string message = "";
            dynamic response = null;
            try {
                string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                dynamic data = GroupAPI.GetGroup (cid, showPrefix);
                response = new { message = message, success = result, data = data };
                return StatusCode (StatusCodes.Status200OK, response);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetGroupData", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{cid}/CheckStatus/{type}")]
        public dynamic CheckChatChannelSatus (string cid, string type) {
            bool result = false;
            string message = "This user can't join this chat";
            dynamic response = null;
            string userId = _tokenData.UserID;

            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence == null) {
                    {
                        message = "There is no active Conference with " + cid;
                        response = new { message = message, success = result, data = "" };
                        return StatusCode (StatusCodes.Status400BadRequest, response);
                    }
                }

                tbl_ConferenceUser userWebinars = new tbl_ConferenceUser ();
                //if(type is SpeakerGroupChatType)
                if (type == GroupChatType.SpeakerGroupChat) {
                    userWebinars = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.UserId == userId && x.WebinarId == cid && x.Status == 1 && (x.AccountType == ConferenceSpeakerType.Speaker || x.AccountType == ConferenceSpeakerType.GuestOfHonor || x.AccountType == ConferenceSpeakerType.KeynoteSpeaker || x.AccountType == ConferenceSpeakerType.SessionModerator)).FirstOrDefault ();
                    if (userWebinars != null) {
                        if (userWebinars.AccountType == ConferenceSpeakerType.Speaker || userWebinars.AccountType == ConferenceSpeakerType.GuestOfHonor || userWebinars.AccountType == ConferenceSpeakerType.KeynoteSpeaker || userWebinars.AccountType == ConferenceSpeakerType.SessionModerator) {
                            if (confrence.SpeakerChatChannelStatus == CurrentGroupChatStatus.open && userWebinars.SpeakerChatChannelJoinStatus == ChatJoinStatus.chat) {
                                result = true;
                                message = "This user can join this conference chat.";
                            }
                            if (confrence.SpeakerChatChannelStatus != CurrentGroupChatStatus.open) {
                                message = "Chat Channel is closed!";
                            }
                        } else {
                            message = "This user is not assigned to this conference!";
                        }

                    } else {
                        message = "This user is not assigned to this conference!";
                    }
                    //if(type is ConferenceGroupChatType)
                } else if (type == GroupChatType.ConferenceGroupChat) {
                    userWebinars = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.UserId == userId && x.WebinarId == cid && x.Status == 1).FirstOrDefault ();
                    if (userWebinars != null) {
                        if (confrence.chatChannelStatus == CurrentGroupChatStatus.open && userWebinars.ChatChannelJoinStatus == ChatJoinStatus.chat) {
                            result = true;
                            message = "This user can join this conference chat.";
                        }

                        if (confrence.chatChannelStatus != CurrentGroupChatStatus.open) {
                            message = "Chat Channel is closed!";
                        }
                    } else {
                        message = "This user is not assigned to this conference!";
                    }
                }

                response = new { message = message, success = result, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);

            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - CheckChatChannelSatus", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = result, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("{cid}/CurrentStatus/{type}")]
        public async Task<dynamic> ChangeCurrentConferenceStatus (string cid, string type) {
            dynamic objResponse = null;
            bool result = false;
            string message = "";

            try {
                if (type == CurrentConferenceStatus.GrandOpening || type == CurrentConferenceStatus.SoftOpening || type == CurrentConferenceStatus.CloseConference || type == CurrentConferenceStatus.InitialState) {
                    tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                    if (conference != null) {
                        if (type == CurrentConferenceStatus.GrandOpening || type == CurrentConferenceStatus.SoftOpening) {
                            string showPrefix = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1).Select (x => x.Value).FirstOrDefault ();
                            //Conference live group chat API
                            string gId = conference.c_ID.Replace ("C", "l");
                            if (!String.IsNullOrEmpty (conference.RoomId)) {
                                gId = conference.RoomId.Replace ("C", "l");
                            }
                            GroupAPI.CreateGroup (gId, conference.c_title + "( Soft Opening Group )", showPrefix);
                        }

                        conference.CurrentConferenceStatus = type;
                        _repositoryWrapper.Confrence.Update (conference);
                        _repositoryWrapper.Save ();
                        if (conference.AutoNotiSend == true) {
                            if (type == CurrentConferenceStatus.GrandOpening || type == CurrentConferenceStatus.SoftOpening) {
                                await SendNotificationByConference (cid, type, NotificationTemplate.ConferenceStartNotification);
                            } else if (type == CurrentConferenceStatus.CloseConference) {
                                await SendNotificationByConference (cid, type, NotificationTemplate.ConferenceCloseNotification);
                            }
                        }

                        message = "Successfully change status";
                        result = true;
                    } else {
                        message = "Conference does not exist";
                    } 
                    await _hubContext.Clients.All.SendAsync ("LiveConference", conference);
                    objResponse = new { data = "", success = result, message = message };
                    GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - ChangeCurrentConferenceStatus", message + ", ConferenceId:" + cid + ", Status:" + type, _tokenData.UserID);
                    return StatusCode (StatusCodes.Status200OK, objResponse);
                } else {
                    message = "Status type is not correct";
                    objResponse = new { data = "", success = result, message = message };
                    GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - ChangeCurrentConferenceStatus", message + ", ConferenceId:" + cid, _tokenData.UserID);
                    return StatusCode (StatusCodes.Status400BadRequest, objResponse);
                }

            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - ChangeCurrentConferenceStatus", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("{cId}/ConferenceVideo")]
        public async Task<dynamic> SaveConferenceVideoLink (string cId, [FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't Save Video Link!";
            dynamic obj = param;
            try {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
                if (conference != null) {
                    conference.Video = obj.videoUrl;
                    _repositoryWrapper.Confrence.Update (conference);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Video Link is uploaded Successfully";
                    await _hubContext.Clients.All.SendAsync ("ConferenceVideo", conference);
                } else {
                    result = false;
                    message = "Conference does not exist";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - SaveConferenceVideoLink", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cId}/AutoSendNoti/{isOpen}")]
        public dynamic AutoSendNoti (string cId, bool isOpen) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't Open Auto Send Notification!";
            try {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
                if (conference != null) {
                    conference.AutoNotiSend = isOpen;
                    _repositoryWrapper.Confrence.Update (conference);
                    _repositoryWrapper.Save ();
                    result = true;
                    if (isOpen) {
                        message = "Auto Send Notification is opened";
                    } else {
                        message = "Auto Send Notification is closed";
                    }
                } else {
                    result = false;
                    message = "Conference does not exist";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - AutoSendNoti", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{cId}/SendNotification")]
        public async Task<dynamic> SendNotifications (string cId, [FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't Send Notification";
            dynamic obj = param;
            try {
                bool isAll = obj.isAll;
                string title = obj.title;
                string body = obj.description;
                string notiTemplate = obj.notiTemplate;
                string notiId = "";
                List<string> userList = new List<string> ();
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.deleteFlag == false && x.c_ID == cId).FirstOrDefault ();
                if (conference != null) {
                    if (!isAll) {
                        userList = _repositoryWrapper.ConferenceUser.GetUserListByType (cId, CurrentConferenceStatus.SoftOpening);
                    } else {
                        userList = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.Status == 1 && x.WebinarId == cId).Select (x => x.UserId).Distinct ().ToList ();
                    }

                    if (notiTemplate != null) {
                        if (notiTemplate == "Conference Start") {
                            tblNotificationTemplate notiModel = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == NotificationTemplate.ConferenceStartNotification).FirstOrDefault ();
                            title = notiModel.TitleMessage;
                            body = notiModel.BodyMessage;

                            if (isAll) {
                                conference.CurrentConferenceStatus = CurrentConferenceStatus.GrandOpening;
                            } else {
                                conference.CurrentConferenceStatus = CurrentConferenceStatus.SoftOpening;
                            }
                            _repositoryWrapper.Confrence.Update (conference);
                            _repositoryWrapper.Save ();

                        }
                        if (notiTemplate == "Conference Close") {
                            tblNotificationTemplate notiModel = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == NotificationTemplate.ConferenceCloseNotification).FirstOrDefault ();
                            title = notiModel.TitleMessage;
                            body = notiModel.BodyMessage;
                        }
                        if (notiTemplate == "Group Chat Open") {
                            tblNotificationTemplate notiModel = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == NotificationTemplate.GroupChatOpenNotification).FirstOrDefault ();
                            title = notiModel.TitleMessage;
                            body = notiModel.BodyMessage;
                        }
                        if (notiTemplate == "Group Chat Close") {
                            tblNotificationTemplate notiModel = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == NotificationTemplate.GroupChatOpenNotification).FirstOrDefault ();
                            title = notiModel.TitleMessage;
                            body = notiModel.BodyMessage;
                        }
                        string confendTime = (DateTime.ParseExact (conference.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes (Int32.Parse (conference.c_etime)).ToString ("HH:mm", CultureInfo.CurrentCulture));
                        string confTime = conference.c_stime + " " + confendTime;
                        body = GetNotificationBodyMessage (body, conference.c_title + "( " + confTime + " )");
                    }

                    string[] deviceTokenList = _repositoryWrapper.Account.FindByCondition (x => userList.Contains (x.a_ID)).Select (x => x.DeviceId).ToArray ();

                    string[] mDeviceTokenList = _repositoryWrapper.Account.FindByCondition (x => userList.Contains (x.a_ID)).Select (x => x.MobileDeviceId).ToArray ();

                    string[] deviceTokens = deviceTokenList.Concat (mDeviceTokenList).ToArray ();
                    var data = new { action = "Play" };

                    var pushSent = await PushNotificationLogic.SendPushNotification (deviceTokens, title, body, data);

                    for (int i = 0; i < userList.Count (); i++) {
                        tblNotificationSendList notiSendModel = new tblNotificationSendList ();
                        notiSendModel.UserId = userList[i];
                        notiSendModel.ViewFlag = false;
                        notiSendModel.SendDate = DateTime.UtcNow; //System.DateTime.Now;
                        notiSendModel.Status = "Send";
                        notiSendModel.CustomTitle = title;
                        notiSendModel.CustomBody = body;
                        if (notiTemplate != null) {
                            notiSendModel.NotificationId = notiId;
                        }
                        // else
                        // {
                        //     notiSendModel.CustomTitle = title;
                        //     notiSendModel.CustomBody = body;
                        // }
                        _repositoryWrapper.NotificationSendList.Create (notiSendModel);

                    }
                    _repositoryWrapper.Save ();

                    message = "Sent Notification Successfully.";
                    result = true;
                } else {
                    result = false;
                    message = "Conference does not exist";
                }
                if (result) {
                    await _hubContext.Clients.All.SendAsync ("NotificationCount", "Check Your Notification Count");
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - SendNotifications", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{cId}/Delegates")]
        public dynamic GetConferenceDelegateList (string cId, [FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to reterieve";
            dynamic obj = param;
            try {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0) {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                dynamic data = _repositoryWrapper.ConferenceUser.GetConferenceDelegateList (cId, sortField, sortBy, obj);
                objResponse = data;
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceDelegateList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("GroupConference/{IsLive}")]
        public dynamic GetGroupConfListForOrg (int IsLive) {
            dynamic objResponse = null;
            string message = "Successfully reterieve";
            bool result = false;
            bool isliveConf = false;
            dynamic currentLiveResult = null;
            dynamic data = null;
            try {
                if (IsLive == 1) {
                    isliveConf = true;
                    currentLiveResult = WebinarAPI.GetCurrentLiveWebinar ();
                    if (currentLiveResult != null) {
                        data = _repositoryWrapper.Confrence.GetconferenceListWithGroupForOrganization (currentLiveResult, isliveConf);
                    } else { message = "No live conferences currently."; }
                } else if (IsLive == 2) {
                    data = _repositoryWrapper.Confrence.GetconferenceListWithGroupForOrganization (currentLiveResult, isliveConf);
                }
                result = true;
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                message = "Failed to reterieve";
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetGroupConfListForOrg", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cId}/Materials")]
        public dynamic GetMaterialListByConferenceId (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.Material.GetMaterialListbyConferenceId (cId);
                result = true;
                message = "Successfully reterieved.";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetMaterialListByConferenceId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        // send notification when chat chanel open and close 
        private async Task SendNotificationForChat (tbl_Conference conference, string notificationId, string type) {
            if (conference != null) {
                List<string> userList = null;
                if (type == GroupChatType.SpeakerGroupChat) {
                    userList = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.Status == 1 && x.WebinarId == conference.c_ID && x.AccountType == UserType.Speaker).Select (x => x.UserId).Distinct ().ToList ();
                } else {
                    var objFreeDelegate = _repositoryWrapper.Account.FindByCondition (x => x.deleteFlag == false && x.a_type == UserType.FreeDelegate).Select (x => x.a_ID).Distinct ().ToArray ();
                    userList = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.Status == 1 && x.WebinarId == conference.c_ID && !objFreeDelegate.Contains (x.UserId)).Select (x => x.UserId).Distinct ().ToList ();
                }
                string image = _repositoryWrapper.Confrence.GetMainSpeakerImage (conference.c_ID);
                tblNotificationTemplate notificationTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == notificationId).FirstOrDefault ();
                if (notificationTemplate != null) {
                    string bodyMessage = notificationTemplate.BodyMessage;
                    notificationTemplate.BodyMessage = GetNotificationBodyMessage (bodyMessage, conference.c_title);
                }

                await GlobalFunction.SendNotification (_repositoryWrapper, userList, notificationTemplate, _tokenData.UserID, image, _hubContext);
            }
        }
        // send notification when conference open and close
        private async Task SendNotificationByConference (string cId, string type, string notificationId) {
            List<string> userList = new List<string> ();
            tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
            if (conference != null) {
                if (type == CurrentConferenceStatus.SoftOpening) {
                    userList = _repositoryWrapper.ConferenceUser.GetUserListByType (cId, type);
                } else if (type == CurrentConferenceStatus.GrandOpening || type == CurrentConferenceStatus.CloseConference) {
                    userList = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.Status == 1 && x.WebinarId == cId).Select (x => x.UserId).Distinct ().ToList ();
                }
                string image = _repositoryWrapper.Confrence.GetMainSpeakerImage (cId);
                tblNotificationTemplate notificationTemplate = _repositoryWrapper.NotificationTemplate.FindByCondition (x => x.NotificationId == notificationId).FirstOrDefault ();
                if (notificationTemplate != null) {
                    string bodyMessage = notificationTemplate.BodyMessage;
                    string confendTime = (DateTime.ParseExact (conference.c_stime, "HH:mm", CultureInfo.InvariantCulture).AddMinutes (Int32.Parse (conference.c_etime)).ToString ("HH:mm", CultureInfo.CurrentCulture));
                    string confTime = conference.c_stime + " - " + confendTime;

                    notificationTemplate.BodyMessage = GetNotificationBodyMessage (bodyMessage, conference.c_title + " (" + confTime + ") ");
                }
                await GlobalFunction.SendNotification (_repositoryWrapper, userList, notificationTemplate, _tokenData.UserID, image, _hubContext);
            }

        }

        private string GetNotificationBodyMessage (string message, string userName) {
            StringBuilder emessage = new StringBuilder (message);
            emessage.Replace ("<ConferenceName>", userName);
            return emessage.ToString ();
        }

        private bool CheckCurrentJoinStatus (string userId, tbl_Conference conference) {
            bool isOk = false;
            if (conference.CurrentConferenceStatus == CurrentConferenceStatus.GrandOpening) {
                isOk = true;
            } else if (conference.CurrentConferenceStatus == CurrentConferenceStatus.SoftOpening) {
                isOk = _repositoryWrapper.ConferenceUser.CheckUserInSoftOpening (userId, conference);
            }
            return isOk;
        }

        [HttpGet ("{cId}/EmbeddedJoin")]
        public dynamic GetComferenceForEmbeddedJoin (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            string returnUrl = "";
            string apiKey = "";
            string secretKey = "";
            string signatureEndpoint = "";
            bool result = false;
            int ConfigId = 0;
            string ZoomId = "";
            string pwd = "";
            try {
                dynamic userData = _repositoryWrapper.Account.FindByCondition (x => x.a_ID == _tokenData.UserID).FirstOrDefault ();
                dynamic expendUserObj = new ExpandedObjectFromApi (userData);

                dynamic conferenceData = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId).FirstOrDefault ();
                dynamic expendConfObj = new ExpandedObjectFromApi (conferenceData);
                if (conferenceData != null) {
                    ConfigId = conferenceData.ConfigId;
                    ZoomId = conferenceData.ZoomId;
                    if (!String.IsNullOrEmpty (conferenceData.RoomId)) {
                        string RoomId = conferenceData.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        expendConfObj = new ExpandedObjectFromApi (shareRoomConf);
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                    }
                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    if (configKeys != null) {
                        apiKey = configKeys.APIKey;
                        secretKey = configKeys.ScrectKey;
                    }
                }
                pwd = expendConfObj.c_Password;
                tblShowConfig returnUrlData = _repositoryWrapper.ShowConfig.FindByCondition (x => x.Name == "EmbeddedReturnUrl" && x.Status == 1).FirstOrDefault ();
                if (returnUrlData != null) {
                    returnUrl = returnUrlData.Value;
                }

                String ts = (ToTimestamp (DateTime.UtcNow.ToUniversalTime ()) - 30000).ToString ();
                signatureEndpoint = GenerateSignatureToken (apiKey, secretKey, ZoomId, ts, Role.Join);

                dynamic resultData = new {
                    meetingNumber = ZoomId,
                    password = pwd,
                    userName = expendUserObj.a_fullname,
                    userEmail = expendUserObj.a_email,
                    returnUrl = returnUrl,
                    apiKey = apiKey,
                    signatureEndpoint = signatureEndpoint
                };
                objResponse = new { data = resultData, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetComferenceForEmbeddedJoin", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/ConferenceCategory")]
        public dynamic GetConferenceCateName (string ID) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.ConferenceCategory.GetConferenceCateName (ID);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceCateName", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Conferences/{Id}")]
        public dynamic GetConferencesById (string Id) {
            dynamic objResponse = null;
            try {
                var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
                var Configuration = appsettingbuilder.Build ();
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == Id && x.deleteFlag == false).FirstOrDefault ();;

                //if (!String.IsNullOrEmpty(confrence.RoomId))
                //{
                //Id = confrence.RoomId;
                //}
                var objResult = _repositoryWrapper.Confrence.GetConferenceById (Id, _tokenData.UserID, confrence);
                objResponse = new { data = objResult };

                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferencesById", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{ID}/SponsorLogo")]
        public dynamic GetSponsorLogo (string ID) {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.Confrence.GetSponsorLogoByConferenceID (ID);
                result = true;
                message = "Successfully reterieve";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetSponsorLogo", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("{cId}/SponsorLogo")]
        public async Task<dynamic> SaveSponsorLogo (IFormFile file, string cId) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't Save sponsor logo";
            try {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
                if (conference != null) {
                    //upload file first
                    var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
                    var Configuration = appsettingbuilder.Build ();
                    string returnUrl = Configuration.GetSection ("appSettings:SingnalUrl").Value;
                    // string url = GlobalFunction.SaveFile (file, cId, returnUrl);
                    string url = await GlobalFunction.SaveFileToS3 (file, cId, Configuration);

                    conference.SponsorLogo = url;
                    _repositoryWrapper.Confrence.Update (conference);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Sponsor logo is uploaded Successfully.";
                } else {
                    result = false;
                    message = "Conference does not exist.";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - SaveSponsorLogo", "Update sponsorLogo of conference." + message, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - SaveSponsorLogo", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("MyFavouriteConferences")]
        public dynamic GetMyFavouriteConferences ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic paraObj = param;
            dynamic objResponse = null;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;
                if (userType == "0") {
                    var objResult = _repositoryWrapper.Confrence.GetMyFavouriteConferences (userId, paraObj, returnUrl);
                    GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetMyFavouriteConferences", "Retrieve my favourite conference.", _tokenData.UserID);
                    objResponse = new { data = objResult };
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetMyFavouriteConferences", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("{ID}/ConferenceSpeakers/{type}")]
        public dynamic GetConferenceSpeakersByConferenceId (string ID, string type) {
            dynamic objResponse = null;
            try {
                dynamic data = _repositoryWrapper.Confrence.GetConferenceSpeakersByConferenceId (ID, type);
                objResponse = new { data = data, success = true, message = "Successfully reterieve" };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceSpeakersByConferenceId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("ConferenceSpeakers/SequenceNo")]
        public dynamic UpdateSpeakerSequenceNo ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic objParam = param;
            dynamic objResponse = null;
            try {
                _repositoryWrapper.ConferenceUser.UpdateSpeakerSequenceNo (objParam);
                ClearConferenceCache ();
                objResponse = new { data = "", success = true, message = "Successfully updated" };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - UpdateSpeakerSequenceNo", "Update speaker sequence No for organizer site.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - UpdateSpeakerSequenceNo", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("Live")]
        public dynamic GetLiveConferences () {
            dynamic objResponse = null;
            try {
                var confList = _repositoryWrapper.Confrence.GetLiveConferences (_tokenData.UserID);
                objResponse = new { data = confList };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - GetLiveConferences", "Retrieve all live conferences by userID.", _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetLiveConferences", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("ConferenceList")]
        public dynamic GetAllConferenceListGroupByTime ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            //Conference list for group template
            dynamic paraObj = param;
            dynamic objResponse = null;
            dynamic objResult;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;
                string paraDate = _repositoryWrapper.Confrence.GetParaDate (paraObj);
                if (userType == "0") {
                    if (GlobalFunction.isRequireCreateCache ()) {
                        dynamic confe = new ExpandedObjectFromApi (ENUM_Cache.ConferenceList);
                        string key = confe.key + paraDate;
                        if (!_cache.TryGetValue (key, out objResult)) {
                            objResult = _repositoryWrapper.Confrence.GetAllWebinarListGroupTemplate (userId, paraObj, returnUrl);
                            GlobalFunction.CacheTryGetValueSet (key, objResult, _cache);
                        }
                    } else {
                        objResult = _repositoryWrapper.Confrence.GetAllWebinarListGroupTemplate (userId, paraObj, returnUrl);
                    }
                    objResponse = new { data = objResult };
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAllConferenceListGroupByTime", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("FavConferences/GroupTemplate")]
        public dynamic GetMyFavouriteConferencesGroupByTime ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic paraObj = param;
            dynamic objResponse = null;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;

                if (userType == "0") {
                    //var objResult = _repositoryWrapper.Confrence.GetMyFavouriteConferencesGroupByTime(userId, paraObj, returnUrl);
                    var objResult = _repositoryWrapper.Confrence.GetMyFavouriteConferencesGroupTemplate (userId, paraObj, returnUrl);
                    objResponse = new { data = objResult };
                }

                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetMyFavouriteConferencesGroupByTime", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost ("MyConferences/GroupTemplate")]
        public dynamic GetAttendanceConferenceGroupByTime ([FromBody] JObject param) {
            dynamic objResponse = null;
            dynamic obj = param;
            try {
                string confDate = obj.conferenceDate;
                string aId = _tokenData.UserID;
                dynamic conferenceList = _repositoryWrapper.ConferenceUser.GetAttandanceConferencGroupTemplate (aId, returnUrl, confDate);
                //dynamic conferenceList = _repositoryWrapper.ConferenceUser.GetAttandanceConferencGroupByTime(aId, returnUrl, confDate);
                objResponse = new { data = conferenceList };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetAttendanceConferenceGroupByTime", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cid}/JoinConferenceOpeningChat")]
        public dynamic JoinConferenceOpeningChat (string cid, string type) {
            string userid = "";
            string userType = "";
            string message = "";
            dynamic response = null;
            string userId = _tokenData.UserID;
            bool isOK = false;
            try {
                userid = _tokenData.UserID;
                string showPrefix = _repositoryWrapper.ShowConfig
                    .FindByCondition (x => x.Name == ShowConfigNames.ShowPrefix && x.Status == 1)
                    .Select (x => x.Value).FirstOrDefault ();

                userType = _repositoryWrapper.ConferenceUser.FindByCondition (x => x.UserId == userid && x.WebinarId == cid && x.Status == 1)
                    .Select (x => x.AccountType).FirstOrDefault ();

                dynamic conferenceData = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid).FirstOrDefault ();
                dynamic expendConfObj = new ExpandedObjectFromApi (conferenceData);
                if (conferenceData != null) {
                    if (!String.IsNullOrEmpty (conferenceData.RoomId)) {
                        cid = conferenceData.RoomId;
                    }
                }

                string gId = cid.Replace ("C", "l");
                isOK = GroupAPI.JoinMember (userId, gId, userType, showPrefix);
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - JoinConferenceOpeningChat", "GroupChatID:" + gId + ", JoinMemberId:" + userId + ", AccountType:" + userType, _tokenData.UserID);
                response = new { message = message, success = isOK, data = "" };
                return StatusCode (StatusCodes.Status200OK, response);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - JoinConferenceOpeningChat", ex.Message.ToString (), _tokenData.UserID);
                response = new { message = message, success = isOK, data = "", error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [HttpGet ("{ExhID}/ShowCase")]
        public dynamic GetExhibitorShowCase (string ExhID) {
            dynamic objResponse = null;
            try {
                var obj = _repositoryWrapper.Confrence.GetExhibitorShowCase (ExhID);
                objResponse = new { data = obj };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetExhibitorShowCase", "Retrieve exhibitor show case by exhibitorID, " + ExhID, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetExhibitorShowCase", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("ConferenceSpeakers/SpeakerTypes")]
        public dynamic GetConferenceSpeakerTypes () {
            dynamic objResponse = null;
            try {
                var obj = _repositoryWrapper.Confrence.GetConferenceSpeakerType ();
                objResponse = new { data = obj };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceSpeakerTypes", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cid}/UserJoinStatus")]
        public dynamic CheckJoinStatusforUser (string cid) {
            bool result = true;
            string nextPageToken = "";
            try {
                string userId = _tokenData.UserID;
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    bool isOk = CheckCurrentJoinStatus (userId, confrence);
                    if (isOk == true) {
                        int ConfigId = confrence.ConfigId;
                        string ZoomId = confrence.ZoomId;

                        if (!String.IsNullOrEmpty (confrence.RoomId)) {
                            string RoomId = confrence.RoomId;
                            tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                            ConfigId = shareRoomConf.ConfigId;
                            ZoomId = shareRoomConf.ZoomId;
                        }
                        ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                        ZoomToken.ConfigKeys = configKeys;

                        bool isLive = false;
                        dynamic currentLiveResult = WebinarAPI.GetCurrentLiveWebinar ();
                        var currentTempLiveResult = JsonConvert.DeserializeObject<LiveWebinarList> (currentLiveResult.ToString ());
                        List<LiveWebinar> liveWebinarList = currentTempLiveResult.webinars;
                        isLive = liveWebinarList.Exists (x => x.id == ZoomId);

                        tbl_Account user = (_repositoryWrapper.Account.FindByCondition (x => x.a_ID == userId && x.deleteFlag == false)).FirstOrDefault ();
                        if (isLive) {
                            LiveWebinar webinarList = liveWebinarList.Find (x => x.id == ZoomId);
                            dynamic participantQuery = WebinarAPI.GetWebinarLiveParticipants (ZoomId, "300", "");
                            ParticipantList currentTempParticipantResult = JsonConvert.DeserializeObject<ParticipantList> (participantQuery.ToString ());
                            List<Participant> liveParticipants = currentTempParticipantResult.participants;
                            liveParticipants = liveParticipants.FindAll (x => x.user_name == user.a_fullname && x.email == user.a_email && (x.leave_time == "" || x.leave_time == null));
                            if (liveParticipants.Count () > 0) {
                                result = false;
                                return new { result = result, message = "User is joining the meeting." };
                            }
                            if (result) {
                                nextPageToken = currentTempParticipantResult.next_page_token;
                                while (!string.IsNullOrEmpty (nextPageToken)) {
                                    participantQuery = WebinarAPI.GetWebinarLiveParticipants (ZoomId, "300", nextPageToken);
                                    if (participantQuery != null) {
                                        currentTempParticipantResult = JsonConvert.DeserializeObject<ParticipantList> (participantQuery.ToString ());
                                        liveParticipants = currentTempParticipantResult.participants;
                                        liveParticipants = liveParticipants.FindAll (x => x.user_name == user.a_fullname && x.email == user.a_email && (x.leave_time == "" || x.leave_time == null));
                                        nextPageToken = currentTempParticipantResult.next_page_token;
                                        if (liveParticipants.Count () > 0) {
                                            result = false;
                                            return new { result = result, message = "User is joining the meeting." };
                                        }
                                    } else {
                                        nextPageToken = "";
                                    }
                                }
                            }

                        }
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - CheckJoinStatusforUser", ex.Message.ToString (), _tokenData.UserID);
            }
            GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - CheckJoinStatusforUser", "Check user join status for the conference: " + cid, _tokenData.UserID);
            return new { result = result, message = "" };
        }

        private string GenerateSignatureToken (string apiKey, string apiSecret, string meetingNumber, string ts, string role) {
            string message = String.Format ("{0}{1}{2}{3}", apiKey, meetingNumber, ts, role);
            apiSecret = apiSecret ?? "";
            var encoding = new System.Text.ASCIIEncoding ();
            byte[] keyByte = encoding.GetBytes (apiSecret);
            byte[] messageBytesTest = encoding.GetBytes (message);
            string msgHashPreHmac = System.Convert.ToBase64String (messageBytesTest);
            byte[] messageBytes = encoding.GetBytes (msgHashPreHmac);
            using (var hmacsha256 = new HMACSHA256 (keyByte)) {
                byte[] hashmessage = hmacsha256.ComputeHash (messageBytes);
                string msgHash = System.Convert.ToBase64String (hashmessage);
                string token = String.Format ("{0}.{1}.{2}.{3}.{4}", apiKey, meetingNumber, ts, role, msgHash);
                var tokenBytes = System.Text.Encoding.UTF8.GetBytes (token);
                return System.Convert.ToBase64String (tokenBytes).TrimEnd (padding);
            }
        }

        private long ToTimestamp (DateTime value) {
            long epoch = (value.Ticks - 621355968000000000) / 10000;
            return epoch;
        }

        private string GenerateSignature (string apiKey, string secretKey, string meetingNumber, string role) {
            string signature = "";
            try {
                DateTime ufcDateTime = DateTime.UtcNow;
                ufcDateTime.AddMinutes (-1);
                BigInteger time = (BigInteger) (ufcDateTime - new DateTime (1970, 1, 1)).TotalSeconds;
                time = time * 1000;
                var dataBytes = Encoding.UTF8.GetBytes (apiKey + meetingNumber + time + role);
                string data = Convert.ToBase64String (dataBytes);
                byte[] key = Encoding.UTF8.GetBytes (secretKey);
                byte[] message = Encoding.UTF8.GetBytes (data);
                var hashValue = new HMACSHA256 (key);
                var hash = hashValue.ComputeHash (message);
                string sig = apiKey + "." + meetingNumber + "." + time + "." + role + "." + Convert.ToBase64String (hash);
                byte[] sigBytes = Encoding.UTF8.GetBytes (sig);
                string sigStrTr = StrTr (Convert.ToBase64String (sigBytes), "+/", "-_");
                signature = sigStrTr.ToString ().Trim ('=');
                return signature;
            } catch {
                return signature;
            }
        }

        private string StrTr (string source, string frm, string to) {
            char[] input = source.ToCharArray ();
            bool[] replaced = new bool[input.Length];

            for (int j = 0; j < input.Length; j++)
                replaced[j] = false;

            for (int i = 0; i < frm.Length; i++) {
                for (int j = 0; j < input.Length; j++)
                    if (replaced[j] == false && input[j] == frm[i]) {
                        input[j] = to[i];
                        replaced[j] = true;
                    }
            }
            return new string (input);
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("{cId}/ConferenceInfoPicture")]
        public async Task<dynamic> SaveConferenceInfoPicture (IFormFile file, string cId) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Can't save conference info picture.";
            try {
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
                if (conference != null) {
                    //upload file first
                    var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
                    var Configuration = appsettingbuilder.Build ();
                    string returnUrl = Configuration.GetSection ("appSettings:SingnalUrl").Value;
                    // string url = GlobalFunction.SaveFile (file, cId, returnUrl);
                    string url = await GlobalFunction.SaveFileToS3 (file, cId, Configuration);

                    conference.InfoPic = url;
                    _repositoryWrapper.Confrence.Update (conference);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Conference info picture is uploaded Successfully.";
                } else {
                    result = false;
                    message = "Conference does not exist.";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - SaveConferenceInfoPicture", message + ", ConferenceID:" + cId, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - SaveConferenceInfoPicture", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("{cid}/SpeakerAccess")]
        public dynamic GetSpeakerAccessUserList ([FromBody] Newtonsoft.Json.Linq.JObject param, string cid) {
            dynamic obj = param;
            string message = "Failed to retrieve.";
            dynamic response = null;
            bool result = false;
            dynamic responseObj = null;
            bool isExit = false;
            string[] regEmailList = null;
            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;
                    string type = confrence.Type;
                    if (!String.IsNullOrEmpty (confrence.RoomId)) {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                        type = shareRoomConf.Type;
                    }
                    if (type == ConferenceType.Default) {
                        isExit = true;
                    }

                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    if (configKeys == null) {
                        message = "Zoom configuration Id is not correct!";
                    } else {
                        ZoomToken.ConfigKeys = configKeys;

                        string sortField = null;
                        string sortBy = "";

                        if (obj.sort.Count > 0) {
                            var sort = obj.sort[0];
                            sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                            sortField = sort.field.Value;
                        }
                        if (sortField == null || sortField == "")
                            sortField = "fullName";
                        if (sortBy == null || sortBy == "")
                            sortBy = "asc";

                        // Get Panelist Access (Speaker Acccess) Email List from Zoom
                        dynamic panelistResult = PanelistAPI.GetPanelists (ZoomId, _tokenData.UserID);
                        Panelist tmpPanelistResult = JsonConvert.DeserializeObject<Panelist> (panelistResult.ToString ());
                        List<Panelists> panelists = tmpPanelistResult.panelists;
                        string[] panelistEmail = panelists.Select (x => x.email).ToArray ();

                        if (isExit) {
                            //check the registrant that exist or not in zoom.
                            regEmailList = getApprovedRegistrants (ZoomId);
                        }
                        responseObj = _repositoryWrapper.Account.GetSpeakerAccessUserList (_tokenData.UserID, cid, sortField, sortBy, obj, regEmailList, panelistEmail, type);
                        message = "Successfully retrieved.";
                        result = true;
                    }
                } else {
                    message = "There is no active conference with " + cid;
                    result = false;
                }
                response = new { success = result, data = responseObj, message = message };
                return StatusCode (StatusCodes.Status200OK, response);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetSpeakerAccessUserList", ex.Message.ToString (), _tokenData.UserID);
                response = new { success = result, data = responseObj, message = message, error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpPost ("{cid}/DelegateAccess")]
        public dynamic GetSpeakerDelegateUserList ([FromBody] Newtonsoft.Json.Linq.JObject param, string cid) {
            dynamic obj = param;
            string message = "Failed to retrieve.";
            dynamic response = null;
            bool result = false;
            dynamic responseObj = null;
            bool isExit = false;

            try {
                tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault ();
                if (confrence != null) {
                    int ConfigId = confrence.ConfigId;
                    string ZoomId = confrence.ZoomId;
                    string type = confrence.Type;

                    if (!String.IsNullOrEmpty (confrence.RoomId)) {
                        string RoomId = confrence.RoomId;
                        tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault ();
                        ConfigId = shareRoomConf.ConfigId;
                        ZoomId = shareRoomConf.ZoomId;
                        type = shareRoomConf.Type;
                    }
                    // Embedded is no need Required Registration 
                    if (type == ConferenceType.Default) {
                        isExit = true;
                    }

                    ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId (ConfigId);
                    if (configKeys == null) {
                        message = "Zoom configuration Id is not correct!";
                    } else {
                        ZoomToken.ConfigKeys = configKeys;

                        string sortField = null;
                        string sortBy = "";
                        if (obj.sort.Count > 0) {
                            var sort = obj.sort[0];
                            sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                            sortField = sort.field.Value;
                        }
                        if (sortField == null || sortField == "")
                            sortField = "fullName";
                        if (sortBy == null || sortBy == "")
                            sortBy = "asc";
                        string[] accessEmailList = null;
                        if (isExit) {
                            //check the registrant that exist or not in zoom.
                            accessEmailList = getApprovedRegistrants (ZoomId);
                        }

                        responseObj = _repositoryWrapper.Account.GetDelegateAccessUserList (_tokenData.UserID, cid, sortField, sortBy, obj, accessEmailList, type);
                        message = "Successfully retrieved.";
                        result = true;
                    }
                } else {
                    message = "There is no active conference with " + cid;
                    result = false;
                }
                response = new { success = result, data = responseObj, message = message };
                return StatusCode (StatusCodes.Status200OK, response);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetSpeakerDelegateUserList", ex.Message.ToString (), _tokenData.UserID);
                response = new { success = result, data = responseObj, message = message, error_code = ex.HResult };
                return StatusCode (StatusCodes.Status500InternalServerError, response);
            }
        }

        [Authorize (Roles = "Organizer")]
        [HttpGet ("{cId}/DataSyn/{Value}")]
        public dynamic UpdateDataSynchronizationStatus (string cId, bool Value) {
            dynamic objResponse = null;
            bool result = false;
            string message = "Failed to update.";
            try {
                tbl_Conference con = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).FirstOrDefault ();
                if (con != null) {
                    con.IsDataSyn = Value;
                    _repositoryWrapper.Confrence.Update (con);
                    _repositoryWrapper.Save ();
                    result = true;
                    message = "Updated Successfully.";
                } else {
                    result = false;
                    message = "Conference does not exist";
                }
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Info, "ConferenceController - UpdateDataSynchronizationStatus", message + ", ConferenceID:" + cId + ", Status:" + Value, _tokenData.UserID);
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - UpdateDataSynchronizationStatus", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("{cId}/Questionnaire")]
        public dynamic GetQuestionnaireIDByConferenceId (string cId) {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try {
                dynamic data = _repositoryWrapper.Confrence.GetQuestionnaireIDByConferenceId (cId);
                result = true;
                message = "Successfully reterieve.";
                objResponse = new { data = data, success = result, message = message };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetQuestionnaireIDByConferenceId", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        private string[] getApprovedRegistrants (string ZoomId) {
            int pageSize = 300;
            int pageNumber = 1;
            bool isExit = true;
            List<string> tmpParticipantArray = new List<string> ();
            string[] result = null;
            while (isExit) {
                dynamic participantQuery = RegistrantAPI.GetRegistrants (ZoomId, pageSize, pageNumber, "");
                RegistrantList currentTempParticipantResult = JsonConvert.DeserializeObject<RegistrantList> (participantQuery.ToString ());
                List<Registrant> participants = currentTempParticipantResult.registrants;

                if (currentTempParticipantResult.total_records > 0) {
                    var val = participants.Select (x => x.email).ToList ();
                    tmpParticipantArray.AddRange (val);
                }
                if (currentTempParticipantResult.total_records > (pageSize * pageNumber)) {
                    pageNumber++;
                } else {
                    isExit = false;
                }
            }
            if (tmpParticipantArray != null) {
                result = tmpParticipantArray.ToArray ();
            }
            return result;
        }

        [HttpPost ("ActiveConferences")]
        public dynamic GetActiveConferenceList ([FromBody] Newtonsoft.Json.Linq.JObject param) {
            dynamic paraObj = param;
            dynamic objResponse = null;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;
                if (userType == "0") {
                    objResponse = _repositoryWrapper.Confrence.GetActiveConferenceList (paraObj, userId);
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetActiveConferenceList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet ("ConferenceVideoLink/{cId}")]
        public dynamic GetConferenceVideolink (string cId) {
            dynamic objResponse = null;
            string link = string.Empty;
            try {
                link = _repositoryWrapper.Confrence.FindByCondition (x => x.c_ID == cId && x.deleteFlag == false).Select (x => x.Video).FirstOrDefault ();
                objResponse = new { data = link, success = true, message = "Successfully reterieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceVideolink", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, null);
            }
        }

        [HttpPost ("VideoList")]
        public dynamic GetVideoList () {
            dynamic objResponse = null;
            dynamic confVideoList;
            try {
                confVideoList = _repositoryWrapper.Confrence.GetConfernceVideoList ();
                objResponse = new { data = confVideoList, success = true, message = "Successfully reterieved." };
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetVideoList", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, null);
            }
        }

        [HttpGet ("ConferenceAccessRight")]
        public dynamic GetConferenceAccessright () {
            dynamic objResponse = null;
            try {
                string userId = _tokenData.UserID;
                string userType = _tokenData.LoginType;
                if (userType == "0") {
                    objResponse = _repositoryWrapper.Confrence.GetConferenceAccessRight (userId);
                }
                return StatusCode (StatusCodes.Status200OK, objResponse);
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "ConferenceController - GetConferenceAccessright", ex.Message.ToString (), _tokenData.UserID);
                return StatusCode (StatusCodes.Status500InternalServerError, objResponse);
            }
        }
        private void ClearConferenceCache () {
            dynamic data = null;
            dynamic keyObj = new ExpandedObjectFromApi (ENUM_Cache.ConferenceList);
            string key = keyObj.key;
            dynamic days = _repositoryWrapper.Day.FindByCondition (x => x.deleteFlag == false).
            Select (x => x.d_date).AsNoTracking ().ToList ();
            foreach (var day in days) {
                if (_cache.TryGetValue (key + day, out data)) {
                    GlobalFunction.CacheRemove (key + day, _cache);
                }
            }
        }

    }
}