using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using IVC.API.ZoomAPI;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PollReportController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public PollReportController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [Authorize(Roles = "Organizer")]
        [HttpGet("{id}")]
        public dynamic GetPollColumns(string id)
        {
            dynamic objResponse = null;
            try
            {
                // Getting poll data from zoom first
                dynamic pollData = GetPollDataListFromZoom(id);
                if (pollData != null)
                {
                    // Saving poll data to local database 
                    _repositoryWrapper.ZoomPollResult.UpdatePollData(pollData,id,_tokenData.UserID);
                }
                var objPoll = _repositoryWrapper.ZoomPollResult.FindByCondition(x => x.WebinarId == id && x.Status == 1).Select(x => x.WebinarId).FirstOrDefault();
                if (objPoll != null)
                {
                    var objResult = _repositoryWrapper.ZoomPollResult.GetPollColumns(id);
                    objResponse = objResult;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PollReportController - GetPollColumns", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}")]
        public dynamic GetPollList([FromBody] JObject param, String id)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "userName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                tbl_Conference conference = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == id && x.deleteFlag == false).FirstOrDefault();
                if (conference != null)
                {
                    GlobalFunction.DataSyn(_repositoryWrapper,conference,_tokenData.UserID);
                    var objPoll = _repositoryWrapper.ZoomPollResult.FindByCondition(x => x.WebinarId == id && x.Status == 1).Select(x => x.WebinarId).FirstOrDefault();
                    if (objPoll != null)
                    {
                        var objResult = _repositoryWrapper.ZoomPollResult.GetPollListByWebinarId(obj, sortField, sortBy, id, false);
                        objResponse = objResult;
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PollReportController - GetPollList", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;
        }

        [Authorize(Roles = "Organizer")]
        [HttpPost("{id}/Report")]
        public dynamic ExportPollList([FromBody] JObject param, String id)
        {
            dynamic obj = param;
            dynamic objResponse = null;
            byte[] fileContents;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "userName";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                var objResult = _repositoryWrapper.ZoomPollResult.GetPollListByWebinarId(obj, sortField, sortBy, id, true);

                List<string> Header = new List<string>();
                Header.Add("UserName");
                List<string> headerList = _repositoryWrapper.ZoomPollResult.FindByCondition(x => x.WebinarId == id && x.Status == 1).Select(x => x.Question).Distinct().ToList();
                Header.AddRange(headerList);
                List<string> fieldName = new List<string>();
                fieldName.Add("userName");
                List<string> fieldNameList = _repositoryWrapper.ZoomPollResult.FindByCondition(x => x.WebinarId == id && x.Status == 1).Select(x => x.Question).Distinct().ToList();
                fieldName.AddRange(fieldNameList);
                fileContents = GlobalFunction.ExportExcel(objResult, Header, fieldName, "PollReport");
                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }
                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"PollReport_{DateTime.Now.ToString("yyyyMMddHHmmss")}.Xlsx"
                );

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PollReportController - ExportPollList", ex.Message.ToString(), _tokenData.UserID);
            }
            return objResponse;
        }

        private dynamic GetPollDataListFromZoom(string cid)
        {
            dynamic response = null;
            tbl_Conference confrence = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == cid && x.deleteFlag == false).FirstOrDefault();
            if (confrence != null)
            {
                int ConfigId = confrence.ConfigId;
                string ZoomId = confrence.ZoomId;
                bool? IsdataSyn = confrence.IsDataSyn;
                if (IsdataSyn == null) IsdataSyn = false;
                if(IsdataSyn == false) return response;

                if (!String.IsNullOrEmpty(confrence.RoomId))
                {
                    string RoomId = confrence.RoomId;
                    tbl_Conference shareRoomConf = _repositoryWrapper.Confrence.FindByCondition(x => x.c_ID == RoomId && x.deleteFlag == false).FirstOrDefault();
                    ConfigId = shareRoomConf.ConfigId;
                    ZoomId = shareRoomConf.ZoomId;
                }
                ConfigKeys configKeys = _repositoryWrapper.ZoomConfiguration.GetConfigurationByConfigId(ConfigId);
                ZoomToken.ConfigKeys = configKeys;
                response = PollAPI.GetPollReportList(ZoomId, _tokenData.UserID);
            }
            return response;
        }

        
        
    }
}