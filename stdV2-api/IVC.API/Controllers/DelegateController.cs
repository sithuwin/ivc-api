using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using IVC.DAL.Models;
using IVC.DAL.Repository.Interface;
using IVC.DAL.Util;
using Serilog;
namespace IVC.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DelegateController : BaseController
    {
        private IRepositoryWrapper _repositoryWrapper;
        public DelegateController(IRepositoryWrapper RW)
        {
            _repositoryWrapper = RW;
        }

        [HttpPost]
        public dynamic GetDelegateList([FromBody] JObject param)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve";
            bool result = true;
            dynamic obj = param;
            try
            {
                string sortField = null;
                string sortBy = "";

                if (obj.sort.Count > 0)
                {
                    var sort = obj.sort[0];
                    sortBy = sort.dir == null ? sortBy : sort.dir.Value;
                    sortField = sort.field.Value;
                }
                if (sortField == null || sortField == "")
                    sortField = "a_fullname";
                if (sortBy == null || sortBy == "")
                    sortBy = "asc";
                dynamic data = _repositoryWrapper.Account.GetDelegateList(_tokenData.UserID, sortField, sortBy, obj);

                objResponse = data;
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "DelegateController - GetDelegateList", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpGet("{id}")]
        public dynamic GetDelegateById(string id)
        {
            dynamic objResponse = null;
            string message = "Failed to reterieve.";
            bool result = false;
            try
            {
                string exhid = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == _tokenData.UserID && x.deleteFlag == false).Select(x => x.ExhId).FirstOrDefault();
                var visitedVisitorList = _repositoryWrapper.Audit.FindByCondition(x => x.AlternativeId == exhid && x.LogType == AuditLogType.VisitExhibitor).Select(x => x.UserId).Distinct().ToArray();
                if (id == _tokenData.UserID || visitedVisitorList.Contains(id))
                {
                    dynamic data = _repositoryWrapper.Account.GetDelegateById(id);
                    result = true;
                    message = "Successfully reterieved";
                    objResponse = new { data = data, success = result, message = message };
                    
                    return StatusCode(StatusCodes.Status200OK, objResponse);
                }else{
                    objResponse = new { data = "", success = false, message = message };
                    return StatusCode(StatusCodes.Status404NotFound, objResponse);
                }
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "DelegateController - GetDelegateById", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }

        [HttpPost("{deviceId}")]
        public dynamic UpdateMobileDeviceId(string deviceId)
        {
            dynamic objResponse = null;
            string message = "Failed to update";
            bool result = false;
            try
            {
                tbl_Account account = _repositoryWrapper.Account.FindByCondition(x => x.a_ID == _tokenData.UserID && x.deleteFlag == false).FirstOrDefault();
                if (account != null)
                {
                    account.MobileDeviceId = deviceId;
                    _repositoryWrapper.Account.Update(account);
                    _repositoryWrapper.Save();
                    result = true;
                    message = "Successfully updated";
                }
                else
                {
                    message = "User does not exist";
                }
                objResponse = new { data = "", success = result, message = message };
                return StatusCode(StatusCodes.Status200OK, objResponse);
            }
            catch (Exception ex)
            {
                objResponse = new { data = "", success = result, message = message };
                GlobalFunction.WriteSystemLog(LogType.Error, "DelegateController - UpdateMobileDeviceId", ex.Message.ToString(), _tokenData.UserID);
                return StatusCode(StatusCodes.Status500InternalServerError, objResponse);
            }
        }
    }
}