using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
using IVC.DAL.Util;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
namespace IVC.API.ZoomAPI
{
    public static class WebinarAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);
        public static dynamic CreateWebinar(dynamic obj, string userId, string accountId)
        {

            dynamic responseData = null;
            bool success = false;
            string message = "";
            string conferenceType = obj.confType;
            ZoomToken.GenerateZoomToken();
            try
            {

                Webinar webinar = new Webinar();
                Recurrence recurrence = new Recurrence();
                Settings settings = new Settings();

                webinar.topic = obj.topic;
                webinar.type = 5;
                DateTime conferenDate = obj.Conf_Date;
                DateTime startTime = obj.start_time;
                webinar.start_time = conferenDate.ToString("yyyy-MM-ddT") + startTime.ToString("HH:mm:ssZ"); //startTime.ToString ("yyyy-MM-ddTHH:mm:ssZ");
                webinar.duration = obj.duration;
                webinar.timezone = obj.timezone;
                webinar.password = obj.password;
                webinar.agenda = obj.agenda;

                settings.host_video = true;
                settings.panelists_video = true;
                settings.practice_session = true;
                settings.hd_video = true;
                settings.audio = "both";
                settings.auto_recording = "none";
                settings.enforce_login = false;
                settings.enforce_login_domains = "";
                settings.alternative_hosts = "";
                settings.close_registration = true;
                settings.show_share_button = true;
                settings.allow_multiple_devices = false;
                settings.registrants_email_notification = false;
                if (ConferenceType.Embedded == conferenceType)
                {
                    settings.approval_type = 2;
                    settings.registration_type = 3;
                }
                else
                {
                    settings.approval_type = 1;
                    settings.registration_type = 2;
                }
                webinar.settings = settings;

                string zoomUserid = accountId;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var content = new StringContent(JsonConvert.SerializeObject(webinar), Encoding.UTF8, "application/json");
                var response = client.PostAsync("https://api.zoom.us/v2/users/" + zoomUserid + "/webinars", content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Creating Webinar API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode == StatusCodes.Status201Created)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                    if (responseJson != null)
                    {
                        success = true;
                        responseData = responseJson.id;
                        message = "Successful!";
                    }
                }
                else
                {
                    success = true;
                    responseData = null;
                    message = "Can't Create Webinar!";
                    GlobalFunction.WriteSystemLog(LogType.Error, "Creating Webinar", response.Content.ReadAsStringAsync().Result, userId);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - CreateWebinar ", ex.Message.ToString(), userId);
            }
            //return responseData;
            return new { success = success, data = responseData, message = message };
        }

        public static dynamic UpdateWebinar(dynamic obj, string zoomId, string userId, string conferenceType)
        {
            ZoomToken.GenerateZoomToken();

            bool result = false;
            string message = "";
            try
            {

                Webinar webinar = new Webinar();
                Recurrence recurrence = new Recurrence();
                Settings settings = new Settings();

                webinar.topic = obj.topic;
                webinar.type = 5;
                //DateTime startTime = obj.start_time;
                //webinar.start_time = startTime.ToString("yyyy-MM-ddTHH:mm:ssZ");
                DateTime conferenDate = obj.Conf_Date;
                DateTime startTime = obj.start_time;
                webinar.start_time = conferenDate.ToString("yyyy-MM-ddT") + startTime.ToString("HH:mm:ssZ"); //startTime.ToString ("yyyy-MM-ddTHH:mm:ssZ");
                webinar.duration = obj.duration;
                webinar.timezone = obj.timezone;
                webinar.password = obj.password;
                webinar.agenda = obj.agenda;

                settings.host_video = true;
                settings.panelists_video = true;
                settings.practice_session = true;
                settings.hd_video = true;
                settings.audio = "both";
                settings.auto_recording = "none";
                settings.enforce_login = false;
                settings.enforce_login_domains = "";
                settings.alternative_hosts = "";
                settings.close_registration = true;
                settings.show_share_button = true;
                settings.allow_multiple_devices = false;
                settings.registrants_email_notification = false;
                if (ConferenceType.Embedded == conferenceType)
                {
                    settings.approval_type = 2;
                    settings.registration_type = 3;
                }
                else
                {
                    settings.approval_type = 1;
                    settings.registration_type = 2;
                }
                webinar.settings = settings;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var content = new StringContent(JsonConvert.SerializeObject(webinar), Encoding.UTF8, "application/json");
                var response = client.PatchAsync("https://api.zoom.us/v2/webinars/" + zoomId, content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Creating Webinar API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode == StatusCodes.Status204NoContent)
                {
                    result = true;
                    message = "Successful!";
                }
                else
                {
                    result = false;
                    message = "Can't Update!";
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - UpdateWebinar ", ex.Message.ToString(), userId);
            }
            return new { data = result, message = message }; ;

        }

        public static dynamic GetWebinarDetails(string zoomId, string userId)
        {
            dynamic responseJson = null;
            ZoomToken.GenerateZoomToken();
            try
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.GetAsync("https://api.zoom.us/v2/webinars/" + zoomId).Result;
                var responseContent = response.Content.ReadAsStringAsync().Result;
                responseJson = JsonConvert.DeserializeObject(responseContent);

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - GetWebinarDetails ", ex.Message.ToString(), userId);
            }
            return responseJson;

        }
        public static dynamic DeleteWebinarById(string zoomId, string userId)
        {

            bool result = false;
            ZoomToken.GenerateZoomToken();
            try
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.DeleteAsync("https://api.zoom.us/v2/webinars/" + zoomId).Result;
                if (response.IsSuccessStatusCode == true)
                {
                    result = true;
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - DeleteWebinarById ", ex.Message.ToString(), userId);
            }
            return result;

        }
        public static dynamic GetCurrentLiveWebinar()
        {
            dynamic result = null;
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                //string type = "live";
                //string pagesize = "50";
                //string nextpagetoken = "";
                DateTime ufcDateTime = DateTime.UtcNow;
                string from = ufcDateTime.ToString("yyyy-MM-dd");
                string to = ufcDateTime.AddDays(7).ToString("yyyy-MM-dd");
                var response = CallLiveWebinarListAPI(from, to, "50");

                //string url = "https://api.zoom.us/v2/metrics/webinars?" + "&type=" + type +"&from=" + from + "&to=" + to + "&page_size=" + pagesize + "&next_page_token=" + nextpagetoken;
                //var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject(responseContent);
                    if (result.total_records > 50)
                    {
                        response = CallLiveWebinarListAPI(from, to, result.total_records);
                        if (response.IsSuccessStatusCode == true)
                        {
                            responseContent = response.Content.ReadAsStringAsync().Result;
                            result = JsonConvert.DeserializeObject(responseContent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - GetCurrentLiveWebinar ", ex.Message.ToString(), "");
            }
            return result;
        }
        public static dynamic GetWebinarLiveParticipants(string zoomId, string pageSize, string nextPageToken)
        {
            dynamic result = null;
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                string type = "live";
                string pagesize = pageSize;
                string nextpagetoken = nextPageToken;
                string url = "https://api.zoom.us/v2/metrics/webinars/" + zoomId + "/participants?" + "type=" + type + "&page_size=" + pagesize + "&next_page_token=" + nextpagetoken;
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject(responseContent);
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - GetWebinarLiveParticipants ", ex.Message.ToString(), "");
            }
            return result;
        }

        public static dynamic GetLiveWebinarList()
        {
            dynamic responseObj = null;
            ZoomToken.GenerateZoomToken();
            string from = DateTime.Today.ToString();
            string to = DateTime.Today.ToString();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                dynamic response = CallLiveWebinarListAPI(from, to, "50");

                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic result = JsonConvert.DeserializeObject(responseContent);
                    if (result.total_records > 50)
                    {
                        response = CallLiveWebinarListAPI(from, to, result.total_records);
                        if (response.IsSuccessStatusCode == true)
                        {
                            var newResponseContent = response.Content.ReadAsStringAsync().Result;
                            dynamic newResult = JsonConvert.DeserializeObject(newResponseContent);
                            responseObj = newResult.webinars;
                        }
                    }
                    else
                    {
                        responseObj = result.webinars;
                    }

                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "WebinarAPI - GetLiveWebinarList ", ex.Message.ToString(), "");
            }
            return responseObj;

        }

        private static dynamic CallLiveWebinarListAPI(string from, string to, string pageSize)
        {
            string type = "live";
            string pagesize = pageSize;
            string nextpagetoken = "";

            string url = "https://api.zoom.us/v2/metrics/webinars?" + "type=" + type + "&from=" + from + "&to=" + to + "&page_size=" + pageSize + "&next_page_token=" + nextpagetoken;
            var response = client.GetAsync(url).Result;
            return response;
        }

        public static bool IsLiveWebiner(string zoomId){
            bool isLive = false;
            dynamic currentLiveResult = GetCurrentLiveWebinar();
            var currentTempLiveResult = Newtonsoft.Json.JsonConvert.DeserializeObject<LiveWebinarList>(currentLiveResult.ToString());
            List<LiveWebinar> liveWebinarList = currentTempLiveResult.webinars;
            isLive = liveWebinarList.Exists(x => x.id == zoomId);
            return isLive;
        }

    }
}