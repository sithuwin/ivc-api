using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public static class UserAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);
        public static string GetUserId()
        {
            dynamic responseJson = null;
            string userId = "";
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);

                var response = client.GetAsync("https://api.zoom.us/v2/users/me").Result;
                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    responseJson = JsonConvert.DeserializeObject(responseContent);
                    userId = responseJson.id;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "UserAPI - GetUserId ", ex.Message.ToString(), userId);
            }

            return userId;
        }

    }
}