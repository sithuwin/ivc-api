using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Http;
namespace IVC.API.ZoomAPI
{
    public static class RegistrantAPI
    {

        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);

        public static dynamic GetRegistrants(string zoomId, int pageSize, int pageNumber, string nextPageToken)
        {
            dynamic result = null;
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                string type = "approved";
                string nextpagetoken = nextPageToken;

                string url = "https://api.zoom.us/v2/webinars/" + zoomId + "/registrants?occurrence_id=&status=" + type + "&page_size=" + pageSize + "&page_number=" + pageNumber;
                var response = client.GetAsync(url).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Get Registrants API Return", "Status Code  : " + response.StatusCode, "");
                if ((int)response.StatusCode == StatusCodes.Status200OK)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject(responseContent);
                }
                else
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "Getting Panelists", response.Content.ReadAsStringAsync().Result, "");
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RegistrantAPI - GetRegistrants ", ex.Message.ToString(), "");
            }
            return result;
        }
        public static dynamic GetRegistrant(string zoomId, string registrantId, string userId)
        {
            dynamic responseJson = null;
            ZoomToken.GenerateZoomToken();
            try
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.GetAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/registrants/" + registrantId).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Get Registrants API Return", "Status Code  : " + response.StatusCode, "");
                if ((int)response.StatusCode == StatusCodes.Status200OK)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    responseJson = JsonConvert.DeserializeObject(responseContent);
                }
                else
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "Getting Panelists", response.Content.ReadAsStringAsync().Result, "");
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RegistrantAPI - GetRegistrant ", ex.Message.ToString(), userId);
            }
            return responseJson;
        }

        public static dynamic AddNewUserRegistrant(string zoomId, tbl_Account user, string userId)
        {
            dynamic responseJson = null;
            ZoomToken.GenerateZoomToken();

            try
            {
                GlobalFunction.WriteSystemLog(LogType.Info, "Add Registrant", "Zoom Webinar Id  : " + zoomId, userId);
                Registrant registrant = new Registrant();
                registrant.email = user.a_email;
                registrant.first_name = user.a_fullname;
                registrant.last_name = " ";
                GlobalFunction.WriteSystemLog(LogType.Info, "Add Panelist", "User Name  : " + user.a_fullname, userId);

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var content = new StringContent(JsonConvert.SerializeObject(registrant), Encoding.UTF8, "application/json");
                var response = client.PostAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/registrants", content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Add Registrants API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode == StatusCodes.Status201Created)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    responseJson = JsonConvert.DeserializeObject(responseContent);
                }
                else
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "Adding Registrant", response.Content.ReadAsStringAsync().Result, userId);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RegistrantAPI - AddNewUserRegistrant ", ex.Message.ToString(), userId);
            }
            return responseJson;
        }

        public static void UpdateregistrantStatus(string zoomId, Dictionary<string, string> reglist, string userId, string action)
        {
            ZoomToken.GenerateZoomToken();
            try
            {
                GlobalFunction.WriteSystemLog(LogType.Info, "Update Registrant", "Zoom Webinar Id  : " + zoomId, userId);
                RegistrantStatus registrantStatus = new RegistrantStatus();
                registrantStatus.action = action;

                List<Registrants> registrantsList = new List<Registrants>();

                foreach (var reg in reglist)
                {
                    Registrants registrants = new Registrants();
                    registrants.id = reg.Value;
                    registrants.email = reg.Key;
                    registrantsList.Add(registrants);
                    GlobalFunction.WriteSystemLog(LogType.Info, "Update Registrants", "Registrant Id  : " + reg.Value, userId);
                }

                registrantStatus.registrants = registrantsList;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var content = new StringContent(JsonConvert.SerializeObject(registrantStatus), Encoding.UTF8, "application/json");
                var response = client.PutAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/registrants/status", content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Updating Registrants API Return", "Status Code  : " + response.StatusCode, userId);
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RegistrantAPI - UpdateregistrantStatus ", ex.Message.ToString(), userId);
            }

        }

    }
}