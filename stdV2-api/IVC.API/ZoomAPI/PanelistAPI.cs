using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public static class PanelistAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);

        public static dynamic GetPanelists(string zoomId, string userId) //userid = loginuserid 
        {
            dynamic responseJson = null;
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.GetAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/panelists").Result;

                GlobalFunction.WriteSystemLog(LogType.Info, "Get Penelist API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode == StatusCodes.Status200OK)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    responseJson = JsonConvert.DeserializeObject(responseContent);
                }
                else
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "Getting Panelists", response.Content.ReadAsStringAsync().Result, userId);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PanelistsAPI - GetPanelists ", ex.Message.ToString(), userId);
            }
            return responseJson;
        }
        public static void AddPanelist(Dictionary<string, string> userlist, string zoomId, string userId) //userid = loginuserid 
        {
            ZoomToken.GenerateZoomToken();
            try
            {
                Panelist panelist = new Panelist();
                List<Panelists> Listpanelists = new List<Panelists>();
                GlobalFunction.WriteSystemLog(LogType.Info, "Add Panelist", "Zoom Webinar Id  : " + zoomId, userId);

                foreach (var user in userlist)
                {
                    Panelists panelists = new Panelists();
                    panelists.name = user.Value;
                    panelists.email = user.Key;
                    Listpanelists.Add(panelists);
                    GlobalFunction.WriteSystemLog(LogType.Info, "Add Panelist", "User Name  : " + user.Value, userId);
                }

                panelist.panelists = Listpanelists;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var content = new StringContent(JsonConvert.SerializeObject(panelist), Encoding.UTF8, "application/json");
                var response = client.PostAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/panelists", content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Add Panelist API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode != StatusCodes.Status201Created)
                {
                    GlobalFunction.WriteSystemLog(LogType.Error, "Creating Panelists", response.Content.ReadAsStringAsync().Result, userId);
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PanelistsAPI - AddPanelist ", ex.Message.ToString(), userId);
            }
        }
        public static dynamic RemovePanelist(dynamic user, string zoomId, string userId)
        {
            bool result = false;
            ZoomToken.GenerateZoomToken();
            try
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.DeleteAsync("https://api.zoom.us/v2/webinars/" + zoomId + "/panelists/" + user.PanelistId).Result;

                GlobalFunction.WriteSystemLog(LogType.Info, "Remove Panelist API Return", "Status Code  : " + response.StatusCode, userId);
                if ((int)response.StatusCode == StatusCodes.Status204NoContent)
                {
                    result = true;
                }
                else
                {
                    GlobalFunction.WriteSystemLog(LogType.Info, "Can't Delete Panelist in Zoom", response.Content.ReadAsStringAsync().Result, userId);
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PanelistsAPI - RemovePanelist ", ex.Message.ToString(), userId);
            }
            return result;
        }
    }
}