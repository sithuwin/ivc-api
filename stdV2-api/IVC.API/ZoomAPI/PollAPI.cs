using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public class PollAPI
    {

        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);

        public static dynamic GetPollReportList(string zoomId, string userId)
        {
            dynamic responseData = null;
            ZoomToken.GenerateZoomToken();
            try
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                var response = client.GetAsync("https://api.zoom.us/v2/report/webinars/" + zoomId + "/polls").Result;

                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                    responseData = responseJson.questions;
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PollAPI - GetPollReportList ", ex.Message.ToString(), userId);
            }
            return responseData;

        }
    }
}