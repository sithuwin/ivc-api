using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public static class ZoomToken
    {
        public static string ZoomTokenString { get; set; }
        public static ConfigKeys ConfigKeys;
        public static void GenerateZoomToken()
        {
            try
            {

                DateTime Expiry = DateTime.UtcNow.AddMinutes(20);

                var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var Configuration = appsettingbuilder.Build();

                int ts = (int)(Expiry - new DateTime(1970, 1, 1)).TotalSeconds;

                // Create Security key  using private key above:
                var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigKeys.ScrectKey));

                // length should be >256b
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                //Finally create a Token
                var header = new JwtHeader(credentials);

                //Zoom Required Payload
                var payload = new JwtPayload { { "iss", ConfigKeys.APIKey }, { "exp", ts },
                };

                var secToken = new JwtSecurityToken(header, payload);
                var handler = new JwtSecurityTokenHandler();

                // Token to String so you can use it in your client
                ZoomTokenString = handler.WriteToken(secToken);

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "ZoomTokens", ex.Message.ToString(), "");
            }
        }

    }

}