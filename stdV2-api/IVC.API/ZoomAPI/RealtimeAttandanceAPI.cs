using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public static class RealtimeAttandanceAPI
    {

        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);
        public static dynamic GetRealtimeAttandance(string zoomId, string userId)
        {
            dynamic responseObj = null;
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                dynamic response = CallRealtimeAttandanceListAPI("30", zoomId);

                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic result = JsonConvert.DeserializeObject<ParticipantList>(responseContent.ToString()); //JsonConvert.DeserializeObject(responseContent.ToString());
                    if (result.total_records > 30)
                    {
                        dynamic newResponse = CallRealtimeAttandanceListAPI(result.total_records, zoomId);
                        if (response.IsSuccessStatusCode == true)
                        {
                            var newResponseContent = newResponse.Content.ReadAsStringAsync().Result;
                            dynamic newResult = JsonConvert.DeserializeObject<ParticipantList>(newResponseContent.ToString()); //JsonConvert.DeserializeObject(newResponseContent.ToString());
                            List<Participant> liveParticipants = newResult.participants;
                            liveParticipants = liveParticipants.FindAll(x => x.leave_time == null || x.leave_time == "");
                            responseObj = liveParticipants;
                        }
                    }
                    else
                    {
                        List<Participant> liveParticipants = result.participants;
                        liveParticipants = liveParticipants.FindAll(x => x.leave_time == null || x.leave_time == "");
                        responseObj = liveParticipants;
                    }

                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "RealtimeAttandanceAPI - GetRealtimeAttandance ", ex.Message.ToString(), userId);
            }
            return responseObj;
        }

        private static dynamic CallRealtimeAttandanceListAPI(string pageSize, string zoomId)
        {
            string type = "live";
            string pagesize = pageSize;
            string nextpagetoken = "";
            string from = DateTime.Today.ToString();
            string to = DateTime.Today.ToString();

            string url = "https://api.zoom.us/v2/metrics/webinars/" + zoomId + "/participants?" + "&type=" + type + "&page_size=" + pagesize + "&next_page_token=" + nextpagetoken;
            var response = client.GetAsync(url).Result;
            return response;
        }
    }
}