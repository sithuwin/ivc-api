using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using IVC.DAL.Models;
using Serilog;
namespace IVC.API.ZoomAPI
{
    public static class ParticipantSummaryAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        private static readonly HttpClient client = new HttpClient(httpClientHandler);

        
        public static dynamic GetWebinarPaticipantsReport(string zoomId, string userId, string pageSize)
        {
            dynamic responseJson = null;
            ZoomToken.GenerateZoomToken();
            string Next_Page_Toke = "";
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);
                pageSize = GetPageSize(zoomId, userId);
                if (pageSize != "" && pageSize != null)
                {
                    string url = "https://api.zoom.us/v2/report/webinars/" + zoomId + "/participants?" + "&page_size=" + pageSize + "&next_page_token=" + Next_Page_Toke;
                    var response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode == true)
                    {
                        var responseContent = response.Content.ReadAsStringAsync().Result;
                        responseJson = JsonConvert.DeserializeObject(responseContent);
                        responseJson = responseJson.participants;
                    }
                }
            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PanticipantSummaryAPI - GetWebinarPaticipantsReport ", ex.Message.ToString(), userId);
            }

            return responseJson;
        }

        public static dynamic GetPageSize(string zoomId, string userId)
        {
            dynamic responseJson = null;
            string pageSize = "";
            ZoomToken.GenerateZoomToken();
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ZoomToken.ZoomTokenString);

                var response = client.GetAsync("https://api.zoom.us/v2/report/webinars/" + zoomId).Result;


                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    responseJson = JsonConvert.DeserializeObject(responseContent);
                    pageSize = responseJson.participants_count;
                }

            }
            catch (Exception ex)
            {
                GlobalFunction.WriteSystemLog(LogType.Error, "PanticipantSummaryAPI - GetPageSize ", ex.Message.ToString(), userId);
            }
            return pageSize;
        }

    }
}