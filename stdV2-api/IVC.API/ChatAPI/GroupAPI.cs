using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using IVC.DAL.Models;
using IVC.DAL.Util;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace IVC.API.ChatAPI {
    public static class GroupAPI {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        //private static readonly HttpClient client;
        public static dynamic CreateGroup (string guId, string groupName, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                ChatGroup groupModel = new ChatGroup ();
                groupModel.guid = showPrefix + "_" + guId;
                groupModel.name = groupName;
                if (groupName.Length > 100) {
                    groupModel.name = groupName.Substring (0, 100);
                }
                groupModel.type = "private";
                groupModel.description = groupName;

                var content = new StringContent (JsonConvert.SerializeObject (groupModel), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                GlobalFunction.WriteSystemLog (LogType.Info, "Creating Chat Group API Return with Group Id : " + groupModel.guid, "Status Code  : " + response.StatusCode, "");
                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    if (responseJson.data != null) {
                        result = true;
                    }
                } else {
                    if (responseJson.error.details.guid != null) {
                        string alreadyMessage = responseJson.error.details.guid[0].ToString ();
                        if (alreadyMessage == "The guid has already been taken.")
                            result = true;
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "Creating Chat Group API ", "Error   : " + ex.Message.ToString (), "");
            }
            return result;
        }

        public static dynamic JoinMember (string userId, string guId, string userType, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                userId = (showPrefix + "_" + userId).ToLower ();
                guId = (showPrefix + "_" + guId).ToLower ();

                string[] userList = new string[1];
                userList[0] = userId;
                dynamic members = new GroupParticipants ();
                if (userType == UserType.Organizer) {
                    members = new GroupAdmins ();
                    members.admins = userList;

                }
                if (userType == UserType.Delegate || userType == UserType.FreeDelegate) {
                    members.participants = userList;
                }

                if (userType == UserType.Speaker) {
                    members = new GroupModerators ();
                    members.moderators = userList;
                }

                var content = new StringContent (JsonConvert.SerializeObject (members), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups/" + guId + "/members";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                GlobalFunction.WriteSystemLog (LogType.Info, "Joining Chat Group API Return with Group Id : " + guId + " for User Id : " + userId, "Status Code  : " + response.StatusCode, "");
                if (response.IsSuccessStatusCode == true) {
                    var responseContent = response.Content.ReadAsStringAsync ().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                    if (responseJson.data != null) {
                        result = true;
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "Joining Chat Group API ", "Error   : " + ex.Message.ToString (), "");
            }
            return result;
        }

        public static dynamic GetGroup (string guId, string showPrefix) {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                guId = (showPrefix + "_" + guId).ToLower ();

                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/groups/" + guId;
                HttpResponseMessage response = client.GetAsync (url).Result;
                GlobalFunction.WriteSystemLog (LogType.Info, "Getting Chat Group API Return with Group Id : " + guId, "Status Code  : " + response.StatusCode, "");

                var responseContent = response.Content.ReadAsStringAsync ().Result;
                dynamic responseJson = JsonConvert.DeserializeObject (responseContent);
                if (response.IsSuccessStatusCode == true) {
                    return responseJson;
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "Getting Chat Group API ", "Error   : " + ex.Message.ToString (), "");
            }
            return result;
        }

    }
}