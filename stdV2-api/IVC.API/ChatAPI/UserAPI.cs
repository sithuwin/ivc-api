using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using IVC.DAL.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
namespace IVC.API.ChatAPI {
    public static class UserAPI {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        // private static readonly HttpClient client = new HttpClient(httpClientHandler);
        public static bool CreateUser (string name, string chatId,dynamic metaObj) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            string chatVersion = Configuration.GetSection("ChatKey:version").Value;
            try {
                if(string.IsNullOrEmpty(regionKey) || string.IsNullOrEmpty(chatVersion)){
                    GlobalFunction.WriteSystemLog (LogType.Info, "Create a new user on Cometchat ", "Check your region or version. Make sure that doesn't empty.", "UserId:" + chatId + ", UserName: "+ name);
                    return false;
                }
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);
                

                dynamic userModel = new System.Dynamic.ExpandoObject ();
                userModel.uid = chatId;
                userModel.name = name;

                //userModel.avatar = "";
                //userModel.link = "";
                //userModel.role = "";
                userModel.metadata = metaObj;
                userModel.withAuthToken = true;
                var content = new StringContent (JsonConvert.SerializeObject (userModel), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/"+chatVersion+"/users";
                HttpResponseMessage response = client.PostAsync (url, content).Result;
                var responseContent = response.Content.ReadAsStringAsync().Result;
                dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                if (response.IsSuccessStatusCode == true)
                {
                    if (responseJson.data != null)
                    {
                        GlobalFunction.WriteSystemLog (LogType.Info, "Create a new user on Cometchat ", "Status Code  : " + response.StatusCode, "UserId:" + chatId + ", UserName: "+ name);
                        return true;
                    }
                }
                else
                {
                    if (responseJson.error.details.uid != null)
                    {
                        string msg = responseJson.error.details.uid[0].ToString();
                        if (msg == "The uid has already been taken.")
                        {
                            GlobalFunction.WriteSystemLog(LogType.Info, "Create a new user on Cometchat ", msg, "UserId:" + chatId + ", UserName: " + name);
                            return true;
                        }            
                    }
                }
                
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "Create a new user on Cometchat  ", "Error   : " + ex.Message.ToString (), "");
            }
            return false;
        }

        public static bool UpdateUser (string name, string chatID) {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            string chatVersion = Configuration.GetSection("ChatKey:version").Value;
            try {
                if(string.IsNullOrEmpty(regionKey) || string.IsNullOrEmpty(chatVersion)){
                    GlobalFunction.WriteSystemLog (LogType.Info, "Create a new user on Cometchat ", "Check your region or version. Make sure that doesn't empty.", "UserId:" + chatID + ", UserName: "+ name);
                    return false;
                }
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);
               
                dynamic userModel = new System.Dynamic.ExpandoObject ();
                string uid = chatID;
                userModel.name = name;
                //userModel.avatar = "https://i0.wp.com/www.martersolutions.com/wp-content/uploads/2018/02/STW.jpg?fit=400%2C480&ssl=1";
                //userModel.link = "http://203.127.83.146/iVC/";
                //userModel.role = "Speaker";
                //userModel.metadata = metadata;
                userModel.withAuthToken = true;
                var content = new StringContent (JsonConvert.SerializeObject (userModel), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/"+chatVersion+"/users/" + uid;
                HttpResponseMessage response = client.PutAsync (url, content).Result;
                GlobalFunction.WriteSystemLog (LogType.Info, "Update a user on Cometchat ", "Status Code  : " + response.StatusCode, "UserId:" + chatID);
                
                var responseContent = response.Content.ReadAsStringAsync().Result;
                dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                if (response.IsSuccessStatusCode == true)
                {
                    if (responseJson.data != null)
                    {
                        return true;
                    }
                }
            } catch (Exception ex) {
                GlobalFunction.WriteSystemLog (LogType.Error, "Update a user on Cometchat  ", "Error   : " + ex.Message.ToString (), "");
            }
            return false;
        }

        public static dynamic BlockUser () {
            var appsettingbuilder = new ConfigurationBuilder ().AddJsonFile ("appsettings.json");
            var Configuration = appsettingbuilder.Build ();
            string appId = Configuration.GetSection ("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection ("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection ("ChatKey:region").Value;
            string chatVersion = Configuration.GetSection("ChatKey:version").Value;
            try {
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add ("appId", appId);
                client.DefaultRequestHeaders.Add ("apiKey", apiKey);

                dynamic userModel = new System.Dynamic.ExpandoObject ();
                string[] strarr = new string[] { "", "" };
                userModel.blockedUids = strarr;
                string uid = "";

                var content = new StringContent (JsonConvert.SerializeObject (userModel), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/"+chatVersion+"/users/" + uid;
                HttpResponseMessage response = client.PutAsync (url, content).Result;

                if (response.IsSuccessStatusCode == true) {

                }
            } catch (Exception ex) {
                Console.WriteLine (ex.Message.ToString ());
            }
            return null;
        }

         public static string CreateUserAuthToken(string uId)
        {
            string authToken = "";
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string appId = Configuration.GetSection("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection("ChatKey:region").Value;
            string chatVersion = Configuration.GetSection("ChatKey:version").Value;
            try{
                if(string.IsNullOrEmpty(regionKey) || string.IsNullOrEmpty(chatVersion)){
                    GlobalFunction.WriteSystemLog (LogType.Info, "Create a new user on Cometchat ", "Check your region or version. Make sure that doesn't empty.", "UserId:" + uId);
                    return authToken;
                }
                var client = new HttpClient ();
                client.DefaultRequestHeaders.Add("appId", appId);
                client.DefaultRequestHeaders.Add("apiKey", apiKey);
                dynamic bodyParams = new System.Dynamic.ExpandoObject();
                bodyParams.force = false;
                var content = new StringContent(JsonConvert.SerializeObject(bodyParams), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/"+chatVersion+"/users/" + uId + "/auth_tokens";
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                
                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                    if (responseJson.data.authToken != null)
                    {
                        authToken = responseJson.data.authToken;
                        return authToken;
                    }
                }
            }
            catch(Exception ex){
                Console.WriteLine(ex.Message.ToString());
            }
            return authToken;
        }

    }
}