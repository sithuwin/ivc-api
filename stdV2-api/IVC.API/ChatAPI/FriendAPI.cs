using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using IVC.DAL.Models;
namespace IVC.API.ChatAPI
{
    public static class FriendAPI
    {
        private static readonly HttpClientHandler httpClientHandler = new HttpClientHandler
        {
            ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true
        };
        //private static readonly HttpClient client = new HttpClient (httpClientHandler);
        public static dynamic AddFriend(string requestUserId, string responseUserId, string showPrefix)
        {
            bool result = false;
            var appsettingbuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var Configuration = appsettingbuilder.Build();
            string appId = Configuration.GetSection("ChatKey:appId").Value;
            string apiKey = Configuration.GetSection("ChatKey:apiKey").Value;
            string regionKey = Configuration.GetSection("ChatKey:region").Value;
            try
            {

                var client = new HttpClient();
                client.DefaultRequestHeaders.Add("appId", appId);
                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                requestUserId = (showPrefix + "_" + requestUserId).ToLower();
                responseUserId = (showPrefix + "_" + responseUserId).ToLower();

                string[] acceptUser = new string[1];
                acceptUser[0] = requestUserId;
                AcceptList acceptObj = new AcceptList();
                acceptObj.accepted = acceptUser;

                var content = new StringContent(JsonConvert.SerializeObject(acceptObj), Encoding.UTF8, "application/json");
                string url = "https://api-" + regionKey + ".cometchat.io/v2.0/users/" + responseUserId + "/friends";
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                GlobalFunction.WriteSystemLog(LogType.Info, "Adding Friend API Return for RequestId : " + requestUserId + " ResponseId : " + responseUserId, "Status Code  : " + response.StatusCode, "");

                if (response.IsSuccessStatusCode == true)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    dynamic responseJson = JsonConvert.DeserializeObject(responseContent);
                    if (responseJson.data.accepted != null)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return result;
        }

    }
}