using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
namespace IVC.API
{
    public class SignalHub : Hub
    {
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();
        public Task ChangeUrl(string data)
        {
            return Clients.All.SendAsync("changeUrl", data);
        }

        public async Task SendMessage(string userId, string signalName, string signalMessage)
        {
            //Send Notification to specific User
            List<string> connectionIds = _connections.GetConnections(userId).ToList<string>();
            if (connectionIds.Count() > 0)
            {
                try
                {
                    await Clients.Clients(connectionIds).SendAsync(signalName, signalMessage);
                }
                catch (Exception) { }
            }
        }
        public async Task AddUserIntoSignalConnection(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                try
                {
                    var connId = Context.ConnectionId.ToString();
                    _connections.Add(userId, connId);
                    await Clients.Clients(connId).SendAsync("ConnectionSuccess", "Connected Successfully");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
            }
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var httpContext = Context.GetHttpContext();
            if (httpContext != null)
            {
                //Remove Logged User
                var username = httpContext.Request.Query["user"];
                var connId = Context.ConnectionId.ToString();
                _connections.Remove(username, Context.ConnectionId);
                await Clients.Clients(connId).SendAsync("ConnectionDisconnected", "Disconnected Successfully");
            }
        }
    }
}